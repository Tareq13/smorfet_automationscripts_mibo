# README #

Selenium tests for the rendering application

### Required to run the tests in eclipse###
.Install eclipse
.Install testNG in eclipse
.Download selenium-java-3.4.0 and import the libraries to java build path
Note: after importing the project, do Run "Maven install" in order to download any needed library.

### Run Tests – xml files ###
Use the xml files in runners folder in order to run a test case, test suite or all the available tests together "AllTests_testng.xml"
I.Within the runner xml files, there are two parameters:
1. broswer, choose which browser to use for testing, now we have two options: firefox and chrome.
2. testlink, true/false in order to report the tests results to testlink or not.

II.There are two available groups to define in the xml files:
1. Full Regression: all the available test cases.
2. Smoke Test: random chosen tests.
3. Bug: tests that have an opened bugs.

### More info about the tests ###
.The tests do print the steps and results to the console, external log text file (Logs folder) and external report - html file(test-output/RenderingReports).
.All the tests inputs are chosen randomly from the dropdowns or the database.
.The tests code are explained clearly by comments and javadocs.
.The tests result are sent to testlink (parameters values could be changes in the GlobalConfig file)
-GlobalConfig.properties file do include all the needed configuration parameters.

Note: before trying to run a new test, it is encouraged to delete the report html file under(test-output/RenderingReports) and clear the screenshot folder (test-output/RenderingReports/ErrorScreenshots)

Note for running tests in Firefox:
In the recent Firefox versions, they added a message to be displayed to the user in the forms fields "This connection is not secure. Logins entered here could be compromised. Learn More".
This message do conflict with the tests and selenium do click on it which preventing running the test cases properly, so in order to override this, need to create a custom profile in Firefox with name "selenium" and disable the SSL connection in it,  then selenium would call this profile and run the tests in without issues.
1.	click Run -> firefox -P
2.	about:config
3.	security.insecure_field_warning.contextual.enabled change to false

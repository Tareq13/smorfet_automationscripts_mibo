package smi.smorfet.test.pages;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.uiltilies.LogUltility;

/**
 * Define elements and methods for the Homepage
 *
 */
public class MonitorTab extends BasePage {
	
	// Monitor tab
	@FindBy(how=How.NAME,using ="Monitor")
	public WebElement tabMonitor;
	
	// Monitor frame  
	@FindBy(how=How.ID,using ="tableLayoutPanel6")
	public WebElement monitorFrame;
		
	// First field to insert sentence
	@FindBy(how=How.ID,using ="tb_raw_sentence")
	public WebElement tb_raw_sentence;
	
	// New button in footer
	@FindBy(how=How.ID,using ="bt_new")
	public WebElement bt_new;
	
	// Ok confirmation button to load new sentence
	@FindBy(how=How.ID,using ="1")
	public WebElement bt_ok;
	
	// Disambiguate button
	@FindBy(how=How.ID,using ="bt_disambiguate")
	public WebElement bt_disambiguate;
	
	// 'Remove tags' text area
	@FindBy(how=How.ID,using ="tb_SSML_tags_removed")
	public WebElement tb_SSML_tags_removed;
	
	// 'Normalize' text area
	@FindBy(how=How.ID,using ="tb_normalized")
	public WebElement tb_normalized;
	
	// 'Space punctuation' text area
	@FindBy(how=How.ID,using ="tb_space_punctuation")
	public WebElement tb_space_punctuation;
	
	// Domain dropdown
	@FindBy(how=How.ID,using="dd_domain_monitor")
	public WebElement domainDropdown;
	
	// Smorfet main window
	@FindBy(how=How.ID,using ="SMI_Root_GUI")
	public WebElement SMI_Root_GUI;
	
	//Ok confirmation button to load new sentence
	@FindBy(how=How.ID,using ="2")
	public WebElement bt_ok_domain;
	
	// Disambiguation data table layout pane
	@FindBy(how=How.ID,using="dgv_output")
	public WebElement tableOutput;
	
	// View tab button
	@FindBy(how=How.NAME,using="View")
	public WebElement viewTab;
	
	// show remove tags checkbox button
	@FindBy(how=How.ID,using="cbx_show_removeTags")
	public WebElement cbx_show_removeTags;
	
	// show space punctuation checkbox button
	@FindBy(how=How.ID,using="cbx_show_spacePunctuation")
	public WebElement cbx_show_spacePunctuation;
	
    /**
     * Get values from dropdowns and lists
     * @param lstBox
     * @return
     * @throws InterruptedException
     */
     public List<String> getValuesFromApp(WebElement lstBox) throws InterruptedException
 	{
     	//lstBox.click();
     	Thread.sleep(1000);
 		//get all children
 		//List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("\"./*[contains(@ControlType, 'ControlType.ListItem')]\""));
//     	try {
//     		List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("//*[contains(@ControlType, 'ControlType.ListItem')]"));
//			} catch (Exception e) {
//				System.out.println("itemlist e: " + e);
//			}
     	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
 		//System.out.println(itemlist.size());
 		//System.out.println(itemlist.get(0).getAttribute('WiniumDriver'));
     	ArrayList<String> itemNames= new ArrayList<String>();
 		for(WebElement e : itemlist)
 		{
 			//System.out.println(e.getAttribute("Name"));
 			itemNames.add(e.getAttribute("Name"));
 		    //itemNames.add(e.getAttribute(Name));
 		}
 		
 		// Delete unknown value, it is not usable
 		if (itemNames.contains("unknown"))
 			itemNames.remove("unknown");
 		
 		return itemNames;
 	}
     
     /**
      * Fetch the text and title of popup windows within Smorfet
      * @param test
      * @param logger
      * @param titleToCheck
      * @param textToCheck
      * @return
      */
     public boolean popupWindowMessage(ExtentTest test, Logger logger, String titleToCheck, String textToCheck)
 	{
     	List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
     	List<WebElement> text = null;
     	List<WebElement> title = null;
     	
     	boolean titleIn = false;
 	    boolean textIn = false;
 	    
     	// Check the main app for popup windows
 		for(WebElement e : windows)
 		{ 
 			text = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	
         	title = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TitleBar')]"));
     	}
 		
 		// Fetch the found title
 		for(WebElement e : title)
 		{
 			LogUltility.log(test, logger, "Title:" + e.getAttribute("Name"));
 			if (e.getAttribute("Name").contains(titleToCheck))
 				titleIn = true;
     	}
 		
 		// Fetch the found title    		
 		for(WebElement e : text)
 		{ 
 			LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
 			if (e.getAttribute("Name").contains(textToCheck))
 				textIn = true;
     	}
 		
 		LogUltility.log(test, logger, "TitleIn: " + titleIn + " - TextIn: " + textIn);
 		
 	    if (titleIn && textIn)
 	    	return true;
 	    else return false;
 	} // close function
     
     
     /**
      * Get chosen value in a dropdown
      * @param lstBox
      * @return
      * @throws InterruptedException
      */
     public List<String> getChosenValueInDropdown(WebElement lstBox) throws InterruptedException
 	{
     	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

     	ArrayList<String> itemNames= new ArrayList<String>();
 		for(WebElement e : itemlist)
 		{
 			boolean isSelected = e.isSelected();
 			if (isSelected)
 				itemNames.add(e.getAttribute("Name"));
 		}
 		return itemNames;
 	}
     
     /**
      * Get the disambiguation data table
      * @return
      */
     public HashMap<String, List<String>> getDataTable()
 	{
    	HashMap<String, List<String>> dataTable = new HashMap<>();

    	List<WebElement> rows = tableOutput.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Custom')]"));
     	List<String> keys = new ArrayList<String>();
     	// Initialize the keys of the table
     	keys.add("[DM] Word");keys.add("[DM] POS");keys.add("[DM] FormType");keys.add("[DM] Frame");keys.add("[DM] Sem-Grp");keys.add("[DM] Sem-Fld");
     	
     	for(WebElement row : rows)
     	{	
     		String info = row.getText();
     		String splitted[] = info.split(";");
     		String key = splitted[0];
     		List<String> rowData = new ArrayList<String>();
     		for(int i=1;i<splitted.length;i++)				
     			rowData.add(splitted[i]);
     		dataTable.put(key, rowData)	;
     	}
     	
     	return dataTable;
 	} // close function
     
	} // Class end

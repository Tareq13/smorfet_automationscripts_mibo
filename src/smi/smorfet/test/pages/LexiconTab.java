package smi.smorfet.test.pages;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;

/**
 * Define elements and methods for the Homepage
 *
 */
public class LexiconTab extends BasePage {
	
	// Dictionary whole frame tab
	@FindBy(how=How.ID,using ="TitleBar")
	public WebElement TitleBar;
	
	// Intro window
	@FindBy(how=How.NAME,using ="Attachment")
	public WebElement WindowAttachment;
	
	// The Dictionary Tab selector:  Dictionary
	@FindBy(how=How.NAME,using ="Dictionary")
	public WebElement dictionaryTab;
	
	// Lexicon table layout
	@FindBy(how=How.ID,using ="tableLayoutPanel7")
	public WebElement tableLayoutPanel7;
		
	// Lexicon  records pane display
	@FindBy(how=How.ID,using ="pn_lexicon_records")
	public WebElement pn_lexicon_records;
	
	// Abort button in menu intro window
	@FindBy(how=How.NAME,using ="Abort")
	public WebElement btnAbort;
	
	// Lexicon whole frame tab
	@FindBy(how=How.ID,using ="tc_TabControl")
	public WebElement tabLexicon;
	
	// Message box tb_message
	@FindBy(how=How.ID,using ="tb_message")
	public WebElement tb_message;
	
	// The Lexicon Tab selector:  Lexicon
	@FindBy(how=How.NAME,using ="Lexicon")
	public WebElement LexiconTab;
	
	// New button
	@FindBy(how=How.ID,using ="bt_new")
	public WebElement btnNew;
	
	// Save button
	@FindBy(how=How.ID,using ="bt_save")
	public WebElement btnSave;
	
	// "YES" in the confirmation message for the new button
	@FindBy(how=How.NAME,using ="Yes")
	public WebElement btnYes;
	
	// Key form text field
	@FindBy(how=How.ID,using ="tb_Lexicon_key_form")
	public WebElement txtKeyForm;
	
	// POS combobox
	@FindBy(how=How.ID,using ="dd_Lexicon_POS")
	public WebElement cmpPOS;
	
	// dd_condition_field
	@FindBy(how=How.ID,using ="dd_condition_field")
	public WebElement cmpCondition_field;
	
	// dd_condition_operation
	@FindBy(how=How.ID,using ="dd_condition_operation")
	public WebElement cmpCondition_operation;
	
	// dd_condition_value
	@FindBy(how=How.ID,using ="dd_condition_value")
	public WebElement cmpCondition_value;
	
	// dd_all_or_next
	@FindBy(how=How.ID,using ="dd_all_or_next")
	public WebElement cmpCondition_all_next;
	
	// Retrieve button
	@FindBy(how=How.ID,using ="bt_retrieve_same")
	public WebElement btnRetrieve_same;
	
	// Lexicon records list
	@FindBy(how=How.ID,using ="lb_lexicon_forms")
	public WebElement lst_records;
	
	// goto corpus blue button
	@FindBy(how=How.ID,using ="bt_goto_corpus")
	public WebElement bt_goto_corpus;
	
	// Corpus records lb_corpus_records
	@FindBy(how=How.ID,using ="lb_corpus_records")
	public WebElement lb_corpus_records;
	
	
	// goto dictionary black button
	@FindBy(how=How.ID,using ="bt_goto_dictionary")
	public WebElement bt_goto_dictionary;

	// Window too many many retrieved records
	@FindBy(how=How.NAME,using ="Too many retrieved records")
	public WebElement window_too_many_records;
	
	// Window too many many retrieved records
	@FindBy(how=How.NAME,using ="Illegal POS selected!\\r\\rValue will be reverted")
	public WebElement window_Illegal;
	
	// Popup window, OK button
	@FindBy(how=How.NAME,using ="OK")
	public WebElement btnOKPopupWindow;
	
	// Popup window, accept button
	@FindBy(how=How.ID,using ="bt_utility_accept")
	public WebElement bt_utility_accept;

	// Volume [POS] dropdown
	@FindBy(how=How.ID,using ="dd_lexicon_volume")
	public WebElement cmpVolumePOS;
	
	// Previous arrow button
	@FindBy(how=How.ID,using ="bt_prev")
	public WebElement bt_prev;
	
	// Semantic Fields list
	@FindBy(how=How.ID,using ="lb_Lexicon_semantic_fields")
	public WebElement lb_Lexicon_semantic_fields;
	
	// Cancel in popups
	@FindBy(how=How.ID,using ="bt_utility_cancel")
	public WebElement btnpopupCancelWindow;	
	
	// Cancel in popups
	@FindBy(how=How.ID,using ="bt_utility_accept")
	public WebElement btnpopupAcceptWindow;

	// Popup value dropdown
	@FindBy(how=How.ID,using ="dd_utility_value")
	public WebElement cmpPopupValue;

	// Delete button
	@FindBy(how=How.ID,using ="bt_delete")
	public WebElement bt_delete;
	
	// Removed record confirmation text 
	@FindBy(how=How.CLASS_NAME,using ="#32770")
	public WebElement text_removed_record_confirmation;

	// First line in popup after delete
	@FindBy(how=How.ID,using ="ll_utility_3")
	public WebElement popup_after_delete_first_line;
	
	// Lexical Semantic grops list
	@FindBy(how=How.ID,using ="lb_Lexicon_semantic_groups")
	public WebElement lst_SemanticGroups;
	
	// Part of speech list
	@FindBy(how=How.ID,using ="dd_Lexicon_POS")
	public WebElement lst_POS;
	
	// Empty value popup message 
	@FindBy(how=How.NAME,using ="Value empty")
	public WebElement Empty_Popup;

	// Left arrow in the footer bt_prev 
	@FindBy(how=How.ID,using ="bt_prev")
	public WebElement Previous_button;
	
	// Right arrow in the footer bt_next 
	@FindBy(how=How.ID,using ="bt_next")
	public WebElement bt_next;

	// "OK" in the confirmation message for the new button
	@FindBy(how=How.NAME,using ="OK")
	public WebElement btnOk;
				
	// W button
	@FindBy(how=How.ID,using ="bt_search_wiktionary")
	public WebElement bt_search_wiktionary;
	
	// "OK" in the confirmation message for the new button
	@FindBy(how=How.ID,using ="TitleBar")
	public WebElement window_TitleBar;
	
	// Too many retieved records window
	@FindBy(how=How.NAME,using ="Too many retrieved records")
	public WebElement window_TooManyRetrievedRecords;
	
	// "OK" in the confirmation message for the new button
	@FindBy(how=How.ID,using ="ll_Lexicon_retrieved_records")
	public WebElement ll_Lexicon_retrieved_records;

	// Smorfet main window
	@FindBy(how=How.ID,using ="SMI_Root_GUI")
	public WebElement SMI_Root_GUI;
	
	// Generate button
	@FindBy(how=How.ID,using ="bt_generate_lexicon")
	public WebElement bt_generate_lexicon;
	
	// Add grammar attribute button
	@FindBy(how=How.ID,using ="bt_add_grammar_attribute")
	public WebElement bt_add_grammar_attribute;
	
	// Start window abort button
	@FindBy(how=How.ID,using ="bt_abort")
	public WebElement bt_abort;
	
	// Scroll Large
	@FindBy(how=How.ID,using ="LargeIncrement")
	public WebElement LargeIncrement;
		
	// POS dropdown values
	public enum POS{
		accusative_word,adadjective,adverb,classifier,conjunction,demonstrative, 
		existential,foreign_word,infinitive,modal,negation,number,phrase_ends, 
		prefix,pronoun,quantifier,suffix,verb,action_name,adjective,article, 
		clause_opener,copula,diacritic,foreign_script,genetive_word,interjection, 
		name,noun,phrase_begins,possessive,preposition,punctuation,question,title,verb_particle
	}
	
	public enum posEnd{punctuation, quantifier, question, suffix, title, verb, verb_particle};
	
    /**
	 * Get random text 
     * @param length - enter the length of the text
	 */
        public String RandomString(int length) {
         char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
         StringBuilder sb = new StringBuilder();
         Random random = new Random();
         for (int i = 0; i < length; i++) {
             char c = chars[random.nextInt(chars.length)];
             sb.append(c);
         }
         String output = sb.toString();
         return output;
        
        }
        
        // # Type in Key Form
        public String keyFormTypeRandom() {
			String random_input = RandomString(5);
			this.txtKeyForm.sendKeys(random_input);
			return random_input;
		}

        // find all elements in dd_condition_field
        // later try to make it globally
        //private List<String> getCompoboxValues(WebElement lstBox)
        public List<String> getValuesFromApp(WebElement lstBox) throws InterruptedException
    	{
        	//lstBox.click();
        	Thread.sleep(1000);
    		//get all children
    		//List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("\"./*[contains(@ControlType, 'ControlType.ListItem')]\""));
//        	try {
//        		List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("//*[contains(@ControlType, 'ControlType.ListItem')]"));
//			} catch (Exception e) {
//				System.out.println("itemlist e: " + e);
//			}
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
    		//System.out.println(itemlist.size());
    		//System.out.println(itemlist.get(0).getAttribute('WiniumDriver'));
        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			//System.out.println(e.getAttribute("Name"));
    			itemNames.add(e.getAttribute("Name"));
    		    //itemNames.add(e.getAttribute(Name));
    		}
    		
    		// Delete unknown value, it is not usable
    		if (itemNames.contains("unknown"))
    			itemNames.remove("unknown");
    		
    		return itemNames;
    	}
        
        // find number of elements in dropdown/list
        public List<String> getValuesFromApp(WebElement lstBox, int amount) throws InterruptedException
    	{
    		//get all children
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

        	ArrayList<String> itemNames= new ArrayList<String>();
        	
        	int i = 0;
    		for(WebElement e : itemlist)
    		{
    			itemNames.add(e.getAttribute("Name"));
    			i++;
    			if (i>amount) break;
    		}
    		return itemNames;
    	}
        
        /**
         * Get chosen value in a dropdown
         * @param lstBox
         * @return
         * @throws InterruptedException
         */
        public List<String> getChosenValueInDropdown(WebElement lstBox) throws InterruptedException
    	{
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			boolean isSelected = e.isSelected();
    			if (isSelected)
    				itemNames.add(e.getAttribute("Name"));
    		}
    		return itemNames;
    	}
        
        /**
         * Choose value from dropdown that is LISTITEM
         * @param lstBox
         * @param ItemName
         * @throws InterruptedException
         */
        public void chooseValueInDropdown(WebElement lstBox, String ItemName) throws InterruptedException
    	{
       	 // Scan first for elemnts in lstBOX, take buttons, lists and scrolls for later use
       	 List<WebElement> allElelements= lstBox.findElements(By.xpath(".//*"));

       	 ArrayList<WebElement> buttonsElements = new ArrayList<WebElement>();
           List<WebElement> listsElements = new ArrayList<WebElement>();
       	List<WebElement> scrollsElements = new ArrayList<WebElement>();
       	
       	 for (WebElement webElement : allElelements) {
       		 if(webElement.getAttribute("ControlType").equals("ControlType.Button"))
       			 buttonsElements.add(webElement);
       		 else if(webElement.getAttribute("ControlType").equals("ControlType.ListItem"))
       			 listsElements.add(webElement);
       		 else if(webElement.getAttribute("ControlType").equals("ControlType.ScrollBar"))
       			 scrollsElements.add(webElement);
   		}
       	 
        	// Open the listbox
//       	List<WebElement> dropdownbutton1= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
       	 	if(!buttonsElements.isEmpty())
       	 		buttonsElements.get(0).click();
//        	lstBox.click();

        	
//   		List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
   		
//   		List<WebElement> scrollCheck= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ScrollBar')]"));
   		
//   		System.out.println();
   		if (!scrollsElements.isEmpty()) {
        	
        	// get list of all the available values
      		List<String> allValues = new ArrayList<String>();
      		int count = 0;
      		for(WebElement e : listsElements) {
      			allValues.add(e.getAttribute("Name"));
      			if(e.getAttribute("ClickablePoint") != null)
      				count++;
      		}
      		if (count != 0) { 
      		
      		// Check the distance of the needed value
      		int place = allValues.indexOf(ItemName);
      		if (place > count)
      			place += place / count;
      		int moves = place / count;
      		int reset = allValues.size()/count;
      		if (allValues.size() > count) reset += 1; 
        	
      		if (reset > 0) {
      		// Look for scrolls
//      		List<WebElement> scroll= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ScrollBar')]"));
        	// Look for scroll button
      		for (WebElement e : scrollsElements)
      			if (e.getAttribute("Name").equals("Vertical Scroll Bar")) {
      			
      		List<WebElement> scrollButtons= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));

      		for(WebElement f : scrollButtons)
    		{
      			// Reset
      			if (f.getAttribute("Name").equals("Back by large amount"))
   				{
    				for (int i = 0; i < reset; i++) 
    					f.click();
   				}
      			// Scorll to place
    			if (f.getAttribute("Name").equals("Forward by large amount"))
   				{
    				for (int i = 0; i < moves; i++) 
    					f.click();
   				}
    		}
        	}
      		}
      		
      		} 
      		
      		} // Scroll check
      		// finally, for all
    		for(WebElement e : listsElements)
    		{
    		//	System.out.println(e.getAttribute("Name"));
    			if (e.getAttribute("Name").equals(ItemName))
   				{
   				  e.click();
   				  return;
   				}
    		}
    	}
        
        /**
         * Get the info from the Lexical display table
         * @param test
          *@param logger
          *@param titleToCheck
          *@param textToCheck
          *@return
         */
        public ArrayList<HashMap<String, String>> lexiconDisplayTable()
    	{
        	// Get the rows in the lexical display table
        	List<WebElement> panes = tableLayoutPanel7.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
        	
        	// To save the table
        	ArrayList<HashMap<String, String>> LexicalTable = new ArrayList<HashMap<String, String>>();
        	// For every row
        	HashMap<String, String> row = null;
        	
        	for (WebElement p : panes) {
//        		System.out.println(p.getAttribute("AutomationId"));
        		if (p.getAttribute("AutomationId").equals("pn_lexicon_records")) {
        			
		        	List<WebElement> paneRows = p.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
		        	for(WebElement e : paneRows)
		    		{ 
		        		List<WebElement> texts = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
//		        			row.clear();
		        			row = new HashMap<String, String>();
		        			row.put("Dictionary Key", texts.get(0).getAttribute("Name"));
		        			row.put("POS", texts.get(1).getAttribute("Name"));
		        			row.put("Form Type", texts.get(2).getAttribute("Name"));
		        			row.put("Frames", texts.get(3).getAttribute("Name"));
		        			row.put("Semantic Fields", texts.get(4).getAttribute("Name"));
		        			row.put("Semantic Group", texts.get(5).getAttribute("Name"));
		        			row.put("Transcription", texts.get(6).getAttribute("Name"));
		        			row.put("Presentations", texts.get(7).getAttribute("Name"));
		        		
		        		// Table
		        		LexicalTable.add(row);
		        	}
        	
        	} // end of if
        	}
        	 return LexicalTable;
    	} 
        

        
        /**
         * choose value from dropdown that is TEXT
         * @param lstBox
         * @param ItemName
         * @throws InterruptedException
         */
        public void chooseValueInDropdownText(WebElement lstBox, String ItemName) throws InterruptedException
    	{
        	List<WebElement> dropdownbutton1= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
			dropdownbutton1.get(0).click();
//        	lstBox.click();
        	Thread.sleep(1000);
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
    		System.out.println(itemlist.size());
    		
//        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			//System.out.println(e.getAttribute("Name"));
    			e.getAttribute("Name");

    			if (e.getAttribute("Name").equals(ItemName))
				{
				  e.click();
				}
    		}
    	}
        
        /**
         * edit value from grammar attribute text list
         * @param lstBox
         * @param ItemName
         * @throws InterruptedException
         */
        public void editValueInGrammarAttributes(WebElement lstBox, String ItemName) throws InterruptedException
    	{
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
    		
//        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			if (e.getAttribute("Name").equals(ItemName))
				{
//					  e.click();
					Actions builder = new Actions(DriverContext._Driver);
			  		builder.moveToElement(e).doubleClick().perform();
				}
    		}
    	}
        
//            window = driver.find_element_by_id('lb_Lexicon_retrieved_records')
//
//            records_list_box = driver.find_element_by_id('lb_Lexicon_retrieved_records')
//            all_records_in_listbox = records_list_box.find_elements_by_xpath(
//                "./*[contains(@ControlType, 'ControlType.ListItem')]")
//            records = []
//
//            for ii in all_records_in_listbox:
//                records.append(ii.get_attribute('Name'))

           

        
       /**
        * Get clean records names from the Lexicon.txt files
        * @return
        * @throws FileNotFoundException
        */
        public List<String> getRecordsFromLexicon() throws FileNotFoundException {

		   		Scanner in = new Scanner(new File(Setting.AppPath +Setting.Language +"\\Data\\lexicon.txt"), "UTF-8");
		   		List<String> lines = new ArrayList<>();
		   		while(in.hasNextLine()) {
		   		    String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");
		   		    
		   		    String[] removeLeft = splitted[0].split("}");
		   		    String[] removeRight = removeLeft[1].split("~");
		   		    lines.add(removeRight[0]);
		   		}
		   		
		   		in.close();
				return lines;
        }
       
        
      
        /**
         * Search in Lexicon
         * @param test
         * @param logger
      	* @param firstDropdownValue
      	* @param conditionDropdownValue
      	* @param searchWord
      	* @param filterDropdownValue
         * @return
      * @throws InterruptedException 
         */
        public void searchInLexicon(ExtentTest test,Logger logger,String firstDropdownValue,String conditionDropdownValue,String searchWord,String filterDropdownValue) throws InterruptedException
    	{
 		LexiconTab LexiconTab = GetInstance(LexiconTab.class).As(LexiconTab.class);
     	
    		// Choose value from the first search dropdown
    		CommonFunctions.chooseValueInDropdown(LexiconTab.cmpCondition_field,firstDropdownValue);
    		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

    		// Choose value from the condition dropdown
    		CommonFunctions.chooseValueInDropdown(LexiconTab.cmpCondition_operation, conditionDropdownValue);
    		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

    		//Type in the search field
    		LexiconTab.cmpCondition_value.sendKeys(searchWord);
    		LogUltility.log(test, logger, "type in the search field: "+searchWord);
    		
    		// Change the last filter value, in case it was changed by other TC
    		CommonFunctions.chooseValueInDropdown(LexiconTab.cmpCondition_all_next,filterDropdownValue);
    		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
    		
    		// Click the Retrieve button
    		LexiconTab.btnRetrieve_same.click();
    		LogUltility.log(test, logger, "Click the Retreive button");
    		
    	} // close function
        
        
        /**
         * Click on the wanted black square to navigate to dictionary
         * @param i - the wanted row index
          *@return
         */
        public void clickOnDisplayTableRow(int index)
    	{
        	// Get the rows in the lexical display table
        	List<WebElement> panes = tableLayoutPanel7.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
        	
        	List<WebElement> buttons = new ArrayList<WebElement>();

        	for (WebElement p : panes) {
//        		System.out.println(p.getAttribute("AutomationId"));
        		if (p.getAttribute("AutomationId").equals("pn_lexicon_records")) {
        			
		        	List<WebElement> paneRows = p.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
		        	for(WebElement e : paneRows)
		    		{ 
		        		WebElement button = e.findElement(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
		        		buttons.add(button);
		        	}
        	
        	} // end of if
        	}
        	
        	// Press the wanted button
        	buttons.get(index).click();
   
    	} 
        
        
        /**
         * Get records names from the Lexicon.txt files with Grammar Attribute values
         * @return 
         * @throws FileNotFoundException
         */
         public List<String> getRecordsFromLexiconWithGA() throws FileNotFoundException {

 		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\Lexicon.txt"), "UTF-8");
 		   		List<String> lines = new ArrayList<>();
 		   		while(in.hasNextLine()) {
 		   		    String line = in.nextLine().trim();
 		   		    String[] splitted = line.split("	");
 		   		    

 		   		    try {
 		   		    if(splitted[splitted.length-1].contains("BAA¿"))
 		   		    {
 		   		    	String[] removeRight = splitted[0].split("\\|");
 		   		    	if(!lines.contains(removeRight[0]))
 		   		    		lines.add(removeRight[0]);	
 		   		    }
	 		   		   
 		   		}	
 		   		catch(Exception e)
 		   		    {	}
 		   		}
 		   		in.close();
 				return lines;
         
         }
         
         /**
          * Get record grammar attributes from file
          * @return 
          * @throws FileNotFoundException
          */
          public String getRecordGrammarAttributesFromFile(String recordName) throws FileNotFoundException {

  		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language+"\\Data\\Lexicon.txt"), "UTF-8");   	
  		   		String grammarAttributes ="";
  		   		while(in.hasNextLine()) {
  		   		    String line = in.nextLine().trim();
  		   		    String[] splitted = line.split("	");
  		   		    
  		   		    if(splitted[0].contains(recordName))
  		   		    {
  		   		    	
  		   		   try {
  	  		   		    if(splitted[splitted.length-1].contains("BAA¿"))
  	  		   		    {
  	  		   		    	grammarAttributes = splitted[splitted.length-1];
  	  		   		    	in.close();
  	  		   		    	return grammarAttributes;
  	  		   		    }
  	  		   		    else
  	  		   		    {
  	  		   		    	// There is no grammar attribute values
  	  		   		    	in.close();
	  		   		    	return grammarAttributes;
  	  		   		    }
  	 	 		   		   
  	  		   		}	
  	  		   		catch(Exception e)
  	  		   		    {	
  	  		   				
  	  		   		    }
  		   		    }		   			}
  		   		in.close();
  		   		return grammarAttributes;
          
          }
        
	} // Class end

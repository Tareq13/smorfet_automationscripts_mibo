package smi.smorfet.test.pages;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;

/**
 * Define elements and methods for the Main Display window
 *
 */
public class MainDisplayWindow extends BasePage {
	
	// Main display window
	@FindBy(how=How.ID,using ="Start_Window")
	public WebElement Start_Window;
	
	// Smorfet button
	@FindBy(how=How.ID,using ="bt_proceed")
	public WebElement bt_Smorfet;
	
	// Abort button
	@FindBy(how=How.ID,using ="bt_abort")
	public WebElement bt_abort;
	
	// Smorfet main window
	@FindBy(how=How.ID,using ="SMI_Root_GUI")
	public WebElement SMI_Root_GUI;
	
	// Merge corpora checkbox
	@FindBy(how=How.ID,using ="cx_merge_corpora")
	public WebElement cx_merge_corpora;
	
	// No button
	@FindBy(how=How.NAME,using ="No")
	public WebElement No;
	
	// Usernames dropdown
	@FindBy(how=How.ID,using="dd_start_user")
	public WebElement dd_start_user;
	
	// SmorphDesktop button
	@FindBy(how=How.ID,using="bt_SmorphDesktopOnly")
	public WebElement bt_SmorphDesktopOnly;
	
	// language dropdown
	@FindBy(how=How.ID,using="dd_main_language")
	public WebElement dd_main_language;
	
	// Warning message for Names tab
	@FindBy(how=How.NAME,using="Name Repository Unavailable")
	public WebElement namesWarningPopup;
	
	// Ok button in popup
	@FindBy(how=How.NAME,using="OK")
	public WebElement btnOk;
	
	// language checkboxes list
	@FindBy(how=How.ID,using="cb_languages")
	public WebElement cb_languages;
		
    public void LaunchSmorfetWait() throws IOException, InterruptedException {
    	
		// launch the app
		String new_dir =  Setting.AppPath;
		System.setProperty("user.dir", new_dir);
		Runtime.getRuntime().exec("cmd.exe /c start SMI_Back_Office.exe", null, new File(new_dir));
	    
	    // Click merge corpora button	
	    Thread.sleep(2000);
	    Start_Window.click();
	    cx_merge_corpora.click();
	   
	    // Choose language
	    switch(Setting.Language)
	    {
	    case "HE":
	    	CommonFunctions.chooseValueInDropdown(dd_main_language, "Hebrew");break;
	    case "EN":
	     	CommonFunctions.chooseValueInDropdown(dd_main_language, "English");break;
	    }
	    
	    // Click the smorfet button
	    bt_Smorfet.click();
		
//	    Thread.sleep(220000);
	    // Wait until smorfet is opened
		try {
		WebDriverWait wait = new WebDriverWait(DriverContext._Driver, 130);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='SMI_Root_GUI']")));
		}catch (Exception e) {
//			e.printStackTrace();
		}
		
		boolean isVisible = false;

		try {
			if(SMI_Root_GUI.isDisplayed())
      		  isVisible = true;
		}catch (Exception e) {
//			e.printStackTrace();
		}
		
		try {
			if(!isVisible)
			{
				if(namesWarningPopup.isDisplayed())
					{btnOk.click();	isVisible = true;}
				WebDriverWait wait = new WebDriverWait(DriverContext._Driver, 20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='SMI_Root_GUI']")));	
			}
				
		} catch (Exception e) {}

  	  assertTrue(isVisible);
  	  
  	  }
    
    
    /**
     * Close smorfet and click "No" if popup displayed
     * @param test
     * @param logger
     * @throws IOException
     */
    public void closeSmorfetNo(ExtentTest test, Logger logger) throws IOException {	
		// Close the app
//		LogUltility.log(test, logger, "Close smorfet");
//		CommonFunctions.close_smorfet.click();
    	
    	WebElement closeSmorfet = SMI_Root_GUI.findElement(By.xpath(".//*[contains(@Name, 'Close')]"));
    	closeSmorfet.click();
    	
		String textToCheck = "Do you want to save changes?";
		boolean popupDisplayed;
		try {	
			popupDisplayed = popupWindowMessage(test, logger, textToCheck);
		}catch (Exception e) {
//			e.printStackTrace();
			popupDisplayed = false;
		}
		
		if(popupDisplayed)
			No.click();
    }
    
    
    /**
     * Fetch the text of pane popup
     * @param test
     * @param logger
     * @param titleToCheck
     * @param textToCheck
     * @return
     */
    public boolean popupWindowMessage(ExtentTest test, Logger logger, String textToCheck)
	{
    	List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
    	List<WebElement> text = null;
	    boolean textIn = false;
	    
    	// Check the main app for popup windows
		for(WebElement e : windows)
			text = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	

		// Fetch the found title    		
		for(WebElement e : text)
		{ 
			LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
			if (e.getAttribute("Name").contains(textToCheck))
				textIn = true;
    	}
		
		LogUltility.log(test, logger, " - TextIn: " + textIn);
		
	    if (textIn)
	    	return true;
	    else return false;
	} // close function
    
    
    public void openSmorfetMainWindow() throws IOException, InterruptedException {
    	
		// launch the app
		String new_dir =  Setting.AppPath;
		System.setProperty("user.dir", new_dir);
	    Runtime.getRuntime().exec(new_dir + "SMI_Back_Office.exe", null, new File(new_dir));
	    
		try {
			WebDriverWait wait = new WebDriverWait(DriverContext._Driver, 2);
		wait.until(ExpectedConditions.visibilityOf(Start_Window));
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean isVisible = false;

		try {
			if(Start_Window.isDisplayed())
      		  isVisible = true;
		}catch (Exception e) {
			e.printStackTrace();
		}
  	  assertTrue(isVisible);
  	  
  	  }
    
    public void LaunchSmorphDesktopWait() throws IOException, InterruptedException {
    	
	    // Click the smorfet button
	    bt_SmorphDesktopOnly.click();
		
	    // Wait until smorfet is opened
		try {
			WebDriverWait wait = new WebDriverWait(DriverContext._Driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='SMI_Root_GUI']")));
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean isVisible = false;

		try {
			if(SMI_Root_GUI.isDisplayed())
      		  isVisible = true;
		}catch (Exception e) {
			e.printStackTrace();
		}
  	  assertTrue(isVisible);
  	  
  	  }

}



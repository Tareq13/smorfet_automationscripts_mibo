package smi.smorfet.test.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.uiltilies.LogUltility;

/**
 * Define elements and methods for the Homepage
 *
 */
public class DisambiguationTab extends BasePage {
	
	// Disambiguation tab
	@FindBy(how=How.NAME,using ="Disambiguation")
	public WebElement tabDisambiguation;
	
	// Disambiguation layout
	@FindBy(how=How.ID,using="disambig_graph_panel")
	public WebElement disambiguationLayout;
	
	// None button
	@FindBy(how=How.ID,using="bt_all_off")
	public WebElement btnNone;
	
	// Update button
	@FindBy(how=How.ID,using="bt_update")
	public WebElement bt_update;
	
	// Smorfet main window
	@FindBy(how=How.ID,using ="SMI_Root_GUI")
	public WebElement SMI_Root_GUI;
	
	// Threshold input field
	@FindBy(how=How.ID,using="nwd_proximity_best_score_threshold")
	public WebElement thresholdInput;
	
	// Next arrow button
	@FindBy(how=How.ID,using ="bt_next")
	public WebElement bt_next;
	
	// Previous arrow button
	@FindBy(how=How.ID,using ="bt_prev")
	public WebElement bt_prev;
	
	// Best next weight 
	@FindBy(how=How.ID,using="nwd_best_next_percent")
	public WebElement best_next_weight;

	// Best next score
	@FindBy(how=How.ID,using="nwd_best_next_score")
	public WebElement best_next_score;
	
	// Proximity weight 
	@FindBy(how=How.ID,using="nwd_proximity_percent")
	public WebElement proximity_weight;
	
	// Proximity positive score
	@FindBy(how=How.ID,using="nwd_proximity_positive_match_score")
	public WebElement proximity_positive_score;
	
	// Proximity negative score
	@FindBy(how=How.ID,using="nwd_proximity_negative_match_score")
	public WebElement proximity_negative_score;
	
	// Compound weight
	@FindBy(how=How.ID,using="nwd_compound_percent")
	public WebElement compound_weight;
	
	// Compound score
	@FindBy(how=How.ID,using="nwd_compound_score")
	public WebElement compound_score;
	
	// Grammar weight
	@FindBy(how=How.ID,using="nwd_grammar_percent")
	public WebElement grammar_weight;
	
	// Grammar p2 score
	@FindBy(how=How.ID,using="nwd_grammar_p2_match_score")
	public WebElement grammar_p2score;
	
	// Grammar p1 score
	@FindBy(how=How.ID,using="nwd_grammar_p1_match_score")
	public WebElement grammar_p1score;
	
	// Grammar n1 score
	@FindBy(how=How.ID,using="nwd_grammar_n1_match_score")
	public WebElement grammar_n1score;
	
	// Grammar n2 score
	@FindBy(how=How.ID,using="nwd_grammar_n2_match_score")
	public WebElement grammar_n2score;
	
	// Grammar same edge score
	@FindBy(how=How.ID,using="nwd_grammar_same_edge_score")
	public WebElement grammar_samescore;
	
	// Grammar near edge score
	@FindBy(how=How.ID,using="nwd_grammar_near_edge_score")
	public WebElement grammar_nearscore;

	// Context weight 
	@FindBy(how=How.ID,using="nwd_context_percent")
	public WebElement context_weight;
	
	// Context score 
	@FindBy(how=How.ID,using="nwd_context_score")
	public WebElement context_score;
	
	// Spelling weight 
	@FindBy(how=How.ID,using="nwd_spelling_percent")
	public WebElement spelling_weight;
	
	// Spelling score 
	@FindBy(how=How.ID,using="nwd_spelling_score")
	public WebElement spelling_score;
		
	// Frequency weight
	@FindBy(how=How.ID,using="nwd_frequency_percent")
	public WebElement frequency_weight;
	
	// Frequency score
	@FindBy(how=How.ID,using="nwd_frequency_score")
	public WebElement frequency_score;
	
	// New for 1.18
	// Semantic field proximity field
	@FindBy(how=How.ID,using="semantic_field_proximity")
	public WebElement semantic_field_proximity;
	
	// Semantic group proximity field
	@FindBy(how=How.ID,using="semantic_group_proximity")
	public WebElement semantic_group_proximity;
	
	// Frame proximity field
	@FindBy(how=How.ID,using="frame_proximity")
	public WebElement frame_proximity;
	
	// ft_proximity field
	@FindBy(how=How.ID,using="ft_proximity")
	public WebElement ft_proximity;
	
	// Compound score field
	@FindBy(how=How.ID,using="compound")
	public WebElement compound_score_field;
	
	// Frequency score field
	@FindBy(how=How.ID,using="frequency")
	public WebElement frequency_score_field;
	
	// Proximity score field
	@FindBy(how=How.ID,using="proximity")
	public WebElement proximity_score_field;
	
	// Grammar score field
	@FindBy(how=How.ID,using="grammar")
	public WebElement grammar_score_field;
	
	 /**
     * Fetch the text and title of popup windows within Smorfet
     * @param test
     * @param logger
     * @param titleToCheck
     * @param textToCheck
     * @return
     */
    public boolean popupWindowMessage(ExtentTest test, Logger logger, String titleToCheck, String textToCheck)
	{
    	List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
    	List<WebElement> text = null;
    	List<WebElement> title = null;
    	
    	boolean titleIn = false;
	    boolean textIn = false;
	    
    	// Check the main app for popup windows
		for(WebElement e : windows)
		{ 
			text = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	
        	title = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TitleBar')]"));
    	}
		
		// Fetch the found title
		for(WebElement e : title)
		{
			LogUltility.log(test, logger, "Title:" + e.getAttribute("Name"));
			if (e.getAttribute("Name").contains(titleToCheck))
				titleIn = true;
    	}
		
		// Fetch the found title    		
		for(WebElement e : text)
		{ 
			LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
			if (e.getAttribute("Name").contains(textToCheck))
				textIn = true;
    	}
		
		LogUltility.log(test, logger, "TitleIn: " + titleIn + " - TextIn: " + textIn);
		
		// Close window
		WebElement okButton = SMI_Root_GUI.findElement(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
		okButton.click();
		LogUltility.log(test, logger, "Press Ok to close window");
		
	    if (titleIn && textIn)
	    	return true;
	    else return false;
	} // close function
	
    
    /**
     * Get displayed sentence text from Disambiguation
     * @param DisLayout
     * @return
     * @throws InterruptedException
     */
     public String getDisplayedSentence() throws InterruptedException
 	{
    	// Get panes from main disambiguation pane
     	List<WebElement> mainPane = disambiguationLayout.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
     	// Get all panes under the pane
     	List<WebElement> paneslist= mainPane.get(1).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
     	ArrayList<String> itemNames= new ArrayList<String>();
 		for(WebElement pane : paneslist)
 		{
 			// Get text for all word squares
 			WebElement textElement = pane.findElement(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
 			if(!textElement.getAttribute("Name").contains("Dummy Node"))
 				itemNames.add(textElement.getAttribute("Name"));
 		}

 		Collections.reverse(itemNames);
 		String sentence = "";
 		for(int i=0;i<itemNames.size();i++)
 			sentence+=itemNames.get(i)+" ";
// 		System.out.println(sentence);
 		return sentence;
 	}// close function
     
     /**
      * Set field value
      * @param inputField
      * @param value
      * @throws InterruptedException
      * @throws AWTException
      */
      public void setElementValue(WebElement inputField,int value) throws InterruptedException, AWTException
	  	{
    	  
    	// Get the element position in order to click and highlight the text
  		String attachPlace = inputField.getAttribute("ClickablePoint"); 
  		String[] XY = attachPlace.split(",");
  	    Robot robot = new Robot();
        robot.mouseMove(Integer.parseInt(XY[0])+5,Integer.parseInt(XY[1]));
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_A);
        robot.keyRelease(KeyEvent.VK_A);
        robot.keyRelease(KeyEvent.VK_CONTROL);
//        inputField.sendKeys(Keys.chord(Keys.CONTROL, "a"));
  		
  		// Change the field value
  		inputField.sendKeys(Integer.toString(value));
	  	}// close method
      
      /**
       * Get field value
       * @param inputField
       * @throws InterruptedException
       * @throws AWTException
     * @throws IOException 
     * @throws UnsupportedFlavorException 
       */
       public double getElementValue(WebElement inputField) throws InterruptedException, AWTException, UnsupportedFlavorException, IOException
 	  	{
     	  
    	   // Get the element position in order to click and highlight the text
 		String attachPlace = inputField.getAttribute("ClickablePoint"); 
 		String[] XY = attachPlace.split(",");
     		
    	// Move to element and highlight the input
    	Robot robot = new Robot();
   		robot.mouseMove(Integer.parseInt(XY[0])+5,Integer.parseInt(XY[1]));
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);

   		// Copy the highlighted input
       	robot.keyPress(KeyEvent.VK_CONTROL);
   		robot.keyPress(KeyEvent.VK_A);
   		robot.keyPress(KeyEvent.VK_C);
   		robot.keyRelease(KeyEvent.VK_C);
   		robot.keyRelease(KeyEvent.VK_A);
   		robot.keyRelease(KeyEvent.VK_CONTROL);

   		
   		// Get data stored in the clipboard that is in the form of a string (text)
   		Thread.sleep(500);
   		Clipboard c=Toolkit.getDefaultToolkit().getSystemClipboard();
   	    String thresholdValueAfterUpdate = (String) c.getData(DataFlavor.stringFlavor);
   	  
   	    return Double.parseDouble(thresholdValueAfterUpdate);
   	    
 	  	}// close method
     
       /**
        * Get rules popup info , rule as key, score and weight as values
        * @return
        * @throws InterruptedException
        */
        public HashMap<String, List<Double>> getRulesPopupInfo() throws InterruptedException
    	{
        	// main disambiguation pane
        	List<WebElement> mainPane = disambiguationLayout.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Pane')]"));
        	
        	// Get all text under the pane
        	List<WebElement> textlist= mainPane.get(1).findElements(By.xpath("./*[contains(@ControlType, 'ControlType.Text')]"));
        	
        	// Click on the first square to display the info popup
        	textlist.get(1).click();
        	
        	// Initialize map
 	 		HashMap<String, List<Double>> popupInfo = new HashMap<String, List<Double>>(); 
        	
        	// Get the popup info 
        	WebElement popupElement = SMI_Root_GUI.findElement(By.xpath("./*[contains(@ControlType, 'ControlType.ToolTip')]"));
        	String info = popupElement.getAttribute("Name");

        	// Split to get each line
        	String lines[] = info.split("\\r");
        	// Get info from each line
        	for(int i=2;i<lines.length;i++)
        	{
        		String values[] = lines[i].split("\\t");
        		// Eliminate the colon :
//        		String key = values[0].substring(0, values[0].length()-1);
        		String key = values[0].split(" ")[0];
        		List<Double> score_weight = new ArrayList<Double>(2);
        		// Set score value
        		score_weight.add(Double.parseDouble(values[1]));
        		// Get weight value, eliminate the square brackets []
        		String weight = values[2].split("\\[")[1].split("\\%")[0];     
        		score_weight.add(Double.parseDouble(weight));
        		popupInfo.put(key, score_weight);
        		}

        return popupInfo;
        		
    	}// close function
        
       /**
        * Get word tagging option info
        * @param word - wanted word
        * @param TaggingIndex - wanted tagging option index
        * @return
        * @throws InterruptedException
     * @throws AWTException 
        */
         public HashMap<String, String> getWordTaggingOptionInfo(String word,int wordIndex, int TaggingIndex) throws InterruptedException, AWTException
     	{
        	// Initialize map
  	 		HashMap<String, String> popupInfo = new HashMap<String, String>(); 
  	 		
        	// Get panes from main disambiguation pane
         	List<WebElement> mainPane = disambiguationLayout.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
         	// Get all panes under the pane
         	List<WebElement> paneslist= mainPane.get(1).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
         	int count = 0;
     		for(WebElement pane : paneslist)
     		{
     			// Get text for all word squares
     			WebElement textElement = pane.findElement(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
     			List<WebElement> buttons = pane.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
     			if(textElement.getAttribute("Name").equals(word) && wordIndex == count++)
     			{
     				// Get the wanted tagging option info
     		 		String attachPlace = buttons.get(TaggingIndex).getAttribute("ClickablePoint"); 
     		 		String[] XY = attachPlace.split(",");
     		     		
     		    	// Move to element 
     		    	Robot robot = new Robot();
     		   		robot.mouseMove(Integer.parseInt(XY[0])+5,Integer.parseInt(XY[1]));
     		      
     		   		// Get the displayed poup info
     	        	WebElement popupElement = SMI_Root_GUI.findElement(By.xpath("./*[contains(@ControlType, 'ControlType.ToolTip')]"));
     	        	String info = popupElement.getAttribute("Name");
     	        	
     	        	// Split to get each line
     	        	String lines[] = info.split("\\r");
     	        	// Add  the dictionary key first
     	        	popupInfo.put("Key",lines[0]);
     	        	// Get info from each line
     	        	for(int i=2;i<lines.length-1;i++)
     	        	{
     	        		try {
     	        			String values[] = lines[i].split("\\:");
         	        		// Eliminate the colon :
         	        		String key = values[0];
         	        		String val = values[1].trim();
         	        		popupInfo.put(key, val);		
     	        		}
     	        		catch (Exception e) {
     	        			e.printStackTrace();
						}
     	        		
     	        	}
     	        	return popupInfo;
     			}
     		}
     		
     		// Not found returns empty hashmap
     		return popupInfo;
     		
     	}// close function
         
         /**
          * Get word tagging option element
          * @param word - wanted word
          * @param TaggingIndex - wanted tagging option index
          * @return
          * @throws InterruptedException
       * @throws AWTException 
          */
           public WebElement getWordTaggingOptionElement(String word,int wordIndex, int TaggingIndex) throws InterruptedException, AWTException
       	{
          	//
        	WebElement taggingOptionElement = null ;
          	// Get panes from main disambiguation pane
           	List<WebElement> mainPane = disambiguationLayout.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
           	// Get all panes under the pane
           	List<WebElement> paneslist= mainPane.get(1).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
           	int count = 0;
       		for(WebElement pane : paneslist)
       		{
       			// Get text for all word squares
       			WebElement textElement = pane.findElement(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
       			List<WebElement> buttons = pane.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
       			if(textElement.getAttribute("Name").equals(word) && wordIndex == count++)
       			{	
       				taggingOptionElement = buttons.get(TaggingIndex);
       				return taggingOptionElement;
       			}
       		}
       		
       		// Not found returns 
       		return taggingOptionElement;
       		
       	}// close function
         
	} // Class end

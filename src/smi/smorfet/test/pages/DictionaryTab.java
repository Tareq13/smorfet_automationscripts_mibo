package smi.smorfet.test.pages;


import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
//import com.gargoylesoftware.htmlunit.javascript.host.media.webkitMediaStream;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.LogUltility;

/**
 * Define elements and methods for the Homepage
 *
 */
public class DictionaryTab extends BasePage {
	
	// Dictionary whole frame tab
	@FindBy(how=How.ID,using ="TitleBar")
	public WebElement TitleBar;
	
	// Intro window
	@FindBy(how=How.NAME,using ="Attachment")
	public WebElement WindowAttachment;
	
	// Abort button in menu intro window
	@FindBy(how=How.NAME,using ="Abort")
	public WebElement btnAbort;
	
	// Dictionary whole frame tab
	@FindBy(how=How.ID,using ="tc_TabControl")
	public WebElement tabDictionary;
	
	// The Dictionary Tab selector:  Dictionary
	@FindBy(how=How.NAME,using ="Dictionary")
	public WebElement dictionaryTab;
	
	// New button
	@FindBy(how=How.ID,using ="bt_new")
	public WebElement btnNew;
	
	// Save button
	@FindBy(how=How.ID,using ="bt_save")
	public WebElement btnSave;
	
	// "YES" in the confirmation message for the new button
	@FindBy(how=How.NAME,using ="Yes")
	public WebElement btnYes;
	
	// Key form text field
	@FindBy(how=How.ID,using ="tb_dictionary_key_form")
	public WebElement txtKeyForm;
	
	// Language combobox
	@FindBy(how=How.ID,using ="dd_dictionary_language")
	public WebElement cmpLanguage;
		
	// Temprorarly
	@FindBy(how=How.NAME,using ="French")
	public WebElement French;
	
	// POS combobox
	@FindBy(how=How.ID,using ="dd_dictionary_POS")
	public WebElement cmpPOS;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="name")
	public WebElement name;
	
	// Origin Combobox
	@FindBy(how=How.ID,using ="dd_dictionary_origin")
	public WebElement cmpOrigin;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="Latin")
	public WebElement Latin;
	
	// Style Combobox
	@FindBy(how=How.ID,using ="dd_dictionary_style")
	public WebElement cmpStyle;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="irony")
	public WebElement irony;
	
	// dd_condition_field
	@FindBy(how=How.ID,using ="dd_condition_field")
	public WebElement cmpCondition_field;
	
	// dd_condition_operation
	@FindBy(how=How.ID,using ="dd_condition_operation")
	public WebElement cmpCondition_operation;
	
	// dd_condition_value
	@FindBy(how=How.ID,using ="dd_condition_value")
	public WebElement cmpCondition_value;
	
	// dd_all_or_next
	@FindBy(how=How.ID,using ="dd_all_or_next")
	public WebElement cmpCondition_all_next;
	
	// Retrieve button bt_retrieve_same
	@FindBy(how=How.ID,using ="bt_retrieve_same")
	public WebElement btnRetrieve_same;
	
 	// Acronym Combobox, choose value
	@FindBy(how=How.ID,using ="dd_dictionary_acronym")
	public WebElement cmpAcronym;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="fusion")
	public WebElement fusion;

    // Register Combobox, choose value
	@FindBy(how=How.ID,using ="dd_dictionary_register")
	public WebElement cmpRegister;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="formal")
	public WebElement formal;

	// Frequency Combobox, choose value
	@FindBy(how=How.ID,using ="dd_dictionary_frequency")
	public WebElement cmpFrequency;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="common")
	public WebElement common;
	
	// Gender Combobox, choose value
	@FindBy(how=How.ID,using ="dd_dictionary_gender")
	public WebElement cmpGender;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="female")
	public WebElement female;
	
	// Number Combobox, choose value
	@FindBy(how=How.ID,using ="dd_dictionary_number")
	public WebElement cmpNumber;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="irrelevant")
	public WebElement irrelevant;

	// Definiteness Combobox, choose value
	@FindBy(how=How.ID,using ="dd_dictionary_definiteness")
	public WebElement cmpDefiniteness;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="both")
	public WebElement both;

	// Declinability Combobox, choose value
	@FindBy(how=How.ID,using ="dd_dictionary_declinability")
	public WebElement cmpDeclinability;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="not declinable")
	public WebElement notDeclinable;

	// Dependency Combobox, choose value
	@FindBy(how=How.ID,using ="dd_dictionary_dependency")
	public WebElement cmpDependency;
	
	// Temprorarly
	@FindBy(how=How.NAME,using ="dependent")
	public WebElement dependent;
	
	// Dictionary records list
	@FindBy(how=How.ID,using ="lb_dictionary_retrieved_records")
	public WebElement lst_records;

	// Frames list
	@FindBy(how=How.ID,using ="lb_dictionary_frames")
	public WebElement lst_frames;
	
	// Window too many many retrieved records
	@FindBy(how=How.NAME,using ="Too many retrieved records")
	public WebElement window_too_many_records;
	
	// Window too many many retrieved records
	@FindBy(how=How.NAME,using ="Illegal POS selected!\\r\\rValue will be reverted")
	public WebElement window_Illegal;
	
	// Popup window, OK button
	@FindBy(how=How.NAME,using ="OK")
	public WebElement btnOKPopupWindow;
	
	// Popup window, accept button
	@FindBy(how=How.ID,using ="bt_utility_accept")
	public WebElement bt_utility_accept;

	// volume [POS] dropdown
	@FindBy(how=How.ID,using ="dd_dictionary_volume")
	public WebElement cmpVolumePOS;
	
	// Previous arrow button
	@FindBy(how=How.ID,using ="bt_prev")
	public WebElement bt_prev;
	
	// Semantic Fields list
	@FindBy(how=How.ID,using ="lb_dictionary_semantic_fields")
	public WebElement lb_dictionary_semantic_fields;
	
	// Duplicate button
	@FindBy(how=How.ID,using ="bt_duplicate")
	public WebElement bt_duplicate;
	
	// Revert button
	@FindBy(how=How.ID,using ="bt_revert")
	public WebElement bt_revert;

	// OK button on the Revert file popup 
	@FindBy(how=How.NAME,using ="OK")
	public  WebElement revertFileMsgOK;
	   
	// Merge button
	@FindBy(how=How.ID,using ="bt_merge_files")
	public WebElement btn_merge_files;
	
	// Cancel in popups
	@FindBy(how=How.ID,using ="bt_utility_cancel")
	public WebElement btnpopupCancelWindow;	
	
	// Cancel in popups
	@FindBy(how=How.ID,using ="bt_utility_accept")
	public WebElement btnpopupAcceptWindow;
	
	// Mass attribute button
	@FindBy(how=How.ID,using ="bt_mass_attribute")
	public WebElement bt_mass_attribute;
	
	// Popup value dropdown
	@FindBy(how=How.ID,using ="dd_utility_value")
	public WebElement cmpPopupValue;

	// Transcriptions list
	@FindBy(how=How.ID,using ="lb_dictionary_transcriptions")
	public WebElement lstTranscriptions;

	// Transcriptions list delete
	@FindBy(how=How.ID,using ="bt_delete_transcription")
	public WebElement bt_delete_transcription;
	
	// Transcriptions list add
	@FindBy(how=How.ID,using ="bt_add_transcription")
	public WebElement bt_add_transcription;
	
	// Definition text box
	@FindBy(how=How.ID,using ="tb_dictionary_definition")
	public WebElement boxDefinition;
	
	// Etymology text box
	@FindBy(how=How.ID,using ="tb_dictionary_etymology")
	public WebElement boxEtymology;
	
	// Delete button
	@FindBy(how=How.ID,using ="bt_delete")
	public WebElement bt_delete;
	
	// Removed record confirmation text 
	@FindBy(how=How.CLASS_NAME,using ="#32770")
	public WebElement text_removed_record_confirmation;

	// First line in popup after delete
	@FindBy(how=How.ID,using ="ll_utility_3")
	public WebElement popup_after_delete_first_line;
	
	// Attach button
	@FindBy(how=How.ID,using ="bt_attach")
	public WebElement bt_attach;
	
	// Spelling list box
	@FindBy(how=How.ID,using ="lb_dictionary_spellings")
	public WebElement lb_dictionary_spellings;	
	
	// Spelling add button
	@FindBy(how=How.ID,using ="bt_add_spelling")
	public WebElement bt_add_spelling;

	// Spelling delete button
	@FindBy(how=How.ID,using ="bt_delete_spelling")
	public WebElement bt_delete_spelling;

	// Form delete button
	@FindBy(how=How.ID,using ="bt_delete_form")
	public WebElement bt_delete_form;
	
	// Form add button         bt_add_form
	@FindBy(how=How.ID,using ="bt_add_form")
	public WebElement bt_add_form;
	
	// Attachment window
	@FindBy(how=How.ID,using ="Attachment")
	public WebElement windowAttachment;
	
	// Attachment message window
	@FindBy(how=How.NAME,using ="Attachment Message:")
	public WebElement windowMessageAttachment;

	// Lexical relations list
	@FindBy(how=How.ID,using ="lb_dictionary_lexical_relations")
	public WebElement lst_Lexical_relations;
	
	// Modified On field
	@FindBy(how=How.ID,using ="tb_dictionary_modified_on")
	public WebElement modified_on;
	
	// Lexical Field list
	@FindBy(how=How.ID,using ="lb_dictionary_semantic_fields")
	public WebElement lst_Lexical_Fields;
	
	// Register list
	@FindBy(how=How.ID,using ="dd_dictionary_register")
	public WebElement lst_Regester;
	
	// Acronym list
	@FindBy(how=How.ID,using ="dd_dictionary_acronym")
	public WebElement lst_Acronym;
	
	// Lexical Semantic grops list
	@FindBy(how=How.ID,using ="lb_dictionary_semantic_groups")
	public WebElement lst_SemanticGroups;
	
	// Language list
	@FindBy(how=How.ID,using ="dd_dictionary_language")
	public WebElement lst_Language;
	
	// Part of speech list
	@FindBy(how=How.ID,using ="dd_dictionary_POS")
	public WebElement lst_POS;
	
	// Frequency list
	@FindBy(how=How.ID,using ="dd_dictionary_frequency")
	public WebElement lst_frequency;
	
	// Definition textbox
	@FindBy(how=How.ID,using ="tb_dictionary_definition")
	public WebElement lst_Definition;
	
	// Semantic relations box
	@FindBy(how=How.ID,using ="lb_dictionary_semantic_relations")
	public WebElement lst_SemanticRelations;
	
	// Lexical relations box
	@FindBy(how=How.ID,using ="lb_dictionary_lexical_relations")
	public WebElement lst_LexicalRelations;
	
    // Version field
	@FindBy(how=How.ID,using ="tb_dictionary_key_version")
	public WebElement Field_Version;
	
	// Style dropdown
	@FindBy(how=How.ID,using ="dd_dictionary_style")
	public WebElement Style_DD;
	
	// Definiteness dropdown
	@FindBy(how=How.ID,using ="dd_dictionary_definiteness")
	public WebElement Definiteness_DD;
	
	// Declinability dropdown
	@FindBy(how=How.ID,using ="dd_dictionary_declinability")
	public WebElement Declinability_DD;
	
	// Dependency dropdown
	@FindBy(how=How.ID,using ="dd_dictionary_dependency")
	public WebElement Dependency_DD;
	
	// Number dropdown
	@FindBy(how=How.ID,using ="dd_dictionary_number")
	public WebElement Number_DD;
	
	// Gender dropdown
	@FindBy(how=How.ID,using ="dd_dictionary_gender")
	public WebElement Gender_DD;
	
	// Etymology text field
	@FindBy(how=How.ID,using ="tb_dictionary_etymology")
	public WebElement Etymology_Text;
	
	// Forms list 
	@FindBy(how=How.ID,using ="lb_dictionary_words")
	public WebElement Forms_list;
	
	// Spellings list 
	@FindBy(how=How.ID,using ="lb_dictionary_spellings")
	public WebElement Spellings_list;
	
	// Empty value popup message 
	@FindBy(how=How.NAME,using ="Value empty")
	public WebElement Empty_Popup;
	
	// Empty popup text : Value is empty! Please enter value 
	@FindBy(how=How.ID,using ="65535")
	public WebElement Empty_Popup_text;
	
	// Left arrow in the footer bt_prev 
	@FindBy(how=How.ID,using ="bt_prev")
	public WebElement Previous_button;
	
	// Right arrow in the footer bt_next 
	@FindBy(how=How.ID,using ="bt_next")
	public WebElement bt_next;
	
	// Revert file  confirmation message
	@FindBy(how=How.NAME,using ="Revert File")
	public WebElement revertMessage;
	
	// Merge button
	@FindBy(how=How.ID,using ="bt_merge_files")
	public WebElement bt_merge_files;
		
	// "OK" in the confirmation message for the new button
	@FindBy(how=How.NAME,using ="OK")
	public WebElement btnOk;
				
	// W button
	@FindBy(how=How.ID,using ="bt_search_wiktionary")
	public WebElement bt_search_wiktionary;
	
	// D button
	@FindBy(how=How.ID,using ="bt_search_dictionarycom")
	public WebElement bt_search_dictionarycom;
	
	// "OK" in the confirmation message for the new button
	@FindBy(how=How.ID,using ="TitleBar")
	public WebElement window_TitleBar;
	
	// Too many retieved records window
	@FindBy(how=How.NAME,using ="Too many retrieved records")
	public WebElement window_TooManyRetrievedRecords;
	
	// "OK" in the confirmation message for the new button
	@FindBy(how=How.ID,using ="ll_dictionary_retrieved_records")
	public WebElement ll_dictionary_retrieved_records;

	// Smorfet main window
	@FindBy(how=How.ID,using ="SMI_Root_GUI")
	public WebElement SMI_Root_GUI;
	
	// Root
	@FindBy(how=How.ID,using ="")
	public WebElement root;

	// Attribute editor
	@FindBy(how=How.ID,using ="Attribute_Editor")
	public WebElement Attribute_Editor;
	
	// Add grammar attribute button
	@FindBy(how=How.ID,using ="bt_add_grammar_attribute")
	public WebElement bt_add_grammar_attribute;
	
	// Add attribute dropdown
	@FindBy(how=How.ID,using ="cb_attribute")
	public WebElement cb_attribute;
	
	// Attribute editor confirm button
	@FindBy(how=How.ID,using ="bt_confirm")
	public WebElement bt_confirm;
	
	// Attribute editor pane
	@FindBy(how=How.ID,using ="pn_grammar_attributes")
	public WebElement pn_grammar_attributes;
	
	// Attribute editor delete button
	@FindBy(how=How.ID,using ="bt_delete_grammar_attribute")
	public WebElement bt_delete_grammar_attribute;
	
	// POS dropdown in attribute editor  cb_4thLine
	@FindBy(how=How.ID,using ="cb_4thLine")
	// this element was changed public WebElement cb_attribute_4;
	public WebElement cb_attribute_4;
	
	// Form Types pane in attribute editor
	@FindBy(how=How.ID,using ="pn_attribute_details")
	public WebElement pn_attribute_details;
	
	// Attribute Editor none button
	@FindBy(how=How.ID,using ="bt_none")
	public WebElement bt_none;
	
	// Attribute Editor all button
	@FindBy(how=How.ID,using ="bt_all")
	public WebElement bt_all;
	
	// Start window abort button
	@FindBy(how=How.ID,using ="bt_abort")
	public WebElement bt_abort;

	// Start window
	@FindBy(how=How.ID,using ="Start_Window")
	public WebElement Start_Window;
	
	// Scroll Large
	@FindBy(how=How.ID,using ="LargeIncrement")
	public WebElement LargeIncrement;
	
	// Cancel button
	@FindBy(how=How.NAME,using ="Cancel")
	public WebElement Cancel;

	// No button
	@FindBy(how=How.NAME,using ="No")
	public WebElement No;

	// Dictionary Manager Message: 
	@FindBy(how=How.NAME,using ="Dictionary Manager Message:")
	public WebElement Dictionary_Manager_Message;
	
	// Dictionary Manager Message:
	@FindBy(how=How.NAME,using ="Close")
	public WebElement XClose;

	// Mass "change its" dropdown
	@FindBy(how=How.ID,using ="dd_utility_field")
	public WebElement massChangeItsDD;
		
	// Mass "to be" dropdown
	@FindBy(how=How.ID,using ="dd_utility_value")
	public WebElement massToBeDD;
		
	// Mass "Dictionary Attributing Message" dialog - No button
	@FindBy(how=How.NAME,using ="No")
	public WebElement massNoBtn;
		
	// Mass "Dictionary Attributing Message" dialog - Yes button
	@FindBy(how=How.NAME,using ="Yes")
	public WebElement massYesBtn;
	
	// Record status dropdown
	@FindBy(how=How.ID,using ="dd_dictionary_status")
	public WebElement cmpRecordStatus;
	
	// Load proximity rules - green button
	@FindBy(how=How.ID,using = "bt_update_rules")
	public WebElement bt_update_rules;
	
	// Proximity rules list s
	@FindBy(how=How.ID,using = "lb_dictionary_proximity_rules")
	public WebElement proximityRulesList;
	
	// Uppercase/lowercase button
	@FindBy(how=How.ID,using="bt_condition_uppercase")
	public WebElement bt_condition_uppercase;
	
	// ListBox list of spellings,transcriptions
//	@FindBy(how=How.ID,using="ListBox")
//	public WebElement ListBox;
	@FindBy(how=How.NAME,using ="??? [unknown]")
	public WebElement ListBox;
	
	// Replacement window
	@FindBy(how=How.ID,using="panel1")
	public WebElement replacementWindow;
	
	// Grammar attribute checkboxes list
	@FindBy(how=How.ID,using="clb_attribute_5")
	public WebElement grammarAttCheckboxList;
	
	// The second dropdown in the grammar attribute window
	@FindBy(how=How.ID,using="cb_2ndLine")
	public WebElement cb_2ndLine;
	
	// The cancel button in grammar attribute window bt_cancel_attributes
	@FindBy(how=How.ID,using="bt_cancel_attributes")
	public WebElement bt_cancel_attributes;
	
	// The ignore button
	@FindBy(how=How.NAME,using ="Ignore")
	public WebElement bt_ignore;
	
	// POS dropdown values
	public enum POS{
		accusative_word,adadjective,adverb,classifier,conjunction,demonstrative, 
		existential,foreign_word,infinitive,modal,negation,number,phrase_ends, 
		prefix,pronoun,quantifier,suffix,verb,action_name,adjective,article, 
		clause_opener,copula,diacritic,foreign_script,genetive_word,interjection, 
		name,noun,phrase_begins,possessive,preposition,punctuation,question,title,verb_particle
	}
	
	public enum posEnd{punctuation, quantifier, question, suffix, title, verb, verb_particle};
	
	
	   /**
		 * Get random text 
	    * @param length - enter the length of the text
		 */
	       public String RandomString(int length) {
	       	   StringBuilder sb = new StringBuilder();
	       	   Random random = new Random();
	       	   char[] chars = null;
	       	switch(Setting.Language)
	       	{
		        	case "EN":
			        chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();break;
		        	case "HE":
		        	chars = "אבגדהוזחטיכלמנסעפצקרשת".toCharArray();break;
	       	}
	       	
	       	for (int i = 0; i < length; i++) {
				    char c = chars[random.nextInt(chars.length)];
				    sb.append(c);
	       	}
	       	
	        String output = sb.toString();
	        return output;
	       
	       }
	
	
	
        /**
//	 * Get random text 
//         * @param length - enter the length of the text
//    	 */
//        public String RandomHEString(int length) {
//         char[] chars = "אבגדהוזחטיכלמנסעפצקרשת".toCharArray();
//        	   StringBuilder sb = new StringBuilder();
//        	   Random random = new Random();
//        	   char[] chars = null;
//        	switch(Setting.Language)
//        	{
//	        	case "EN":
//		        chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();break;
//	        	case "HE":
//	        	chars = "אבגדהוזחטיכלמנסעפצקרשת".toCharArray();break;
//        	}
//        	
//        	for (int i = 0; i < length; i++) {
//			    char c = chars[random.nextInt(chars.length)];
//			    sb.append(c);
//        	}
//        	
//         String output = sb.toString();
//         return output;
//        
//        }
	
//    /**
//	 * Get random text 
//     * @param length - enter the length of the text
//	 */
//        public String RandomString(int length) {
//         char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
//         StringBuilder sb = new StringBuilder();
//         Random random = new Random();
//         for (int i = 0; i < length; i++) {
//             char c = chars[random.nextInt(chars.length)];
//             sb.append(c);
//         }
//         String output = sb.toString();
//         return output;
//        }
//        
//        // # Type in Key Form
//        public String keyFormTypeRandom() {
//			String random_input = RandomString(5);
//			this.txtKeyForm.sendKeys(random_input);
//			return random_input;
//		}

       /**
        * Get values from dropdowns and lists
        * @param lstBox
        * @return
        * @throws InterruptedException
        */
        public List<String> getValuesFromApp(WebElement lstBox) throws InterruptedException
    	{
        	//lstBox.click();
        	Thread.sleep(1000);
    		//get all children
    		//List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("\"./*[contains(@ControlType, 'ControlType.ListItem')]\""));
//        	try {
//        		List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("//*[contains(@ControlType, 'ControlType.ListItem')]"));
//			} catch (Exception e) {
//				System.out.println("itemlist e: " + e);
//			}
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
    		//System.out.println(itemlist.size());
    		//System.out.println(itemlist.get(0).getAttribute('WiniumDriver'));
        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			//System.out.println(e.getAttribute("Name"));
    			itemNames.add(e.getAttribute("Name"));
    		    //itemNames.add(e.getAttribute(Name));
    		}
    		
    		// Delete unknown value, it is not usable
    		if (itemNames.contains("unknown"))
    			itemNames.remove("unknown");
    		
    		return itemNames;
    	}
        
        // find number of elements in dropdown/list
        public List<String> getValuesFromApp(WebElement lstBox, int amount) throws InterruptedException
    	{
    		//get all children
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')][position()<="+amount+"]"));
        	ArrayList<String> itemNames= new ArrayList<String>();

    		for(WebElement e : itemlist)
    			itemNames.add(e.getAttribute("Name"));

    		return itemNames;
    	}
        
        /**
         * Get chosen value in a dropdown
         * @param lstBox
         * @return
         * @throws InterruptedException
         */
        public List<String> getChosenValueInDropdown(WebElement lstBox) throws InterruptedException
    	{
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			boolean isSelected = e.isSelected();
    			if (isSelected)
    				itemNames.add(e.getAttribute("Name"));
    		}
    		return itemNames;
    	}
        
    	        
//        /**
//         * Choose value from dropdown that is LISTITEM
//         * @param lstBox
//         * @param ItemName
//         * @throws InterruptedException
//         */
//        public void chooseValueInDropdown(WebElement lstBox, String ItemName) throws InterruptedException
//    	{
//        	// Open the listbox
     //  	System.out.println("---------------------------");
//       	 for (WebElement webElement : itemlist) {
//       		 System.out.println(webElement.getAttribute("ControlType"));
//       	 }
       	 
       //	 System.out.println();
//        	lstBox.click();
//        	Thread.sleep(1000);
//        	
////        	// each scroll and how much displayed items
////        	Map<String, Integer> scrollVisible = new HashMap<String, Integer>();
////        	scrollVisible.put(30 );
////        	scrollVisible.put();
//        	
//        	// First need to reset the scroll to the start
//        	// maybe complete this later if needed
//        	
//        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
//
//        	// Part of Speach case, use the scroll if needed
//        	if (lstBox == cmpPOS) {
//        		String checkPOS = null;
//        		if (ItemName.contains(" "))
//        			checkPOS = ItemName.replace(" ", "_");
//        		
//        		Boolean atEndOfList = false;
//        		  for (posEnd pos : posEnd.values()) {
//        		        if (pos.name().equals(checkPOS)) {
//        		        	atEndOfList = true;
//        		        	break;
//        		        }
//        		    }
//        	// Move the scroll
//        	if (atEndOfList)
//        		CurrentPage.As(DictionaryTab.class).LargeIncrement.click();
//        	}
//        	
//        	// Semantic field
//        	if (lstBox == lb_dictionary_semantic_fields || lstBox == lst_SemanticGroups) {
//        	// get list of all the available values
//      		List<String> allValues = new ArrayList<String>();
//      		int count = 0;
//      		for(WebElement e : itemlist) {
//      			allValues.add(e.getAttribute("Name"));
//      			if(e.getAttribute("ClickablePoint") == null)
//      				count++;
//      		}
//      		
//      		if (count != 0) { 
//      		// Check the distance of the needed value
//      		int place = allValues.indexOf(ItemName);
//      		if (place > count)
//      			place += place / count;
//      		int moves = place / count;
//      		int reset = allValues.size()/count;
//      		if (allValues.size() > count) reset += 1; 
//        	
//      		if (reset > 0) {
//      		// Look for scrolls
//      		List<WebElement> scroll= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ScrollBar')]"));
//        	// Look for scroll button
//      		List<WebElement> scrollButtons= scroll.get(0).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
//
//      		for(WebElement e : scrollButtons)
//    		{
//      			// Reset
//      			if (e.getAttribute("Name").equals("Back by large amount"))
//				{
//    				for (int i = 0; i < reset; i++) 
//    					e.click();
//				}
//      			// Scorll to place
//    			if (e.getAttribute("Name").equals("Forward by large amount"))
//				{
//    				for (int i = 0; i < moves; i++) 
//    					e.click();
//				}
//    		}
//        	}
//        	}
//        	} // End of count
//        	
//        	// Origin dropdown
//        	if (lstBox == cmpOrigin) {
//        		// Look for scrolls
//          		List<WebElement> scroll= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ScrollBar')]"));
//            	// Look for scroll button
//          		List<WebElement> scrollButtons= scroll.get(0).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
//
//          		System.out.println();
//          		for(WebElement e : scrollButtons)
//        		{
////          			System.out.println("ItemName.equals(\"unknown\"): " + ItemName.equals("unknown") + 
////          			"&& e.getAttribute(\"Name\")" + e.getAttribute("Name") );
////          			System.out.println("e.getAttribute(\"Name\")" + e.getAttribute("Name") );
//          			// Scroll up one
//          			if (ItemName.equals("unknown") && e.getAttribute("Name").equals("Back by small amount")) {
//        					e.click();
//        					break;
//          			}else {
//          			// Scroll down one
//        			if (!ItemName.equals("unknown") && e.getAttribute("Name").equals("Forward by small amount")) {
//        					e.click();
//        					break;
//          			}
//          			}
//        		}
//        		
//            }
//        	
//        	
//        	
//      		// finally, for all
//    		for(WebElement e : itemlist)
//    		{
//    			if (e.getAttribute("Name").equals(ItemName))
//				{
//				  e.click();
//				  return;
//				}
//    		}
//    	}
        
        
        /**
         * choose value from dropdown that is TEXT
         * @param lstBox
         * @param ItemName
         * @throws InterruptedException
         */
        public void chooseValueInDropdownText(WebElement lstBox, String ItemName) throws InterruptedException
    	{
//        	List<WebElement> dropdownbutton1= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
//			dropdownbutton1.get(0).click();
//        	lstBox.click();
//        	Thread.sleep(1000);
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
    		System.out.println(itemlist.size());
    		
//        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			//System.out.println(e.getAttribute("Name"));
    			e.getAttribute("Name");

    			if (e.getAttribute("Name").equals(ItemName))
				{
				  e.click();
				}
    		}
    	}
        
        /**
         * edit value from grammar attribute text list
         * @param lstBox
         * @param ItemName
         * @throws InterruptedException
         */
        public void editValueInGrammarAttributes(WebElement lstBox, String ItemName) throws InterruptedException
    	{
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
    		
//        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			if (e.getAttribute("Name").equals(ItemName))
				{
//					  e.click();
					Actions builder = new Actions(DriverContext._Driver);
			  		builder.moveToElement(e).doubleClick().perform();
				}
    		}
    	}
        
//            window = driver.find_element_by_id('lb_dictionary_retrieved_records')
//
//            records_list_box = driver.find_element_by_id('lb_dictionary_retrieved_records')
//            all_records_in_listbox = records_list_box.find_elements_by_xpath(
//                "./*[contains(@ControlType, 'ControlType.ListItem')]")
//            records = []
//
//            for ii in all_records_in_listbox:
//                records.append(ii.get_attribute('Name'))

           
           /**
            * Choose POS value
            * @return
            */
           public String POS_Select() {
			return null;
//          #  POS dictionary values
//          pos = {"Action name": '{e}', "accusative word": '{w}', "adadjective": '{d}', "adjective": '{a}',
//                 "adverb": '{r}',
//                 "article": '{t}', "classifier": '{l}', "Clause opener": '{s}', "Conjunction": '{c}', "copula": '{u}',
//                 "demonstrative": '{z}', "diacritic": '{D}', "existential": '{C}', "foreign script": '{L}',
//                 "foreign word": '{S}',
//                 "genetive word": '{F}', "infinitive": '{i}', "  nterjection": '{j}', "modal": '{m}', "name": '{x}',
//                 "negation": '{g}', "noun": '{n}', "number": '{N}', "phrase begins": '{B}', "phrase ends": '{E}',
//                 "possessive": '{V}', "prefix": '{f}', "preposition": '{p}', "pronoun": '{o}'}
//
//          # pos = {"Action name": '{e}', "accusative word": '{w}', "adadjective": '{d}', "adjective": '{a}', "adverb": '{r}',
//          #        "article": '{t}', "classifier": '{l}', "Clause opener": '{s}', "Conjunction": '{c}', "copula": '{u}',
//          #        "demonstrative": '{z}', "diacritic": '{D}', "existential": '{C}', "foreign script": '{L}', "foreign word":'{S}',
//          #        "genetive word": '{F}', "infinitive": '{i}', "  nterjection": '{j}', "modal": '{m}', "name": '{x}',
//          #        "negation": '{g}', "noun": '{n}', "number": '{N}', "phrase begins": '{B}', "phrase ends": '{E}',
//          #        "possessive": '{V}', "prefix": '{f}', "preposition": '{p}', "pronoun": '{o}', "punctuation": '{P}',
//          #        "quantifier": '{q}', "question": '{Q}', "suffix": '{U}', "title": '{l}', "verb": '{v}', "verb particle": '{A}'}
//
//          key, value = random.choice(list(pos.items()))
//          print ('key: {0}, value: {1}'.format(key, value))
//          driver.find_element_by_id('dd_dictionary_POS').click()
//          driver.find_element_by_name(key).click()
//          return value
        	
        }
        
       /**
        * Get clean records names from the dictionary.txt files
        * @return
        * @throws FileNotFoundException
        */
        public List<String> getRecordsFromDictionary() throws FileNotFoundException {
		   			
		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
		   		List<String> lines = new ArrayList<>();
		   		while(in.hasNextLine()) {
		   		    String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");
		   		    
		   		    String[] removeLeft = splitted[0].split("}");
		   		    String[] removeRight = removeLeft[1].split("~");
		   		    lines.add(removeRight[0]);
		   		}
		   		
		   		in.close();
				return lines;
        }
        
        /**
         * Get the frames for specified record name
         * @param record
         * @return
         * @throws FileNotFoundException
         */
        public String getFramesOfRecordFromDictionary(String record) throws FileNotFoundException {
   			
	   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
//	   		List<String> lines = new ArrayList<>();
	   		while(in.hasNextLine()) {
	   		    String line = in.nextLine().trim();
	   		    String[] splitted = line.split("	");

	   		    if (record.equals(splitted[0])) {
	   		    	in.close();
	   		    	return splitted[10].replace("_", " ");
	   		    }
	   		}
	   		
	   		in.close();
			return "";
    }

        /**
         * Get records that have semantic relations values
         * @param record
         * @return
         * @throws FileNotFoundException
         */
        public List<String> getRecordsThatHaveSemanticRelationsFromDictionary() throws FileNotFoundException {
   			
		    	Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
		   		List<String> lines = new ArrayList<>();
		   		while(in.hasNextLine()) {
		   			
		   			String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");
		   			
		   	    // Check if the record have a semantic relation value then add it to the list
//		   		if (extractRecordSemanticRelation(splitted) != "")    
//		   			lines.add(extractRecordSemanticRelation(splitted));

		   		 if (!splitted[13].isEmpty()) lines.add(splitted[0]);
		   		}

		   		in.close();
				return lines;
    }
        
        public String extractRecordSemanticRelation(String[] splitted) {
        	try {
   		    	if (!splitted[13].isEmpty()) { 
		   		    String[] removeLeft = splitted[0].split("}");
		   		    String[] removeRight = removeLeft[1].split("~");
		   		    return(removeRight[0]);
				    }
			} catch (Exception e) {
				// no index 13
			}
        	return "";
		}
        
        /**
         * Get records that have lexical relations values
         * @param record
         * @return
         * @throws FileNotFoundException
         */
        public List<String> getRecordsThatHaveLexicalRelationsFromDictionary() throws FileNotFoundException {
   			
		    	Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
		   		List<String> lines = new ArrayList<>();
		   		while(in.hasNextLine()) {
		   			
		   			String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");

		   	    // Check if the record have a semantic relation value then add it to the list
//		   		if (extractRecordSemanticRelation(splitted) != "")    
//		   			lines.add(extractRecordSemanticRelation(splitted));
		   		
		   		 if (!splitted[14].isEmpty()) lines.add(splitted[0]);
		   		}

		   		in.close();
				return lines;
    }
        
        public String getOnlyRecordName(String fullRecord) {
        	String[] removeLeft = fullRecord.split("}");
    		String[] removeRight = removeLeft[1].split("~");
    		return removeRight[0];	
		}
        
        public List<String> mibos(WebElement lstBox) throws InterruptedException
    	{
        	//lstBox.click();
        	Thread.sleep(1000);
    		//get all children
    		//List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("\"./*[contains(@ControlType, 'ControlType.ListItem')]\""));
//        	try {
//        		List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("//*[contains(@ControlType, 'ControlType.ListItem')]"));
//			} catch (Exception e) {
//				System.out.println("itemlist e: " + e);
//			}
        	//@LocalizedControlType='edit'
        	//List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
        	
        	//List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Edit')]"));
        	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
        	
        	
        	//List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[[contains(@ControlType,'ControlType.Window')]"));
        	
    		//System.out.println(itemlist.size());
    		//System.out.println(itemlist.get(0).getAttribute('WiniumDriver'));
        	ArrayList<String> itemNames= new ArrayList<String>();
    		for(WebElement e : itemlist)
    		{
    			//System.out.println(e.getAttribute("Name"));
    			itemNames.add(e.getAttribute("Name"));
    		    //itemNames.add(e.getAttribute(Name));
    		}
    		return itemNames;
    	}
        
        // Clean the text within DucomentControl like Definition and Etymology
        public void cleanDucomentControl(WebElement ducomentBox) {
        	ducomentBox.click();
    		String notEmptyYet = "text";
    		while (!notEmptyYet.isEmpty()) {
    			notEmptyYet = ducomentBox.getText();
    		//System.out.println(notEmptyYet);
    			ducomentBox.sendKeys(Keys.CLEAR);
    		}
		}

        /**
         * Get clean records names from the dictionary.txt files
         * @return
         * @throws FileNotFoundException
         */
         public List<String> availableSemanticGroups() throws FileNotFoundException {
 		   			
        	 int i = 0;
 		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
 		   		List<String> lines = new ArrayList<>();
 		   		while(in.hasNextLine()) {
 		   			System.out.println(i);
 		   		    String line = in.nextLine().trim();
 		   		    String[] splitted = line.split("	");
 		   		
 		   		    String chosen = "";
 		   		    try {
 		   		    	try {
 		   		    	chosen = splitted[11];
						} catch (Exception e) {
							e.printStackTrace();
						}
			   		    
			   		    if (chosen.isEmpty()) continue;
			   		    String[] removeLeft = chosen.split("_");
			   		    boolean alreadyIN = lines.contains(removeLeft[1]);
			   		    if (!alreadyIN)
			   		    	lines.add(removeLeft[1]);
					} catch (Exception e) {
						System.out.println("issue: " + e);
					}
 		   		    i++;
 		   		}

 		   		in.close();
 				return lines;
         }
 
        // Get the Semantic groups values from the DB
 		public HashMap<String, String> semanticGroupsValues(Connection con) throws SQLException {
 		
 			// Query to get all the Semantic groups values
 	 		String query = "select semantic_group from enum_semantic_group";
 	 		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

 	 		// Get POS and their codes
 	 		HashMap<String, String> POSmap = POSCodes(con);

 	 		// Save them in a map
 	 		HashMap<String, String> SemanticGroupMap = new HashMap<String, String>(); 
 	 	    while (resultSet.next()) {
 	 	    	String value = resultSet.getString(1);
 	 	    	if (value.equals("unknown")) continue;
 	 	    	String pos_letter = "" + value.charAt(0);

 	 	    	// Get the POS Name from code prefix
 	 	      Iterator it = POSmap.entrySet().iterator();
 	 	      String POSName = "";
 	 	    while (it.hasNext()) {
 	 	        Map.Entry pair = (Map.Entry)it.next();
 	 	        String pairGetKey = pair.getKey().toString(); 
 	 	        if (pairGetKey.equals(pos_letter)) {
 	 	        	POSName = pair.getValue().toString();
 	 	        	break;
 	 	        }
 	 	        //it.remove(); // avoids a ConcurrentModificationException
 	 	    }
 	 	    	String posClean = value.substring(2).replace("_", " ");
 	 	    	if(posClean.equals("alfabeth")) 
 	 	    		System.out.println("Hi");
 	 	    	SemanticGroupMap.put(posClean, POSName);
 	 	    }
 	 	    
 	 	    return SemanticGroupMap;
		}
        
        // Get POS and their codes
		public HashMap<String, String> POSCodes(Connection con) throws SQLException {
			
			// Query to get all the POS codes
	 		String query = "select pos, pos_code from enum_pos";
	 		ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);

	 		// Save them in a map
	 		HashMap<String, String> POSmap = new HashMap<String, String>();
	 	    while (resultSet.next()) {
	 	    	POSmap.put(resultSet.getString(2), resultSet.getString(1));
	 	    }
	 	 
	 	    return POSmap;
		}
		
        /**
         * Get definition for a specific record
         * @param record
         * @return
         * @throws FileNotFoundException
         */
        public String getDefinitionForRecord(String record) throws FileNotFoundException {
   				
//        		record = this.getOnlyRecordName(record);
		    	Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
		   		String definition = "";
		   		while(in.hasNextLine()) {
		   			
		   			String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");
		   		
			   		 if (splitted[0].equals(record)) {
			   			 definition = splitted[8];
			   			 break;
			   		 }
		   		 }

		   		in.close();
				return definition;
    }
        
        /**
         * Get etymology for a specific record
         * @param record
         * @return
         * @throws FileNotFoundException
         */
        public String getEtymologyForRecord(String record) throws FileNotFoundException {
   				
//        		record = this.getOnlyRecordName(record);
		    	Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
		   		String definition = "";
		   		while(in.hasNextLine()) {
		   			
		   			String line = in.nextLine().trim();
		   		    String[] splitted = line.split("	");
		   		
			   		 if (splitted[0].equals(record)) {
			   			 definition = splitted[9];
			   			 break;
			   		 }
		   		 }

		   		in.close();
				return definition;
    }
        /**\
         * Convert POS name to POS code
         * @param POSInput
         * @return
         */
        public String POSConverter(String POSInput) {
        	POSInput = POSInput.replace(" ", "_");
        	
        	// Put manually the values here, later fetch from DB
        	Map<String,String> POS = new HashMap<String,String>();
        	POS.put("accusative_word", "W");POS.put("clause_predicate", "e"); 
//        	POS.put("action_name", "e"); 
        	POS.put("adadjective", "d"); POS.put("adjective", "a"); 
        	POS.put("adverb", "r"); POS.put("article", "t"); 
        	POS.put("classifier", "I"); POS.put("clause_opener", "s"); 
        	POS.put("conjunction", "c"); POS.put("copula", "u"); 
        	POS.put("demonstrative", "z"); POS.put("diacritic", "D"); 
        	POS.put("existential", "C"); POS.put("foreign_script", "L"); 
        	POS.put("foreign_word", "S"); POS.put("genetive_word", "F"); 
        	POS.put("infinitive", "i"); POS.put("interjection", "j"); 
        	POS.put("modal", "m"); POS.put("name", "x"); 
        	POS.put("negation", "g"); POS.put("noun", "n"); 
        	POS.put("number", "N"); POS.put("phrase_begins", "B"); 
        	POS.put("phrase_ends", "E"); POS.put("possessive", "V"); 
        	POS.put("prefix", "f"); POS.put("preposition", "p"); 
        	POS.put("pronoun", "o"); POS.put("punctuation", "P"); 
        	POS.put("quantifier", "q"); POS.put("question", "Q"); 
        	POS.put("suffix", "U"); POS.put("title", "l"); 
        	POS.put("verb", "v"); POS.put("verb_particle", "A");

        	return POS.get(POSInput);
        }
        
        /**\
         * Convert Spelling key name to spelling code
         * @param POSInput
         * @return
         */
        public String SpellingKeyConverter(String SpellingInput) {
        	SpellingInput = SpellingInput.replace(" ", "_");
        	
        	// Put manually the values here, later fetch from DB
        	Map<String,String> spellings = new HashMap<String,String>();
        	spellings.put("unknown", "???");  spellings.put("compound_1", "CM1"); 
        	spellings.put("symbol", "SMB"); spellings.put("compound_2", "CM2"); 
        	spellings.put("symbol_1", "SM1"); spellings.put("compound_3", "CM3"); 
        	spellings.put("symbol_2", "SM2"); spellings.put("acronym", "ACR"); 
        	spellings.put("symbol_3", "SM3"); spellings.put("acronym_1", "AC1"); 
        	spellings.put("number", "NUM"); spellings.put("acronym_2", "AC2"); 
        	spellings.put("number_1", "NU1"); spellings.put("acronym_3", "AC3"); 
        	spellings.put("number_2", "NU2"); spellings.put("initialism", "INI"); 
        	spellings.put("number_3", "NU3"); spellings.put("initialism_1", "IN1"); 
        	spellings.put("roman_number", "ROM"); spellings.put("initialism_2", "IN2"); 
        	spellings.put("roman_number_1", "RO1"); spellings.put("initialism_3", "IN3"); 
        	spellings.put("roman_number_2", "RO2"); spellings.put("standard", "STD"); 
        	spellings.put("roman_number_3", "RO3"); spellings.put("standard_1", "ST1"); 
        	spellings.put("abbreviation", "ABR"); spellings.put("standard_2", "ST2"); 
        	spellings.put("abbreviation_1", "AB1"); spellings.put("standard_3", "ST3"); 
        	spellings.put("abbreviation_2", "AB2"); spellings.put("standard_4", "ST4"); 
        	spellings.put("abbreviation_3", "AB3"); spellings.put("standard_5", "ST5"); 
        	spellings.put("compound", "CMP"); spellings.put("standard_6", "ST6");
         	spellings.put("standard_7", "ST7"); spellings.put("standard_8", "ST8");
         	spellings.put("standard_9", "ST9"); spellings.put("archaic", "ARC");
         	spellings.put("archaic_1", "AR1"); spellings.put("archaic_2", "AR2");
         	spellings.put("archaic_3", "AR3"); spellings.put("informal", "INF");
         	spellings.put("informal_1", "IF1"); spellings.put("informal_2", "IF2");
         	spellings.put("informal_3", "IF3"); spellings.put("emphatic", "EMP");         	
         	spellings.put("emphatic_1", "EM1"); spellings.put("emphatic_2", "EM2");
         	spellings.put("emphatic_3", "EM3"); spellings.put("American", "AME");
         	spellings.put("American_1", "AM1"); spellings.put("American_2", "AM2");
          	spellings.put("American_3", "AM3"); spellings.put("British", "BRI");
          	spellings.put("British_1", "BR1"); spellings.put("British_2", "BR2");
          	spellings.put("Academia", "ACA"); spellings.put("no_accents", "NAC");
          	
        	return spellings.get(SpellingInput);
        }
        
        
        /**\
         * Convert Spelling code to spelling name
         * @param POSInput
         * @return
         */
        public String SpellingCodeConverter(String SpellingInput) {
        	SpellingInput = SpellingInput.replace(" ", "_");
        	
        	// Put manually the values here, later fetch from DB
        	Map<String,String> spellings = new HashMap<String,String>();
        	spellings.put("unknown", "???");  spellings.put("compound_1", "CM1"); 
        	spellings.put("symbol", "SMB"); spellings.put("compound_2", "CM2"); 
        	spellings.put("symbol_1", "SM1"); spellings.put("compound_3", "CM3"); 
        	spellings.put("symbol_2", "SM2"); spellings.put("acronym", "ACR"); 
        	spellings.put("symbol_3", "SM3"); spellings.put("acronym_1", "AC1"); 
        	spellings.put("number", "NUM"); spellings.put("acronym_2", "AC2"); 
        	spellings.put("number_1", "NU1"); spellings.put("acronym_3", "AC3"); 
        	spellings.put("number_2", "NU2"); spellings.put("initialism", "INI"); 
        	spellings.put("number_3", "NU3"); spellings.put("initialism_1", "IN1"); 
        	spellings.put("roman_number", "ROM"); spellings.put("initialism_2", "IN2"); 
        	spellings.put("roman_number_1", "RO1"); spellings.put("initialism_3", "IN3"); 
        	spellings.put("roman_number_2", "RO2"); spellings.put("standard", "STD"); 
        	spellings.put("roman_number_3", "RO3"); spellings.put("standard_1", "ST1"); 
        	spellings.put("abbreviation", "ABR"); spellings.put("standard_2", "ST2"); 
        	spellings.put("abbreviation_1", "AB1"); spellings.put("standard_3", "ST3"); 
        	spellings.put("abbreviation_2", "AB2"); spellings.put("standard_4", "ST4"); 
        	spellings.put("abbreviation_3", "AB3"); spellings.put("standard_5", "ST5"); 
        	spellings.put("compound", "CMP"); spellings.put("standard_6", "ST6");
         	spellings.put("standard_7", "ST7"); spellings.put("standard_8", "ST8");
         	spellings.put("standard_9", "ST9"); spellings.put("archaic", "ARC");
         	spellings.put("archaic_1", "AR1"); spellings.put("archaic_2", "AR2");
         	spellings.put("archaic_3", "AR3"); spellings.put("informal", "INF");
         	spellings.put("informal_1", "IF1"); spellings.put("informal_2", "IF2");
         	spellings.put("informal_3", "IF3"); spellings.put("emphatic", "EMP");         	
         	spellings.put("emphatic_1", "EM1"); spellings.put("emphatic_2", "EM2");
         	spellings.put("emphatic_3", "EM3"); spellings.put("American", "AME");
         	spellings.put("American_1", "AM1"); spellings.put("American_2", "AM2");
          	spellings.put("American_3", "AM3"); spellings.put("British", "BRI");
          	spellings.put("British_1", "BR1"); spellings.put("British_2", "BR2");
          	spellings.put("Academia", "ACA"); spellings.put("no_accents", "NAC");
          		
          	List<String> keys = new ArrayList<String>(spellings.keySet());	
        	for(int i=0;i<keys.size();i++)
        		if(spellings.get(keys.get(i)).equals(SpellingInput))
        			return keys.get(i);
        	
        	//if not found
        	return null;
        }
        
        
     
        
        /**
         * Fetch the text and title of popup windows within Smorfet
         * @param test
         * @param logger
         * @param titleToCheck
         * @param textToCheck
         * @return
         */
        public boolean popupWindowMessage(ExtentTest test, Logger logger, String titleToCheck, String textToCheck)
    	{
        	List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
        	List<WebElement> text = null;
        	List<WebElement> title = null;
        	
        	boolean titleIn = false;
    	    boolean textIn = false;
    	    
        	// Check the main app for popup windows
    	    try {
				
    	    	for(WebElement e : windows)
        		{ 
        			text = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	
                	title = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TitleBar')]"));
            	}
        		
        		// Fetch the found title
        		for(WebElement e : title)
        		{
        			LogUltility.log(test, logger, "Title:" + e.getAttribute("Name"));
        			if (e.getAttribute("Name").contains(titleToCheck))
        				titleIn = true;
            	}
        		
        		// Fetch the found title    		
        		for(WebElement e : text)
        		{ 
        			LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
        			if (e.getAttribute("Name").contains(textToCheck))
        				textIn = true;
            	}
        		
			} catch (Exception e) {
				// If no popup were found
				return false;
			}
    	      		
    		LogUltility.log(test, logger, "TitleIn: " + titleIn + " - TextIn: " + textIn);
    		
    	    if (titleIn && textIn)
    	    	return true;
    	    else return false;
    	} // close function
        
        /**
         * Fetch the text and title of popup windows outside Smorfet
         * for example the attribute editor
         * @param test
         * @param logger
         * @param titleToCheck
         * @param textToCheck
         * @return
         */
        public boolean popupWindowMessageInAttributeEditor(ExtentTest test, Logger logger, String titleToCheck, String textToCheck)
    	{
//        	List<WebElement> windows = root.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
        	List<WebElement> text = null;
        	List<WebElement> title = null;
        	
        	boolean titleIn = false;
    	    boolean textIn = false;
    	    
//        	// Check the main app for popup windows
//    		for(WebElement e : windows)
//    		{ 
    			text = Attribute_Editor.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	
            	title = Attribute_Editor.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TitleBar')]"));
//        	}

    		// Fetch the found title
    		for(WebElement e : title)
    		{ 
    			LogUltility.log(test, logger, "Title:" + e.getAttribute("Name"));
    			if (e.getAttribute("Name").contains(titleToCheck))
    				titleIn = true;
        	}
    		
    		// Fetch the found title    		
    		for(WebElement e : text)
    		{ 
    			LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
    			if (e.getAttribute("Name").contains(textToCheck))
    				textIn = true;
        	}
    		
    		LogUltility.log(test, logger, "TitleIn: " + titleIn + " - TextIn: " + textIn);
    		
    	    if (titleIn && textIn)
    	    	return true;
    	    else return false;
    	} // close function
        
        /**
         * Check that no Popup window is open
         * @return
         */
        public boolean checkNoPopupWindow()
    	{
        	List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
//    	    System.out.println("mibo");
//    	    for (WebElement e : windows) {
//				e.getAttribute("name");
//			}
        	if (windows.size() == 0)
        		return true;
        	else return false;
    	}
        
        /**
         * Check for Smorfet intro windows and close them
         * @return
         */
        public boolean closeSmorfetIntroWindows()
    	{
        	List<WebElement> windows = root.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
    	    System.out.println("windows number: " + windows.size());
    	    
//    	    boolean continueChecking = true;
//    	    int i = 0;
//    	    while (continueChecking) {
//    	    try {
//    	    	i++;
//    	    	System.out.println(i);
//    	    	Start_Window.click();
//    	    	bt_abort.click();
//			} catch (Exception e) {
//				e.printStackTrace();
//				continueChecking = false;
//			}
//    	    }
    	    System.out.println("mibo");
    	    	for (WebElement e : windows) {
    	    		try {
        	    	System.out.println(e.getAttribute("AutomationId"));
        	    	if (e.getAttribute("AutomationId").equals("Start_Window")) {
        	    		e.click();
        	    		bt_abort.click();
        	    	}
    				} catch (Exception f) {
    					f.printStackTrace();
    				}
        	    	}
    			
    	    
        	if (windows.size() == 0)
        		return true;
        	else return false;
    	}
        
        /**
         * Get transcriptions values
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public List<String> getTranscriptionsValues() throws InterruptedException, AWTException
    	{
        	// Get the transcriptions values
        	List<WebElement> transcriptionsList= lstTranscriptions.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
        	ArrayList<String> itemNames= new ArrayList<String>();
    		
        	for(WebElement f : transcriptionsList) {
        		itemNames.add(f.getAttribute("Name"));
        		System.out.println(f.getAttribute("Name"));
        	}
    		
    		return itemNames;
    	}
        
        /**
         * Add new Transcription value
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public void addTranscriptions(String key, String value) throws InterruptedException, AWTException
    	{
        	
    		// Click the add/blue button for transciptions
    		CurrentPage.As(DictionaryTab.class).bt_add_transcription.click();
    		
    		// Entry a value
    		// Get the transcriptions values  
        	List<WebElement> transcriptionsList= lstTranscriptions.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
//    		List<WebElement> transcriptionsList= lstTranscriptions.findElements(By.xpath(".//*[contains(@ControlType.ComboBox, 'ControlType.ListItem')]"));
        	for(WebElement f : transcriptionsList)
    		{
    			if (f.getAttribute("Name").equals("???~")) 
    			{
    				try {
    					f.click();
    					f.sendKeys(value);
					} catch (Exception e) {
						System.out.println("not clickable");
					}
    			}
    		} // end of for
        	
    		// Choose a key
        	chooseTransSpellKey(key);
    		
    		// Click the add/blue button for transciptions approve
    		CurrentPage.As(DictionaryTab.class).bt_add_transcription.click();
    		
    	} // close function
        
        /**
         * Edit Transcription value
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public void editTranscriptions(ExtentTest test, Logger logger) throws InterruptedException, AWTException
    	{
        	LogUltility.log(test, logger, "Start of edit transcription function");
        	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
        	
        	// Get all transcriptions
        	List<String> listedTranscriptions =  getTranscriptionsValues();
        	LogUltility.log(test, logger, "Current transcriptions list: " + listedTranscriptions);
        	
        	// Choose random transcription
        	Random randomizer = new Random();
    		String randomTranscription = listedTranscriptions.get(randomizer.nextInt(listedTranscriptions.size()));
    		CommonFunctions.chooseValueInDropdown(DictionaryTab.lstTranscriptions, randomTranscription);
    		LogUltility.log(test, logger, "Random Chosen transcription to edit: " + randomTranscription);
    		
        	String keyTochange = randomTranscription.split("~")[0];
//        	String valueTochange = randomTranscription.split("~")[1];
        	
        	String randomNewValue = RandomString(5);
        	
        	// Choose the transcription to be modified
        	Actions shiftClick = new Actions(DriverContext._Driver);
    		// Get the transcriptions values
        	List<WebElement> transcriptionsList= lstTranscriptions.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
        	for(WebElement f : transcriptionsList)
    		{
    			if (f.getAttribute("Name").equals(randomTranscription)) 
    			{
    				try {
//    					f.click();
    					shiftClick.keyDown(Keys.SHIFT).click(f).keyUp(Keys.SHIFT).perform();
    		    		LogUltility.log(test, logger, "Enter random new value: " + randomNewValue);
    					f.sendKeys(randomNewValue);
					} catch (Exception e) {
						System.out.println("not clickable");
					}
    			}
    		} // end of for
        	
        	//-------------------------------
        	String randomKey = "";
        	
    		// Choose the key to be modified
        	// Get all the available comboboxes in the app
        	List<WebElement> comboboxesList= SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
        	
        	// Click on the Transcriptions key dropdown
        	outerLoop : for(WebElement f : comboboxesList)
    		{ // buttons
        		String cleanKey = f.getAttribute("Name").substring(0, 3);
        		
    			if (cleanKey.equals(keyTochange)) 
    			{
    				try {
    					f.click();
//    					Thread.sleep(2000);
    					
    					List<String> keysList = DictionaryTab.getValuesFromApp(f);
    					randomKey = keysList.get(randomizer.nextInt(keysList.size()));
    					
    					// Choose random new key
    					LogUltility.log(test, logger, "Enter random new key: " + randomKey);
    					
    		    		CommonFunctions.chooseValueInDropdown(f, randomKey);
    		    		break outerLoop;
    		    		
					} catch (Exception f1) {
						System.out.println("not clickable");
					}
    			}
    		} // end of outside for	
        	
    		// Click the add/blue button for transciptions approve
        	DictionaryTab.bt_add_transcription.click();
    		
    		//---------------------------------------
    		// Check edit is successfully changed
    		// remove the modified values from the first transcriptions list
    		listedTranscriptions.remove(randomTranscription);
    		// add the new modified values to the list
    		String newValue = randomKey.substring(0, 3) + "~" + randomNewValue;
    		listedTranscriptions.add(newValue);
    		
    		// Get the new transcription list
    		List<String> newListedTranscriptions =  getTranscriptionsValues();
    		
    		// Sort lists
    		Collections.sort(listedTranscriptions);
    		Collections.sort(newListedTranscriptions);
    		
    		LogUltility.log(test, logger, "Old list: " + listedTranscriptions);
    		LogUltility.log(test, logger, "New list: " + newListedTranscriptions);
    		
    		// Compare the two lists are equal
    		boolean equal = newListedTranscriptions.equals(listedTranscriptions);
    		LogUltility.log(test, logger, "Are two lists equal: " + equal);
    		assertTrue(equal);
    		
    	} // close function
        
        /**
         * Remove random Transcription value
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public String removeTranscription() throws InterruptedException, AWTException
    	{
        	// Choose random transcription
    		// Get the transcriptions values
        	List<WebElement> transcriptionsList= lstTranscriptions.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

        	Random randomizer = new Random();
        	WebElement randomElement = transcriptionsList.get(randomizer.nextInt(transcriptionsList.size()));

        	String randomTranscription = randomElement.getAttribute("Name");
        	try {
        		randomElement.click();
			} catch (Exception e) {
				System.out.println("not clickable");
			}

        	// Click the delete button for transcriptions
    		CurrentPage.As(DictionaryTab.class).bt_delete_transcription.click();

    		return randomTranscription;
    	} // close function
        
        /**
         * Choose transcription key from the left dropdown
         * @param key
         */
        public void chooseTransSpellKey(String key) {
        	// Get all the available comboboxes in the app
        	List<WebElement> comboboxesList= SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
        	
        	// Click on the Transcriptions key dropdown
        	for(WebElement f : comboboxesList)
    		{ // buttons
    			if (f.getAttribute("Name").equals("??? [unknown]")) 
    			{
    				try {
    					f.click();
    					Thread.sleep(2000);
    					
    					//-----
    					List<WebElement> keysList= f.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.List')]"));
    					// Click on the Transcriptions key
    		        	for(WebElement e : keysList)
    		    		{ // buttons
    		    			if (e.getAttribute("Name").equals(key)) 
    		    			{
    		    				try {
    		    					e.click();
    							} catch (Exception e1) {
    								System.out.println("not clickable");
    							}
    		    			}
    		    		} // end of inside for
    		        	//-----
    					
					} catch (Exception f1) {
						System.out.println("not clickable");
					}
    			}
    		} // end of outside for	
        }
        
        /**
         * Get spelling values
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public List<String> getSpellinigValues() throws InterruptedException, AWTException
    	{
        	// Get the transcriptions values
        	List<WebElement> spellingList= lb_dictionary_spellings.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
        	ArrayList<String> itemNames= new ArrayList<String>();
    		
        	for(WebElement f : spellingList) {
        		itemNames.add(f.getAttribute("Name"));
        		System.out.println(f.getAttribute("Name"));
        	}

    		return itemNames;
    	}

        /**
         * Add new Spelling value
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public void addSpelling(String key, String value) throws InterruptedException, AWTException
    	{
    		// Click the add/blue button for spelling
        	CurrentPage = GetInstance(DictionaryTab.class);
    		CurrentPage.As(DictionaryTab.class).bt_add_spelling.click();

    		// Entry a value
    		// Get the spelling values
        	List<WebElement> spellingList= lb_dictionary_spellings.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
        	
        	for(WebElement f : spellingList)
    		{
    			if (f.getAttribute("Name").equals("???~")) 
    			{
    				try {
    					f.click();
    					f.sendKeys(value);
					} catch (Exception e) {
						System.out.println("not clickable");
					}
    			}
    		} // end of for
        	
    		// Choose a key
        	chooseTransSpellKey(key);
    		
    		// Click the add/blue button for spelling approve
    		CurrentPage.As(DictionaryTab.class).bt_add_spelling.click();
    	} // close function
        
        /**
         * Remove random Spelling value
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public String removeSpelling() throws InterruptedException, AWTException
    	{
        	// Choose random spelling
    		// Get the spelling values
        	List<WebElement> spellingList= lb_dictionary_spellings.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

        	Random randomizer = new Random();
        	WebElement randomElement = spellingList.get(randomizer.nextInt(spellingList.size()));

        	String randomSpelling = randomElement.getAttribute("Name");
        	try {
        		randomElement.click();
			} catch (Exception e) {
				System.out.println("not clickable");
			}

        	// Click the delete button for Spellings
    		CurrentPage.As(DictionaryTab.class).bt_delete_spelling.click();

    		return randomSpelling;
    	} // close function
        
        /**
         * Get forms values
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public List<String> getFormsValues() throws InterruptedException, AWTException
    	{
        	// Get the forms values
        	List<WebElement> formsList= Forms_list.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
        	ArrayList<String> itemNames= new ArrayList<String>();
    		
        	for(WebElement f : formsList) {
        		itemNames.add(f.getAttribute("Name"));
//        		System.out.println(f.getAttribute("Name"));
        	}

    		return itemNames;
    	}

        /**
         * Choose random form value
         * @param key
         */
        public String addRandomForm() {
    		// Click the add/blue button for Forms
    		CurrentPage.As(DictionaryTab.class).bt_add_form.click();

        	// Get all the available comboboxes in the app
        	List<WebElement> comboboxesList= SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));

        	List<String> formsList = new ArrayList<>();
        	String randomForm = "";
        	// Click on the new form dropdown
        	for(WebElement f : comboboxesList)
    		{
    			if (f.getAttribute("Name").equals("")) 
    			{
    				try {
    					f.click();

    					List<WebElement> keysList= f.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
    					// Click on the Transcriptions key
    		        	for(WebElement e : keysList)
    		    		{
    		        		formsList.add(e.getAttribute("Name"));
    		    		} // end of inside for
    		        	
    		        	// Get random form
    		        	Random randomizer = new Random();
    		        	randomForm = formsList.get(randomizer.nextInt(formsList.size()));
    		        	
    		        	// Choose the random form
    		        	for(WebElement e : keysList)
    		    		{
    		        		if (e.getAttribute("Name").equals(randomForm))
    		        			{
    		        			CommonFunctions.chooseValueInDropdown(f,randomForm);
    		            		CurrentPage.As(DictionaryTab.class).bt_add_form.click();
    		        			return randomForm;
    		        			}
    		    		} // end of inside for
    					
					} catch (Exception f1) {
						System.out.println("not clickable");
					}
    			}
    			// Click the add/blue button for Forms to confirm
        		CurrentPage.As(DictionaryTab.class).bt_add_form.click();
    		} // end of outside for	
        	
			return randomForm;
        }
        /**
         * Remove random form value other than simple
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public String removeForm() throws InterruptedException, AWTException
    	{
        	// Choose random spelling
        	List<WebElement> formsListElement= Forms_list.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
        	
        	// Remove the first option which is default
//        	formsList.remove(0);
        	
        	// Get the values of the list
        	List<String> formsListValues = new ArrayList<String>();
        	for(int i=0;i<formsListElement.size();i++)
        		formsListValues.add(formsListElement.get(i).getAttribute("Name"));
        	
        	// Remove the default form
        	formsListValues.remove("m_single");
        	
        	// Choose random form from the rest
        	Random randomizer = new Random();
        	int index = randomizer.nextInt(formsListValues.size());
        	String randomForm = formsListValues.get(index);
        	try {
        		formsListElement.get(index).click();
			} catch (Exception e) {
				System.out.println("not clickable");
			}

        	// Click the delete button for Spellings
    		bt_delete_form.click();

    		return randomForm;
    	} // close function
        
        /**
         * Get POS that is valid with the current one
         * @param currentPOS
         * @return
         */
        public List<String> checkPOSValidity(String currentPOS) {
         List<String> POSSvailable = convertPOSEnumToList();
         List<String> POS2Delete = new ArrayList<String>();
         POSSvailable.remove("pronoun");
         POSSvailable.remove("modal");

         // 1
         if (currentPOS.equals("verb")) {
        	 for(String POS : POSSvailable) {
     if (!POS.equals("copula")) POS2Delete.add(POS);	 
    }
        	 POSSvailable.removeAll(POS2Delete);
        	 POS2Delete.clear();
         }
         // 2
         if (!currentPOS.equals("verb")) {
          POSSvailable.remove("copula");
         }
         // 3
         if (currentPOS.equals("copula")) {
        	 for(String POS : POSSvailable){
     if (!POS.equals("verb")) POS2Delete.add(POS);	
    }
        	 POSSvailable.removeAll(POS2Delete);
        	 POS2Delete.clear();
         }
         // 4
         if (!currentPOS.equals("copula")) {
          POSSvailable.remove("verb");
         }
         // 5
         if (currentPOS.equals("noun")) {
        	 for(String POS : POSSvailable)
        	 {
        		  if (!POS.equals("title") && !POS.equals("name") && !POS.equals("demonstrative")) 
        			  POS2Delete.add(POS);
        	 }
    		 POSSvailable.removeAll(POS2Delete);
        	 POS2Delete.clear();
         }
         // 6
         if (currentPOS.equals("name")) {
        	 for(String POS : POSSvailable) {
     if (!POS.equals("title") && !POS.equals("noun")  && !POS.equals("demonstrative")) POS2Delete.add(POS);
    }
    		 POSSvailable.removeAll(POS2Delete);
        	 POS2Delete.clear();
         }
         // 7
         if (currentPOS.equals("title")) {
        	 for(String POS : POSSvailable) {
     if (!POS.equals("name") && !POS.equals("noun")  && !POS.equals("demonstrative")) POS2Delete.add(POS);
    }
    		 POSSvailable.removeAll(POS2Delete);
        	 POS2Delete.clear();
         }
         // 8
         if (currentPOS.equals("demonstrative")) {
        	 for(String POS : POSSvailable) {
     if (!POS.equals("title") && !POS.equals("noun")  && !POS.equals("name")) POS2Delete.add(POS);
    }
      		 POSSvailable.removeAll(POS2Delete);
        	 POS2Delete.clear(); 
         }
         // 9
         if (!currentPOS.equals( "noun") && !currentPOS.equals("name") && !currentPOS.equals("title")) {
          POSSvailable.remove("demonstrative");
         }
         // 10
         if (!currentPOS.equals("title") && !currentPOS.equals("name") && !currentPOS.equals("demonstrative")) {
          POSSvailable.remove("noun");
         }
         // 11
         if (!currentPOS.equals("demonstrative") && !currentPOS.equals("title") && !currentPOS.equals("noun")) {
          POSSvailable.remove("name");
         }
         // 12
         if (!currentPOS.equals("demonstrative") && !currentPOS.equals("name") && !currentPOS.equals("noun")) {
          POSSvailable.remove("title");
         }
         // 13
         if (currentPOS.equals("pronoun")) return null;
         // 14
         if (currentPOS.equals("modal")) return null;
         
         POSSvailable.remove(currentPOS);
         
         return POSSvailable;
        }
        
        public static List<String>  convertPOSEnumToList(){
           List<String> POSList=new ArrayList<String>();
           for (POS pos : POS.values()) {
            POSList.add( pos.name());
           }
           return POSList;
        }
        
        
        public List<String> getGrammarAttributeValues(){
        	
           	List<WebElement> list = pn_grammar_attributes.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
           	List<String> values = new ArrayList<>();
           	
    		// Fetch grammar attribute values
    		for(WebElement e : list)
    		{ 
    			values.add(e.getAttribute("Name"));
        	}
			return values;
        	
        }
        
        /**
         * Get Forms Types in Attribute Editor
         * @param dropdown
         * @param Column
         * @param value
         * @throws InterruptedException
         * @throws AWTException
         */
        public HashMap<String, Boolean> getAllFormsTypesFromAttributeEditorWithSelection() throws InterruptedException, AWTException
    	{
        	// Get the forms values
        	HashMap<String, Boolean> formTypes = new HashMap<String, Boolean>();

        	List<WebElement> panes = Attribute_Editor.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Pane')]"));
        	for(WebElement f : panes) {
        		if (f.getAttribute("AutomationId").equals("pn_attribute_details")) {
        			
    			List<WebElement> formsTypesList= pn_attribute_details.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
    			
    			System.out.println("ToggleState");
            	for(WebElement e : formsTypesList) {
            		System.out.println(e.getAttribute("ToggleState"));
            		System.out.println(e.getAttribute("Toggle"));
            		System.out.println(e.getAttribute("checked"));
            		System.out.println(e.getAttribute("selected"));

//            		System.out.println(DriverContext._Driver.executeScript("automation: TogglePattern.ToggleState", e));
//            		System.out.println(DriverContext._Driver.executeScript("Toggle.ToggleState", e));
//            		this.state.Toggled
//            		List<WebElement> withinItem= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
            		
            		System.out.println(e.isDisplayed());
            		System.out.println(e.isEnabled());
            		System.out.println(e.getAttribute("*"));
            		
            		formTypes.put(e.getAttribute("Name"), e.isSelected());
            	}

        		}
        	}
        	
    		return formTypes;
    	}
        
        /**
         * Close every window
         * @param _Driver 
         * @throws AWTException 
         * @throws InterruptedException 
         */
        public void closeEveryWindow(String page) throws AWTException, InterruptedException 
        {
        	// Starting time
        	long startTime = System.nanoTime();
        	
         	System.out.println(page);
    		String[] pageSplit = page.split("\\.");
    		int lastCell = pageSplit.length-1;
    		String name = pageSplit[lastCell];
    		
    		System.out.println("Run 'close every window' function before trying again");
        		
        	List<WebElement> all = new ArrayList<WebElement>();
        	List<WebElement> windows = new ArrayList<WebElement>();
        	List<WebElement> AttributeEditor = new ArrayList<WebElement>();
        	
        	boolean attachemntWindowfound = false;
        	boolean attributeEditorfound = false;
    		all = root.findElements(By.xpath(".//*"));
    		for (WebElement e : all) {
    			try {
    				if (e.getAttribute("ControlType").equals("ControlType.Window"))
    		    		windows.add(e);
    		    	if ((e.getAttribute("ControlType").equals("ControlType.Window")) && (e.getAttribute("Name").equals("Attachment")))
    		    		attachemntWindowfound = true;
    		    	if ((e.getAttribute("ControlType").equals("ControlType.Window")) && (e.getAttribute("Name").equals("Attribute Editor")))
    		    		attributeEditorfound = true;
				} catch (Exception e2) {	
				}
    		}
    		
    		long endTimeAllElements   = System.nanoTime();
    		System.out.println("endTimeAllElements: 0: " + ( ((endTimeAllElements - startTime) / 1000000000 / 60) + ":" + ((endTimeAllElements - startTime)/ 1000000000 % 60)) );
    		
    		//-------------------------
    		
        	// Flag, if already closed one popup then return from this function because at most there is one popup window
        	boolean alreadyClosedOne = false;
        	
        	// I - Click Escape, just in case any unneeded popup or message is open, help close many popups or messages
        	System.out.println("I - Click Escape 5 times in 5 seconds, just in case any unneeded popup or message is open, help close many popups or messages");
        	for (int i = 0; i < 5; i++) {
        		Thread.sleep(1000);
        		Robot r = new Robot();
      		  r.keyPress(KeyEvent.VK_ESCAPE);
      		  r.keyRelease(KeyEvent.VK_ESCAPE);
 			}
        	long endTimeI   = System.nanoTime();
        	System.out.println("timeToFinish: I: " + ( ((endTimeI - startTime) / 1000000000 / 60) + ":" + ((endTimeI - startTime)/ 1000000000 % 60)) );
        	
        	//-------------------------
        	
        	// II - Close windows and popups within smofet app itself
        	System.out.println("II - Close windows and popups within smofet app itself");
//        	List<WebElement> windows = new ArrayList<WebElement>();
        	try {
//        		windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
        		for (WebElement e : windows) {
        			System.out.println(e.getAttribute("Name"));
        	    	if (e.getAttribute("Name").equals("Saving") || e.getAttribute("Name").equals("Revert File") || e.getAttribute("Name").equals("Clean Functions Selector") 
        	    			|| e.getAttribute("Name").equals("Attachment Message:") || e.getAttribute("Name").equals("Warning: bad key:")
        	    			|| e.getAttribute("Name").equals("window_too_many_records")
        	    			|| e.getAttribute("Name").equals("Saving disambiguation rules")
        	    			|| e.getAttribute("Name").equals("Saving normalization rules")
        	    			|| e.getAttribute("Name").equals("Saving prosody rules")
        	    			|| e.getAttribute("Name").equals("Saving transcription rules")) {
        	    		XClose.click();
        	    		alreadyClosedOne = true;
        	    	}
        	    	else if (e.getAttribute("Name").equals("Dictionary Manager Message:") || e.getAttribute("Name").equals("DictionaryForm_Version Manager Message:")
        	    			 || e.getAttribute("Name").equals("disambiguation Rule Delete warning:")
        	    			 || e.getAttribute("Name").equals("normalization Rule Delete warning:")
        	    			 || e.getAttribute("Name").equals("prosody Rule Delete warning:")
        	    			 || e.getAttribute("Name").equals("transcription Rule Delete warning:")
        	    			 || e.getAttribute("Name").equals("Deleting disambiguation rule")) {
        	    			No.click();
        	    			alreadyClosedOne = true;
        	    	}
    			}
 			} catch (Exception g) {
 				System.out.println("II exception: " + g);
 			}
        	
        	long endTimeII   = System.nanoTime();
        	System.out.println("timeToFinish: II: " + ( ((endTimeII - startTime) / 1000000000 / 60) + ":" + ((endTimeII - startTime)/ 1000000000 % 60)) );
        	
        	//-------------------------
        	
 			// III - Grammar attribute window
 			if (alreadyClosedOne == false && attributeEditorfound == true) {
 			System.out.println("III - Close Grammar attribute windows");
//        	List<WebElement> AttributeEditor = new ArrayList<WebElement>();
        	try {
        		AttributeEditor = Attribute_Editor.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
        		for (WebElement e : AttributeEditor) {
 	 				System.out.println(e.getAttribute("Name"));
 	 				if (e.getAttribute("Name").equals("Cancel")) {
 	 					e.click();
 	 					alreadyClosedOne = true;
 	 				}
 	 				if (e.getAttribute("Name").equals("OK")) {
 	 					e.click();
 	 					alreadyClosedOne = true;
 	 				}
 	 			}
 			} catch (Exception g) {
 				System.out.println("III exception: " + g);
 			}
        	}
 			long endTimeIII   = System.nanoTime();
 			System.out.println("timeToFinish: III: " + ( ((endTimeIII - startTime) / 1000000000 / 60) + ":" + ((endTimeIII - startTime)/ 1000000000 % 60)) );
 			
 			//-------------------------
 			
 			// IV - Close windows and popups outside of smorfet app
 			if (alreadyClosedOne == false && attachemntWindowfound == true) {
 			System.out.println("IV - Close windows and popups outside of smorfet app");
        	List<WebElement> buttons = new ArrayList<WebElement>();
        	try {
        		buttons = WindowAttachment.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
        		for (WebElement e : buttons) {
     				if (e.getAttribute("Name").equals("Abort"))
     					e.click();
     			}
 			} catch (Exception e) {
 				System.out.println("IV exception: " + e);
 			}
 			}
 			long endTimeIV   = System.nanoTime();
 			System.out.println("timeToFinish: IV: " + ( ((endTimeIV - startTime) / 1000000000 / 60) + ":" + ((endTimeIV - startTime)/ 1000000000 % 60)) );
 			
 			//-------------------------
 			
 			// V - Reset search and revert according the tab
 			try {
 				System.out.println("V - Reset search and revert according the tab");
 				
 				// in rules click the revert button
 				if (name.contains("Rules")) {
 					bt_revert.click();
 					Thread.sleep(3000);
 					revertFileMsgOK.click();
 					btnYes.click();
 				}
 				
 				// Reset the search field which may lead to unneeded results
 				if (name.contains("Dictinoary") || name.contains("Compounds") ||
 					name.contains("Lexicon") || name.contains("Corpus") ||
 					name.contains("Prosody")) {
 						cmpCondition_value.clear();
 				}
 				
 				// where having search fields reset them
 			} catch (Exception e) {
 				System.out.println("V exception: " + e);
 			}
 			
 			long endTimeV   = System.nanoTime();
				System.out.println("timeToFinish: V: " + ( ((endTimeV - startTime) / 1000000000 / 60) + ":" + ((endTimeV - startTime)/ 1000000000 % 60)) );
        }
       
//        /**
//         * Close every window
//         * @param _Driver 
//         * @throws AWTException 
//         * @throws InterruptedException 
//         */
//        public void closeEveryWindow(String page) throws AWTException, InterruptedException 
//        {
//        	
//        	// Starting time
//        	long startTime = System.nanoTime();
//        	
//     	System.out.println(page);
//    		String[] pageSplit = page.split("\\.");
//    		int lastCell = pageSplit.length-1;
//    		String name = pageSplit[lastCell];
//    		
//     	System.out.println("Run 'close every window' function before trying again");
//     	   
//        	// Flag, if already closed one popup then return from this function because at most there is one popup window
//        	boolean alreadyClosedOne = false;
//        	
//        	// I - Click Escape, just in case any unneeded popup or message is open, help close many popups or messages
//        	System.out.println("I - Click Escape 5 times in 5 seconds, just in case any unneeded popup or message is open, help close many popups or messages");
//        	for (int i = 0; i < 5; i++) {
//        		Thread.sleep(1000);
//        		Robot r = new Robot();
//      		  r.keyPress(KeyEvent.VK_ESCAPE);
//      		  r.keyRelease(KeyEvent.VK_ESCAPE);
// 			}
//        	long endTimeI   = System.nanoTime();
//        	System.out.println("timeToFinish: I: " + ( ((endTimeI - startTime) / 1000000000 / 60) + ":" + ((endTimeI - startTime)/ 1000000000 % 60)) );
//        	
//        	
//        	// II - Close windows and popups within smofet app itself
//        	System.out.println("II - Close windows and popups within smofet app itself");
//        	List<WebElement> windows = new ArrayList<WebElement>();
//        	try {
//        		windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
//        		for (WebElement e : windows) {
//        	    	if (e.getAttribute("Name").equals("Saving") || e.getAttribute("Name").equals("Revert File") || e.getAttribute("Name").equals("Clean Functions Selector") 
//        	    			|| e.getAttribute("Name").equals("Attachment Message:") || e.getAttribute("Name").equals("Warning: bad key:")
//        	    			|| e.getAttribute("Name").equals("window_too_many_records")) {
//        	    		XClose.click();
//        	    		alreadyClosedOne = true;
//        	    	}
//        	    	else if (e.getAttribute("Name").equals("Dictionary Manager Message:") || e.getAttribute("Name").equals("DictionaryForm_Version Manager Message:")) {
//        	    			No.click();
//        	    			alreadyClosedOne = true;
//        	    	}
//    			}
// 			} catch (Exception g) {
// 				System.out.println("II exception: " + g);
// 			}
//        	
//        	long endTimeII   = System.nanoTime();
//        	System.out.println("timeToFinish: II: " + ( ((endTimeII - startTime) / 1000000000 / 60) + ":" + ((endTimeII - startTime)/ 1000000000 % 60)) );
//        	
// 			// III - Grammar attribute window
// 			if (alreadyClosedOne == false) {
// 			System.out.println("III - Close Grammar attribute windows");
//        	List<WebElement> AttributeEditor = new ArrayList<WebElement>();
//        	try {
//        		AttributeEditor = Attribute_Editor.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
//        		for (WebElement e : AttributeEditor) {
// 	 				System.out.println(e.getAttribute("Name"));
// 	 				if (e.getAttribute("Name").equals("Cancel")) {
// 	 					e.click();
// 	 					alreadyClosedOne = true;
// 	 				}
// 	 				if (e.getAttribute("Name").equals("OK")) {
// 	 					e.click();
// 	 					alreadyClosedOne = true;
// 	 				}
// 	 			}
// 			} catch (Exception g) {
// 				System.out.println("III exception: " + g);
// 			}
//        	}
// 			long endTimeIII   = System.nanoTime();
// 			System.out.println("timeToFinish: III: " + ( ((endTimeIII - startTime) / 1000000000 / 60) + ":" + ((endTimeIII - startTime)/ 1000000000 % 60)) );
// 			
// 			// IV - Close windows and popups outside of smorfet app
// 			if (alreadyClosedOne == false) {
// 			System.out.println("IV - Close windows and popups outside of smorfet app");
//        	List<WebElement> buttons = new ArrayList<WebElement>();
//        	try {
//        		buttons = WindowAttachment.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
//        		for (WebElement e : buttons) {
//     				if (e.getAttribute("Name").equals("Abort"))
//     					e.click();
//     			}
// 			} catch (Exception e) {
// 				System.out.println("IV exception: " + e);
// 			}
// 			}
// 			long endTimeIV   = System.nanoTime();
// 			System.out.println("timeToFinish: IV: " + ( ((endTimeIV - startTime) / 1000000000 / 60) + ":" + ((endTimeIV - startTime)/ 1000000000 % 60)) );
// 			
// 			// V - Reset search and revert according the tab
// 			try {
// 				System.out.println("V - Reset search and revert according the tab");
// 				
// 				// in rules click the revert button
// 				if (name.contains("Rules")) {
// 					bt_revert.click();
// 					Thread.sleep(3000);
// 					revertFileMsgOK.click();
// 					btnYes.click();
// 				}
// 				
// 				// Reset the search field which may lead to unneeded results
// 				if (name.contains("Dictinoary") || name.contains("Compounds") ||
// 					name.contains("Lexicon") || name.contains("Corpus") ||
// 					name.contains("Prosody")) {
// 						cmpCondition_value.clear();
// 				}
// 				
// 				// where having search fields reset them
// 			} catch (Exception e) {
// 				System.out.println("V exception: " + e);
// 			}
// 			
// 			long endTimeV   = System.nanoTime();
//				System.out.println("timeToFinish: V: " + ( ((endTimeV - startTime) / 1000000000 / 60) + ":" + ((endTimeV - startTime)/ 1000000000 % 60)) );
//        }
        
        
        
        /**
         * Fetch the text of pane popup
         * @param test
         * @param logger
         * @param titleToCheck
         * @param textToCheck
         * @return
         */
        public boolean popupWindowMessage(ExtentTest test, Logger logger, String textToCheck)
    	{
        	List<WebElement> windows = SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Window')]"));
        	List<WebElement> text = null;
    	    boolean textIn = false;
    	    
        	// Check the main app for popup windows
    		for(WebElement e : windows)
    			text = e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	

    		// Fetch the found title    		
    		for(WebElement e : text)
    		{ 
    			LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
    			if (e.getAttribute("Name").contains(textToCheck))
    				{textIn = true;break;}
        	}
    		
    		LogUltility.log(test, logger, " - TextIn: " + textIn);
    		
    	    if (textIn)
    	    	return true;
    	  return false;
    	} // close function
        
        
        /**
         * Fetch the text of outside popup
         * @param test
         * @param logger
         * @param titleToCheck
         * @param textToCheck
         * @return
         */
        public boolean popupWindowMessageOutside(ExtentTest test, Logger logger, String textToCheck)
    	{
//        	List<WebElement> windows = WindowAttachment.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));
        	List<WebElement> text = null;
    	    boolean textIn = false;
    	    
        	// Check the main app for popup windows
//    		for(WebElement e : windows)
    			text = WindowAttachment.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Text')]"));        	

    		System.out.println();
    		// Fetch the found title    		
    		for(WebElement e : text)
    		{ 
    			LogUltility.log(test, logger, "Text:" + e.getAttribute("Name"));
    			if (e.getAttribute("Name").contains(textToCheck))
    				textIn = true;
        	}
    		
    		LogUltility.log(test, logger, " - TextIn: " + textIn);
    		
    	    if (textIn)
    	    	return true;
    	    else return false;
    	} // close function
        
        /**
         * Get records names with version zero from the dictionary.txt files
         * @return
         * @throws IOException 
         */
         public List<String> getRecordsFromWithVersionZero() throws IOException {
        	 
        		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
		   		List<String> lines = new ArrayList<>();
		   		while(in.hasNextLine()) {
		   		    String line = in.nextLine().trim();
 		   		    String[] splitted = line.split("	");
 		   		    String[] removeLeft = splitted[0].split("}",2);
 		   		    String[] removeRight = removeLeft[1].split("~");
 		   		    if(removeRight[1].equals("0"))
 		   		    	lines.add(removeRight[0]);
 		   		}
 		   		
 		   		in.close();
 				return lines;
         }
         
         
         /**
          * edit transcription key from the left dropdown
          * @param key
          */
         public void editTransSpellKey(String key,String oldKey) {
         	// Get all the available comboboxes in the app
         	List<WebElement> comboboxesList= SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
         	
         	// Click on the Transcriptions key dropdown
         	for(WebElement f : comboboxesList)
     		{ // buttons
     			if (f.getAttribute("Name").equals(oldKey)) 
     			{
     				try {
     					f.click();
     					Thread.sleep(2000);
     					
     					//-----
     					List<WebElement> keysList= f.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.List')]"));
     					// Click on the Transcriptions key
     		        	for(WebElement e : keysList)
     		    		{ // buttons
     		    			if (e.getAttribute("Name").equals(key)) 
     		    			{
     		    				try {
     		    					e.click();
     							} catch (Exception e1) {
     								System.out.println("not clickable");
     							}
     		    			}
     		    		} // end of inside for
     		        	//-----
     					
 					} catch (Exception f1) {
 						System.out.println("not clickable");
 					}
     			}
     		} // end of outside for	
         }
         
         
         /**
		  * Get regular records all info from the Dictionary.txt files
		  * @return
         * @throws IOException 
		  */
		  public  HashMap<String, String> getRegularRecordsInfoFromDictionary(String searchedRecord) throws IOException {
			  
			  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\Dictionary.txt"));
			  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
			  HashMap<String, String> info = new HashMap<>();
			  String line;
			   	while((line = reader.readLine()) != null)
			   	{
			   		    line = line.trim();
			   		    String[] splitted = line.split("	");
			   		    
			   		    if (splitted[0].equals(searchedRecord)) 
			   		    {
			   		    	info.put("record status", splitted[1]);
			   		    	info.put("modified on",splitted[2]);
			   		    	info.put("register",splitted[3]);
			   		    	info.put("acronym", splitted[4]);
			   		    	info.put("style",splitted[5]);
			   		    	info.put("origin", splitted[6]);
			   		    	info.put("frequency", splitted[7]);
			   		    	info.put("definition", splitted[8]);
			   		    	info.put("etymology", splitted[9]);
			   		    	info.put("frames", splitted[10]);
			   		    	info.put("semantic groups", splitted[11]);
			   		    	info.put("semantic fields", splitted[12]);
			   		    	info.put("semantic relation", splitted[13]);
			   		    	info.put("lexical relation",splitted[14]);
			   		    	info.put("proximity rules", splitted[15]);
			   		    	info.put("translations", splitted[16]);
			   		    	info.put("definiteness",splitted[17]);
			   		    	info.put("declinability", splitted[18]);
			   		    	info.put("dependency", splitted[19]);
			   		    	info.put("number", splitted[20]);
			   		    	info.put("gender", splitted[21]);   	
			   		    	// Split transcriptions from spellings
			   		    	String[] spellingTrans = splitted[22].split("¬");
			   		    	// Get clean spellings and add them to List
			   		    	String[] cleanSpellingTrans = spellingTrans[1].split("\\|",2);
			   		    	info.put("spellings", cleanSpellingTrans[0]);
			   		    	info.put("transcriptions", cleanSpellingTrans[1]);
			   		    	if(splitted.length == 24)
			   		    	info.put("grammar attributes", splitted[23]);
			   		    	
			   		    	reader.close();
			   		    	return info;
			   		    }
			   		    	
			     }
	
			   		reader.close();
					return info;
		  }
		  
		  /**
	          * Get all frames for specified record name
	          * @param record
	          * @return
	          * @throws FileNotFoundException
	          */
	         public List<String> getAllFramesOfRecordFromDictionary(String record) throws FileNotFoundException {
	    			
	 	   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
	 	   		List<String> frames = new ArrayList<>();
	 	   		while(in.hasNextLine()) {
	 	   			
	 	   		    String line = in.nextLine().trim();
	 	   		    String[] splitted = line.split("	");

	 	   		    if (record.equals(splitted[0])) 
	 	   		    	frames.add(splitted[10].replace("_", " "));
	 	   		    
	 	   		}
	 	   		
	 	   		in.close();
	 			return frames;
	     }
		  
	         /**
	          * Search in dictionary
	          * @param test
	          * @param logger
	       	* @param firstDropdownValue
	       	* @param conditionDropdownValue
	       	* @param searchWord
	       	* @param filterDropdownValue
	          * @return
	       * @throws InterruptedException 
	          */
	         public void searchInDictionary(ExtentTest test,Logger logger,String firstDropdownValue,String conditionDropdownValue,String searchWord,String filterDropdownValue) throws InterruptedException
	     	{
	  		DictionaryTab dictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
	      	
	     		// Choose value from the first search dropdown
	     		CommonFunctions.chooseValueInDropdown(dictionaryTab.cmpCondition_field,firstDropdownValue);
	     		LogUltility.log(test, logger, firstDropdownValue+" is chosen from the first search dropdown");

	     		// Choose value from the condition dropdown
	     		CommonFunctions.chooseValueInDropdown(dictionaryTab.cmpCondition_operation, conditionDropdownValue);
	     		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

	     		//Type in the search field
	     		dictionaryTab.cmpCondition_value.sendKeys(searchWord);
	     		LogUltility.log(test, logger, "Type in the search field:"+searchWord);
	     		
	     		// Change the last filter value, in case it was changed by other TC
	     		CommonFunctions.chooseValueInDropdown(dictionaryTab.cmpCondition_all_next,filterDropdownValue);
	     		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
	     		
	     		// Click the Retrieve button
	     		dictionaryTab.btnRetrieve_same.click();
	     		LogUltility.log(test, logger, "Click the Retreive button");
	     		
	     	} // close function
	     	
	         
	         /**
	          * Check if the case sensitivity/A button is active
	          * @return
	          */
	         public boolean isUppercaseActive()
	         {
	        	 // If it is active (displayed as "A") then the Name attribute will be set to A otherwise to a
	        	 if(bt_condition_uppercase.getAttribute("Name").equals("A"))
	        		 return true;
	        	 return false;
	         }
	         
	         /**
			  * Get all records with the required status from the Dictionary.txt file
			  * @return
	         * @throws IOException 
			  */
			  public  List<String> getRecordsWithStatus(String status) throws IOException {
				  
				  File file = new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt");
				  BufferedReader reader = new BufferedReader(new FileReader(file));
				  List<String> records = new ArrayList<String>() ;
				  String line;
				   	while((line = reader.readLine()) != null)
				   	{
				   		    line = line.trim();
				   		    String[] splitted = line.split("	");
				   		    
				   		    if (splitted[1].equals(status)) 
				   		    	records.add(splitted[0]);
				   		    	
				     }
		
				   		reader.close();
						return records;
			  }
	         
			  /**
				  * Get the record forms info from the Dictionary.txt files
				  * @return
		         * @throws IOException 
				  */
				  public  String getRecordForms(String searchedRecord) throws IOException {
					  
					  InputStream readFile = new FileInputStream(Setting.AppPath +Setting.Language +"\\Data\\Dictionary.txt");
	            	  BufferedReader reader = new BufferedReader(new InputStreamReader(readFile,StandardCharsets.UTF_8));
					  String forms ="";
					  String line;
					   	while((line = reader.readLine()) != null)
					   	{
					   		    line = line.trim();
					   		    String[] splitted = line.split("	");
					   		    
					   		    if (splitted[0].toLowerCase().contains(searchedRecord.toLowerCase())) 
					   		    {
					   		    	// avoid getting Grammar attributes from the last index ¿
					   		    	forms = splitted[splitted.length-1].contains("¿")? splitted[splitted.length-2]: splitted[splitted.length-1];
					   		    	
					   		    	reader.close();
					   		    	return forms;
					   		    }
					   		    	
					     }	
					   		reader.close();
							return forms;
				  }
				  
				  
		             /**
		              * Choose value from dropdown that is contains LISTITEM
		              * @param lstBox
		              * @param ItemName
		              * @throws InterruptedException
		              */
		             public void chooseValueInDropdownContains(WebElement lstBox, String ItemName) throws InterruptedException
		         	{
		            	// Open the listbox
//		            	 List<WebElement> dropdownbutton1= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
//		     			dropdownbutton1.get(0).click();
//		             	lstBox.click();
		             	
		             	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

		           		// finally, for all
		         		for(WebElement e : itemlist)
		         		{
		         			System.out.println(e.getAttribute("Name"));
		         			if (e.getAttribute("Name").contains(ItemName))
		        				{
		        				  e.click();
		        				  return;
		        				}
		         		}
		         	}
		             
		             /**
		              * Modify grammar attribute chosen chekboxes
		              * @param lstBox
		              * @param ItemName
		              * @throws InterruptedException
		              */
		             public void modifyGrammarAttributeCheckboxes(ExtentTest test,Logger logger) throws InterruptedException
		         	{
		             	List<WebElement> itemlist= grammarAttCheckboxList.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
		             	itemlist = itemlist.subList(0,5);
		           		//  Change the checkbox state for all elements
		         		for(WebElement e : itemlist)
		         		{
		        				  e.click();
		        				  LogUltility.log(test, logger,"Change Checkbox state: "+e.getAttribute("Name"));
		         		}
		         		
		         		// in case all the fields are disabled, click on the first one
		         		itemlist.get(0).click();
		         	}
				  
		             /**
		              * Get clean Abbreviations names from the dictionary.txt files
		              * @return
		              * @throws FileNotFoundException
		              */
		              public List<String> getAbbreviationsFromDictionary() throws FileNotFoundException {
		      		   			
		      		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt"), "UTF-8");
		      		   		List<String> lines = new ArrayList<>();
		      		   		while(in.hasNextLine()) {
		      		   		    String line = in.nextLine().trim();
		      		   		    String[] splitted = line.split("	");
		      		   		    
		      		   		    String[] removeLeft = splitted[0].split("}");
		      		   		    String[] removeRight = removeLeft[1].split("~");
		      		   		    int count = StringUtils.countMatches(removeRight[0], ".");
		      		   		    // Check if there only one dot and it is at the end 
		      		   		    if(count == 1 && removeRight[0].charAt(removeRight[0].length()-1) == '.' && removeRight[0].length()>1)
		      		   		    	lines.add(removeRight[0]);
		      		   		}
		      		   		
		      		   		in.close();
		      				return lines;
		              }
		              
		              
		              /**
		               * Edit existing spelling
		               * @param dropdown
		               * @param Column
		               * @param value
		               * @throws InterruptedException
		               * @throws AWTException
		               */
		              public void editSpelling(ExtentTest test, Logger logger) throws InterruptedException, AWTException
		          	{
		              	LogUltility.log(test, logger, "Start of edit spelling function");
		              	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		              	
		              	// Get all spellings
		              	List<String> listedSpellings =  getSpellinigValues();
		              	LogUltility.log(test, logger, "Current spelling list: " + listedSpellings);
		              	
		              	// Choose random spelling
		              	Random randomizer = new Random();
		          		String randomSpell = listedSpellings.get(randomizer.nextInt(listedSpellings.size()));
		          		CommonFunctions.chooseValueInDropdown(DictionaryTab.Spellings_list, randomSpell);
		          		LogUltility.log(test, logger, "Random Chosen spelling to edit: " + randomSpell);
		          		
		              	String keyTochange = randomSpell.split("~")[0];
//		              	String valueTochange = randomTranscription.split("~")[1];
		              	
		              	String randomNewValue = RandomString(5);
		              	
		              	// Choose the spelling to be modified
		              	Actions shiftClick = new Actions(DriverContext._Driver);
		          		// Get the spelling values
		              	List<WebElement> spellingsList= Spellings_list.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
		              	for(WebElement f : spellingsList)
		          		{
		          			if (f.getAttribute("Name").equals(randomSpell)) 
		          			{
		          				try {
//		          					f.click();
		          					shiftClick.keyDown(Keys.SHIFT).click(f).keyUp(Keys.SHIFT).perform();
		          		    		LogUltility.log(test, logger, "Enter random new value: " + randomNewValue);
		          					f.sendKeys(randomNewValue);
		      					} catch (Exception e) {
		      						System.out.println("not clickable");
		      					}
		          			}
		          		} // end of for
		              	
		              	//-------------------------------
		              	String randomKey = "";
		              	
		          		// Choose the key to be modified
		              	// Get all the available comboboxes in the app
		              	List<WebElement> comboboxesList= SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
		              	
		              	// Click on the spelling key dropdown
		              	boolean foundKey = false;
		              	for(WebElement f : comboboxesList)
		          		{ 
		              		
		              		if(!foundKey) {
		              		
		              		// buttons
		              		String cleanKey = f.getAttribute("Name").substring(0, 3);
		              		
		          			if (cleanKey.equals(keyTochange)) 
		          			{
		          				try {
		          					f.click();
//		          					Thread.sleep(2000);
		          					
		          					List<String> keysList = DictionaryTab.getValuesFromApp(f);
		          					// Remove the first option = unknown option
		          					keysList.remove(0);
		          					randomKey = keysList.get(randomizer.nextInt(keysList.size()));
		          					
		          					// Choose random new key
		          					LogUltility.log(test, logger, "Enter random new key: " + randomKey);
		          					
		          		    		CommonFunctions.chooseValueInDropdown(f, randomKey);
		          		    		foundKey = true;
		          					
		      					} catch (Exception f1) {
		      						System.out.println("not clickable");
		      					}
		          			}
		          			
		          		  }
		              		else
		              			break;
		          		} // end of outside for	
		              	
		          		// Click the add/blue button for spelling approve
		              	DictionaryTab.bt_add_spelling.click();
		          		
		          		//---------------------------------------
		          		// Check edit is successfully changed
		              	listedSpellings.remove(randomSpell);
		          		// add the new modified values to the list
		          		String newValue = randomKey.substring(0, 3) + "~" + randomNewValue;
		          		listedSpellings.add(newValue);
		          		
		          		// Get the new spelling list
		          		List<String> newListedSpelling =  getSpellinigValues();
		          		
		          		// Sort lists
		          		Collections.sort(listedSpellings);
		          		Collections.sort(newListedSpelling);
		          		
		          		LogUltility.log(test, logger, "Old list: " + listedSpellings);
		          		LogUltility.log(test, logger, "New list: " + newListedSpelling);
		          		
		          		// Compare the two lists are equal
		          		boolean equal = newListedSpelling.equals(listedSpellings);
		          		LogUltility.log(test, logger, "Are two lists equal: " + equal);
		          		assertTrue(equal);
		          		
		          	} // close function
		              
		              
              /**\
               * Convert POS code to POS name
               * @param POSInput
               * @return
               */
              public String POSCodeConverter(String POSInput) {
//              	POSInput = POSInput.replace(" ", "_");
              	
              	// Put manually the values here, later fetch from DB
              	Map<String,String> POS = new HashMap<String,String>();
              	POS.put("accusative_word", "W"); POS.put("clause_predicate", "e"); 
//              POS.put("action_name", "e"); 
              	POS.put("adadjective", "d"); POS.put("adjective", "a"); 
              	POS.put("adverb", "r"); POS.put("article", "t"); 
              	POS.put("classifier", "I"); POS.put("clause_opener", "s"); 
              	POS.put("conjunction", "c"); POS.put("copula", "u"); 
              	POS.put("demonstrative", "z"); POS.put("diacritic", "D"); 
              	POS.put("existential", "C"); POS.put("foreign_script", "L"); 
              	POS.put("foreign_word", "S"); POS.put("genetive_word", "F"); 
              	POS.put("infinitive", "i"); POS.put("interjection", "j"); 
              	POS.put("modal", "m"); POS.put("name", "x"); 
              	POS.put("negation", "g"); POS.put("noun", "n"); 
              	POS.put("number", "N"); POS.put("phrase_begins", "B"); 
              	POS.put("phrase_ends", "E"); POS.put("possessive", "V"); 
              	POS.put("prefix", "f"); POS.put("preposition", "p"); 
              	POS.put("pronoun", "o"); POS.put("punctuation", "P"); 
              	POS.put("quantifier", "q"); POS.put("question", "Q"); 
              	POS.put("suffix", "U"); POS.put("title", "l"); 
              	POS.put("verb", "v"); POS.put("verb_particle", "A");
              	
              	List<String> keys = new ArrayList<String>(POS.keySet());	
              	for(int i=0;i<keys.size();i++)
              		if(POS.get(keys.get(i)).equals(POSInput))
              			return keys.get(i);
              	
              	// if not found
              	return null;
              }
				  
              
              /**
               * Build dictionary merge file 'Dictionary_merge.txt' 
               * @param size - the wanted merge file lines number
               * @return
             * @throws IOException 
               */
               public void buildMergeFile(ExtentTest test, Logger logger,int size) throws IOException {
       		   			
            	   LogUltility.log(test, logger, "Building dictionary merge file...");
            	   OutputStream writeFile = new FileOutputStream(Setting.AppPath + Setting.Language + "\\Data\\Dictionary_merge.txt");
    			   BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(writeFile,StandardCharsets.UTF_8));
    				  
    			   InputStream readFile = new FileInputStream(Setting.AppPath + Setting.Language + "\\Data\\Dictionary.txt");
            	   BufferedReader reader = new BufferedReader(new InputStreamReader(readFile,StandardCharsets.UTF_8));
            	   String line;
            	   int count =0;
            		while((line = reader.readLine()) != null)
       		   		{
	   				 int splitIndex = line.indexOf('~');
	   				   
	   				   // Generate new record
	   				 String newLine = line.substring(0, splitIndex)+ RandomString(3)+line.substring(splitIndex,line.length());
	   				 LogUltility.log(test, logger, "Adding new line: "+newLine);
	   				 writer.write(newLine);
 					 count++;
   					 if(count == size)
   						 break;
   					 writer.newLine();
  
       		   		}
       		   		
            		writer.close();
            		reader.close();
       			
               }//close function
               
               /**
                * Get clean records names from the Dictionary_merge.txt files
                * @return
                * @throws FileNotFoundException
                */
                public List<String> getRecordsFromDictionaryMerge() throws FileNotFoundException {
        		   			
        		   		Scanner in = new Scanner(new File(Setting.AppPath + Setting.Language + "\\Data\\Dictionary_merge.txt"), "UTF-8");
        		   		List<String> lines = new ArrayList<>();
        		   		while(in.hasNextLine()) {
        		   		    String line = in.nextLine().trim();
        		   		    String[] splitted = line.split("	");
        		   		    
        		   		    String[] removeLeft = splitted[0].split("}");
        		   		    String[] removeRight = removeLeft[1].split("~");
        		   		    lines.add(removeRight[0]);
        		   		}
        		   		
        		   		in.close();
        				return lines;
                }
                
          	  /**
	  			   * Get clean dictionary keys that have more than one representation
	  			   * @throws IOException
	  			   */
	   			  public static  List<String> getDictionaryKeysMultiReps() throws IOException {
	   				  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\Dictionary.txt"));
	   				  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	   				  List<String> records = new ArrayList<String>();
	   				  List<String> finalRecords = new ArrayList<String>();
	   				  String line;
	   				  
	   				  // Get all dictionary clean records
	   				   	while((line = reader.readLine()) != null)
	   				   	{
	   				   		line = line.trim();
				   		    String[] splitted = line.split("	"); 
				   		    String cleanRecord = splitted[0].split("\\}")[1].split("\\~")[0];
				   		    records.add(cleanRecord);
				   		
	   				   	}
	   				   	
	   				  // Get dictionary keys that have more than one representation
	   				  for(int i=0;i<records.size();i++)
	   				  {
	   					int occurrences = Collections.frequency(records, records.get(i));
	   					if(occurrences>1)
	   						if(onlyCharacters(records.get(i).toLowerCase()))
	   						if(!finalRecords.contains(records.get(i).toLowerCase()))
	   							finalRecords.add(records.get(i).toLowerCase());
	   				  }
	   					  
	   				reader.close();	   		
	   				return finalRecords;
	   			  } 
	   			  
	   			/**
	    	        * Check if the string contains only characters
	    	        */
	   	     public static boolean onlyCharacters(String str) {
	   	      return Setting.Language.equals("EN") ? str.matches("[a-zA-Z]+") : str.matches (".*[א-ת]+.*") ;
	   	  }
	   			 /**
	  			   * Get dictionary keys that has more than one form
	  			   * @throws IOException
	  			   */
	   			  public static  List<String> getDictionaryKeysMultiForms() throws IOException {
	   				  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\Dictionary.txt"));
	   				  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	   				  List<String> forms = new ArrayList<String>();
	   				  String line;
	   				  
	   				  // Get all dictionary records
	   				   	while((line = reader.readLine()) != null)
	   				   	{
	   				   		line = line.trim();
				   		    String[] splitted = line.split("	"); 
				   		    
				   		    // If it has more than one form
				   		    if(splitted[splitted.length-1].contains("¦"))
				   		    {
//				   		    String numberOfForms[] = splitted[splitted.length-1].split("ï¿½");
//				   		    if(numberOfForms.length>1)
				   		    	forms.add(splitted[0]);
				   		    }
				   		
	   				   	}

	   				reader.close();	   		
	   				return forms;
	   			  } 

	   			  
	   	        /**
	   	         * Remove the wanted form value
	   	         * @param dropdown
	   	         * @param Column
	   	         * @param value
	   	         * @throws InterruptedException
	   	         * @throws AWTException
	   	         */
	   	        public void removeForm(String form) throws InterruptedException, AWTException
	   	    	{
	   	        	// Choose random spelling
	   	        	List<WebElement> formsList= Forms_list.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
	   	        	 	
	   	        	formsList.remove(0);
	   	        	
	   	        	// Click on the wanted form
	   	        	for(int i=0;i<formsList.size();i++)
	   	        		if(formsList.get(i).getAttribute("Name").equals(form))
	   	        			formsList.get(i).click();
	 
	   	        	// Click the delete button for Spellings
	   	        	bt_delete_form.click();

	   	    	} // close function
	   	        
	   			  
	   	     /**
	   		  * Get dictionary key's with more than one spelling from Dictionary.txt files
	   		  * @return
	            * @throws IOException 
	   		  */
	   		  public  List<String> getDictionaryKeysWithMultiSpellings() throws IOException {
	   			  
	   			  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\Dictionary.txt"));
	   			  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
	   			  List<String> keys = new ArrayList<String>();
	   			  String line;
	   			   	while((line = reader.readLine()) != null)
	   			   	{
	   			   		    line = line.trim();
	   			   		    String[] splitted = line.split("	");
   			   
   			   		    	// Split transcriptions from spellings
	   			   		    String[] spellingTrans = splitted[22].split("¬");
   			   		    	// Get clean spellings and add them to List
   			   		    	String[] cleanSpellingTrans = spellingTrans[1].split("\\|",2);
   			   		    	if(cleanSpellingTrans[0].contains("¡"))
   			   		    	{
   			   		    		String cleanRecord =  splitted[0].split("\\}")[1].split("~")[0];
   			   		    		if(onlyCharacters(cleanRecord) && !keys.contains(splitted[0]))
   			   		    			keys.add(splitted[0]);	   		    
   			   		    	}
	   			   	}

	   			   		reader.close();
	   					return keys;
	   		  }
	   		  
	   		  
	   		  /**
	           * Remove specific Spelling value
	           * @param dropdown
	           * @param Column
	           * @param value
	           * @throws InterruptedException
	           * @throws AWTException
	           */
	          public String removeSpelling(String spelling) throws InterruptedException, AWTException
	      	{
	        
	      		// Get the spelling values
	          	List<WebElement> spellingList= lb_dictionary_spellings.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

	        for(WebElement e : spellingList)
	        	if(e.getAttribute("Name").equals(spelling))
	        		e.click();
	        
	          	// Click the delete button for Spellings
	      		bt_delete_spelling.click();
	      		
	      		return spelling;
	      	} // close function
	          
          /**
			  * Get the clean dictionary key available forms info from the Dictionary.txt file
			  * @return
	         * @throws IOException 
			  */
			  public  List<String> getCleanRecordForms(String searchedRecord) throws IOException {
				  
				 String formInfo = getRecordForms(searchedRecord);
				 String splittedForms[] = formInfo.split("¦");
				 List<String> forms = new ArrayList<String>();
				 
				 for(int i=0;i<splittedForms.length;i++)
				 {
					 String form = splittedForms[i].split("¬")[0].replaceAll("_", " ");
					 forms.add(form);
				 }
				 
				 // Default form is the first value (index 0)
				 return forms;
			  }

		 
   	     /**
   		  * Get dictionary key's with grammar attributes from Dictionary.txt file
   		  * @return
            * @throws IOException 
   		  */
   		  public  List<String> getDictionaryKeysWithGrammarAttribute() throws IOException {
   			  
   			  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\Dictionary.txt"));
   			  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
   			  List<String> keys = new ArrayList<String>();
   			  String line;
   			   	while((line = reader.readLine()) != null)
   			   	{
   			   		    line = line.trim();
   			   		    String[] splitted = line.split("	");
   			   		    // If it has attach to (=BAA) or change to (=TAA) or suffix (=SAT) then add to the list
	   		    		if(splitted[splitted.length-1].contains("BAA¿") || splitted[splitted.length-1].contains("TAA¿") || splitted[splitted.length-1].contains("SAT¿"))
		   		    		keys.add(splitted[0]);
   			   	}

   			   		reader.close();
   					return keys;
   		  }
   		  
   		  
   	     /**
    		  * Get dictionary key's with frames from Dictionary.txt file
    		  * @return
             * @throws IOException 
    		  */
    		  public  List<String> getDictionaryKeysWithFrames() throws IOException {
    			  
    			  InputStream file = new FileInputStream((Setting.AppPath + Setting.Language +"\\Data\\Dictionary.txt"));
    			  BufferedReader reader = new BufferedReader(new InputStreamReader(file,"utf-8"));
    			  List<String> keys = new ArrayList<String>();
    			  String line;
    			   	while((line = reader.readLine()) != null)
    			   	{
    			   		    line = line.trim();
    			   		    String[] splitted = line.split("	");
    			   		    // Check that there is frame value
    			   		    if(!splitted[10].isEmpty())
    			   		    	keys.add(splitted[0]);
    			   	}

    			   		reader.close();
    					return keys;
    		  }
    		  
    		  
    		   /**
               * Edit existing spelling value without the "key" value
               * @param dropdown
               * @param Column
               * @param spellingValue
               * @throws InterruptedException
               * @throws AWTException
               */
              public void editSpelling(ExtentTest test, Logger logger,String spellingValue) throws InterruptedException, AWTException
          	{
              	LogUltility.log(test, logger, "Start of edit spelling function");
              	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
              	
              	// Get all spellings
              	List<String> listedSpellings =  getSpellinigValues();
              	LogUltility.log(test, logger, "Current spelling list: " + listedSpellings);
              	
              	// Choose random spelling
          		String randomSpell = spellingValue;
          		CommonFunctions.chooseValueInDropdown(DictionaryTab.Spellings_list, randomSpell);
          		LogUltility.log(test, logger, "Chosen spelling to edit: " + randomSpell);

              	String randomNewValue = RandomString(5);
              	// Choose the spelling to be modified
              	Actions shiftClick = new Actions(DriverContext._Driver);
          		// Get the spelling values
              	List<WebElement> spellingsList= Spellings_list.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
              	for(WebElement f : spellingsList)
          		{
          			if (f.getAttribute("Name").equals(randomSpell)) 
          			{
          				try {
//          					f.click();
          					shiftClick.keyDown(Keys.SHIFT).click(f).keyUp(Keys.SHIFT).perform();
          		    		LogUltility.log(test, logger, "Enter random new value: " + randomNewValue);
          					f.sendKeys(randomNewValue);
      					} catch (Exception e) {
      						System.out.println("not clickable");
      					}
          			}
          		} // end of for
              	
          		// Click the add/blue button for spelling approve
              	DictionaryTab.bt_add_spelling.click();
   		
          	} // close function
              
				  
              
              /**
               * Edit Transcription value without changing the key value
               * @param dropdown
               * @param Column
               * @param value
               * @throws InterruptedException
               * @throws AWTException
               */
              public void editTranscriptions(ExtentTest test, Logger logger,String currentTrans) throws InterruptedException, AWTException
          	{
              	LogUltility.log(test, logger, "Start of edit transcription function");
              	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
              	
              	// Get all transcriptions
              	List<String> listedTranscriptions =  getTranscriptionsValues();
              	LogUltility.log(test, logger, "Current transcriptions list: " + listedTranscriptions);
              	
              	// Get index of the current transcription
              	// Some LHB has spaces in dictionary but not in corpus
              	int index  = -1;
              	for(int i=0;i<listedTranscriptions.size();i++)
              		if(listedTranscriptions.get(i).replace(" ", "").contains(currentTrans))
              		{index = i;break;}
              	
              	// Choose random transcription
              	Random randomizer = new Random();
          		String randomTranscription = listedTranscriptions.get(index);
          		CommonFunctions.chooseValueInDropdown(DictionaryTab.lstTranscriptions, randomTranscription);
          		LogUltility.log(test, logger, "Random Chosen transcription to edit: " + randomTranscription);
          		
//              	String keyTochange = randomTranscription.split("~")[0];
//              	String valueTochange = randomTranscription.split("~")[1];
              	
              	String randomNewValue = RandomString(5);
              	
              	// Choose the transcription to be modified
              	Actions shiftClick = new Actions(DriverContext._Driver);
          		// Get the transcriptions values
              	List<WebElement> transcriptionsList= lstTranscriptions.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
              	for(WebElement f : transcriptionsList)
          		{
          			if (f.getAttribute("Name").equals(randomTranscription)) 
          			{
          				try {
//          					f.click();
          					shiftClick.keyDown(Keys.SHIFT).click(f).keyUp(Keys.SHIFT).perform();
          		    		LogUltility.log(test, logger, "Enter random new value: " + randomNewValue);
          					f.sendKeys(randomNewValue);
      					} catch (Exception e) {
      						System.out.println("not clickable");
      					}
          			}
          		} // end of for
              	
              	//-------------------------------
//              	String randomKey = "";
              	
//          		// Choose the key to be modified
//              	// Get all the available comboboxes in the app
//              	List<WebElement> comboboxesList= SMI_Root_GUI.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ComboBox')]"));
//              	
//              	// Click on the Transcriptions key dropdown
//              	outerLoop : for(WebElement f : comboboxesList)
//          		{ // buttons
//              		String cleanKey = f.getAttribute("Name").substring(0, 3);
//              		
//          			if (cleanKey.equals(keyTochange)) 
//          			{
//          				try {
//          					f.click();
////          					Thread.sleep(2000);
//          					
//          					List<String> keysList = DictionaryTab.getValuesFromApp(f);
//          					randomKey = keysList.get(randomizer.nextInt(keysList.size()));
//          					
//          					// Choose random new key
//          					LogUltility.log(test, logger, "Enter random new key: " + randomKey);
//          					
//          		    		CommonFunctions.chooseValueInDropdown(f, randomKey);
//          		    		break outerLoop;
//          		    		
//      					} catch (Exception f1) {
//      						System.out.println("not clickable");
//      					}
//          			}
//          		} // end of outside for	
              	
          		// Click the add/blue button for transciptions approve
              	DictionaryTab.bt_add_transcription.click();
          		
    
          		
          	} // close function
              
	} // Class end



package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.internal.collections.Pair;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.MainDisplayWindow;
import smi.smorfet.test.pages.MonitorTab;

/**
 * 
 * All tests for Corpus
 *
 */
public class Corpus_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Corpus tab");
	
		// Logger
		logger = LogManager.getLogger(CorpusTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite = Corpus tab");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}

		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(CorpusTab.class);
//			CurrentPage.As(CorpusTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Corpus");
			
			// Click the reset button
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			CorpusTab.bt_reset_search.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * DIC_TC--58:Verify Arrows button works successfully
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_58_Verify_Arrows_button_works_successfully() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--58", "Verify Arrows button works successfully");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		//Choose sentence from the display window
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "ocean" : "אגם";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Pick any random sentence between low and high indexes
		Random randomizer = new Random();
		int Low = 1;
		int High = sentencesList.size();
		int randomRecordIndex = randomizer.nextInt(High-Low) + Low;
		
		String randomRecordFirst = sentencesList.get(randomRecordIndex);
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecordFirst);
		
		//Check if the next arrow works
		//Click the next arrow
		CorpusTab.bt_next.click();
		LogUltility.log(test, logger, "Click the next arrow");
		
		//Retrieve the next sentence
		String nextRecord = sentencesList.get(randomRecordIndex+1);
		List<String> expectedNextRecord =  CorpusTab.getChosenValueInDropdown(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "Retrieve the next sentence: " + expectedNextRecord.get(0));
		
		//Check if the sentences are equal
		boolean compareNext = nextRecord.equals(expectedNextRecord.get(0));
		LogUltility.log(test, logger, "Comparation result: " + compareNext);
		
		assertTrue(compareNext);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
		//Check if the previous arrow works
		//Click the previous arrow
		CorpusTab.bt_prev.click();
		LogUltility.log(test, logger, "Click the previous arrow");
		
		//Retrieve the previous sentence
		String prevRecord = randomRecordFirst;
		List<String> expectedPrevRecord = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "Retrieve the previous sentence: " + expectedPrevRecord.get(0));
		
		//check if the sentences are equal
		boolean comparePrev = prevRecord.equals(expectedPrevRecord.get(0));
		LogUltility.log(test, logger, "Comparation result: " + comparePrev);
		
		assertTrue(comparePrev);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--452 : Check adding comma OR dot to exist sentence will add a new box for it in the top right corpus part
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_452_Check_adding_comma_OR_dot_to_exist_sentence_will_add_a_new_box_for_it_in_the_top_right_corpus_part() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--452", "Check adding comma OR dot to exist sentence will add a new box for it in the top right corpus part");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Pick random number and use go to in order to choose it
		Random randomizer = new Random();
		String status="";
		String randomNumber;
		//Check if the sentence is not active -removed-
		do {
			int  sentenceNumber = randomizer.nextInt(15);
			randomNumber = Integer.toString(sentenceNumber);
			CorpusTab.tb_goto_line.sendKeys(randomNumber);
			LogUltility.log(test, logger, "Inserting number: " + randomNumber);
			
			//Click Enter
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			LogUltility.log(test, logger, "Click Enter");	
			
			// Get sentence status
			status  = CorpusTab.getSenteceStatusAcoordingNumber(randomNumber);
			}
		while(status.equals("not_active"));
		
		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		
		// Currently chosing sentences that does not contain "'s", fix that later
		int wordNumber ;
		String randomWord ;		
		boolean clean = false;
		// The word is non-composition
		do {
			wordNumber = randomizer.nextInt(wordsInRecord.size());
			randomWord = wordsInRecord.get(wordNumber);
			// String contains characters only
			boolean NotcharOnly = !CorpusTab.onlyCharacters(randomWord);
			
			clean = randomWord.contains(" ") || NotcharOnly ;
		}while(clean);
		
		CorpusTab.clickSentencesRecord(randomWord, wordNumber);
		LogUltility.log(test, logger, "Click on: "+randomWord);
	
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Add comma
		String comma = ",";
		CorpusTab.keyType(test, logger, comma);
		LogUltility.log(test, logger, "Test adding comma ");
		
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click on update");
		
		// Go check the updated record
		// Get list of records again
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	

		List<String> updatedWordsInRecord = CorpusTab.getSentencesRecords();
		
		// Comparing
		boolean compareWord = wordsInRecord.get(wordNumber).equals(updatedWordsInRecord.get(wordNumber)) ;
		LogUltility.log(test, logger, "Compare word result: " + compareWord);
		boolean compareComma = updatedWordsInRecord.get(wordNumber+1).equals(",");
		LogUltility.log(test, logger, "Compare comma is updated result: " + compareComma);
		assertTrue(compareWord && compareComma);
		
		LogUltility.log(test, logger, "Starting testing dot \".\"" );
		// Click the revert button
		CorpusTab.bt_revert.click();
		LogUltility.log(test, logger, "Click revert button" );
		
		// Click ok in popup
		CorpusTab.revertFileMsgOK.click();
		LogUltility.log(test, logger, "Click Ok" );
		
		// Click on the sentence again
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// Click word again
		CorpusTab.clickSentencesRecord(randomWord, wordNumber);
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Add dot
		String dot = ".";
		CorpusTab.keyType(test, logger, dot);
		LogUltility.log(test, logger, "Test adding dot ");
		
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click on update");
		
		// Go check the updated record
		// Get list of records again
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// Get updated words with dot
		List<String> updatedWordsInRecordDot = CorpusTab.getSentencesRecords();
		
		// Comparing
		boolean compareWordDot = wordsInRecord.get(wordNumber).equals(updatedWordsInRecordDot.get(wordNumber)) ;
		LogUltility.log(test, logger, "Compare word result: " + compareWordDot);
		boolean compareDot = updatedWordsInRecordDot.get(wordNumber+1).equals(".");
		LogUltility.log(test, logger, "Compare dot is updated result: " + compareDot);
				
		assertTrue(compareDot && compareWordDot);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--470:Check the process to remove words from exist sentence in corpus
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_470_Check_the_process_to_remove_words_from_exist_sentence_in_corpus() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--470", "Check the process to remove words from exist sentence in corpus");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Pick random number and use go to in order to choose it
		Random randomizer = new Random();
		String status="";
		String randomNumber;
		Robot r = new Robot();
		//Check if the sentence is not active -removed-
		do {
			int  sentenceNumber = randomizer.nextInt(15);
			randomNumber = Integer.toString(sentenceNumber);
			CorpusTab.tb_goto_line.sendKeys(randomNumber);
			LogUltility.log(test, logger, "Inserting number: " + randomNumber);
			
			//Click Enter
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			LogUltility.log(test, logger, "Click Enter");	
			
			// Get sentence status
			status  = CorpusTab.getSenteceStatusAcoordingNumber(randomNumber);
			}
		while(status.equals("not_active"));
		
		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		
		// Choose random word 
		int	wordNumber = randomizer.nextInt(wordsInRecord.size());
		String	randomWord = wordsInRecord.get(wordNumber);
		// Click on word
		CorpusTab.clickSentencesRecord(randomWord, wordNumber);
		LogUltility.log(test, logger, "Click on: "+randomWord);
		
		// Click on the word's end
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Delete the word using backspace	
		LogUltility.log(test, logger, "Press backspace to delete the word");
		
		int size = randomWord.length();
		while(size-- > 0)
			{CorpusTab.keyType(test, logger, "\b");}
		
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click on update");
		
		// Go check the updated record
		// Get list of records again
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// Get sentence after updating
		List<String> updatedWordsInRecord = CorpusTab.getSentencesRecords();
		
		// Compare changes in length, they shouldn't be equal
		boolean isDeleted = !(wordsInRecord.size() == updatedWordsInRecord.size());
		
		LogUltility.log(test, logger, "Length before deletion: "+wordsInRecord.size()+" Length after deletion: "+updatedWordsInRecord.size());	
		LogUltility.log(test, logger, "Compare result should be true: "+isDeleted);	
		assertTrue(isDeleted);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--247:Check the new added sentence are saved correctly in the database file
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_247_Check_the_new_added_sentence_are_saved_correctly_in_the_database_file() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--247", "Check the new added sentence are saved correctly in the database file");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
					
		//Click the new button in the footer
		CorpusTab.bt_new.click();
		//Click ok 
		CorpusTab.bt_ok_new.click();
		
		//Insert the sentence in the field
		String sentenceToAdd = Setting.Language.equals("EN")? "I like to go to the ocean" : "אני אוהב שוקולד";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		
		//Click new button in footer
		MonitorTab.bt_new.click();
		//Confirm and click ok
		MonitorTab.bt_ok.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord =  Setting.Language.equals("EN")? "ocean" : "שוקולד";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			CorpusTab.tabCorpus.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		/*Check if the new sentence is added to the records display window*/

		//Retrieve the last added sentence including it's serial number
		String newSentence = CommonFunctions.getLastValueFromApp(CorpusTab.lb_corpus_records);
		//Eliminate the serial number 
		String sentence = newSentence.substring(6);
		LogUltility.log(test, logger, "Retrieve the new sentence from the display window: " + sentence);
		boolean result = sentence.equals(sentenceToAdd);
		LogUltility.log(test, logger, "Comparation result: " + result);
		assertTrue(result);
		
		/*check if the new sentence saved in the database file*/	
		List<String> corpusData = CorpusTab.getRecordsFromCorpus();
		LogUltility.log(test, logger, "Retrieve the new sentence from the corpus file: " + corpusData.get(corpusData.size()-1));
		
		result = corpusData.get(corpusData.size()-1).equals(sentenceToAdd);
		LogUltility.log(test, logger, "Comparation result: " + result);
		assertTrue(result);
		 
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}

	/**
	 * DIC_TC--253:Check the deleted sentence status in the database file
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_253_Check_the_deleted_sentence_status_in_the_database_file() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--253", "Check the deleted sentence status in the database file");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		//Retrieve corpus file data
//		List<String> corpusData = CorpusTab.getRecordsFromCorpus();	
		List<String> corpusData = CorpusTab.getFirst15Senteces();	
		List<String> sentencesList = corpusData.subList(0, 20 > corpusData.size() ? corpusData.size() : 20);

		// Pick random activated sentence between low and high indexes
		int counter = 0;
		Random randomizer = new Random();
		int Low = 1;
		int High = sentencesList.size();
		int randomRecordIndex;
		String randomRecord;
		String recordStatus;
		
		do {
			counter++;
			randomRecordIndex = randomizer.nextInt(High-Low) + Low;;
			randomRecord= sentencesList.get(randomRecordIndex);
			recordStatus = CorpusTab.getSenteceStatus(randomRecord);
			//Avoid infinite loop
			if(counter>=sentencesList.size())
				break;
		}while(recordStatus.equals("not_active"));
		
		//If no activated sentence were found
		if(recordStatus.equals("not_active"))
		{
			CorpusTab.chooseValueInDropdownContains(CorpusTab.lb_corpus_records, randomRecord);
			
			//Click the delete button
			CorpusTab.bt_revive.click();
			LogUltility.log(test, logger, "Click the revive button");
			
			//Click the save button
			CorpusTab.bt_save.click();
			CorpusTab.bt_ok_save.click();
			LogUltility.log(test, logger, "Click the Save button");
			
			// Change the last filter to "all records", in case it was changed by other TC
			CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
			LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		}
		
		
		CorpusTab.chooseValueInDropdownContains(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
		//Click the delete button
		CorpusTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// For syncing
			CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
			LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//Check if the sentence status has been changed in the file
		boolean sentenceIsDeleted = false;
		recordStatus = CorpusTab.getSenteceStatus(randomRecord);
		if(recordStatus.equals("not_active"))
			sentenceIsDeleted = true;
		
		LogUltility.log(test, logger, "Comparation result should be not_active: " + sentenceIsDeleted);	
		assertTrue(sentenceIsDeleted);
		
		
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--498:Check Retrieved results number
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_498_Check_Retrieved_results_number() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--498", "Check Retrieved results number");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
				
		//Retrieve records from corpus display window
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "ocean" : "אגם";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		//Get the results number
		String retRecords = CorpusTab.tb_retrieved_records.getText();
		String splitMessage [] = retRecords.split(":");
		int retRecordsNum = Integer.parseInt(splitMessage[1]);
		LogUltility.log(test, logger, "Retrieved records number: " + retRecordsNum);
		
		CorpusTab.bt_revert.click();
		LogUltility.log(test, logger, "Click revert button" );
		
		// Click ok in popup
		CorpusTab.revertFileMsgOK.click();
		LogUltility.log(test, logger, "Click Ok" );
		
		assertEquals(retRecordsNum, sentencesList.size());
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--613:Test the delete/revive button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_613_Test_the_delete_revive_button() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--613", "Test the delete/revive button");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		

//		// Choose value from the first search dropdown
//		String firstDD = "status";
//   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
//   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");
//
//   		// Choose value from the condition dropdown
//   		String conditionDropdownValue = "equal";
//   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
//   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");
//
//   		// Choose random value
//   		List<String> statusValues = CorpusTab.getValuesFromApp(CorpusTab.cmpCondition_value);
//   		Random random = new Random();
//   		String chosenStatus = "";
//   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
// 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
//   		
//   		// Change the last filter value, in case it was changed by other TC
// 		String filterDropdownValue = "all records";
//
//   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
//   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
//   		
//   		// Click the Retrieve button
//   		CorpusTab.btnRetrieve_same.click();
//   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		
		//Retrieve corpus file data
//		List<String> corpusData = CorpusTab.getRecordsFromCorpus();	
		List<String> corpusData = CorpusTab.getFirst15Senteces();	
		List<String> sentencesList = corpusData.subList(0, 20 > corpusData.size() ? corpusData.size() : 20);

		// Pick random deleted sentence between low and high indexes
		int counter = 0;
		int randomRecordIndex;
		String randomRecord;
		String recordStatus;
		do {
			//*********************
			// changed randomRecordIndex to check somethingSS
			randomRecordIndex = counter++;
//			randomRecordIndex = randomizer.nextInt(High-Low) + Low;;
			randomRecord= sentencesList.get(randomRecordIndex);
			recordStatus = CorpusTab.getSenteceStatus(randomRecord);
			//Avoid infinite loop
			if(counter>=sentencesList.size())
				break;
		}while(recordStatus.equals("fully_tagged"));
		
		//If no deleted sentence were found
		if(recordStatus.equals("fully_tagged"))
		{
			CorpusTab.chooseValueInDropdownContains(CorpusTab.lb_corpus_records, randomRecord);
			
			//Click the delete button
			CorpusTab.bt_delete.click();
			LogUltility.log(test, logger, "Click the delete button");
			
			//Click the save button
			CorpusTab.bt_save.click();
			CorpusTab.bt_ok_save.click();
			LogUltility.log(test, logger, "Click the Save button");
			
			// Change the last filter to "all records", in case it was changed by other TC
			CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
			LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		}
		
		CorpusTab.chooseValueInDropdownContains(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
		String actualWarning = CorpusTab.getTaggingWindowStatus();
		//Check if the tagging window displays the warning
		String expectedWarning = "This sentence is de-activated.";
		
		boolean windowMessageMatch = actualWarning.contains(expectedWarning);
		
		LogUltility.log(test, logger, "Tagging window comparation result: " + windowMessageMatch);	
		assertTrue(windowMessageMatch);
		
		//Click the revive button
		CorpusTab.bt_revive.click();
		LogUltility.log(test, logger, "Click the revive button");
		
		//Click the next button
		CorpusTab.bt_next.click();
		LogUltility.log(test, logger, "Click the next button");
		
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		//Thread.sleep(3000);
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		//Check if the sentence status has been changed in the file
		boolean sentenceIsRevived = false;
		recordStatus = CorpusTab.getSenteceStatus(randomRecord);
		if(recordStatus.equals("fully_tagged"))
			sentenceIsRevived = true;
		
		LogUltility.log(test, logger, "Comparation result: " + sentenceIsRevived);	
		assertTrue(sentenceIsRevived);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--302:Check "Go to line"
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_302_Check_Go_to_line() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--302", "Check 'Go to line' ");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "like" : "אגם";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Retrieve sentences
		List<String> sentencesList =  CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		//Get the available serial numbers from the sublist
		ArrayList<Integer> sentencesNumbers = new ArrayList<Integer>() ;
		
		for(int i=0;i<sentencesList.size();i++)
		{
			String sentence = sentencesList.get(i);
			String subString = sentence.substring(0, 6);
			String trimmed = subString.trim();
			sentencesNumbers.add(Integer.parseInt(trimmed));
		}
		
		// Pick random number to search for
		Random randomizer = new Random();
		int randomRecordIndex = sentencesNumbers.get(randomizer.nextInt(sentencesNumbers.size()));
		
		//Insert value in the 'Go To' field
		String randomNumber = Integer.toString(randomRecordIndex);
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		
	
		
		//Retrieve the selected sentence
		List<String> selectedResult = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_corpus_records);
		String actualSubString = selectedResult.get(0).substring(0, 6);
		String actualTrimmed = actualSubString.trim();
		boolean compareNumber = randomNumber.equals(actualTrimmed);
		assertTrue(compareNumber);
		
		LogUltility.log(test, logger, "Comparation result: " + compareNumber);	
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--308:Check typing characters in "Go to line"
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_308_Check_typing_characters_in_Go_to_line() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--308", "Check typing characters in 'Go to line' ");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		//Insert characters value in the 'Go To' field
		String randomString = CorpusTab.RandomString(3) ;
		CorpusTab.tb_goto_line.sendKeys(randomString);
		LogUltility.log(test, logger, "Inserting charechters: " + randomString);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		
		boolean popupMessage = CorpusTab.popupWindowMessage(test,logger,"Corpus Go To Line Message:","invalid line number!");
		assertTrue(popupMessage);
		
		LogUltility.log(test, logger, "Comparation result: " + popupMessage);	
		//click ok button
		CorpusTab.bt_ok_save.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--309:Check typing out of range line number in "Go to line"
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_309_Check_typing_out_of_range_line_number_in_Go_to_line() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--309", "Check typing out of range line number in 'Go to line' ");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		//Insert out of range value in the 'Go To' field
		Random randomizer = new Random();
		//max=-1 min=-10
		int randomNegativeValue = randomizer.nextInt(10) - 10;
		CorpusTab.tb_goto_line.sendKeys(Integer.toString(randomNegativeValue));
		LogUltility.log(test, logger, "Inserting charechters: " + randomNegativeValue);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		
		boolean popupMessage = CorpusTab.popupWindowMessage(test,logger,"Corpus Go To Line Message:","invalid record number !");
		assertTrue(popupMessage);
		
		LogUltility.log(test, logger, "Comparation result: " + popupMessage);	
		
		// Click the ok button
		CorpusTab.bt_ok_save.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--14:Verify the process of tagging words in a phrases
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_14_Verify_the_process_of_tagging_words_in_a_phrases() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--14", "Verify the process of tagging words in a phrases");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		CurrentPage = GetInstance(MonitorTab.class);
		MonitorTab MonitorTab = CurrentPage.As(MonitorTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// For Hebrew	
		//Click the new button in the footer
		String sentenceToAdd = Setting.Language.equals("EN")? "Hello how are you" : "אני בן חמש";
		Random randomizer = new Random();

		CorpusTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		//Click ok 
		CorpusTab.bt_ok_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		//Insert the sentence in the field
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Add sentence: "+sentenceToAdd);

		//Click new button in footer
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		//Confirm and click ok
		MonitorTab.bt_ok.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		// Tag all the words
		int wordIndex = 0;
		List<String> wordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + wordsList);
		
	for (String record : wordsList) {
		LogUltility.log(test, logger, "Record to tag: " + record);
		
		// Choose a record
		CorpusTab.clickSentencesRecord(record, wordIndex++);
		
		// Get all tagging options
		List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
		LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
	
		// Random choose another option from the taggingOptions
		List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
		System.out.println("choose random tagging option: " + randomTaggingOption);
		
		// Choose tagging option
		try {
			CorpusTab.chooseTaggingOption(randomTaggingOption);
			LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
		} catch (Exception e) {
			System.out.println("exception: " + e);}
		}
		
		
		// Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Saving...: ");
		
		// Wait for the save to finish
		try {
			CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");	
		} catch (Exception e) {
			// TODO: handle exception
		}

		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord =  sentenceToAdd ;
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retrieve button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve sentences
//		List<String> sentencesList = CorpusTab.getValuesFromApp(CorpusTab.lb_corpus_records);
//		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Pick random number to search for
		// Currently chosing sentences that does not contain "'s", fix that later
//		boolean clean = false;
//		String randomSentence = "";
//		do {
//			randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
//			clean = randomSentence.contains("'");
//		} while (clean);
//		String randomSentence = " 5122 The viewpoint overlooked the ocean .";
		//*****
		String randomSentence = CommonFunctions.getLastValueFromApp(CorpusTab.lb_corpus_records);
		String sentenceNumber = randomSentence.substring(0, 5).trim();
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// GO OVER ALL THE RECORDS IN THE CHOSEN SENTENCE, CHOOSE A NEW TAGGING AND SAVE THE NEW CHOSEN VALUES
		// TO BE CHECKED LATER
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);

//		HashMap<String, List<String>> saveChangeTagging = new HashMap<String, List<String>>();
		Map<Pair<Integer, String>, List<String>> saveChangeTagging = new HashMap<Pair<Integer, String>, List<String>>();
		
		int recordNumber = 0;
		LogUltility.log(test, logger, "Records list: " + recordsList);
		for (String record : recordsList) {
			
			LogUltility.log(test, logger, "------------------------");
			LogUltility.log(test, logger, "Record to tag: " + record);
			
			// Choose a record
			CorpusTab.clickSentencesRecord(record, recordNumber);
			System.out.println("Choose a record");
//			Thread.sleep(3000);
			
			// Get the currently chosen tag
			List<String> chosenRecord = CorpusTab.getRecordTaggingFromCorpusFile(sentenceNumber, record);
//			List<String> chosenRecord = CorpusTab.getRecordTaggingFromCorpusFile("The viewpoint overlooked the ocean .", "overlooked");

			LogUltility.log(test, logger, "Current tagging: " + chosenRecord);

			// Get all tagging options
			List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
			LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
			
			// Remove the already chosen tag from the tagging options if they are more than 1
			if (taggingOptions.size() != 1)
				taggingOptions.remove(chosenRecord);
			System.out.println("remove the current record from list");
			
			// Random choose another option from the taggingOptions
			List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
			System.out.println("choose random tagging option: " + randomTaggingOption);
			
			// Choose tagging option
			try {
				CorpusTab.chooseTaggingOption(randomTaggingOption);
				LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
			} catch (Exception e) {
				System.out.println("exception: " + e);
			}
			
			// Save what is chosen for later check
			saveChangeTagging.put(new Pair<Integer, String>(recordNumber, record), randomTaggingOption);
			recordNumber++;
		}
		
		// Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "------------------");
		LogUltility.log(test, logger, "------------------");
		LogUltility.log(test, logger, "Saving...: ");
		
		// Wait for the save to finish
		try {
			CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		recordNumber = 0;
		// Check now that the new tagged are saved correctly
		LogUltility.log(test, logger, "Check new tagging");
		for (String record : recordsList) {
			LogUltility.log(test, logger, "check record: " + record);
			// Choose a record
			CorpusTab.tabCorpus.click();
			CorpusTab.clickSentencesRecord(record, recordNumber);
			
			// Get the currently chosen tag
			List<String> chosenRecord = CorpusTab.getRecordTaggingFromCorpusFile(sentenceNumber, record);
			LogUltility.log(test, logger, "New tagging: " + chosenRecord);
			
			// Check and compare the currently chosen tagging which the recently picked values
//			saveChangeTagging.get(Pair<Integer, String>(recordNumber, record));
			List<String> taggedOption = saveChangeTagging.get(new Pair<Integer, String>(recordNumber, record));
			LogUltility.log(test, logger, "Previous tagged tagging: " + taggedOption);
			
			boolean savedCorrectly = chosenRecord.equals(taggedOption);
			LogUltility.log(test, logger, "chosed correctly: " + savedCorrectly);
			assertTrue(savedCorrectly);
			
			recordNumber++;
		}
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--29:verify user can edit records in corpus
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_29_verify_user_can_edit_records_in_corpus() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--29", "verify user can edit records in corpus");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = Setting.Language.equals("EN") ? "ocean" : "ילד";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retrieve button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
//		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Pick random number to search for
		Random randomizer = new Random();
		// Currently chosing sentences that does not contain "'s", fix that later
		boolean clean = false;
		String randomSentence = "";
		do {
			randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			clean = randomSentence.contains("'");
		} while (clean);
//		Random randomizer = new Random();
//		String randomSentence = " 5122 The viewpoint overlooked the ocean .";
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);

		// Get list of the records for the chosen sentence
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);

		// Choose and click on random record
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		LogUltility.log(test, logger, "Click random record " + randomRecord);
		int recordNumber = recordsList.indexOf(randomRecord);
		CorpusTab.clickSentencesRecord(randomRecord, recordNumber);
		
//		// Click End to move cursor to end of the record
//		Actions endClick = new Actions(DriverContext._Driver);
//		endClick.keyDown(Keys.END).perform();
////		Thread.sleep(1000);
//		endClick.keyUp(Keys.END).perform();
		
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_END);
//		Thread.sleep(1000);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Add text
		String str = CorpusTab.RandomString(3);
//		CorpusTab.keyType(test, logger, str);
		CorpusTab.keyType(test, logger, str);
//	    r.keyPress(str);
//		endClick.sendKeys(str);
		
		// Click the update button
		CorpusTab.bt_update.click();
		
		// Go check the updated record
		// Get list of records again
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list after change: " + recordsList);
		String modifiedRecord = recordsList.get(recordNumber);
		
		// Comparing
		boolean compare = modifiedRecord.equals(randomRecord + str);
		LogUltility.log(test, logger, "compare: " + compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--52:Verify the "Delete" button function as expected
	 * @throws FileNotFoundException 
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_52_Verify_the_Delete_button_function_as_expected() throws FileNotFoundException, InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--52", "DIC_TC--52:Verify the \"Delete\" button function as expected");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Pick random number and use go to in order to choose it
		Random randomizer = new Random();
		int  sentenceNumber = randomizer.nextInt(15);
		
		String randomNumber = Integer.toString(sentenceNumber);
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		
		// Click the delete button
		CorpusTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");
		
		// Choose again the deleted sentence
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Enter agin the sentence number in go to: " + randomNumber);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		
		// Check the system message
		String actualWarning = CorpusTab.getTaggingWindowStatus();
		boolean windowMessageMatch = actualWarning.contains("This sentence is de-activated.");
		LogUltility.log(test, logger, "Tagging window comparation result: " + windowMessageMatch);	
		assertTrue(windowMessageMatch);
		
		// Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// For the test to wait
		try {
			CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		} catch (Exception e) {
			// TODO: handle exception
		}

		//Check if the sentence status has been changed in the file
		String status = CorpusTab.getSenteceStatusAcoordingNumber(randomNumber);
		LogUltility.log(test, logger, "Status of the deleted sentence: " + status);
		boolean statusCheck = status.equals("not_active");
		assertTrue(statusCheck);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--192:Verify the record versions in the corpus are the same as in the lexicon
	 * @throws FileNotFoundException 
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_192_Verify_the_record_versions_in_the_corpus_are_the_same_as_in_the_lexicon() throws FileNotFoundException, InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--192", "Verify the record versions in the corpus are the same as in the lexicon");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Pick random number and use go to in order to choose it
		Random randomizer = new Random();
		String status="";
		//Check if the sentence is not active -removed-
		do {
			int  sentenceNumber = randomizer.nextInt(15);
			String randomNumber = Integer.toString(sentenceNumber);
			CorpusTab.tb_goto_line.sendKeys(randomNumber);
			LogUltility.log(test, logger, "Inserting number: " + randomNumber);
			
			//Click Enter
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			LogUltility.log(test, logger, "Click Enter");	
			
			// Get sentence status
			status  = CorpusTab.getSenteceStatusAcoordingNumber(randomNumber);
			}
		while(status.equals("not_active"));
		
		// Get the words from record separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();
		
		// Choose random word from the right pane
		String randomWord = wordsInRecord.get(randomizer.nextInt(wordsInRecord.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.pn_phrase, randomWord);
		LogUltility.log(test, logger, "Click on: "+randomWord);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--210:Verify user can revert changes in corpus - part 1 tagging records
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_210_Verify_user_can_revert_changes_in_corpus_part_1_tagging_records() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--210", "DIC_TC--210:Verify user can revert changes in corpus - part 1 tagging records");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = Setting.Language.equals("EN") ? "ocean" : "ילד";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retrieve button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
//		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Pick random number to search for
		Random randomizer = new Random();
		// Currently chosing sentences that does not contain "'s", fix that later
		boolean clean = false;
		String randomSentence = "";
		do {
			randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			clean = randomSentence.contains("'");
		} while (clean);
//		Random randomizer = new Random();
//		String randomSentence = " 5122 The viewpoint overlooked the ocean .";
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		String sentenceNumber = randomSentence.substring(0, 5).trim();
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// GO OVER ALL THE RECORDS IN THE CHOSEN SENTENCE, CHOOSE A NEW TAGGING AND SAVE THE NEW CHOSEN VALUES
		// TO BE CHECKED LATER
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);

//		HashMap<String, List<String>> saveChangeTagging = new HashMap<String, List<String>>();
		Map<Pair<Integer, String>, List<String>> saveChangeTagging = new HashMap<Pair<Integer, String>, List<String>>();
		
		int recordNumber = 0;
		LogUltility.log(test, logger, "Records list: " + recordsList);
		for (String record : recordsList) {
			
			LogUltility.log(test, logger, "------------------------");
			LogUltility.log(test, logger, "Record to tag: " + record);
			
			// Choose a record
			CorpusTab.clickSentencesRecord(record, recordNumber);
			System.out.println("Choose a record");
//			Thread.sleep(3000);
			
			// Get the currently chosen tag
			List<String> chosenRecord = CorpusTab.getRecordTaggingFromCorpusFile(sentenceNumber, record);
//			List<String> chosenRecord = CorpusTab.getRecordTaggingFromCorpusFile("The viewpoint overlooked the ocean .", "overlooked");

			LogUltility.log(test, logger, "Current tagging: " + chosenRecord);

			// Get all tagging options
			List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
			LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
			
			// Remove the already chosen tag from the tagging options if they are more than 1
			if (taggingOptions.size() != 1)
				taggingOptions.remove(chosenRecord);
			System.out.println("remove the current record from list");
			
			// Random choose another option from the taggingOptions
			List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
			LogUltility.log(test, logger, "choose random tagging option: " + randomTaggingOption);
			
			// Choose tagging option
			try {
				CorpusTab.chooseTaggingOption(randomTaggingOption);
				LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
			} catch (Exception e) {
				System.out.println("exception: " + e);
			}
			
			// Save the current tagging for later check
			saveChangeTagging.put(new Pair<Integer, String>(recordNumber, record), chosenRecord);
			recordNumber++;
		}
		
		// Click the save button
		CorpusTab.bt_revert.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "------------------");
		LogUltility.log(test, logger, "------------------");
		LogUltility.log(test, logger, "Reverting...: ");
		
		// Wait for the revert to finish
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		
		recordNumber = 0;
		// Check now that the old tags do exist after reverting
		LogUltility.log(test, logger, "Check old tagging");
		for (String record : recordsList) {
			LogUltility.log(test, logger, "check record: " + record);
			// Choose a record
			CorpusTab.tabCorpus.click();
			CorpusTab.clickSentencesRecord(record, recordNumber);
			
			// Get the currently chosen tag
			List<String> chosenRecord = CorpusTab.getRecordTaggingFromCorpusFile(sentenceNumber, record);
			LogUltility.log(test, logger, "New tagging: " + chosenRecord);
			
			// Check and compare the currently chosen tagging which the recently picked values
			// Save what is chosen for later check
//			saveChangeTagging.get(Pair<Integer, String>(recordNumber, record));
			List<String> taggedOption = saveChangeTagging.get(new Pair<Integer, String>(recordNumber, record));
			LogUltility.log(test, logger, "Previous tagged tagging: " + taggedOption);
			
			boolean savedCorrectly = chosenRecord.equals(taggedOption);
			LogUltility.log(test, logger, "chosed correctly: " + savedCorrectly);
			assertTrue(savedCorrectly);
			
			recordNumber++;
		}
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--210:Verify user can revert changes in corpus - part 2 edit records
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_210_Verify_user_can_revert_changes_in_corpus_part_2_edit_records() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--210", "Verify user can revert changes in corpus - part 2 edit records");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
//		// Search in corpus and that in order to have less number of sentences to work with
//		// Choose "form" from the first search dropdown
//		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
//		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//
//		// Choose "contains" from the condition dropdown
//		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
//		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
//
//		//Type in the search field
//		String searchWord = Setting.Language.equals("EN") ? "ocean" : "ילד";
//		CorpusTab.cmpCondition_value.sendKeys(searchWord);
//		LogUltility.log(test, logger, "word is typed in the search field");
//		
//		// Change the last filter to "all records", in case it was changed by other TC
//		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
//		
//		// Click the Retrieve button
//		CorpusTab.btnRetrieve_same.click();
//		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
//		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Pick random number to search for
		Random randomizer = new Random();
		// Currently chosing sentences that does not contain "'s", fix that later
		boolean clean = false;
		String randomSentence = "";
		do {
			randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			clean = randomSentence.contains("'");
		} while (clean);
//		Random randomizer = new Random();
//		String randomSentence = " 5122 The viewpoint overlooked the ocean .";
			
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);

		// Get list of the records for the chosen sentence
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);

		// Choose and click on random record
		String randomRecordForEdit = recordsList.get(randomizer.nextInt(recordsList.size()));
		LogUltility.log(test, logger, "Click random record for edit: " + randomRecordForEdit);
		int recordNumber = recordsList.indexOf(randomRecordForEdit);
		CorpusTab.clickSentencesRecord(randomRecordForEdit, recordNumber);
		
//		// Click End to move cursor to end of the record
//		Actions endClick = new Actions(DriverContext._Driver);
//		endClick.keyDown(Keys.END).perform();
////		Thread.sleep(1000);
//		endClick.keyUp(Keys.END).perform();
		
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_END);
//		Thread.sleep(1000);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Add text
		String str = CorpusTab.RandomString(3);
//		CorpusTab.keyType(test, logger, str);
		CorpusTab.keyType(test, logger, str);
//	    r.keyPress(str);
//		endClick.sendKeys(str);
		
		// Click the revert button
		CorpusTab.bt_revert.click();
		CorpusTab.bt_ok_new.click();
		
		// Wait for the revert to finish
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		
		// Click the Retrieve button again
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose the same random sentence again
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Go check the reverted record
		// Get list of records again
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list after change and revert: " + recordsList);
		String modifiedRecord = recordsList.get(recordNumber);
		
		// Comparing
		boolean compare = modifiedRecord.equals(randomRecordForEdit);
		LogUltility.log(test, logger, "compare: " + compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--210:Verify user can revert changes in corpus - part 3 modify left side dropdowns
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_210_Verify_user_can_revert_changes_in_corpus_part_3_modify_left_side_dropdowns() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--210", "Verify user can revert changes in corpus - part 3 modify left side dropdowns");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = Setting.Language.equals("EN") ? "ocean" : "ילד";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retrieve button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,30);
//		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Pick random number to search for
		Random randomizer = new Random();
		// Currently chosing sentences that does not contain "'s", fix that later
		boolean clean = false;
		String randomSentence = "";
		do {
			randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			clean = randomSentence.contains("'");
		} while (clean);
//		Random randomizer = new Random();
//		String randomSentence = " 5122 The viewpoint overlooked the ocean .";
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);

		// Chose random dropdown
		LogUltility.log(test, logger, "Chosen random dropdown");
		List<WebElement> allDropdowns = CorpusTab.getListsAndDropdowns("dd"); 
		WebElement randomDropdown = allDropdowns.get(randomizer.nextInt(allDropdowns.size()));
		LogUltility.log(test, logger, "dropdown: " + randomDropdown.getAttribute("Name"));
		
		// Get the current value in the chosen dropdown
		List<String> currentValue = CorpusTab.getChosenValueInDropdown(randomDropdown);
		LogUltility.log(test, logger, "Current value: " + currentValue.get(0));
		
		// All available values in that dropdown
		List<String> allValues =  CommonFunctions.getValuesFromApp(randomDropdown);
		
		// Choose a new value
		allValues.remove(currentValue.get(0));
		String randomNewValue = allValues.get(randomizer.nextInt(allValues.size()));
		LogUltility.log(test, logger, "Random new value to choose: " + randomNewValue);
		CommonFunctions.chooseValueInDropdown(randomDropdown, randomNewValue);

		// Click the revert button
		CorpusTab.bt_revert.click();
		CorpusTab.bt_ok_new.click();
		
		// Wait for the revert to finish
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		
		// Click the Retrieve button again
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose the same random sentence again
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Go check the reverted dropdowns
		List<String> currentValueAfterRevert = CorpusTab.getChosenValueInDropdown(randomDropdown);
		LogUltility.log(test, logger, "Current value after revert: " + currentValueAfterRevert.get(0));

		// Comparing
		boolean compare = currentValue.equals(currentValueAfterRevert);
		LogUltility.log(test, logger, "compare: " + compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--210:Verify user can revert changes in corpus - part 3 modify left side lists
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_210_Verify_user_can_revert_changes_in_corpus_part_3_modify_left_side_lists() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--210", "Verify user can revert changes in corpus - part 3 modify left side lists");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = Setting.Language.equals("EN") ? "ocean" : "ילד";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retrieve button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,30);
//		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Pick random number to search for
		Random randomizer = new Random();
		// Currently chosing sentences that does not contain "'s", fix that later
		boolean clean = false;
		String randomSentence = "";
		do {
			randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			clean = randomSentence.contains("'");
		} while (clean);
//		Random randomizer = new Random();
//		String randomSentence = " 5122 The viewpoint overlooked the ocean .";
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);

		// Choose random list
		LogUltility.log(test, logger, "Chosen random list");
		List<WebElement> allList = CorpusTab.getListsAndDropdowns("lb"); 
		WebElement randomList = allList.get(randomizer.nextInt(allList.size()));
		LogUltility.log(test, logger, "list: " + randomList.getAttribute("Name"));
		
		// Get the current values in the chosen list
		List<String> currentValue = CorpusTab.getChosenValueInDropdown(randomList);
		LogUltility.log(test, logger, "Current values: " + currentValue);
		
		// All available values in that list
		List<String> allValues =  CommonFunctions.getValuesFromApp(randomList);
		
		// Choose a new value
		allValues.removeAll(currentValue);
		String randomNewValue = allValues.get(randomizer.nextInt(allValues.size()));
		LogUltility.log(test, logger, "Random new value to choose: " + randomNewValue);
		CommonFunctions.chooseValueInDropdown(randomList, randomNewValue);

		// Click the revert button
		CorpusTab.bt_revert.click();
		CorpusTab.bt_ok_new.click();
		
		// Wait for the revert to finish
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		
		// Click the Retrieve button again
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose the same random sentence again
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);
		
		// Go check the reverted list
		List<String> currentValueAfterRevert = CorpusTab.getChosenValueInDropdown(randomList);
		LogUltility.log(test, logger, "Current values after revert: " + currentValueAfterRevert);

		// Comparing
		boolean compare = currentValue.equals(currentValueAfterRevert);
		LogUltility.log(test, logger, "compare: " + compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC_489_Check_the_update_function_can_handle_deleting_multiple_words_in_one_shoot
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	
	public void DIC_TC_489_Check_the_update_function_can_handle_deleting_multiple_words_in_one_shoot() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--489", "Check the update function can handle deleting multiple words in one shoot");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Pick random number and use go to in order to choose it
		Random randomizer = new Random();
		String status="";
		String randomNumber;
		Robot r = new Robot();
		List<String> wordsInRecord;
		int multipleDeletion = 3;
		
		//Check if the sentence is not active -removed-, and it's length is bigger than 3
		do {
			int  sentenceNumber = randomizer.nextInt(15);
			randomNumber = Integer.toString(sentenceNumber);
			CorpusTab.tb_goto_line.sendKeys(randomNumber);
			LogUltility.log(test, logger, "Inserting number: " + randomNumber);
			
			//Click Enter
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			LogUltility.log(test, logger, "Click Enter");	
			
			// Get sentence status
			status  = CorpusTab.getSenteceStatusAcoordingNumber(randomNumber);
			
			// Get the words from sentence separately
			wordsInRecord = CorpusTab.getSentencesRecords();	
			}
		while(status.equals("not_active") && wordsInRecord.size()>=3);
		
		// Delete multiple words
		while(multipleDeletion-- > 0)
		{
			// Choose first 3 words 
			int	wordNumber = multipleDeletion;
			String	randomWord = wordsInRecord.get(wordNumber);
			
			// Click on word
			CorpusTab.clickSentencesRecord(randomWord, wordNumber);
			LogUltility.log(test, logger, "Click on: "+randomWord);
			
			// Click on the word's end
			r.keyPress(KeyEvent.VK_END);
			r.delay(100);
			r.keyRelease(KeyEvent.VK_END);
			
			// Delete the word using backspace	
			LogUltility.log(test, logger, "Press backspace to delete the word");
			
			int size = randomWord.length();
			while(size-- > 0)
				{CorpusTab.keyType(test, logger, "\b");}
		}

		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click on update");
		
		// Go check the updated record
		// Get list of records again
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// Get sentence after updating
		List<String> updatedWordsInRecord = CorpusTab.getSentencesRecords();
		
		// Compare changes in length, they shouldn't be equal
		boolean isDeleted = !(wordsInRecord.size() == updatedWordsInRecord.size());
		
		LogUltility.log(test, logger, "Length before deletion: "+wordsInRecord.size()+" Length after deletion: "+updatedWordsInRecord.size());	
		LogUltility.log(test, logger, "Compare result should be true: "+isDeleted);	
		assertTrue(isDeleted);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--454:Check the Mass Attribution button - part1 in dropdowns
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_454_Check_the_Mass_Attribution_button_part1_in_dropdowns() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--454", "Check the Mass Attribution button - part1 in dropdowns");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in order to have less number of sentences
		String searchWord = Setting.Language.equals("EN")? "my boy" : "הילד";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Click on the mass button
		CorpusTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the Mass button");
		
		// Available dropdowns on the left in Corpus 
		Random randomizer = new Random();
		String[] dropDownLists = {"audience","complexity","formality","medium"};
		String chosenDD = dropDownLists[randomizer.nextInt(dropDownLists.length)];
		
		// Chosen dropdown webelement
		WebElement lstBox = null;
		switch(chosenDD)
		{
		case "audience":
			 lstBox = CorpusTab.dd_audience;break;
		case "complexity":
			 lstBox = CorpusTab.dd_complexity;break;
		case "formality":
			 lstBox = CorpusTab.dd_formality;break;
		case "medium":
			 lstBox = CorpusTab.dd_medium;
		}
		
		// Choose first dropdown value
		CommonFunctions.chooseValueInDropdown(CorpusTab.massChangeItsDD, chosenDD);
		LogUltility.log(test, logger, "Change value in changes its dropdown: "+chosenDD);
		
		// Get value before change
		List<String> valueB4Change = CorpusTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Value before change in dropdown: "+valueB4Change);
				
		// Choose second dropdown value
		List<String> dropdownValues = CommonFunctions.getValuesFromApp(CorpusTab.massToBeDD);
		String chosenDD2 = dropdownValues.get(randomizer.nextInt(dropdownValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.massToBeDD, chosenDD2);
		LogUltility.log(test, logger, "Change value in to be dropdown: "+chosenDD2);
		
		// Click on change
		CorpusTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change buton");
		
		// Get dropdown value
		List<String> valueAfterChange = CorpusTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Value after change in dropdown: "+valueAfterChange);
		
		boolean isChanged = valueAfterChange.get(0).equals(chosenDD2);
		LogUltility.log(test, logger, "The value is changed: "+isChanged);
		assertTrue(isChanged);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--454:Check the Mass Attribution button - part2 in marking fields with No
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_454_Check_the_Mass_Attribution_button_part2_in_marking_fields_with_No() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--454", "Check the Mass Attribution button - part2 in marking fields with No");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in order to have less number of sentences
		String searchWord = Setting.Language.equals("EN")? "my boy" : "הילד";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Click on the mass button
		CorpusTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the Mass button");
		
		// Available marking fields on the left in Corpus 
		Random randomizer = new Random();
		String[] fieldsLists = {"sentence type","social status","speech act","lingo","mood","dialect"};
		String chosenMF = fieldsLists[randomizer.nextInt(fieldsLists.length)];

		// Chosen field webelement
		WebElement lstBox = null;
		switch(chosenMF)
		{
		case "sentence type":
			 lstBox = CorpusTab.lb_sentence_type;break;
		case "social status":
			 lstBox = CorpusTab.lb_social_status;break;
		case "speech act":
			 lstBox = CorpusTab.lb_speech_act;break;
		case "lingo":
			 lstBox = CorpusTab.lb_lingo;break;
		case "mood":
			 lstBox = CorpusTab.lb_mood;break;
		case "dialect":
			 lstBox = CorpusTab.lb_dialect;
		}
		
		// Choose first dropdown value
		CommonFunctions.chooseValueInDropdown(CorpusTab.massChangeItsDD, chosenMF);
		LogUltility.log(test, logger, "Change value in changes its dropdown: "+chosenMF);
		
		// Get values before change
		List<String> valueB4Change = CorpusTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Values before change: "+valueB4Change);
				
		// Choose second dropdown value
		List<String> dropdownValues = CommonFunctions.getValuesFromApp(CorpusTab.massToBeDD);
		String chosenDD2 = dropdownValues.get(randomizer.nextInt(dropdownValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.massToBeDD, chosenDD2);
		LogUltility.log(test, logger, "Change value in to be dropdown: "+chosenDD2);
		
		// Click on change
		CorpusTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change button");
		
		// Click on NO
		CorpusTab.massNoBtn.click();
		LogUltility.log(test, logger, "Click No");
		
		// Get values after change
		List<String> valueAfterChange = CorpusTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Value after change in dropdown: "+valueAfterChange);
		
		boolean isChanged = valueAfterChange.get(0).equals(chosenDD2);
		LogUltility.log(test, logger, "The value is changed: "+isChanged);
		assertTrue(isChanged);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--454:Check the Mass Attribution button - part3 in marking fields with Yes
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_454_Check_the_Mass_Attribution_button_part3_in_marking_fields_with_Yes() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--454", "Check the Mass Attribution button - part3 in marking fields with Yes");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in order to have less number of sentences
		String searchWord = Setting.Language.equals("EN")? "my boy" : "הילד";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Click on the mass button
		CorpusTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the Mass button");
		
		// Available marking fields on the left in Corpus 
		Random randomizer = new Random();
		String[] fieldsLists = {"sentence type","social status","speech act","lingo","mood","dialect"};
		String chosenMF = fieldsLists[randomizer.nextInt(fieldsLists.length)];

		// Chosen field webelement
		WebElement lstBox = null;
		switch(chosenMF)
		{
		case "sentence type":
			 lstBox = CorpusTab.lb_sentence_type;break;
		case "social status":
			 lstBox = CorpusTab.lb_social_status;break;
		case "speech act":
			 lstBox = CorpusTab.lb_speech_act;break;
		case "lingo":
			 lstBox = CorpusTab.lb_lingo;break;
		case "mood":
			 lstBox = CorpusTab.lb_mood;break;
		case "dialect":
			 lstBox = CorpusTab.lb_dialect;
		}
		
		// Choose first dropdown value
		CommonFunctions.chooseValueInDropdown(CorpusTab.massChangeItsDD, chosenMF);
		LogUltility.log(test, logger, "Change value in changes its dropdown: "+chosenMF);
		
		// Get values before change
		List<String> valueB4Change = CorpusTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Values before change: "+valueB4Change);
				
		// Choose second dropdown value
		List<String> dropdownValues = CommonFunctions.getValuesFromApp(CorpusTab.massToBeDD);
		String chosenDD2 = dropdownValues.get(randomizer.nextInt(dropdownValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.massToBeDD, chosenDD2);
		LogUltility.log(test, logger, "Change value in to be dropdown: "+chosenDD2);
		
		// Click on change
		CorpusTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change button");
		
		// Click on Yes
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click yes");
		
		// Get values after change
		List<String> valueAfterChange = CorpusTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Values after change in dropdown: "+valueAfterChange);
		
		// Check if the new value is added alongside old values and old values still appear
		boolean isAdded =  valueAfterChange.contains(chosenDD2) && valueAfterChange.containsAll(valueB4Change);
		LogUltility.log(test, logger, "The value is added: "+isAdded);
		assertTrue(isAdded);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	
	
	/**
	 * DIC_TC--490:Check the update function can handle edit multiple words in one shoot
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_490_Check_the_update_function_can_handle_edit_multiple_words_in_one_shoot() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--490", "Check the update function can handle edit multiple words in one shoot");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Pick random number and use go to in order to choose it
		Random randomizer = new Random();
		String status="";
		String randomNumber;
		Robot r = new Robot();
		int multipleEdit = 3;
		List<String> wordsInRecord;
		//Check if the sentence is not active -removed-, and it's length is bigger than 3
		do {
//			int  sentenceNumber = randomizer.nextInt(15);
		//	randomNumber = Integer.toString(sentenceNumber);
			randomNumber = Integer.toString(1);
			CorpusTab.tb_goto_line.sendKeys(randomNumber);
			LogUltility.log(test, logger, "Inserting number: " + randomNumber);
			
			//Click Enter
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			LogUltility.log(test, logger, "Click Enter");	
			
			// Get sentence status
			status  = CorpusTab.getSenteceStatusAcoordingNumber(randomNumber);
			
			// Get the words from sentence separately
			 wordsInRecord = CorpusTab.getSentencesRecords();
			}
		while(status.equals("not_active")&& wordsInRecord.size()>=3);
		
		
		// Currently chosing sentences that does not contain "'s", fix that later
		int wordNumber ;
		String randomWord ;		
		boolean clean = false;
		int wordsInRecordSize = wordsInRecord.size();
		// The word is non-composition
		do {
			wordNumber = randomizer.nextInt(wordsInRecord.size());
			randomWord = wordsInRecord.get(wordNumber);
			// String contains characters only
			boolean charOnly = CorpusTab.onlyCharacters(randomWord);
			clean = randomWord.contains(" ") && charOnly;
		}while(clean && wordsInRecordSize>0);
		
		// Edit multiple words
		String[] editValues = new String[multipleEdit];
		
		while(multipleEdit-- > 0)
		{
			// Choose first 3 words 
			wordNumber = multipleEdit;
			randomWord = wordsInRecord.get(wordNumber);
			
			// Click on word
			CorpusTab.clickSentencesRecord(randomWord, wordNumber);
			LogUltility.log(test, logger, "Click on: "+randomWord);
			
			// Click on the word's end
			r.keyPress(KeyEvent.VK_END);
			r.delay(100);
			r.keyRelease(KeyEvent.VK_END);
			
			String str = CorpusTab.RandomString(3);
			editValues[multipleEdit] = str;
			LogUltility.log(test, logger, "Edit to: "+randomWord+str);	
			WebElement e = CorpusTab.sentencesRecordElement(randomWord, wordNumber);
			e.sendKeys(randomWord+str);
//			CorpusTab.keyType(test, logger, str);
			
		}

		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click on update");
		
		// Go check the updated record
		// Get list of records again
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// Get sentence after updating
		List<String> updatedWordsInRecord = CorpusTab.getSentencesRecords();
		
		// Compare 
		multipleEdit = 3;
		while(multipleEdit-- > 0)
		{
			String modifiedRecord = updatedWordsInRecord.get(multipleEdit);
			String oldRecord;
			if(wordsInRecord.get(multipleEdit).equals(","))
				oldRecord = editValues[multipleEdit];
			else
				oldRecord = wordsInRecord.get(multipleEdit) + editValues[multipleEdit];
			oldRecord = Setting.Language.equals("HE")? oldRecord.replace(" | ",""): oldRecord;
			LogUltility.log(test, logger,"Modified record: "+ modifiedRecord);	
			boolean isModified = modifiedRecord.equals(oldRecord);
			LogUltility.log(test, logger, "The "+multipleEdit +" record is modified: "+isModified);	
			assertTrue(isModified);
		}
		
		CorpusTab.bt_revert.click();
		LogUltility.log(test, logger, "Click revert button" );
	
		// Click ok in popup
		CorpusTab.revertFileMsgOK.click();
		LogUltility.log(test, logger, "Click Ok" );
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--492:Check applying a sort on the corpus records and deleting any sentence
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_492_Check_applying_a_sort_on_the_corpus_records_and_deleting_any_sentence() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--492", "Check applying a sort on the corpus records and deleting any sentence");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Choose dropdown value to sort
		Random randomizer = new Random();
		List<String> dropdownValues = CommonFunctions.getValuesFromApp(CorpusTab.dd_sort_mode);
		
		// Remove default state
		dropdownValues.remove("number");
		
		// choose random value to sort
		String chosenDD = dropdownValues.get(randomizer.nextInt(dropdownValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_sort_mode, chosenDD);
		LogUltility.log(test, logger, "Change value in to be dropdown: "+chosenDD);
		
		// press delete/revive button for first setence
		CorpusTab.bt_delete.click();
		LogUltility.log(test, logger, "Press delete or revive button");
			
		// Check if we get the warning popup
		boolean result = CorpusTab.popupWindowMessage(test, logger, "", "You cannot delete/revive a sentence when in SORT mode!\r\rPlease use the reset button before deleting.");
		
		LogUltility.log(test, logger, "Can't delete or revive in sort mode: "+result);
		assertTrue(result);
		
		// click on ok
		CorpusTab.bt_ok_save.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	
	
	/**
	 * DIC_TC--554:Check the user can revert changes after executing an update
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_554_Check_the_user_can_revert_changes_after_executing_an_update() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--554", "Check the user can revert changes after executing an update");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Pick random number and use go to in order to choose it
		Random randomizer = new Random();
		String status="";
		String randomNumber;
		Robot r = new Robot();
		
		//Check if the sentence is not active -removed-
		do {
			int  sentenceNumber = randomizer.nextInt(15);
			randomNumber = Integer.toString(sentenceNumber);
			CorpusTab.tb_goto_line.sendKeys(randomNumber);
			LogUltility.log(test, logger, "Inserting number: " + randomNumber);
			
			//Click Enter
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			LogUltility.log(test, logger, "Click Enter");	
			
			// Get sentence status
			status  = CorpusTab.getSenteceStatusAcoordingNumber(randomNumber);
			}
		while(status.equals("not_active"));
		
		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		
		// Choose random word 
		int	wordNumber = randomizer.nextInt(wordsInRecord.size());
		String	randomWord = wordsInRecord.get(wordNumber);
		// Click on word
		CorpusTab.clickSentencesRecord(randomWord, wordNumber);
		LogUltility.log(test, logger, "Click on: "+randomWord);
		
		// Click on the word's end
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Edit the word using backspace	
		LogUltility.log(test, logger, "Press backspace to edit the word");
		CorpusTab.keyType(test, logger, "\b");
		
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click on update");

		// Click on revert
		CorpusTab.bt_revert.click();
		LogUltility.log(test, logger, "Click on revert");
		
		// Click ok 
		CorpusTab.revertFileMsgOK.click();
		LogUltility.log(test, logger, "Click on ok");
		
		// Choose the sentence again
		LogUltility.log(test, logger, "Choose sentence again");
		CorpusTab.tb_goto_line.sendKeys(randomNumber);
		LogUltility.log(test, logger, "Inserting number: " + randomNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// Get words after revert
		List<String> WordsInRecordAfterRevert = CorpusTab.getSentencesRecords();
		
		boolean noChange = WordsInRecordAfterRevert.get(wordNumber).equals(wordsInRecord.get(wordNumber));
		LogUltility.log(test, logger, "word before update: "+wordsInRecord.get(wordNumber)+" ,word after revert: "+WordsInRecordAfterRevert.get(wordNumber));
		LogUltility.log(test, logger, "There should be no change: "+noChange);
		assertTrue(noChange);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--13:Check the sort by drop down
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_13_Check_the_sort_by_drop_down() throws InterruptedException, AWTException, FileNotFoundException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--13", "Check the sort by drop down");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord =Setting.Language.equals("EN")? "boy" : "ילד" ;
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchWord, "all records");
		
		// Choose value from sort by dropdown
		Random randomizer = new Random();	
		List<String> sortList = CommonFunctions.getValuesFromApp(CorpusTab.dd_sort_mode);
		sortList.remove("text before");
		sortList.remove("text after");
		String chosenSortBy = sortList.get(randomizer.nextInt(sortList.size()));
//		String chosenSortBy = "text before";
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_sort_mode, chosenSortBy);
		LogUltility.log(test, logger, "Choose value in sort by dropdown: " + chosenSortBy);
		
		// Retrieve sentences list
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// number and text case, no need to access second column|| chosenSortBy.equals("text")
		boolean isSorted = false;
		
		if(chosenSortBy.equals("number"))
		{	
			ArrayList<Integer> numbersFromList= new ArrayList<Integer>();
			for(int i=0;i<sentencesList.size();i++)
			{
				String sentenceNumber = sentencesList.get(i).substring(0, 5).trim();
				numbersFromList.add(Integer.parseInt(sentenceNumber));
			}
			
			// Check if the list is sorted
			List copy = new ArrayList(numbersFromList);
		    Collections.sort(copy);
			isSorted = copy.equals(numbersFromList); 
		
			LogUltility.log(test, logger, "Sorted values by: " + numbersFromList);
		}
		
		if(chosenSortBy.equals("text"))
		{
			ArrayList<String> recievedList = new ArrayList<String>();
			for(int i=0;i<sentencesList.size();i++)
			{
				String cleanSentence = sentencesList.get(i).substring(6).trim();
				recievedList.add(cleanSentence.toLowerCase());
			}
			
			// Check if the list is sorted
			List copy = new ArrayList(recievedList);
		    Collections.sort(copy);
			isSorted = copy.equals(recievedList); 

			LogUltility.log(test, logger, "Sorted values by: " + recievedList);
		}
		else
		{
			ArrayList<String> recievedList = new ArrayList<String>();
			// index 20
			for(int i=0;i<sentencesList.size();i++)
			{
				String status = sentencesList.get(i).substring(6,21).trim();
				recievedList.add(status.toLowerCase());
			}
			
			// Check if the list is sorted
			List copy = new ArrayList(recievedList);
		    Collections.sort(copy);
			isSorted = copy.equals(recievedList); 

			LogUltility.log(test, logger, "Sorted values by: " + recievedList);
		}
		
		
		LogUltility.log(test, logger, "The List is sorted: " + isSorted);
		assertTrue(isSorted);	
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--16:Verify User can add sentence with underscore
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_16_Verify_User_can_add_sentence_with_underscore() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--16", "Verify User can add sentence with underscore");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
					
		//Click the new button in the footer
		CorpusTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		//Click ok 
		CorpusTab.bt_ok_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		//Insert the sentence in the field
		String sentenceToAdd = CorpusTab.RandomString(5)+"_"+CorpusTab.RandomString(5);
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		
		//Click new button in footer
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		//Confirm and click ok
		MonitorTab.bt_ok.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		// Search in corpus and that in order to have less number of sentences to work with
		if(Setting.Language.equals("EN"))	sentenceToAdd = sentenceToAdd.replace("_"," underscore ");
		CorpusTab.searchInCorpus(test,logger,"text","contains",sentenceToAdd,"all records");
		
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		/*Check if the new sentence is added to the records display window*/
		//Retrieve the last added sentence including it's serial number
		String newSentence = CommonFunctions.getLastValueFromApp(CorpusTab.lb_corpus_records);
		//Eliminate the serial number 
		String sentence = newSentence.substring(6);
		LogUltility.log(test, logger, "Retrieve the new sentence from the display window: " + sentence);
		boolean result = sentence.equals(sentenceToAdd);
		LogUltility.log(test, logger, "Comparation result: " + result);
		assertTrue(result);
		
		/*check if the new sentence saved in the database file*/	
		List<String> corpusData = CorpusTab.getRecordsFromCorpus();
		LogUltility.log(test, logger, "Retrieve the new sentence from the corpus file: " + corpusData.get(corpusData.size()-1));
		
		result = corpusData.get(corpusData.size()-1).equals(sentenceToAdd);
		LogUltility.log(test, logger, "Comparation result: " + result);
		assertTrue(result);
		 
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}

	
	
	/**
	 * DIC_TC--39:Check corpus- Values from dropdowns and list boxes
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_39_Check_corpus_Values_from_dropdowns_and_list_boxes() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--39", "Check corpus- Values from dropdowns and list boxes");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
					
		// Search in order to have less number of sentences
		String searchWord = Setting.Language.equals("EN") ? "dear" : "ילד";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose record
		Random randomizer = new Random();
		String chosenRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, chosenRecord);
		LogUltility.log(test, logger, "Choose record from the list: " + chosenRecord);
		
		// unselect lists values
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_lingo);
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_social_status);
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_mood);
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_sentence_type);
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_speech_act);
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_dialect);
		LogUltility.log(test, logger, "Unselect values from lists ");
		
		// Click next and previous button to sync
		CorpusTab.bt_next.click();
		CorpusTab.bt_prev.click();
		
		// Click save button
				CorpusTab.bt_save.click();
				LogUltility.log(test, logger, "Click the save button" );
				CorpusTab.bt_ok_save.click();
				LogUltility.log(test, logger, "Click the ok button" );
				
			
				try {
					CorpusTab.tabCorpus.click();
					} catch (Exception e) {
					e.printStackTrace();
				}
				
				
		// Get available dropdowns and lists
		List<String> audienceValues = CommonFunctions.getValuesFromApp(CorpusTab.dd_audience);
		List<String> complexityValues = CommonFunctions.getValuesFromApp(CorpusTab.dd_complexity);
		List<String> formalityValues = CommonFunctions.getValuesFromApp(CorpusTab.dd_formality);
		List<String> mediumValues = CommonFunctions.getValuesFromApp(CorpusTab.dd_medium);
		List<String> lingoValues = CommonFunctions.getValuesFromApp(CorpusTab.lb_lingo);
		List<String> sentencetypeValues = CommonFunctions.getValuesFromApp(CorpusTab.lb_sentence_type);
		List<String> moodValues = CommonFunctions.getValuesFromApp(CorpusTab.lb_mood);
		List<String> dialectValues = CommonFunctions.getValuesFromApp(CorpusTab.lb_dialect);
		List<String> speechactValues = CommonFunctions.getValuesFromApp(CorpusTab.lb_speech_act);
		List<String> socialstatusValues = CommonFunctions.getValuesFromApp(CorpusTab.lb_social_status);
		
		// Get chosen values from drowpdowns and lists
		String audienceB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_audience).get(0);
		audienceValues.remove(audienceB4Change);
		String complexityB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_complexity).get(0);
		complexityValues.remove(complexityB4Change);
		String formalityB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_formality).get(0);
		formalityValues.remove(formalityB4Change);
		String mediumB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_medium).get(0);
		mediumValues.remove(mediumB4Change);
		List<String> lingoB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_lingo);
		lingoValues.removeAll(lingoB4Change);
		List<String> sentencetypeB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_sentence_type);
		sentencetypeValues.removeAll(sentencetypeB4Change);
		List<String> moodB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_mood);
		moodValues.removeAll(moodB4Change);
		List<String> dialectB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_dialect);
		dialectValues.removeAll(dialectB4Change);
		List<String> speechactB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_speech_act);
		speechactValues.removeAll(speechactB4Change);
		List<String> socialstatusB4Change = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_social_status);
		socialstatusValues.removeAll(socialstatusB4Change);
		
		// Get values from file before modifying
		HashMap<String, String> fileValuesB4Change =CorpusTab.getSentenceDropdownsListsValuesFromFile(chosenRecord.substring(6));
		LogUltility.log(test, logger, "Record information from file before change: "+fileValuesB4Change);
		
		// Choose new values
		String newAudience = audienceValues.get(randomizer.nextInt(audienceValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_audience, newAudience);
		LogUltility.log(test, logger, "Choose new audience value: " + newAudience);
		
		String newComplexity = complexityValues.get(randomizer.nextInt(complexityValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_complexity, newComplexity);
		LogUltility.log(test, logger, "Choose new complexity value: " + newComplexity);
		
		String newFormality = formalityValues.get(randomizer.nextInt(formalityValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_formality, newFormality);
		LogUltility.log(test, logger, "Choose new formality value: " + newFormality);
		
		String newMedium = mediumValues.get(randomizer.nextInt(mediumValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_medium, newMedium);
		LogUltility.log(test, logger, "Choose new medium value: " + newMedium);
		
		String newLingo = lingoValues.get(randomizer.nextInt(lingoValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_lingo, newLingo);
		LogUltility.log(test, logger, "Choose new lingo value: " + newLingo);
		
		String newSentencetype = sentencetypeValues.get(randomizer.nextInt(sentencetypeValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_sentence_type, newSentencetype);
		LogUltility.log(test, logger, "Choose new sentence type value: " + newSentencetype);
		
		String newMood = moodValues.get(randomizer.nextInt(moodValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_mood, newMood);
		LogUltility.log(test, logger, "Choose new mood value: " + newMood);
		
		String newDialect = dialectValues.get(randomizer.nextInt(dialectValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_dialect, newDialect);
		LogUltility.log(test, logger, "Choose new dialect value: " + newDialect);
		
		String newSpeechact = speechactValues.get(randomizer.nextInt(speechactValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_speech_act, newSpeechact);
		LogUltility.log(test, logger, "Choose new speech act value: " + newSpeechact);
		
		String newSocialstatus = socialstatusValues.get(randomizer.nextInt(socialstatusValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_social_status, newSocialstatus);
		LogUltility.log(test, logger, "Choose new social status value: " + newSocialstatus);
		
		// Click next and previous button to sync
		CorpusTab.bt_next.click();
		CorpusTab.bt_prev.click();
		LogUltility.log(test, logger, "Click next and previous to sync " );
		
		// Click save button
		CorpusTab.bt_save.click();
		LogUltility.log(test, logger, "Click the save button" );
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the ok button" );
		
	
		try {
			CorpusTab.tabCorpus.click();
			} catch (Exception e) {
			e.printStackTrace();
					}
		
		String audienceAfterChange = null;
		try {
			audienceAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_audience).get(0);
			} catch (Exception e) {
			e.printStackTrace();
					}
		
		// retrieve app values after save
		String complexityAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_complexity).get(0);
		String formalityAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_formality).get(0);
		String mediumAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_medium).get(0);
		List<String> lingoAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_lingo);
		List<String> sentencetypeAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_sentence_type);
		List<String> moodAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_mood);
		List<String> dialectAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_dialect);
		List<String> speechactAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_speech_act);
		List<String> socialstatusAfterChange = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_social_status);
		
		// Get values from file after modifying
		HashMap<String, String> fileValuesAfterChange = CorpusTab.getSentenceDropdownsListsValuesFromFile(chosenRecord.substring(6));
		LogUltility.log(test, logger, "Record information from file after change: "+fileValuesAfterChange);
		
		// compare values in app and file
		boolean allValuesChangedApp = true;
		boolean allValuesChangedFile = true;
		
		// compare in app
		if(audienceAfterChange.equals(audienceB4Change)) allValuesChangedApp=false;
		if(complexityAfterChange.equals(complexityB4Change)) allValuesChangedApp=false;
		if(formalityAfterChange.equals(formalityB4Change)) allValuesChangedApp=false;
		if(mediumAfterChange.equals(mediumB4Change)) allValuesChangedApp=false;
		if(lingoAfterChange.equals(lingoB4Change)) allValuesChangedApp=false;
		if(sentencetypeAfterChange.equals(sentencetypeB4Change)) allValuesChangedApp=false;
		if(moodAfterChange.equals(moodB4Change)) allValuesChangedApp=false;
		if(dialectAfterChange.equals(dialectB4Change)) allValuesChangedApp=false;
		if(speechactAfterChange.equals(speechactB4Change)) allValuesChangedApp=false;
		if(socialstatusAfterChange.equals(socialstatusB4Change)) allValuesChangedApp=false;
		LogUltility.log(test, logger, "All values has been changed in App: " + allValuesChangedApp);
		assertTrue(allValuesChangedApp);
		
		// compare in file
		if(fileValuesAfterChange.get("audience").equals(fileValuesB4Change.get("audience"))) allValuesChangedFile=false;
		if(fileValuesAfterChange.get("social status").equals(fileValuesB4Change.get("social status"))) allValuesChangedFile=false;
		if(fileValuesAfterChange.get("sentence type").equals(fileValuesB4Change.get("sentence type"))) allValuesChangedFile=false;
		if(fileValuesAfterChange.get("complexity").equals(fileValuesB4Change.get("complexity"))) allValuesChangedFile=false;
		if(fileValuesAfterChange.get("formality").equals(fileValuesB4Change.get("formality"))) allValuesChangedFile=false;
		if(fileValuesAfterChange.get("speech act").equals(fileValuesB4Change.get("speech act"))) allValuesChangedFile=false;
		if(fileValuesAfterChange.get("medium").equals(fileValuesB4Change.get("medium"))) allValuesChangedFile=false;	
		if(fileValuesAfterChange.get("lingo").equals(fileValuesB4Change.get("lingo"))) allValuesChangedFile=false;	
		if(fileValuesAfterChange.get("mood").equals(fileValuesB4Change.get("mood"))) allValuesChangedFile=false;	
		if(fileValuesAfterChange.get("dialect").equals(fileValuesB4Change.get("dialect"))) allValuesChangedFile=false;		
		LogUltility.log(test, logger, "All values has been changed in File: " + allValuesChangedFile);
		assertTrue(allValuesChangedFile);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--241:Check the load button part 1 No
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_241_Check_the_load_button_part1_No() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--241", "Check the load button part 1 No");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in order to have less displayed sentences
		String searchWord = Setting.Language.equals("EN") ? "ocean" : "ילד";
		CorpusTab.searchInCorpus(test, logger, "text", "contains",searchWord, "all records");
		
		// Retrieve the displayed sentences
		List<String> sentencesB4Load = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "Sentences before loading: "+sentencesB4Load);
		
		// Build load file
		CorpusTab.buildSentencesForLoad();
		LogUltility.log(test, logger, "Building load file");
		
		// click load button
		CorpusTab.bt_load.click();
		LogUltility.log(test, logger, "Click the load button");
		
		// click yes button
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button");
		
		// click no button
		CorpusTab.massNoBtn.click();
		LogUltility.log(test, logger, "Click the no button");
		
		// Get the sentences from load file 
		List<String> loadFile = CorpusTab.sentencesFromLoadFile();
		LogUltility.log(test, logger, "Load file sentences: "+loadFile);
		
		// Retrieve the displayed sentences after load
		List<String> sentencesAfterLoad = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "Sentences after loading: "+sentencesAfterLoad);
		
		// Check if sentences after pressing load has the load file sentences
		boolean isLoaded = true ;
		for(int j=0, i=sentencesB4Load.size();i<sentencesAfterLoad.size();i++,j++)
		{
			String cleanSentence = sentencesAfterLoad.get(i).substring(5).trim();
			if(!cleanSentence.equals(loadFile.get(j).trim()))
				isLoaded = false;
		}
		
		LogUltility.log(test, logger, "Sentences are loaded successfully: "+isLoaded);
		assertTrue(isLoaded);
		
		CorpusTab.bt_revert.click();
		CorpusTab.revertFileMsgOK.click();
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 * DIC_TC--241:Check the load button part 2 Yes
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_241_Check_the_load_button_part2_Yes() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--241", "Check the load button part 2 Yes");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Build load file
		CorpusTab.buildSentencesForLoad();
		LogUltility.log(test, logger, "Building load file");
		
		// click load button
		CorpusTab.bt_load.click();
		LogUltility.log(test, logger, "Click the load button");
		
		// click yes button
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button");
		
		// click yes button to save
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button to save");
		
		// click Ok
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the ok button ");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Get the sentences from load file 
		List<String> loadFile = CorpusTab.sentencesFromLoadFile();
		LogUltility.log(test, logger, "Load file sentences: "+loadFile);
		
		List<String> corpusFileAfterLoad = CorpusTab.getRecordsFromCorpus();
		// Check corpus file after loading
		boolean loadedToCorpusFile = true;
		for(int i=0;i<loadFile.size();i++)
			if(!corpusFileAfterLoad.contains(loadFile.get(i).trim()))
				loadedToCorpusFile = false;
		
		LogUltility.log(test, logger, "Sentences are loaded successfully in file: "+loadedToCorpusFile);
		assertTrue(loadedToCorpusFile);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--317:Check the filtering dropdowns in the tagging window part 1 POS
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_317_Check_the_filtering_dropdowns_in_the_tagging_window_part1_POS() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--317", "Check the filtering dropdowns in the tagging window part 1 POS");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// search in order to have less sentences
		String searchedWord = Setting.Language.equals("EN")? "Wanted": "אגם";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// choose sentence from list
		String randomRecord = Setting.Language.equals("EN")? "    0 Wanted : Chief Justice of the Massachusetts Supreme Court ." : " 5001 האגם ידוע כמקום לדייג ותיירות .";
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose record: "+randomRecord);
		
		// Get tagging word with more than one POS options
		String taggingWord = Setting.Language.equals("EN")?"Wanted": "ידוע";
		int index =  Setting.Language.equals("EN")? 0 : 1;
		CorpusTab.clickSentencesRecord(taggingWord,index);
		LogUltility.log(test, logger, "Choose tagging word: "+taggingWord);
		
		// Get displayed tagged word info from app
		ArrayList<HashMap<String, String>> wordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		List<String> availablePOS = new ArrayList<String>();
		for(int i=0;i<wordInfo.size();i++)
			// Check to prevent duplicate values
			if(!availablePOS.contains(wordInfo.get(i).get("POS")))
				availablePOS.add(wordInfo.get(i).get("POS"));
		
		// Choose random POS
		Random randomizer = new Random();
		String randomPOS = availablePOS.get(randomizer.nextInt(availablePOS.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_pos, randomPOS);
		LogUltility.log(test, logger, "Choose POS value: "+randomPOS);
		
		// Get displayed tagged word info from app
		ArrayList<HashMap<String, String>> wordInfoAfterFiltiring = CorpusTab.getTaggingRecordsRawAllInfo();
		boolean relevantValues = true;
		for(int i=0;i<wordInfoAfterFiltiring.size();i++)
			if(! randomPOS.equals(wordInfoAfterFiltiring.get(i).get("POS")))
				relevantValues = false;
		
		LogUltility.log(test, logger, "All displayed values are relevant: "+relevantValues);
		assertTrue(relevantValues);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 * DIC_TC--317:Check the filtering dropdowns in the tagging window part 2 Form Type
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_317_Check_the_filtering_dropdowns_in_the_tagging_window_part2_FormType() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--317", "Check the filtering dropdowns in the tagging window part 2 Form Type");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
			
		// search in order to have less sentences
		String searchedWord = Setting.Language.equals("EN")? "Wanted": "אגם";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// choose sentence from list
		String randomRecord = Setting.Language.equals("EN")? "    0 Wanted : Chief Justice of the Massachusetts Supreme Court ." : " 5001 האגם ידוע כמקום לדייג ותיירות .";
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose record: "+randomRecord);
		
		// Get tagging word with more than one POS options
		String taggingWord = Setting.Language.equals("EN")?"Wanted": "ידוע";
		int index =  Setting.Language.equals("EN")? 0 : 1;
		CorpusTab.clickSentencesRecord(taggingWord,index);
		LogUltility.log(test, logger, "Choose tagging word: "+taggingWord);
		
		// Get displayed tagged word info from app
		ArrayList<HashMap<String, String>> wordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		List<String> availableFormType = new ArrayList<String>();
		for(int i=0;i<wordInfo.size();i++)
			// Check to prevent duplicate values
			if(!availableFormType.contains(wordInfo.get(i).get("Form Type")))
				availableFormType.add(wordInfo.get(i).get("Form Type"));
		
		// Choose random Form Type
		Random randomizer = new Random();
		String randomFormType = availableFormType.get(randomizer.nextInt(availableFormType.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_form_type, randomFormType);
		LogUltility.log(test, logger, "Choose Form Type value: "+randomFormType);
		
		// Get displayed tagged word info from app
		ArrayList<HashMap<String, String>> wordInfoAfterFiltiring = CorpusTab.getTaggingRecordsRawAllInfo();
		boolean relevantValues = true;
		for(int i=0;i<wordInfoAfterFiltiring.size();i++)
			if(! randomFormType.equals(wordInfoAfterFiltiring.get(i).get("Form Type")))
				relevantValues = false;
		
		LogUltility.log(test, logger, "All displayed values are relevant: "+relevantValues);
		assertTrue(relevantValues);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--434:Copy sentence by shift click
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_434_Copy_sentence_by_shift_click() throws InterruptedException, AWTException, IOException, UnsupportedFlavorException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--434", "Copy sentence by shift click");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// search in order to have less sentences
		String searchWord = Setting.Language.equals("EN") ? "Wanted" : "אגם";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchWord, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// choose sentence from list
		Random randomizer = new Random();
		String randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose record: "+randomRecord);
		
		// Eliminate sentence serial number
		String cleanRecord = randomRecord.substring(6);
		
		// Copy sentence to the clipboard using SHIFT + CLICK
		Thread.sleep(1500);
		Actions shiftClick = new Actions(DriverContext._Driver);
		shiftClick.keyDown(Keys.SHIFT).click().keyUp(Keys.SHIFT).perform();
		LogUltility.log(test, logger, "Press SHIFT and click LEFT MOUSE");
		Thread.sleep(1500);

		// Create a Clipboard object using getSystemClipboard() method
        Clipboard c= Toolkit.getDefaultToolkit().getSystemClipboard();
        
        // Get data stored in the clipboard that is in the form of a string (text)
        String clipboardData = (String) c.getData(DataFlavor.stringFlavor);
//      String clipboardData = (String) c.getContents(null).getTransferData(DataFlavor.stringFlavor);
        LogUltility.log(test, logger, "Retrieve the copied sentence from the clipboard: "+clipboardData);
        
        // Compare if the copied sentence to clipboard same as the chosen sentence
        boolean copiedSuccessfully = clipboardData.equals(cleanRecord);
        LogUltility.log(test, logger, "The chosen sentence got copied to the clipboard successfully: "+copiedSuccessfully);
		assertTrue(copiedSuccessfully);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--448:Check function to update \ edit sentence in corpus
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_448_Check_function_to_update_edit_sentence_in_corpus() throws InterruptedException, AWTException, IOException, UnsupportedFlavorException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--448", "Copy sentence by shift click");

		if(Setting.Language.equals("HE")) return;

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
					
		// Click the new button in the footer
		CorpusTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click ok 
		CorpusTab.bt_ok_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		// Insert the sentence in the field
		String sentenceToAdd = "He is going to be there!";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: "+sentenceToAdd);
		
		// Click new button in footer
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Confirm and click ok
		MonitorTab.bt_ok.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		// Choose "he" record
		//Random randomizer = new Random();
		List<String> wordsInSentence = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Words in sentence before change: " + wordsInSentence);
		//int wordIndex = randomizer.nextInt(wordsInSentence.size());
		int wordIndex  = 0;
		String wordB4Update = wordsInSentence.get(wordIndex);
		CorpusTab.clickSentencesRecord(wordB4Update, wordIndex);
		LogUltility.log(test, logger, "Choose word: "+ wordB4Update);
		
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Add text
		String str = "s";
    	CorpusTab.keyType(test, logger, str);
		LogUltility.log(test, logger, "Update word with adding: "+str);
				
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// Go check the updated record
		// Get list of records again
		wordsInSentence = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Words in sentence after change: " + wordsInSentence);
		String modifiedRecord = wordsInSentence.get(wordIndex);
		
		// Comparing
		boolean compare = modifiedRecord.equals(wordB4Update +" + " + str);
		LogUltility.log(test, logger, "Word got updated: " + compare);
		assertTrue(compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--450:Check the user can tag records after editing it from corpus
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws UnsupportedFlavorException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_450_Check_the_user_can_tag_records_after_editing_it_from_corpus() throws InterruptedException, AWTException, IOException, UnsupportedFlavorException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--450", "Check the user can tag records after editing it from corpus");

		if(Setting.Language.equals("HE")) return;

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
					
		// Click the new button in the footer
		CorpusTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click ok 
		CorpusTab.bt_ok_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		// Insert the sentence in the field
		String sentenceToAdd = "He is going to be there !";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: "+sentenceToAdd);
		
		// Click new button in footer
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Confirm and click ok
		MonitorTab.bt_ok.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		// Choose "he" record
		Random randomizer = new Random();
		List<String> wordsInSentence = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Words in sentence before change: " + wordsInSentence);
		//int wordIndex = randomizer.nextInt(wordsInSentence.size());
		int wordIndex  = 0;
		String wordB4Update = wordsInSentence.get(wordIndex);
		CorpusTab.clickSentencesRecord(wordB4Update, wordIndex);
		LogUltility.log(test, logger, "Choose word: "+ wordB4Update);
		
		// Tag the word before change
		List<List<String>> taggingOptionsB4Change = CorpusTab.getTaggingRecords();
		LogUltility.log(test, logger, "Get tagging Options: " + taggingOptionsB4Change);
		
		// Choose tagging option
		List<String> randomTaggingOptionB4 = taggingOptionsB4Change.get(randomizer.nextInt(taggingOptionsB4Change.size()));
		CorpusTab.chooseTaggingOption(randomTaggingOptionB4);
		LogUltility.log(test, logger,"choose random tagging option: " + randomTaggingOptionB4);
		
		// Click save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
		// Search to focus
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		}
		 catch (Exception e) {
		e.printStackTrace();
		 	}
			
   		// Get info before changing
		CorpusTab.bt_reset_search.click();
   		String sentence = "abcdef"+sentenceToAdd;
		String sentenceNumber = CommonFunctions.getLastValueFromApp(CorpusTab.lb_corpus_records).substring(0, 5).trim();
   		List<String> infoB4Change = CorpusTab.getRecordTaggingFromCorpusFile(sentenceNumber, "He");
		LogUltility.log(test, logger, "Record info from file: "+ infoB4Change);
		
		// Click word again
		CorpusTab.clickSentencesRecord(wordB4Update, wordIndex);
		LogUltility.log(test, logger, "Click the word again ");
		
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Add text
		String str = "s";
    	CorpusTab.keyType(test, logger, str);
		LogUltility.log(test, logger, "Update word with adding: "+str);
				
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// Go check the updated record
		// Get list of records again
		wordsInSentence = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Words in sentence after change: " + wordsInSentence);
		String modifiedRecord = wordsInSentence.get(wordIndex);
			
		// Click word again
		CorpusTab.clickSentencesRecord(modifiedRecord, wordIndex);
		LogUltility.log(test, logger, "Choose the updated record: "+modifiedRecord);

		// Get all tagging options
		List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
		LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
		
		// Choose POS and Form Type in filters dropdowns
		// Choose random POS
		List<String> availablePOS = CommonFunctions.getValuesFromApp(CorpusTab.cb_filter_pos);
		availablePOS.remove("POS");
		String randomPOS = availablePOS.get(randomizer.nextInt(availablePOS.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_pos, randomPOS);
		LogUltility.log(test, logger, "Choose POS value: "+randomPOS);
		
		// Choose random Form Type
		List<String> formTypeValues = CommonFunctions.getValuesFromApp(CorpusTab.cb_filter_form_type);
		formTypeValues.remove("Form Type");
		//String randomFormType = formTypeValues.get(randomizer.nextInt(formTypeValues.size()));
		String randomFormType = "present_s3";
		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_form_type, randomFormType);
		LogUltility.log(test, logger, "Choose Form Type value: "+randomFormType);
		
		// Random choose another option from the taggingOptions -- tag first part "he"
		taggingOptions = CorpusTab.getTaggingRecords();
		List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
		CorpusTab.chooseTaggingOption(randomTaggingOption);
		LogUltility.log(test, logger,"choose random tagging option: " + randomTaggingOption);
				
		 
		
//		// Choose POS and Form Type in filters dropdowns
//		// Choose random POS
//		availablePOS = CorpusTab.getValuesFromApp(CorpusTab.cb_filter_form_type);
//		randomPOS = availablePOS.get(randomizer.nextInt(availablePOS.size()));
//		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_pos, randomPOS);
//		LogUltility.log(test, logger, "Choose POS value: "+randomPOS);
//		
//		// Choose random Form Type
//		formTypeValues = CorpusTab.getValuesFromApp(CorpusTab.cb_filter_form_type);
//		randomFormType = formTypeValues.get(randomizer.nextInt(formTypeValues.size()));
//		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_form_type, randomFormType);
//		LogUltility.log(test, logger, "Choose Form Type value: "+randomFormType);
		
		randomPOS = "verb";
		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_pos, randomPOS);
		LogUltility.log(test, logger, "Choose POS value: "+randomPOS);
		
		taggingOptions = CorpusTab.getTaggingRecords();
		LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
		
		// Tag second part in word "s"
		randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
		CorpusTab.chooseTaggingOption(randomTaggingOption);
		LogUltility.log(test, logger,"choose random tagging option: " + randomTaggingOption);
			
		// Click save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
		// Search to focus
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		}
		 catch (Exception e) {
		e.printStackTrace();
		 	}
			
		// Retrieve info after save
		CorpusTab.bt_reset_search.click();
		String sentenceAfterUpdate = "abcdef"+ "Hes is going to be there !";
		sentenceNumber = CommonFunctions.getLastValueFromApp(CorpusTab.lb_corpus_records).substring(0, 5).trim();
		List<String> infoAfterChange = CorpusTab.getRecordTaggingFromCorpusFile(sentenceNumber, "Hes");
		LogUltility.log(test, logger, "Record info from file: "+ infoAfterChange);
		
		// Compare result
		boolean changed = !infoAfterChange.equals(infoB4Change);
		LogUltility.log(test, logger, "Values has been changed: "+ changed);
		assertTrue(changed);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	

	/**
	 * DIC_TC--453:Check adding apostrophe s to exist sentence will add it as contraction
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_453_Check_adding_apostrophe_s_to_exist_sentence_will_add_it_as_contraction() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--453", "Check adding apostrophe s to exist sentence will add it as contraction");
		
		if(Setting.Language.equals("HE")) return;

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in corpus and that in order to have less number of sentences to work with
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "ocean", "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
//		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
//		// Currently chosing sentences that does not contain "'s", fix that later
//		boolean clean = false;
//		String randomSentence = "";
//		do {
//			randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));
//			clean = randomSentence.contains("'");
//		} while (clean);
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);

		// Get list of the records for the chosen sentence
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);

		// Choose and click on random record
		String randomRecord ;
		boolean onlyLetters;
		do
		{
			randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			onlyLetters = !CorpusTab.onlyCharacters(randomRecord);
		}while(onlyLetters);
		
		LogUltility.log(test, logger, "Click random record " + randomRecord);
		int recordNumber = recordsList.indexOf(randomRecord);
		CorpusTab.clickSentencesRecord(randomRecord, recordNumber);
		
		// Click End to move cursor to end of the record
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Add text
		String str = "'s";
		CorpusTab.keyType(test, logger, str);
	
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click the update button");
		
		// Go check the updated record
		// Get list of records again
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list after change: " + recordsList);
		String modifiedRecord = recordsList.get(recordNumber);
		
		// Comparing
		boolean compare = modifiedRecord.equals(randomRecord + " + " + str);
		LogUltility.log(test, logger, "compare: " + compare);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--522:check sorting then adding new sentence
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_522_check_sorting_then_adding_new_sentence() throws InterruptedException, AWTException, FileNotFoundException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--522", "check sorting then adding new sentence");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		// Click on the Corpus tab
		CommonFunctions.clickTabs("Corpus");	
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchedWord = Setting.Language.equals("EN") ? "boy" : "ילד";
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");
		
		// Choose value from sort by dropdown
		List<String> sortByDD = CommonFunctions.getValuesFromApp(CorpusTab.dd_sort_mode);
		Random randomizer = new Random();
		String chosenSortBy = sortByDD.get(randomizer.nextInt(sortByDD.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_sort_mode, chosenSortBy);
		LogUltility.log(test, logger, "Choose value in sort by dropdown: " + chosenSortBy);
		
		// Retrieve sentences list before adding
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Click the new button in the footer
		CorpusTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click ok 
		CorpusTab.bt_ok_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		// Insert the sentence in the field
		String sentenceToAdd = Setting.Language.equals("EN")? "Hello its TC number 522" : "בדיקה מספר 522";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: "+sentenceToAdd);
		
		// Click new button in footer
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Confirm and click ok
		MonitorTab.bt_ok.click();
		LogUltility.log(test, logger, "Click the ok button");
		
		// Retrieve list after adding new sentence
		List<String> listAfterAdding = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived after adding new sentence: " + listAfterAdding);
		
		// number and text case, no need to access second column|| chosenSortBy.equals("text")
		boolean isSorted = false;
		
		if(chosenSortBy.equals("number"))
		{	
			ArrayList<Integer> numbersFromList= new ArrayList<Integer>();
			for(int i=0;i<listAfterAdding.size();i++)
			{
				String sentenceNumber = listAfterAdding.get(i).substring(0, 5).trim();
				numbersFromList.add(Integer.parseInt(sentenceNumber));
			}
			
			// Check if the list is sorted
			List copy = new ArrayList(numbersFromList);
		    Collections.sort(copy);
			isSorted = copy.equals(numbersFromList); 
		
			LogUltility.log(test, logger, "Sorted values by: " + numbersFromList);
		}
		
		if(chosenSortBy.equals("text"))
		{
			ArrayList<String> recievedList = new ArrayList<String>();
			for(int i=0;i<listAfterAdding.size();i++)
			{
				String cleanSentence = listAfterAdding.get(i).substring(6).trim();
				recievedList.add(cleanSentence.toLowerCase());
			}
			
			// Check if the list is sorted
			List copy = new ArrayList(recievedList);
		    Collections.sort(copy);
			isSorted = copy.equals(recievedList); 

			LogUltility.log(test, logger, "Sorted values by: " + recievedList);
		}
		else
		{
			ArrayList<String> recievedList = new ArrayList<String>();
			// index 20
			for(int i=0;i<listAfterAdding.size();i++)
			{
				String status = listAfterAdding.get(i).substring(6,21).trim();
				recievedList.add(status.toLowerCase());
			}
			
			// Check if the list is sorted
			List copy = new ArrayList(recievedList);
		    Collections.sort(copy);
			isSorted = copy.equals(recievedList); 

			LogUltility.log(test, logger, "Sorted values by: " + recievedList);
		}
		
		
		LogUltility.log(test, logger, "The List is sorted: " + isSorted);
		assertTrue(isSorted);	
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--528:Check editing loaded sentence
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_528_Check_editing_loaded_sentence() throws InterruptedException, AWTException, IOException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--528", "Check editing loaded sentences");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
	
		// Build load file
		CorpusTab.buildSentencesForLoad();
		LogUltility.log(test, logger, "Building load file");
		
		// click load button
		CorpusTab.bt_load.click();
		LogUltility.log(test, logger, "Click the load button");
		
		// click yes button
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button");
		
		// click yes button to save
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button to save");
		
		// click Ok
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the ok button ");
		
		try {
			CorpusTab.bt_reset_search.click();
		} catch (Exception e) {
//			e.printStackTrace();
			}
		
		// Get corpus file records after loading and before editing
		List<String> corpusFileB4Edit = CorpusTab.getRecordsFromCorpus();

		// Choose sentence
		Random randomizer = new Random();
		String chosenSentence = corpusFileB4Edit.get(corpusFileB4Edit.size()-1);
		String chosenSentenceNumber = CorpusTab.getSentenceNumber(chosenSentence);

		CorpusTab.bt_reset_search.click();
		
		// Go to the sentence
		CorpusTab.tb_goto_line.sendKeys(chosenSentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + chosenSentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// Get words before updating
		List<String> wordsInSentence = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Words in sentence before change: " + wordsInSentence);
		
		// Choose word to update
		String wordB4Update = wordsInSentence.get(randomizer.nextInt(wordsInSentence.size()));
		int wordIndex = wordsInSentence.indexOf(wordB4Update);
		CorpusTab.clickSentencesRecord(wordB4Update, wordIndex);
		WebElement recordElement = CorpusTab.sentencesRecordElement(wordB4Update, wordIndex);
		LogUltility.log(test, logger, "Click random word " + wordB4Update);
		
		// Add text
		String str = CorpusTab.RandomString(3);
//    	CorpusTab.keyType(test, logger, str);
		recordElement.click();
		recordElement.sendKeys(str);
		LogUltility.log(test, logger, "Update word with adding: "+str);
				
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// Click save
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Go check the updated record
		// Get list of records again
		wordsInSentence = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Words in sentence after change: " + wordsInSentence);
		String modifiedRecord = wordsInSentence.get(wordIndex);
		
		// Comparing in app
		boolean compareInApp = !modifiedRecord.equals(wordB4Update);
		LogUltility.log(test, logger, "Word got updated in App: " + compareInApp);
		assertTrue(compareInApp);
		
		// Compare in File
		// Get corpus file records after editing
		List<String> corpusFileAfterEdit = CorpusTab.getRecordsFromCorpus();
		String chosenSentenceAfterEdit = corpusFileAfterEdit.get(corpusFileAfterEdit.size()-1);
		boolean compareInFile = !chosenSentenceAfterEdit.equals(chosenSentence);
		LogUltility.log(test, logger, "Word got updated in file: " + compareInFile);
		assertTrue(compareInFile);
		
				
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--529:Check removing words from loaded sentence
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_529_Check_removing_words_from_loaded_sentence() throws InterruptedException, AWTException, IOException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--529", "Check removing words from loaded sentence");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// Build load file
		CorpusTab.buildSentencesForLoad();
		LogUltility.log(test, logger, "Building load file");
		
		// click load button
		CorpusTab.bt_load.click();
		LogUltility.log(test, logger, "Click the load button");
		
		// click yes button
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button");
		
		// click yes button to save
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button to save");
		
		// click Ok
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the ok button ");
		
		try {
			CorpusTab.bt_reset_search.click();
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Get corpus file records after loading and before editing
		List<String> corpusFileB4Edit = CorpusTab.getRecordsFromCorpus();

		// Choose sentence
		Random randomizer = new Random();
		String chosenSentence = corpusFileB4Edit.get(corpusFileB4Edit.size()-1);
		String chosenSentenceNumber = CorpusTab.getSentenceNumber(chosenSentence);
		
		// Go to the sentence
		CorpusTab.bt_reset_search.click();
		CorpusTab.tb_goto_line.sendKeys(chosenSentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + chosenSentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// Get words before updating
		List<String> wordsInSentenceB4Delete= CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Words in sentence before deleting: " + wordsInSentenceB4Delete);
		
		// Choose word to update
		String wordB4Update = wordsInSentenceB4Delete.get(randomizer.nextInt(wordsInSentenceB4Delete.size()));
		int wordIndex = wordsInSentenceB4Delete.indexOf(wordB4Update);
		CorpusTab.clickSentencesRecord(wordB4Update, wordIndex);
		LogUltility.log(test, logger, "Click random word " + wordB4Update);
		
		// Navigate to the end of the word
		r.keyPress(KeyEvent.VK_END);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Remove word
		int size = wordB4Update.length();
		while(size-- > 0)
			{CorpusTab.keyType(test, logger, "\b");}
		LogUltility.log(test, logger, "Remove word");
		
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// Click save
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Go check the updated record
		// Get list of records again
		List<String> wordsInSentenceAfterDelete = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Words in sentence after deleting: " + wordsInSentenceAfterDelete);
		
		// Comparing in app
		boolean compareInApp = wordsInSentenceB4Delete.size() - 1 == wordsInSentenceAfterDelete.size() ;
		LogUltility.log(test, logger, "Word got deleted in App: " + compareInApp);
		assertTrue(compareInApp);
		
		// Compare in File
		// Get corpus file records after editing
		List<String> corpusFileAfterEdit = CorpusTab.getRecordsFromCorpus();
		String chosenSentenceAfterEdit = corpusFileAfterEdit.get(corpusFileAfterEdit.size()-1);
		
		// In case for Hebrew, remove Nikud from Strings
		chosenSentence = Setting.Language.equals("EN")? chosenSentence : CommonFunctions.cleanStringNikud(chosenSentence);
		wordB4Update = Setting.Language.equals("EN")? wordB4Update : CommonFunctions.cleanStringNikud(wordB4Update);
		chosenSentenceAfterEdit = Setting.Language.equals("EN")? chosenSentenceAfterEdit : CommonFunctions.cleanStringNikud(chosenSentenceAfterEdit);
		
		boolean compareInFile = chosenSentence.length() -  wordB4Update.length() -1 == chosenSentenceAfterEdit.length() ;
		LogUltility.log(test, logger, "Word got deleted in file: " + compareInFile);
		assertTrue(compareInFile);
					
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--530:check mass effects loaded sentences
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_530_check_mass_effects_loaded_sentences() throws InterruptedException, AWTException, IOException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--530", "check mass effects loaded sentences");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Build load file
		CorpusTab.buildSentencesForLoad();
		LogUltility.log(test, logger, "Building load file");
		
		// click load button
		CorpusTab.bt_load.click();
		LogUltility.log(test, logger, "Click the load button");
		
		// click yes button
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button");
		
		// click yes button to save
		CorpusTab.massYesBtn.click();
		LogUltility.log(test, logger, "Click the yes button to save");
		
		// click Ok
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the ok button ");
		
		try {
			CorpusTab.bt_reset_search.click();
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Get corpus file records after loading and before editing
		List<String> corpusFileB4Edit = CorpusTab.getRecordsFromCorpus();

		// Choose sentence
		Random randomizer = new Random();
		String chosenSentence = corpusFileB4Edit.get(corpusFileB4Edit.size()-1);
		String chosenSentenceNumber = CorpusTab.getSentenceNumber(chosenSentence);
		
		// Go to the sentence
		CorpusTab.bt_reset_search.click();
		CorpusTab.tb_goto_line.sendKeys(chosenSentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + chosenSentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");	
		
		// lists and dropdowns values before mass
		HashMap<String, String> fileValuesB4Mass = CorpusTab.getSentenceDropdownsListsValuesFromFile(chosenSentence);
		LogUltility.log(test, logger, "Values before mass for the sentence in file: "+fileValuesB4Mass );
		
		// Click on the mass button
		CorpusTab.bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the Mass button");
				
		// Available valus in "changes its" dropdown
		List<String> dropDownLists = CommonFunctions.getValuesFromApp(CorpusTab.massChangeItsDD);
		String chosenDD = dropDownLists.get(randomizer.nextInt(dropDownLists.size()));
				
		// Chosen dropdown webelement
		WebElement lstBox = null;
		boolean dropDown = true;
		switch(chosenDD)
		{
		case "audience":
			 lstBox = CorpusTab.dd_audience;break;
		case "complexity":
			 lstBox = CorpusTab.dd_complexity;break;
		case "formality":
			 lstBox = CorpusTab.dd_formality;break;
		case "medium":
			 lstBox = CorpusTab.dd_medium;break;
			 
		case "sentence type":
			 lstBox = CorpusTab.lb_sentence_type;
			 dropDown = false;break;
		case "social status":
			 lstBox = CorpusTab.lb_social_status;
			 dropDown = false;break;
		case "speech act":
			 lstBox = CorpusTab.lb_speech_act;
			 dropDown = false;break;
		case "lingo":
			 lstBox = CorpusTab.lb_lingo;
			 dropDown = false;break;
		case "mood":
			 lstBox = CorpusTab.lb_mood;
			 dropDown = false;break;
		case "dialect":
			 lstBox = CorpusTab.lb_dialect;
			 dropDown = false;break;
		}
				
		// Choose first dropdown value
		CommonFunctions.chooseValueInDropdown(CorpusTab.massChangeItsDD, chosenDD);
		LogUltility.log(test, logger, "Change value in changes its dropdown: "+chosenDD);
		
		// Get value before change
		List<String> valueB4Change = CorpusTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Value before change in dropdown: "+valueB4Change);
				
		// Choose second dropdown value
		List<String> dropdownValues = CommonFunctions.getValuesFromApp(CorpusTab.massToBeDD);
		String chosenDD2 = dropdownValues.get(randomizer.nextInt(dropdownValues.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.massToBeDD, chosenDD2);
		LogUltility.log(test, logger, "Change value in to be dropdown: "+chosenDD2);
		
		// Click on change
		CorpusTab.bt_utility_accept.click();
		LogUltility.log(test, logger, "Click change buton");
			
		// if list was chosen
		if(!dropDown)
		{
			// Click on NO
			CorpusTab.massNoBtn.click();
			LogUltility.log(test, logger, "Click No");
		}
				
		// Click save
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		
		// Go to the sentence
		CorpusTab.tb_goto_line.sendKeys(chosenSentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + chosenSentenceNumber);
		
		// Get value after change
		List<String> valueAfterChange = CorpusTab.getChosenValueInDropdown(lstBox);
		LogUltility.log(test, logger, "Value after change in dropdown: "+valueAfterChange);
				
		// lists and dropdowns values after mass
		HashMap<String, String> fileValuesAfterMass = CorpusTab.getSentenceDropdownsListsValuesFromFile(chosenSentence);
		LogUltility.log(test, logger, "Values after mass for the sentence in file: "+fileValuesAfterMass );
			
		// Compare changes in app
		boolean appChange = ! valueAfterChange.equals(valueB4Change);
		LogUltility.log(test, logger, "Values changed in App: "+appChange );
		assertTrue(appChange);
		
		// Compare changes in file
		boolean fileChange = ! fileValuesAfterMass.get(chosenDD).equals(fileValuesB4Mass.get(chosenDD));
		LogUltility.log(test, logger, "Values changed in file: "+fileChange );
		assertTrue(fileChange);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--598:Check tagging a composition word
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_598_Check_tagging_a_composition_word() throws InterruptedException, FileNotFoundException, AWTException 
	{
		test = extent.createTest("Corpus Tests - DIC_TC--598", "Check tagging a composition word");

		if(Setting.Language.equals("HE")) return;

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// search in order to have less sentences
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "hello", "all records");
	
		//Click the new button in the footer
		CorpusTab.bt_new.click();
		//Click ok 
		CorpusTab.bt_ok_new.click();
		LogUltility.log(test, logger, "Click the new button");
		
		//Insert the sentence in the field
		String sentenceToAdd = "He is anti-social";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert text: "+sentenceToAdd);
		
		//Click new button in footer
		LogUltility.log(test, logger, "Click new button");
		MonitorTab.bt_new.click();
		//Confirm and click ok
		LogUltility.log(test, logger, "Click ok button");
		MonitorTab.bt_ok.click();
		
		// Click on tagging word
		String taggingWord = "anti- + social";
		CorpusTab.clickSentencesRecord(taggingWord,2);
		LogUltility.log(test, logger, "Choose tagging word: "+taggingWord);
		
		// Get all tagging options
		List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
		LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
		
		//  choose option from the taggingOptions before selecting POS and Form Type
		Random randomizer = new Random();
		List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
		CorpusTab.chooseTaggingOption(randomTaggingOption);
		LogUltility.log(test, logger,"choose random tagging option: " + randomTaggingOption);
			
		// Getting the warning popup
		CorpusTab.popupWindowMessage(test, logger, "", "You have to select first the POS and Formtype of the composition In the purple line!");
		CorpusTab.bt_ok_new.click();
		
		// Choose POS and Form Type in filters dropdowns
		// Choose random POS
		List<String> availablePOS = CommonFunctions.getValuesFromApp(CorpusTab.cb_filter_pos);
		availablePOS.remove("POS");
		//String randomPOS = availablePOS.get(randomizer.nextInt(availablePOS.size()));
		String randomPOS = "noun";
		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_pos, randomPOS);
		LogUltility.log(test, logger, "Choose POS value: "+randomPOS);
		
		// Choose random Form Type
		List<String> formTypeValues = CommonFunctions.getValuesFromApp(CorpusTab.cb_filter_form_type);
		formTypeValues.remove("Form Type");
		//String randomFormType = formTypeValues.get(randomizer.nextInt(formTypeValues.size()));
		String randomFormType = "single";
		CommonFunctions.chooseValueInDropdown(CorpusTab.cb_filter_form_type, randomFormType);
		LogUltility.log(test, logger, "Choose Form Type value: "+randomFormType);
		
		// Click on composition
		CorpusTab.clickSentencesCompositionRecord(taggingWord, 2);
		LogUltility.log(test, logger, "Click on composition");
		
		ArrayList<HashMap<String, String>> wordInfoFirstPart = CorpusTab.getTaggingRecordsRawAllInfo();
		String posValue = wordInfoFirstPart.get(0).get("POS");
		String formValue = wordInfoFirstPart.get(0).get("Form Type");
		
		// choose a taggingOptions for the first composition part
		taggingOptions = CorpusTab.getTaggingRecords();
	    randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
		CorpusTab.chooseTaggingOption(randomTaggingOption);
		LogUltility.log(test, logger,"choose random tagging option: " + randomTaggingOption);
		
		ArrayList<HashMap<String, String>> wordInfoSecondPart = CorpusTab.getTaggingRecordsRawAllInfo();
		String posValue2 = wordInfoSecondPart.get(0).get("POS");
		String formValue2 = wordInfoSecondPart.get(0).get("Form Type");
		
		// choose a taggingOptions for the second composition part
		taggingOptions = CorpusTab.getTaggingRecords();
	    randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
		CorpusTab.chooseTaggingOption(randomTaggingOption);
		LogUltility.log(test, logger,"choose random tagging option: " + randomTaggingOption);
		
		// Click save
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		int recordNumber = 2;
		// Check now that the new tagged are saved correctly
		LogUltility.log(test, logger, "Check new tagging");
		CorpusTab.clickSentencesRecord(taggingWord, recordNumber);
			
		// Get the currently chosen tag
		String taggingWord4File = "anti-social";
		ArrayList<ArrayList<String>>  chosenRecordInfo = CorpusTab.getCompositionRecordTaggingFromCorpusFile("123456"+sentenceToAdd,taggingWord4File);
		LogUltility.log(test, logger, "New tagging: " + chosenRecordInfo);

		
		boolean savedCorrectly = true;
		// Compare chosen values and the ones in file
		for(int i=0;i<chosenRecordInfo.size();i++)
		{
			
			// First composition part
			if (i==0)
			{
			// compare POS value
			if(!chosenRecordInfo.get(i).get(1).equals(CorpusTab.POSConverter(posValue)))
				savedCorrectly = false;
			// compare form type value
			if(!chosenRecordInfo.get(i).get(2).equals(formValue))
				savedCorrectly = false;
			}
			// second composition part
			if(i==1)
			{
			// compare POS value
			if(!chosenRecordInfo.get(i).get(1).equals(CorpusTab.POSConverter(posValue2)))
					savedCorrectly = false;
			// compare form type value
			if(!chosenRecordInfo.get(i).get(2).equals(formValue2))
				savedCorrectly = false;
			}
			
		}
					
		LogUltility.log(test, logger, "saved correctly: " + savedCorrectly);
		assertTrue(savedCorrectly);

		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	
	/**
 * DIC_TC--606:Editing a record that exist in a sentence should be synchronized automatically part1 semantic groups
 * @throws InterruptedException 
 * @throws AWTException 
 * @throws IOException 
 * @throws UnsupportedFlavorException 
 * @throws IllegalAccessException 
 * @throws IllegalArgumentException 
 * @throws NoSuchFieldException 
 * @throws SecurityException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_606_Editing_a_record_that_exist_in_a_sentence_should_be_synchronized_automatically_part1_semantic_groups() throws InterruptedException, AWTException, IOException, UnsupportedFlavorException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
{
	test = extent.createTest("Corpus Tests - DIC_TC--606", "Editing a record that exist in a sentence should be synchronized automatically part 1 semantic groups");

	
	CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
	
	// Click on the Corpus tab
	CorpusTab.tabCorpus.click();

	// search in order to have less sentences
	String searchedWord = Setting.Language.equals("EN")? "test" : "בדיקה";
	CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");

	// Retrieve sentences
	List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
	LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);

	// Pick any random sentence between low and high indexes
	Random randomizer = new Random();
	int randomRecordIndex = randomizer.nextInt(sentencesList.size());
	String randomRecord = sentencesList.get(randomRecordIndex);
	CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
	LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
	
	// Choose any record
	List<String> wordsInSentence = CorpusTab.getSentencesRecords();
	LogUltility.log(test, logger, "Words in sentence : " + wordsInSentence);
	
	// Pick a record that is found in dictionary not compounds
	String chosenWord;
	int wordIndex ;
	boolean inCompound = false;
	boolean noPipe = false;
	do {
		inCompound = false;
		 wordIndex = randomizer.nextInt(wordsInSentence.size());
		chosenWord =  wordsInSentence.get(wordIndex);
		CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
		if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Compound Key"))
			inCompound = true;
		
		// For Hebrew
		noPipe = chosenWord.contains("|");
		
	}
	while(inCompound || noPipe ||  CorpusTab.onlyCharacters(chosenWord));
	
	LogUltility.log(test, logger, "Choose word: "+ chosenWord);
	
	String dictionaryKey = CorpusTab.getDictionaryKeyFromFile(randomRecord.substring(0,6).trim(), wordIndex);
	LogUltility.log(test, logger, "The record dictionary key from file: "+dictionaryKey);
	
	// Get info from app
	ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
	String chosenSemanticGroupsB4Change = null ;
	for(int i=0;i<recordInfo.size();i++)
	{
		if(recordInfo.get(i).get("Dictionary Key").equals(dictionaryKey))
			chosenSemanticGroupsB4Change = recordInfo.get(i).get("Semantic Group");
	}
	
	// Navigate to Dictionary tab
	DictionaryTab.dictionaryTab.click();
	LogUltility.log(test, logger, "Navigate to dictionary tab");
		
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
					
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
					
	//Type in the search field
	String searchWord = dictionaryKey.split("\\}")[1].split("~")[0];
	DictionaryTab.cmpCondition_value.sendKeys(searchWord);
	LogUltility.log(test, logger, "word is typed in the search field");
			
	// Change the last filter value, in case it was changed by other TC
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
	// Click the Retreive button
	DictionaryTab.btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	DictionaryTab.btnRetrieve_same.click();
	
	// Choose record from the list
	CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, dictionaryKey);
	LogUltility.log(test, logger, "Choose record: "+dictionaryKey);
	
	// Get semantic groups values and remove the chosen ones in order to choose a new value
	List<String> availableSemanticGroups = DictionaryTab.getValuesFromApp(DictionaryTab.lst_SemanticGroups);
	List<String> markedSemanticGroups = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_SemanticGroups);
	availableSemanticGroups.removeAll(markedSemanticGroups);
	
	// Choose semantic group value
	String chosenSG = availableSemanticGroups.get(randomizer.nextInt(availableSemanticGroups.size()));
	CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_SemanticGroups, chosenSG);
	LogUltility.log(test, logger, "Choose semantic group value: "+chosenSG);
		
	// Choose a value from Volume[POS] dropdown to synchronize
	String volumePOS = "adadjective";
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown to synchronize: " + volumePOS);
	
	// Navigate back to corpus tab
	CorpusTab.tabCorpus.click();
	LogUltility.log(test, logger, "Navigate back to corpus");
	
	// Click on record again to refresh
	CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
	LogUltility.log(test, logger, "Click on record to refresh");
	
	// Get the new updated values
	// Get info from app
	recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
	String chosenSemanticGroupsAfterChange = null ;
	for(int i=0;i<recordInfo.size();i++)
	{
		if(recordInfo.get(i).get("Dictionary Key").equals(dictionaryKey))
			chosenSemanticGroupsAfterChange = recordInfo.get(i).get("Semantic Group");
	}

	// Compare and verify that semantic group values has been changed
	boolean isChanged = ! chosenSemanticGroupsAfterChange .equals(chosenSemanticGroupsB4Change);
	LogUltility.log(test, logger, "The semantic group values before change: "+	chosenSemanticGroupsB4Change);
	LogUltility.log(test, logger, "The semantic group values after change: "+	chosenSemanticGroupsAfterChange );
	LogUltility.log(test, logger, "The value has been changed succesfully: "+isChanged);
	
	assertTrue(isChanged);
	LogUltility.log(test, logger, "Test Case PASSED");
	
}
	
	
/**
 * DIC_TC--606:Editing a record that exist in a sentence should be synchronized automatically part2 semantic fields
 * @throws InterruptedException 
 * @throws AWTException 
 * @throws IOException 
 * @throws UnsupportedFlavorException 
 * @throws IllegalAccessException 
 * @throws IllegalArgumentException 
 * @throws NoSuchFieldException 
 * @throws SecurityException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_606_Editing_a_record_that_exist_in_a_sentence_should_be_synchronized_automatically_part2_semantic_fields() throws InterruptedException, AWTException, IOException, UnsupportedFlavorException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
{
	test = extent.createTest("Corpus Tests - DIC_TC--606", "Editing a record that exist in a sentence should be synchronized automatically part 2 semantic fields");

	
	CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
	
	// Click on the Corpus tab
	CorpusTab.tabCorpus.click();
				
	// search in order to have less sentences
	String searchedWord = Setting.Language.equals("EN") ? "test" : "בדיקה";
	CorpusTab.searchInCorpus(test, logger, "text", "contains",searchedWord, "all records");

	// Retrieve sentences
	List<String> sentencesList =  CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
	LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);

	// Pick any random sentence between low and high indexes
	Random randomizer = new Random();
	int randomRecordIndex = randomizer.nextInt(sentencesList.size());
	String randomRecord = sentencesList.get(randomRecordIndex);
	CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
	LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
	
	// Choose any record
	List<String> wordsInSentence = CorpusTab.getSentencesRecords();
	LogUltility.log(test, logger, "Words in sentence : " + wordsInSentence);
	
	// Pick a record that is found in dictionary not compounds
	String chosenWord;
	int wordIndex ;
	boolean inCompound = false;
	boolean noPipe = false;
	do {
		inCompound = false;
		 wordIndex = randomizer.nextInt(wordsInSentence.size());
		chosenWord =  wordsInSentence.get(wordIndex);
		CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
		if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Compound Key"))
			inCompound = true;
		
		// For Hebrew
		noPipe  = chosenWord.contains("|");
	}
	while(inCompound || noPipe ||  CorpusTab.onlyCharacters(chosenWord));
	
	LogUltility.log(test, logger, "Choose word: "+ chosenWord);
	
	String dictionaryKey = CorpusTab.getDictionaryKeyFromFile(randomRecord, chosenWord, wordIndex);
	LogUltility.log(test, logger, "The record dictionary key from file: "+dictionaryKey);
	
	// Get info from app
	ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
	String chosenSemanticFieldsB4Change = null ;
	for(int i=0;i<recordInfo.size();i++)
	{
		if(recordInfo.get(i).get("Dictionary Key").equals(dictionaryKey))
			chosenSemanticFieldsB4Change = recordInfo.get(i).get("Semantic Fields");
	}
	
	// Navigate to Dictionary tab
	DictionaryTab.dictionaryTab.click();
	LogUltility.log(test, logger, "Navigate to dictionary tab");
		
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
					
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
					
	//Type in the search field
	String searchWord = dictionaryKey.split("\\}")[1].split("~")[0];
	DictionaryTab.cmpCondition_value.sendKeys(searchWord);
	LogUltility.log(test, logger, "word is typed in the search field");
			
	// Change the last filter value, in case it was changed by other TC
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
	// Click the Retreive button
	DictionaryTab.btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	DictionaryTab.btnRetrieve_same.click();
	
	// Choose record from the list
	CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, dictionaryKey);
	LogUltility.log(test, logger, "Choose record: "+dictionaryKey);
	
	// Get semantic fields values and remove the chosen ones in order to choose a new value
	List<String> availableSemanticFields = DictionaryTab.getValuesFromApp(DictionaryTab.lb_dictionary_semantic_fields);
	List<String> markedSemanticFields = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields);
	availableSemanticFields.removeAll(markedSemanticFields);
	LogUltility.log(test, logger, "Chosen semantic fields: "+markedSemanticFields);
	
	// Choose semantic field value
	String chosenSF = availableSemanticFields.get(randomizer.nextInt(availableSemanticFields.size()));
	CommonFunctions.chooseValueInDropdown(DictionaryTab.lb_dictionary_semantic_fields, chosenSF);
	LogUltility.log(test, logger, "Choose semantic field value: "+chosenSF);
		
	// Choose a value from Volume[POS] dropdown to synchronize
	String volumePOS = "adadjective";
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown to synchronize: " + volumePOS);
	
	// Navigate back to corpus tab
	CorpusTab.tabCorpus.click();
	LogUltility.log(test, logger, "Navigate back to corpus");
	
	// Click on record again to refresh
	CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
	LogUltility.log(test, logger, "Click on record to refresh");
	
	// Get the new updated values
	// Get info from app
	recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
	String chosenSemanticFieldsAfterChange = null ;
	for(int i=0;i<recordInfo.size();i++)
	{
		if(recordInfo.get(i).get("Dictionary Key").equals(dictionaryKey))
			chosenSemanticFieldsAfterChange = recordInfo.get(i).get("Semantic Fields");
	}

	// Compare and verify that semantic group values has been changed
	boolean isChanged = ! chosenSemanticFieldsAfterChange.equals(chosenSemanticFieldsB4Change);
	LogUltility.log(test, logger, "The semantic fields values before change: "+	chosenSemanticFieldsB4Change);
	LogUltility.log(test, logger, "The semantic fields values after change: "+	chosenSemanticFieldsAfterChange);
	LogUltility.log(test, logger, "The value has been changed succesfully: "+isChanged);
	
	assertTrue(isChanged);
	LogUltility.log(test, logger, "Test Case PASSED");
	
}
	
	
/**
 * DIC_TC--606:Editing a record that exist in a sentence should be synchronized automatically part3 transcriptions
 * @throws InterruptedException 
 * @throws AWTException 
 * @throws IOException 
 * @throws UnsupportedFlavorException 
 * @throws IllegalAccessException 
 * @throws IllegalArgumentException 
 * @throws NoSuchFieldException 
 * @throws SecurityException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_606_Editing_a_record_that_exist_in_a_sentence_should_be_synchronized_automatically_part3_transcriptions() throws InterruptedException, AWTException, IOException, UnsupportedFlavorException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
{
	test = extent.createTest("Corpus Tests - DIC_TC--606", "Editing a record that exist in a sentence should be synchronized automatically part 3 transcriptions");

	CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
	
	// Click on the Corpus tab
	CorpusTab.tabCorpus.click();
				
	// search in order to have less sentences
	String searchedWord = Setting.Language.equals("EN") ? "test" : "בדיקה";
	CorpusTab.searchInCorpus(test, logger, "text", "contains",searchedWord, "all records");

	// Retrieve sentences
	List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,20);
	LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);

	// Pick any random sentence between low and high indexes
	Random randomizer = new Random();
	int randomRecordIndex = randomizer.nextInt(sentencesList.size());
	String randomRecord = sentencesList.get(randomRecordIndex);
	CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
	LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
	
	// Choose any record
	List<String> wordsInSentence = CorpusTab.getSentencesRecords();
	LogUltility.log(test, logger, "Words in sentence : " + wordsInSentence);
	
	// Pick a record that is found in dictionary not compounds
	String chosenWord;
	int wordIndex ;
	boolean inCompound = false;
	boolean noPipe = false;
	do {
		inCompound = false;
		 wordIndex = randomizer.nextInt(wordsInSentence.size());
		chosenWord =  wordsInSentence.get(wordIndex);
		CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
		if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Compound Key"))
			inCompound = true;
		
		// For Hebrew
		noPipe = chosenWord.contains("|");
		
	}
	while(inCompound || noPipe || CorpusTab.onlyCharacters(chosenWord));
	
	LogUltility.log(test, logger, "Choose word: "+ chosenWord);
	
	String dictionaryKey = CorpusTab.getDictionaryKeyFromFile(randomRecord, chosenWord, wordIndex);
	LogUltility.log(test, logger, "The record dictionary key from file: "+dictionaryKey);
	
	// Get info from app
	ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
	String chosenTranscriptionB4Change = null ;
	String chosenForm = "";
	for(int i=0;i<recordInfo.size();i++)
		if(recordInfo.get(i).get("Dictionary Key").equals(dictionaryKey))
		{
			chosenTranscriptionB4Change = recordInfo.get(i).get("Transcription");
			chosenForm = recordInfo.get(i).get("Form Type");
			break;
		}
	
	LogUltility.log(test, logger, "Transcription before change:"+chosenTranscriptionB4Change);

	
	// Navigate to Dictionary tab
	CommonFunctions.clickTabs("Dictionary");
	DictionaryTab.dictionaryTab.click();
	LogUltility.log(test, logger, "Navigate to dictionary tab");
		
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
					
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
					
	//Type in the search field
	String searchWord = dictionaryKey.split("\\}")[1].split("~")[0];
	DictionaryTab.cmpCondition_value.sendKeys(searchWord);
	LogUltility.log(test, logger, "word is typed in the search field");
			
	// Change the last filter value, in case it was changed by other TC
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
	// Click the Retreive button
	DictionaryTab.btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	DictionaryTab.btnRetrieve_same.click();
	
	// Choose record from the list
	CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, dictionaryKey);
	LogUltility.log(test, logger, "Choose record: "+dictionaryKey);
	
	// Choose the used form to change it's spelling
	DictionaryTab.chooseValueInDropdownText(DictionaryTab.Forms_list, chosenForm);
	LogUltility.log(test, logger, "Choose from the Forms list: "+chosenForm);

	// Edit transcription
	DictionaryTab.editTranscriptions(test,logger,chosenTranscriptionB4Change);
		
	// Choose a value from Volume[POS] dropdown to synchronize
	String volumePOS = "adadjective";
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown to synchronize: " + volumePOS);
	
	// Navigate back to corpus tab
	CorpusTab.tabCorpus.click();
	LogUltility.log(test, logger, "Navigate back to corpus");
	
	// Click on record again to refresh
	CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
	LogUltility.log(test, logger, "Click on record to refresh");
	
	// Get the new updated values
	// Get info from app
	recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
	String chosenTranscriptionAfterChange = null ;
	for(int i=0;i<recordInfo.size();i++)
	{
		if(recordInfo.get(i).get("Dictionary Key").equals(dictionaryKey))
		{	chosenTranscriptionAfterChange  = recordInfo.get(i).get("Transcription");break;}
	}

	// Compare and verify that semantic group values has been changed
	boolean isChanged = ! chosenTranscriptionAfterChange  .equals(chosenTranscriptionB4Change);
	LogUltility.log(test, logger, "The transcription values before change: "+	chosenTranscriptionB4Change);
	LogUltility.log(test, logger, "The transcription values after change: "+	chosenTranscriptionAfterChange  );
	LogUltility.log(test, logger, "The value has been changed succesfully: "+isChanged);
	
	assertTrue(isChanged);
	
	// Click the revert button
	CorpusTab.bt_revert.click();
	LogUltility.log(test, logger, "Click revert button" );
	
	// Click ok in popup
	CorpusTab.revertFileMsgOK.click();
	LogUltility.log(test, logger, "Click Ok" );
	
	LogUltility.log(test, logger, "Test Case PASSED");
	
}


/**
 * DIC_TC--608:Delete a record that exist in a sentence and replace it with other record
 * @throws InterruptedException 
 * @throws AWTException 
 * @throws IOException 
 * @throws UnsupportedFlavorException 
 * @throws IllegalAccessException 
 * @throws IllegalArgumentException 
 * @throws NoSuchFieldException 
 * @throws SecurityException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_608_Delete_a_record_that_exist_in_a_sentence_and_replace_it_with_other_record() throws InterruptedException, AWTException, IOException, UnsupportedFlavorException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
{
	test = extent.createTest("Corpus Tests - DIC_TC--608", "Delete a record that exist in a sentence and replace it with other record");

	CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
	DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
	
	// Click on the Corpus tab
	CorpusTab.tabCorpus.click();
				
//	// search in order to have less sentences
	String searchedWord = Setting.Language.equals("EN") ? "test" : "בדיקה";
	CorpusTab.searchInCorpus(test, logger, "text", "contains", searchedWord, "all records");

	// Retrieve sentences
	List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
	LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);

	// Pick any random sentence between low and high indexes
	Random randomizer = new Random();
	int randomRecordIndex = randomizer.nextInt(sentencesList.size());
	String randomRecord = sentencesList.get(randomRecordIndex);

	CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
	LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
	
	// Choose any record
	List<String> wordsInSentence = CorpusTab.getSentencesRecords();
	LogUltility.log(test, logger, "Words in sentence : " + wordsInSentence);

	// Pick a record that is found in dictionary not compounds
	String chosenWord;
	int wordIndex ;
	boolean moreThanOneTaggingOpt = false;
	boolean inCompound = false;
	boolean noPipe = false;
	do {
		inCompound = false;
		 wordIndex = randomizer.nextInt(wordsInSentence.size());
		chosenWord =  wordsInSentence.get(wordIndex);
		CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
		if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Compound Key"))
			inCompound = true;
		
		List<List<String>> t = CorpusTab.getTaggingRecords();
		if(t.size() > 1)	
			moreThanOneTaggingOpt = true; 
		
		// For Hebrew avoid choosing records with pipe |
		noPipe  = chosenWord.contains("|");
		
//		if(moreThanOneTaggingOpt)
//			if(!inCompound)
//				break;
	}
	while(inCompound || noPipe || !moreThanOneTaggingOpt );
	
//	String chosenWord= "tested";
//	int wordIndex = 6 ;
	CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
	LogUltility.log(test, logger, "Choose word: "+ chosenWord);
	
	// Get the value before changing from file
	String dictionaryKeyFileB4Delete = CorpusTab.getDictionaryKeyFromFile(randomRecord, chosenWord, wordIndex);
	LogUltility.log(test, logger, "The record dictionary key from file: "+dictionaryKeyFileB4Delete);
	
	// Dictionary Key before change
	String dictionaryKey = CorpusTab.getDictionaryKeyFromFile(randomRecord, chosenWord, wordIndex);
	LogUltility.log(test, logger, "The record dictionary key from file: "+dictionaryKey);
	
	// Navigate to Dictionary tab
	DictionaryTab.dictionaryTab.click();
	LogUltility.log(test, logger, "Navigate to dictionary tab");
		
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
					
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
					
	//Type in the search field
	String searchWord = dictionaryKey.split("\\}")[1].split("~")[0];
	DictionaryTab.cmpCondition_value.sendKeys(searchWord);
	LogUltility.log(test, logger, "word is typed in the search field");
			
	// Change the last filter value, in case it was changed by other TC
	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
	// Click the Retreive button
	DictionaryTab.btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	DictionaryTab.btnRetrieve_same.click();
	
	// Choose record from the list
	CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, dictionaryKey);
	LogUltility.log(test, logger, "Choose record: "+dictionaryKey);
	
	// Press delete
	DictionaryTab.bt_delete.click();
	DictionaryTab.btnYes.click();
	LogUltility.log(test, logger, "Click the delete button");
	
	// Check if the record is attached to other sentences\lexicon 
	List<String> replacments = new ArrayList<String>();
	boolean attached2Other;
	do 
	{
		// Get values from "replace by" list
		List<String> availableReplacments = DictionaryTab.getValuesFromApp(DictionaryTab.cmpPopupValue);
		
		// Choose replacment value
		String chosenReplacment = availableReplacments.get(randomizer.nextInt(availableReplacments.size()));
		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpPopupValue, chosenReplacment);
		LogUltility.log(test, logger, "Choose replacment value: "+chosenReplacment);
		// Get all replacement values in order to compare in corpus
		if(chosenReplacment.equals("Leave empty"))
			chosenReplacment="";
		else
			chosenReplacment = chosenReplacment.split("\\|")[0];
		replacments.add(chosenReplacment);
		DictionaryTab.btnpopupAcceptWindow.click();
		try {
			attached2Other = DictionaryTab.popupWindowMessage(test, logger, "Which is pointed from the corpus file");
		}catch (Exception e) {
			e.printStackTrace();
			attached2Other = false;
		}
		
	}
	while(attached2Other);
	
	// Click ok
	DictionaryTab.btnOk.click();
	LogUltility.log(test, logger, "Click ok");
	
//	// Choose a value from Volume[POS] dropdown to synchronize
//	String volumePOS = "adadjective";
//	CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpVolumePOS, volumePOS);
//	LogUltility.log(test, logger, "Choose value in voluem POS dropdown to synchronize: " + volumePOS);
	
	// Navigate back to corpus tab
	CorpusTab.tabCorpus.click();
	LogUltility.log(test, logger, "Navigate back to corpus");
	
	// Click on record again to refresh
	CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
	LogUltility.log(test, logger, "Click on record to refresh");
//	
//	//  Check that the record isn't tagged anymore in corpus
//	boolean foundInApp = false;
//	WebElement element = CorpusTab.sentencesRecordElement(chosenWord, wordIndex);
//	String notTaggedColor = CommonFunctions.getColor(element);
//	if(notTaggedColor.equals("White"))
//		foundInApp=true;
//	LogUltility.log(test, logger, "The dictionary key isn't tagged in corpus after deletion: "+foundInApp);
//	assertTrue(foundInApp);
	
	// Get info from app
	CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
	LogUltility.log(test, logger, "Choose word: "+ chosenWord);
	ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();

	// Check if found in App
	boolean foundInApp = false;
	for(int i=0;i<recordInfo.size();i++)
		if(replacments.contains(recordInfo.get(i).get("Dictionary Key")) || replacments.contains(""))
			{foundInApp = true;break;}
	
	LogUltility.log(test, logger, "The new dictionary key value found in app - synchronized success:"+foundInApp);
	assertTrue(foundInApp);
	
	// Click save
	CorpusTab.bt_save.click();
	CorpusTab.bt_ok_save.click();
	LogUltility.log(test, logger, "Click the Save button");
	
	try {
		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
	} catch (Exception e) {
//		e.printStackTrace();
		}
			
	// Get the new updated values
	String dictionaryKeyFileAfterDelete = CorpusTab.getDictionaryKeyFromFile(randomRecord, chosenWord, wordIndex);
	LogUltility.log(test, logger, "The record dictionary key from file: "+dictionaryKeyFileAfterDelete);
	boolean isChanged = !dictionaryKeyFileB4Delete.equals(dictionaryKeyFileAfterDelete);
	LogUltility.log(test, logger, "Value is changed: "+isChanged);
	assertTrue(isChanged);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}



/**
 * DIC_TC--657:Check specific words doesn`t have duplicated readings
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 * @throws AWTException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_657_Check_specific_words_doesnt_have_duplicated_readings() throws InterruptedException, FileNotFoundException, AWTException 
{
	test = extent.createTest("Corpus Tests - DIC_TC--657", "Check specific words doesn`t have duplicated readings");

	if(Setting.Language.equals("HE")) return;

	CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
	
	// Click on the Corpus tab
	CorpusTab.tabCorpus.click();
			
	// Get one word from the set
	String[] set = {"bed","red","bee"};
	Random random = new Random();
	String searchWord = set[random.nextInt(3)];
	LogUltility.log(test, logger, "Get sentence with the following word: " + searchWord);
	
	//Get sentences with specific word
	List<String> sentences = CorpusTab.getSentencesWithSpecificWord(searchWord);
	
	String sentence = sentences.get(random.nextInt(sentences.size()));
	String chosenSentenceNumber = CorpusTab.getSentenceNumber(sentence);
	LogUltility.log(test, logger, "Go to the following sentence: " + sentence);
	
	// Go to the sentence
	CorpusTab.tb_goto_line.sendKeys(chosenSentenceNumber);
	LogUltility.log(test, logger, "Inserting number: " + chosenSentenceNumber);
	
	//Click Enter
	Robot r = new Robot();
	r.keyPress(KeyEvent.VK_ENTER);
	r.keyRelease(KeyEvent.VK_ENTER);
	LogUltility.log(test, logger, "Click Enter");	
	
	// Choose the searched word 
	List<String> wordsInSentence = CorpusTab.getSentencesRecords();	
	int wordIndex = wordsInSentence.indexOf(searchWord);
	LogUltility.log(test, logger, "Words in sentence : " + wordsInSentence);

	// Click on the word
	CorpusTab.clickSentencesRecord(searchWord, wordIndex);
	LogUltility.log(test, logger, "Choose word: "+ searchWord);
	
	// Get tagging options
	List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
	LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
	
	// Check that all tagging options are different by comparing form type
	boolean isDifferent = true;
	for(int i=0;i<taggingOptions.size()-1;i++)
	{
		String formType = taggingOptions.get(i).get(2);
		for(int j=i+1;j<taggingOptions.size();j++)
			if(formType.equals(taggingOptions.get(j).get(2)))
				{isDifferent = false;break;}
	}
	
	assertTrue(isDifferent);
	LogUltility.log(test, logger, "There is no duplicated readings (not same Form Type): " + isDifferent);
	LogUltility.log(test, logger, "Test Case PASSED");
		
}
	
	
		/**
		 * DIC_TC--692:Check contraction display in the corpus
		 * @throws IOException 
		 * @throws InterruptedException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_692_Check_contraction_display_in_the_corpus() throws IOException, InterruptedException, AWTException 
	{
		test = extent.createTest("Corpus Tests - DIC_TC--692", "Check contraction display in the corpus");
		
		if(Setting.Language.equals("HE")) return;
		
		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Pick random search word			
		String[] contractions = {"i'm","they're","he's","she's"};
		String[] contractionsDisplayS = {"I | 'm","they | 're","he | 's","she | 's"};
		String[] contractionsDisplayC = {"I | 'm","They | 're","He | 's","She | 's"};
		Random random = new Random();
		int index = random.nextInt(contractions.length);
		String searchWord = contractions[index];
		
		// search in order to have less sentences
		CorpusTab.searchInCorpus(test, logger, "text", "contains", searchWord, "all records");
				
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random sentence
		int randomRecordIndex = random.nextInt(sentencesList.size());
		String randomRecord = sentencesList.get(randomRecordIndex);
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
		// Choose any record
		List<String> wordsInSentence = CorpusTab.getSentencesRecords();
		String tagWord;
		tagWord = contractionsDisplayS[index];
		if(wordsInSentence.indexOf(tagWord)== -1)
			tagWord = contractionsDisplayC[index];
	
		int wordIndex = wordsInSentence.indexOf(tagWord);
		LogUltility.log(test, logger, "Words in sentence : " + wordsInSentence);
		
		// Click on record
		CorpusTab.clickSentencesRecord(tagWord, wordIndex);
		LogUltility.log(test, logger, "Click on the word: "+tagWord);
		
		// Point at the element in order to click on the second compound part
		WebElement place = CorpusTab.sentencesRecordElement(tagWord, wordIndex);
    	String attachPlace = place.getAttribute("ClickablePoint");
		String[] XY = attachPlace.split(",");
	    int XPoint = 0, YPoint = 0;
		XPoint = Integer.parseInt(XY[0])+20;
	    YPoint = Integer.parseInt(XY[1]);
		
		// navigate to the end of the word
		Robot r = new Robot();
		r.mouseMove(XPoint, YPoint);
		r.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		r.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
		
		String actualColor = CommonFunctions.getColor(CorpusTab.sentencesRecordElement(tagWord, wordIndex));
		boolean isContraction = actualColor.equals("Pink");
		assertTrue(isContraction);
		LogUltility.log(test, logger, "Defined as contraction and color is pink: "+isContraction);
		
		// Get tagging options
		ArrayList<HashMap<String, String>> taggingOptions = CorpusTab.getTaggingRecordsRawAllInfo();
		boolean isCopula = false;
		for(int i=0;i<taggingOptions.size();i++)
			if(taggingOptions.get(i).get("POS").equals("copula"))
				isCopula = true;
		
		assertTrue(isCopula);
		LogUltility.log(test, logger, "Defined POS as Copula: "+isCopula);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

		
		/**
		 * DIC_TC--359:Delete the last sentence and click load button
		 * @throws InterruptedException 
		 * @throws AWTException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_359_Delete_the_last_sentence_and_click_load_button() throws InterruptedException, AWTException, IOException
		{
			test = extent.createTest("Corpus Tests - DIC_TC--359", "Delete the last sentence and click load button");

			CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
			
			// Click on the Corpus tab
			CorpusTab.tabCorpus.click();
			
			// Click the reset button
			CorpusTab.bt_reset_search.click();
			LogUltility.log(test, logger, "Click the reset button");
			
			// Retrieve sentences
			List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
//			LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);

			// Pick the last sentence 
			String randomRecord = sentencesList.get(sentencesList.size()-1);
			CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
			LogUltility.log(test, logger, "Choose last sentence from the sentences list: " + randomRecord);		
			
			//Click the delete button
			CorpusTab.bt_delete.click();
			LogUltility.log(test, logger, "Click the revive button");
				
			// Build load file
			CorpusTab.buildSentencesForLoad();
			LogUltility.log(test, logger, "Building load file");
			
			// click load button
			CorpusTab.bt_load.click();
			LogUltility.log(test, logger, "Click the load button");
			
			// click yes button
			CorpusTab.massYesBtn.click();
			LogUltility.log(test, logger, "Click the yes button");
			
			// click no button to save
			CorpusTab.massNoBtn.click();
			LogUltility.log(test, logger, "Click the no button");
			
			// get the list after loading	
//			Thread.sleep(5000);
			List<String> sentencesListAfter = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);	
			boolean loadedSuccesfully = sentencesListAfter.size() > sentencesList.size();	
			LogUltility.log(test, logger, "The sentences were succesfully loaded: "+loadedSuccesfully);
			assertTrue(loadedSuccesfully);
			
			// Click the revert button
			CorpusTab.bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CorpusTab.revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
			
			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
		
		/**
		 * DIC_TC--610:Delete a record that exist in a sentence and leave an empty replacement
		 * @throws InterruptedException 
		 * @throws AWTException 
		 * @throws FileNotFoundException 
		 * @throws IllegalAccessException 
		 * @throws IllegalArgumentException 
		 * @throws NoSuchFieldException 
		 * @throws SecurityException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_610_Delete_a_record_that_exist_in_a_sentence_and_leave_an_empty_replacement() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
		{
			test = extent.createTest("Corpus Tests - DIC_TC--610", "Delete a record that exist in a sentence and leave an empty replacement");
			
			// Initialize pages
			CurrentPage = GetInstance(CorpusTab.class);
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			
			CurrentPage = GetInstance(DictionaryTab.class);
			DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
			
			// Click on the Corpus tab
			CorpusTab.tabCorpus.click();		
			
			// Pick random number and use go to in order to choose it
			Random randomizer = new Random();
			String status="";
			String randomNumber;
			//Check if the sentence is not active -removed-
			do {
				int  sentenceNumber = randomizer.nextInt(15);
				randomNumber = Integer.toString(sentenceNumber);
				CorpusTab.tb_goto_line.sendKeys(randomNumber);
				LogUltility.log(test, logger, "Inserting number: " + randomNumber);
				
				//Click Enter
				Robot r = new Robot();
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
				LogUltility.log(test, logger, "Click Enter");	
				
				// Get sentence status
				status  = CorpusTab.getSenteceStatusAcoordingNumber(randomNumber);
				}
			while(status.equals("not_active"));
			
			// Get the chosen sentence
			String randomSentence = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_corpus_records).get(0);
			LogUltility.log(test, logger, "Choose sentence:"+randomSentence);
			
			
			// Get the words from sentence separately
			List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
			
			// Pick a record that is found in dictionary not compounds
			String chosenWord;
			int wordIndex ;
			boolean inCompound = false;
			boolean notHebrewCont = false;
			do {
				inCompound = false;
				notHebrewCont = false;
				 wordIndex = randomizer.nextInt(wordsInRecord.size());
				chosenWord =  wordsInRecord.get(wordIndex);
				CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
				if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Compound Key"))
					inCompound = true;
				notHebrewCont = chosenWord.contains("|");
			}
			while(inCompound && notHebrewCont);
			
			// Choose word
			CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
			LogUltility.log(test, logger, "Click on: "+chosenWord);
		
			// Get the currently chosen tag
			List<String> wordTagging = CorpusTab.getRecordTaggingFromCorpusFile(randomSentence.substring(0, 5).trim(),chosenWord);
			LogUltility.log(test, logger, "Current tagging: " + wordTagging);
			
			// Navigate to dictionary tab
			CommonFunctions.clickTabs("Dictionary");
			LogUltility.log(test, logger, "Navigate to dictionary tab");
			
			// Search for the record
			DictionaryTab.searchInDictionary(test, logger, "form", "equal", wordTagging.get(0), "all records");
			
			// reassembel the wanted record
			String chosenDic = Setting.Language+"{"+wordTagging.get(1)+"}"+wordTagging.get(0);
			LogUltility.log(test, logger, "Choose from the list: "+chosenDic);
			DictionaryTab.chooseValueInDropdownContains(DictionaryTab.lst_records, chosenDic);
			
			String randomDictionary = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_records).get(0);
			// Click the delete button
			 DictionaryTab.bt_delete.click();
			LogUltility.log(test, logger, "Click the delete button");
			// Click the yes button
			DictionaryTab.btnYes.click();

			boolean popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, randomDictionary);
			while(popupDisplayed)
			{
				// Choose "Leave empty" as a replacement value	
				String repValue = "Leave empty";
		
				// Choose from dropdown
				LogUltility.log(test, logger, "Chosen value: "+repValue);
				DictionaryTab.cmpPopupValue.click();
				CommonFunctions.chooseValueInDropdown(DictionaryTab.replacementWindow, repValue);
				
				// Press replace
				DictionaryTab.bt_utility_accept.click();
				LogUltility.log(test, logger, "Click the 'Replace' button");
				
				// The successfully removed popup displayed
				boolean end = popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, "Record "+randomDictionary+" was successfully removed");
				if(end)
					break;
				else
					popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, randomDictionary);
			}
				
			// Click ok
			DictionaryTab.btnOk.click();
			LogUltility.log(test, logger, "Click the 'Ok' button");
			
			// Navigate back to corpus
			CommonFunctions.clickTabs("Corpus");
			LogUltility.log(test, logger, "Click on Corpus tab");
			
			// Choose word again
			CorpusTab.clickSentencesRecord(chosenWord, wordIndex);
			LogUltility.log(test, logger, "Click on: "+chosenWord);
			
			// Get the available tagging options 
			List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
			LogUltility.log(test, logger, "Get available tagging Options: " + taggingOptions);
			
			// Check that the removed tag is no longer available
			boolean notFound = !taggingOptions.contains(wordTagging);
			LogUltility.log(test, logger, "The deleted tagging option: "+wordTagging+", Is no longer available: "+notFound);
			assertTrue(notFound);
			
			// Click the revert button
			CorpusTab.bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CorpusTab.revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
						
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * DIC_TC--618:Verify the contraction display in the corpus
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_618_Verify_the_contraction_display_in_the_corpus() throws InterruptedException, IOException, AWTException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Corpus Tests - DIC_TC_618", "Verify the contraction display in the corpus");
			 
			if(Setting.Language.equals("HE")) return;

			// Go to monitor Tab
			CurrentPage = GetInstance(MonitorTab.class);
			CurrentPage.As(MonitorTab.class).tabMonitor.click();
			LogUltility.log(test, logger, "Navigate to Monitor tab");
			
			//Insert the sentence in the field
			String sentenceToAdd = "john's going to the sea";
			CurrentPage.As(MonitorTab.class).tb_raw_sentence.sendKeys(sentenceToAdd);
			LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
			
			//Click new button in footer
			LogUltility.log(test, logger, "Click New, Ok buttons");
			CurrentPage.As(MonitorTab.class).bt_new.click();
			//Confirm and click ok
			CurrentPage.As(MonitorTab.class).bt_ok.click();
			
			// Get the words from sentence separately
			CurrentPage = GetInstance(CorpusTab.class);
			List<String> wordsInRecord = CurrentPage.As(CorpusTab.class).getSentencesRecords();	
			LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
			
			// Click on the composition
			CurrentPage.As(CorpusTab.class).clickSentencesRecord(wordsInRecord.get(0), 0);
			LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(0));
						
			// Get all tagging options
			List<List<String>> taggingOptions = CurrentPage.As(CorpusTab.class).getTaggingRecords();
			LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
			
			// Random choose another option from the taggingOptions
			Random randomizer = new Random();
			List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
			LogUltility.log(test, logger, "choose random tagging option: " + randomTaggingOption);
			
			// Choose tagging option
			try {
				CurrentPage.As(CorpusTab.class).chooseTaggingOption(randomTaggingOption);
				LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
			} catch (Exception e) {
				System.out.println("exception: " + e);
			}
			
			String expectedMessage = "You have to select first the POS and Formtype of the composition In the purple line!";
			boolean popupDisplayed = CurrentPage.As(CorpusTab.class).popupWindowMessage(test, logger, "", expectedMessage);
			LogUltility.log(test, logger, "The popup for selecting POS and Formtype has been displayed: "+popupDisplayed);
			assertTrue(popupDisplayed);
			LogUltility.log(test, logger, "Click the Ok button");
			CurrentPage.As(CorpusTab.class).bt_ok_new.click();
			
			// Click the revert button
			CurrentPage.As(CorpusTab.class).bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CurrentPage.As(CorpusTab.class).revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * DIC_TC--619:Verify the contraction with 3 options displayed in the corpus - part 1 contraction
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_619_Verify_the_contraction_with_3_options_displayed_in_the_corpus_part_1_contraction() throws InterruptedException, IOException, AWTException
		{
			// The second option : Levi + 's
			//Thread.sleep(5000);
			test = extent.createTest("Corpus Tests - DIC_TC_619", "Verify the contraction with 3 options displayed in the corpus part 1 contraction");	
			
			if(Setting.Language.equals("HE")) return;

			// Go to monitor Tab
			CurrentPage = GetInstance(MonitorTab.class);
			CurrentPage.As(MonitorTab.class).tabMonitor.click();
			LogUltility.log(test, logger, "Navigate to Monitor tab");
			
			//Insert the sentence in the field
			String sentenceToAdd = "Levi's is a big company";
			CurrentPage.As(MonitorTab.class).tb_raw_sentence.sendKeys(sentenceToAdd);
			LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
			
			//Click new button in footer
			LogUltility.log(test, logger, "Click New, Ok buttons");
			CurrentPage.As(MonitorTab.class).bt_new.click();
			//Confirm and click ok
			CurrentPage.As(MonitorTab.class).bt_ok.click();
			
			// Get the words from sentence separately
			CurrentPage = GetInstance(CorpusTab.class);
			List<String> wordsInRecord = CurrentPage.As(CorpusTab.class).getSentencesRecords();	
			LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
			
			// Click on the composition
			CurrentPage.As(CorpusTab.class).clickSentencesRecord(wordsInRecord.get(0), 0);
			LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(0));
			
			// Get the composition element
			WebElement element = CurrentPage.As(CorpusTab.class).sentencesRecordElement(wordsInRecord.get(0), 0);
			
			// Choose second option from menu
			CurrentPage.As(CorpusTab.class).chooseWordOptionFromMenu( element,1);
			LogUltility.log(test, logger, "Choose the second tagging option" );
				
			// Get all tagging options
			List<List<String>> taggingOptions = CurrentPage.As(CorpusTab.class).getTaggingRecords();
			LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
			
			// Random choose another option from the taggingOptions
			Random randomizer = new Random();
			List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
			LogUltility.log(test, logger, "choose random tagging option: " + randomTaggingOption);
			
			// Choose tagging option
			try {
				CurrentPage.As(CorpusTab.class).chooseTaggingOption(randomTaggingOption);
				LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
			} catch (Exception e) {
				System.out.println("exception: " + e);
			}
			
			String expectedMessage = "You have to select first the POS and Formtype of the composition In the purple line!";
			boolean popupDisplayed = CurrentPage.As(CorpusTab.class).popupWindowMessage(test, logger, "", expectedMessage);
			LogUltility.log(test, logger, "The popup for selecting POS and Formtype has been displayed: "+popupDisplayed);
			assertTrue(popupDisplayed);
			LogUltility.log(test, logger, "Click the Ok button");
			CurrentPage.As(CorpusTab.class).bt_ok_new.click();
			
			// Click the revert button
			CurrentPage.As(CorpusTab.class).bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CurrentPage.As(CorpusTab.class).revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--619:Verify the contraction with 3 options displayed in the corpus - part 2 dictionary
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_619_Verify_the_contraction_with_3_options_displayed_in_the_corpus_part_2_dictionary() throws InterruptedException, IOException, AWTException
		{
			// The first and third options : Levi's and Levi | 's
			//Thread.sleep(5000);
			test = extent.createTest("Corpus Tests - DIC_TC_619", "Verify the contraction with 3 options displayed in the corpus part 2 dictionary");	
			
			if(Setting.Language.equals("HE")) return;

			// Go to monitor Tab
			CurrentPage = GetInstance(MonitorTab.class);
			CurrentPage.As(MonitorTab.class).tabMonitor.click();
			LogUltility.log(test, logger, "Navigate to Monitor tab");
			
			//Insert the sentence in the field
			String sentenceToAdd = "Levi's is a big company";
			CurrentPage.As(MonitorTab.class).tb_raw_sentence.sendKeys(sentenceToAdd);
			LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
			
			//Click new button in footer
			LogUltility.log(test, logger, "Click New, Ok buttons");
			CurrentPage.As(MonitorTab.class).bt_new.click();
			//Confirm and click ok
			CurrentPage.As(MonitorTab.class).bt_ok.click();
			
			// Get the words from sentence separately
			CurrentPage = GetInstance(CorpusTab.class);
			List<String> wordsInRecord = CurrentPage.As(CorpusTab.class).getSentencesRecords();	
			LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
					
			// Click on the composition
			CurrentPage.As(CorpusTab.class).clickSentencesRecord(wordsInRecord.get(0), 0);
			LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(0));
			
			// Get the composition element
			WebElement element = CurrentPage.As(CorpusTab.class).sentencesRecordElement(wordsInRecord.get(0), 0);
			
			// Choose random option from menu (not the second)
			Random randomizer = new Random();
			int[] options= {0,2};
			int index = options[randomizer.nextInt(options.length)];
			
			CurrentPage.As(CorpusTab.class).chooseWordOptionFromMenu(element,index);
			LogUltility.log(test, logger, "Choose from menu: "+wordsInRecord.get(index) );
				
			// Get all tagging options
			List<List<String>> taggingOptions = CurrentPage.As(CorpusTab.class).getTaggingRecords();
			LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
			
			// Random choose another option from the taggingOptions
			randomizer = new Random();
			List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
			LogUltility.log(test, logger, "choose random tagging option: " + randomTaggingOption);
			
			boolean taggedSuccessfully = false;
			// Choose tagging option
			try {
				CurrentPage.As(CorpusTab.class).chooseTaggingOption(randomTaggingOption);
				taggedSuccessfully = true;
				LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
			} catch (Exception e) {
				System.out.println("exception: " + e);
			}
			
			LogUltility.log(test, logger, "The record has been tagged successfully: "+taggedSuccessfully);
			assertTrue(taggedSuccessfully);
			
			// Click the revert button
			CurrentPage.As(CorpusTab.class).bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CurrentPage.As(CorpusTab.class).revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * DIC_TC--621:Verify the defined compounds display in the corpus
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_621_Verify_the_defined_compounds_display_in_the_corpus() throws InterruptedException, IOException, AWTException
		{
			test = extent.createTest("Corpus Tests - DIC_TC--621", "Verify the defined compounds display in the corpus");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			CurrentPage = GetInstance(MonitorTab.class);
			MonitorTab MonitorTab = CurrentPage.As(MonitorTab.class);
			CurrentPage = GetInstance(CorpusTab.class);
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
					
			// Get text to use for search
			List<String> records = CompoundsTab.getRecordsFromCompoundsWithCompletedStatus();
			LogUltility.log(test, logger, "Getting clean records from compounds.txt ");		
			
			// Pick random compound record
			Random random = new Random();
			String randomRecord = records.get(random.nextInt(records.size()));
//			String randomRecord ="שפה זרה";
			LogUltility.log(test, logger, "Compound to use: " + randomRecord);
			
			// Add new sentence with compound and tag it as compound to make sure that the compound can be found in corpus
			// Go to monitor and insert a new sentence with the compound
			MonitorTab.tabMonitor.click();
			LogUltility.log(test, logger, "Click on the Monitor tab");
			
			String sentenceToAdd = randomRecord +" "+ CorpusTab.getRandomSentence();
			LogUltility.log(test, logger, "Insert the following sentence: "+sentenceToAdd);
			MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
				
			// Press the new button
			LogUltility.log(test, logger, "Click the new button");
			MonitorTab.bt_new.click();
			LogUltility.log(test, logger, "Click the ok button");
			MonitorTab.bt_ok.click();
			
			// Get the sentence records
			List<String> recordsList = CorpusTab.getSentencesRecords();
			LogUltility.log(test, logger, "Records list: " + recordsList);
			
			// Tag the compound 
			WebElement element = CorpusTab.sentencesRecordElement(recordsList.get(0), 0);
			CorpusTab.chooseCompoundTaggingOption(randomRecord,element);
			
			// Click on the compound beginning again
			CorpusTab.clickSentencesRecord(recordsList.get(0), 0);
			LogUltility.log(test, logger, "Click again on: "+recordsList.get(0));
			
			// Check if it has been tagged as compound
			boolean taggedAsCompound = false;
			if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Compound Key"))
				taggedAsCompound = true;
			
			LogUltility.log(test, logger, "The record has been tagged as compound successfully: "+taggedAsCompound);
			assertTrue(taggedAsCompound);
			
			// Click the revert button
			CorpusTab.bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CorpusTab.revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--629:Verify compound with composition displayed in the corpus
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_629_Verify_compound_with_composition_displayed_in_the_corpus() throws InterruptedException, IOException, AWTException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Corpus Tests - DIC_TC--629", "Verify compound with composition displayed in the corpus");	
			
			if(Setting.Language.equals("HE")) return;

			// Click reset button
			CurrentPage.As(CorpusTab.class).bt_reset_search.click();
			LogUltility.log(test, logger, "Click on the reset button");
			
			// Go to the sentence
			String sentenceNum = "5529";
			CurrentPage.As(CorpusTab.class).tb_goto_line.sendKeys(sentenceNum);
			LogUltility.log(test, logger, "Inserting number: " + sentenceNum);
			
			//Click Enter
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			LogUltility.log(test, logger, "Click Enter");	
			
			// Get sentence words
			List<String> wordsInRecord = CurrentPage.As(CorpusTab.class).getSentencesRecords();	
			LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
			
			// Choose the 7th words, "Area + 's"
			CurrentPage.As(CorpusTab.class).clickSentencesRecord(wordsInRecord.get(6), 6);
			LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(6));
			
			// Get the composition element
			WebElement element = CurrentPage.As(CorpusTab.class).sentencesRecordElement(wordsInRecord.get(6), 6);
			
			// Choose the first option "Area + 's"	
			CurrentPage.As(CorpusTab.class).chooseWordOptionFromMenu( element,0);
			LogUltility.log(test, logger, "Choose from menu for: "+wordsInRecord.get(6) );
				
			// Get all tagging options
			List<List<String>> taggingOptions = CurrentPage.As(CorpusTab.class).getTaggingRecords();
			LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
			
			// Check if it tagged as composition - purple bar
			boolean taggedAsComposition = false;
			if(CurrentPage.As(CorpusTab.class).ll_dictionary_key.getAttribute("Name").equals("Composition:"))
				taggedAsComposition = true;
			LogUltility.log(test, logger, "The record is set as composition: "+taggedAsComposition);
			assertTrue(taggedAsComposition);
			
			// Choose the second option "Area | 's"	
			CurrentPage.As(CorpusTab.class).chooseWordOptionFromMenu( element,1);
			LogUltility.log(test, logger, "Choose from menu for: "+wordsInRecord.get(6) );
			
			
			// Check that the purple bar disappears
			boolean noBar = false;
			if(CurrentPage.As(CorpusTab.class).ll_dictionary_key.getAttribute("Name").equals("Dictionary Key"))
				noBar = true;
			LogUltility.log(test, logger, "There is no purple bar: "+noBar);
			assertTrue(noBar);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * DIC_TC--685:Check when select the grammar from the focus dropdown
		 * @throws InterruptedException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_685_Check_when_select_the_grammar_from_the_focus_dropdown() throws InterruptedException 
		{
			test = extent.createTest("Corpus Tests - DIC_TC--685", "Check when select the grammar from the focus dropdown");
			
			CurrentPage = GetInstance(CorpusTab.class);
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			
			// Click on the Corpus tab
			CorpusTab.tabCorpus.click();		
			
			// Set focus dropdown value
			String focusValue = "Grammar";
			CommonFunctions.chooseValueInDropdown(CorpusTab.cb_show, focusValue);
			LogUltility.log(test, logger, "Change value in 'focus' dropdown: "+focusValue);
			
			// Click on the 'Color Legends'-question mark button
			CorpusTab.bt_help.click();
			LogUltility.log(test, logger, "Click on the 'help' button to open the 'Colors Legend' ");
					
			// Get the displayed Colors legend values
	    	List<String> displayedValues = CorpusTab.getColorsLegendDisplayedValues();
			LogUltility.log(test, logger, "Displayed values in Colors Legend: "+displayedValues);
			
			// Initialize the expected values
			List<String> expectedValues = new ArrayList<String>();
			expectedValues.add("Tagged as part of a compound");
			expectedValues.add(	"Tagged as contraction");
			expectedValues.add("Tagged as composition");
			expectedValues.add("Tagged as regular word");
			expectedValues.add(	"Tagged as regular word, but could \r\nbe a composition, contraction or \r\npart of a compound");
			expectedValues.add("Tagged as regular word, potentially \r\npart of an UNDEFINED compound");
			expectedValues.add("Not tagged, \r\ntext is not selected yet");
			expectedValues.add(	"Not tagged, \r\nrecognized as part of compound");
			expectedValues.add("Not tagged, \r\nrecognized as a contraction");
			expectedValues.add("Not tagged, \r\nrecognized as a composition");
			expectedValues.add("Not tagged, \r\nfound in lexicon");
			expectedValues.add("not found in lexicon");
			
			LogUltility.log(test, logger, "Expected values: "+expectedValues);
			
			// Compare the expected and actual values
			Collections.sort(expectedValues);
			Collections.sort(displayedValues);
			boolean same = expectedValues.equals(displayedValues);
			LogUltility.log(test, logger, "The values are displayed as expected: "+same);
			assertTrue(same);
			
			// Close the colors legend window
			CorpusTab.close_pn_legend.click();
			LogUltility.log(test, logger, "Close color legends window");
			
			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
		/**
		 * DIC_TC--686:Check when select the "S Type" from the focus dropdown
		 * @throws InterruptedException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_686_Check_when_select_the_S_Type_from_the_focus_dropdown() throws InterruptedException 
		{
			test = extent.createTest("Corpus Tests - DIC_TC--686", "Check when select the \"S Type\" from the focus dropdown");
			
			CurrentPage = GetInstance(CorpusTab.class);
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			
			// Click on the Corpus tab
			CorpusTab.tabCorpus.click();		
			
			// Set focus dropdown value
			String focusValue = "S Type";
			CommonFunctions.chooseValueInDropdown(CorpusTab.cb_show, focusValue);
			LogUltility.log(test, logger, "Change value in 'focus' dropdown: "+focusValue);
			
			// Click on the 'Color Legends'-question mark button
			CorpusTab.bt_help.click();
			LogUltility.log(test, logger, "Click on the 'help' button to open the 'Colors Legend' ");
					
			// Get the displayed Colors legend values
	    	List<String> displayedValues = CorpusTab.getColorsLegendDisplayedValues();
			LogUltility.log(test, logger, "Displayed values in Colors Legend: "+displayedValues);
			
			// Initialize the expected values
			List<String> expectedValues = new ArrayList<String>();
			expectedValues.add("WH Question");
			expectedValues.add("Yes No Question");
			expectedValues.add("Alternative Question");
			expectedValues.add("Cleft Sentence");
			expectedValues.add("Pseudo Cleft");
			expectedValues.add("Topicalized");
			expectedValues.add("Imperative");
			expectedValues.add(	"Exclamative");
			expectedValues.add("Carrier Phrase");
			expectedValues.add("apposition");
			expectedValues.add("Declarative");
			
			LogUltility.log(test, logger, "Expected values: "+expectedValues);
			
			// Compare the expected and actual values
			Collections.sort(expectedValues);
			Collections.sort(displayedValues);
			boolean same = expectedValues.equals(displayedValues);
			LogUltility.log(test, logger, "The values are displayed as expected: "+same);
			assertTrue(same);
			
			// Close the colors legend window
			CorpusTab.close_pn_legend.click();
			LogUltility.log(test, logger, "Close color legends window");
			
			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
		/**
		 * DIC_TC--687:Check when select the Focus from the focus drop-down
		 * @throws InterruptedException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_687_Check_when_select_the_Focus_from_the_focus_drop_down() throws InterruptedException 
		{
			test = extent.createTest("Corpus Tests - DIC_TC--687", "Check when select the Focus from the focus drop-down");
			
			CurrentPage = GetInstance(CorpusTab.class);
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			
			// Click on the Corpus tab
			CorpusTab.tabCorpus.click();		
			
			// Set focus dropdown value
			String focusValue = "Focus";
			CommonFunctions.chooseValueInDropdown(CorpusTab.cb_show, focusValue);
			LogUltility.log(test, logger, "Change value in 'focus' dropdown: "+focusValue);
			
			// Click on the 'Color Legends'-question mark button
			CorpusTab.bt_help.click();
			LogUltility.log(test, logger, "Click on the 'help' button to open the 'Colors Legend' ");
					
			// Get the displayed Colors legend values
	    	List<String> displayedValues = CorpusTab.getColorsLegendDisplayedValues();
			LogUltility.log(test, logger, "Displayed values in Colors Legend: "+displayedValues);
			
			// Initialize the expected values
			List<String> expectedValues = new ArrayList<String>();
			expectedValues.add("Hard Giveness");
			expectedValues.add("Soft Giveness");
			expectedValues.add("Hard Contrast");
			expectedValues.add("Soft Contrast");

			LogUltility.log(test, logger, "Expected values: "+expectedValues);
			
			// Compare the expected and actual values
			Collections.sort(expectedValues);
			Collections.sort(displayedValues);
			boolean same = expectedValues.equals(displayedValues);
			LogUltility.log(test, logger, "The values are displayed as expected: "+same);
			assertTrue(same);
			
			// Close the colors legend window
			CorpusTab.close_pn_legend.click();
			LogUltility.log(test, logger, "Close color legends window");
			
			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
		
		/**
		 * DIC_TC--689:Check Abbreviations words in sentences
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_689_Check_Abbreviations_words_in_sentences() throws InterruptedException, IOException, AWTException 
		{
			test = extent.createTest("Corpus Tests - DIC_TC--689", "Check Abbreviations words in sentences");
			
			if(Setting.Language.equals("HE")) return;

			// Initialize pages
			CurrentPage = GetInstance(CorpusTab.class);
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			
			CurrentPage = GetInstance(DictionaryTab.class);
			DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
			
			MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
			
			// Get abbreviations from dictionary.txt file
			List<String> abbreviations = DictionaryTab.getAbbreviationsFromDictionary();
			LogUltility.log(test, logger,"Getting abbreviations from Dictionary.txt file");
			Random random = new Random();
			String abbv = abbreviations.get(random.nextInt(abbreviations.size()));
			LogUltility.log(test, logger, "Chosen abbreviation: "+abbv);
			
			// Click on the Corpus tab
			CorpusTab.tabCorpus.click();
						
			//Click the new button in the footer
			CorpusTab.bt_new.click();
			//Click ok 
			CorpusTab.bt_ok_new.click();
			
			//Insert the sentence in the field
			String sentenceToAdd = CorpusTab.getRandomSentence()+" "+abbv;
			MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
			
			//Click new button in footer
			MonitorTab.bt_new.click();
			//Confirm and click ok
			MonitorTab.bt_ok.click();
			
			// Get sentence words
			List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
			LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
			
			// Choose the abbreviation word
			String word = wordsInRecord.get(wordsInRecord.size()-1);
			int index =  wordsInRecord.size()-1;
			CorpusTab.clickSentencesRecord(word,index);
			LogUltility.log(test, logger, "Click on: "+word);
			
			// Get the abbreviation element
			WebElement element =CorpusTab.sentencesRecordElement(word, index);
			
			// Choose the first option 
			CorpusTab.chooseWordOptionFromMenu(element,0);
			LogUltility.log(test, logger, "Choose the first option from the menu");
			
			// Get the displayed word text
			List<String> wordsInRecordFirstOption = CorpusTab.getSentencesRecords();
			String firstOptionWord = wordsInRecordFirstOption.get(wordsInRecordFirstOption.size()-1);
			LogUltility.log(test, logger, "Chosen word: "+firstOptionWord);
			
			// Verify that the dot is included in the text
			boolean dotIncluded = !firstOptionWord.contains("|");
			LogUltility.log(test, logger, "The dot is included: "+dotIncluded);
			
			// Choose the second option 
			CorpusTab.chooseWordOptionFromMenu(element,1);
			LogUltility.log(test, logger, "Choose the second option from the menu");
			
			// Get the displayed word text
			List<String> wordsInRecordSecondOption = CorpusTab.getSentencesRecords();
			String secondOptionWord = wordsInRecordSecondOption.get(wordsInRecordSecondOption.size()-1);
			LogUltility.log(test, logger, "Chosen word: "+secondOptionWord);
			
			// Verify that the dot is excluded from the text
			boolean dotExcluded = secondOptionWord.contains("|");
			LogUltility.log(test, logger, "The dot is excluded: "+dotExcluded);
			assertTrue(dotExcluded);
			
			// Click the revert button
			CorpusTab.bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CorpusTab.revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
			
			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
		/**
		 * DIC_TC--716:Synchronization - Compounds words doesn`t losing tagging after modifying the semantics- Bug DIC-118
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_716_Synchronization_Compounds_words_doesnt_losing_tagging_after_modifying_the_semantics() throws InterruptedException, IOException, AWTException
		{
			test = extent.createTest("Corpus Tests - DIC_TC--716", "Synchronization - Compounds words doesn`t losing tagging after modifying the semantics- Bug DIC-118");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			CurrentPage = GetInstance(MonitorTab.class);
			MonitorTab MonitorTab = CurrentPage.As(MonitorTab.class);
			CurrentPage = GetInstance(CorpusTab.class);
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			CurrentPage = GetInstance(DictionaryTab.class);
			DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);	
			
			// Get text to use for search
			List<String> records = CompoundsTab.getRecordsFromCompoundsWithCompletedStatus();
			LogUltility.log(test, logger, "Getting clean records from compounds.txt ");		
			
			// Pick random compound record
			Random random = new Random();
			records.remove("אין גבול");records.remove("אין צורך");
			String randomRecord = records.get(random.nextInt(records.size()));
//			String randomRecord = "אין צורך";
			LogUltility.log(test, logger, "Compound to use: " + randomRecord);
			
			// Add new sentence with compound and tag it as compound to make sure that the compound can be found in corpus
			// Go to monitor and insert a new sentence with the compound
			MonitorTab.tabMonitor.click();
			LogUltility.log(test, logger, "Click on the Monitor tab");
			
			String sentenceToAdd = randomRecord +" "+ CorpusTab.getRandomSentence();
			LogUltility.log(test, logger, "Insert the following sentence: "+sentenceToAdd);
			MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
				
			// Press the new button
			LogUltility.log(test, logger, "Click the new button");
			MonitorTab.bt_new.click();
			LogUltility.log(test, logger, "Click the ok button");
			MonitorTab.bt_ok.click();
			
			// Get the sentence records
			List<String> recordsList = CorpusTab.getSentencesRecords();
			LogUltility.log(test, logger, "Records list: " + recordsList);
			
			// Tag the compound 
			WebElement element = CorpusTab.sentencesRecordElement(recordsList.get(0), 0);
			CorpusTab.chooseCompoundTaggingOption(randomRecord,element);
			
			// Navigate to compound tab and search for the used compound
			CommonFunctions.clickTabs("Compounds");
			LogUltility.log(test, logger, "Navigate to compounds tab");		
			CompoundsTab.searchInCompound(test, logger, "form", "equal", randomRecord, "all records");
			
			// Choose the record and modify semantic groups
			// Retrieve records
			List<String> compoundsRecordsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records retreived: " + compoundsRecordsList);

			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = compoundsRecordsList.get(randomizer.nextInt(compoundsRecordsList.size()));
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Change the first lemma value
			String lemmaValue = CompoundsTab.getSelectedTaggingValue("Lemma",0);
			LogUltility.log(test, logger, "The first lemma value is: "+lemmaValue);
						
			// Get the chosen Semantic Groups list value
			List<String> chosen1lexicalGroupsList = CompoundsTab.getChosenValueInDropdown(CompoundsTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "chosen Semantic Groups list before change: " + chosen1lexicalGroupsList);
			
			// Change or Select New Semantic Groups
			// Get all the values from the Semantic Groups list
			List<String> lst_SemanticGroups = CompoundsTab.getValuesFromApp(CompoundsTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "all the values from the Semantic Groups list: " + lst_SemanticGroups);
			
			// Change or Select New Semantic Groups
			
			Random randomfield = new Random();
			lst_SemanticGroups.removeAll(chosen1lexicalGroupsList);
			String randomselect = lst_SemanticGroups.get(randomfield.nextInt(lst_SemanticGroups.size()));
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_SemanticGroups, randomselect);
			LogUltility.log(test, logger, "Choose from Semantic Groups list: " + randomselect);
			
			// Navigate back to corpus
			CommonFunctions.clickTabs("Corpus");
			LogUltility.log(test, logger, "Navigate to corpus tab");		
			
			// For syncing
			CorpusTab.bt_prev.click();
			CorpusTab.bt_next.click();
			
			// Click on the compound beginning again
			CorpusTab.clickSentencesRecord(recordsList.get(0), 0);
			LogUltility.log(test, logger, "Click again on: "+recordsList.get(0));
			
			// Check if it is tagged as compound
			boolean taggedAsCompound = false;
			if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Compound Key"))
				taggedAsCompound = true;
			
			LogUltility.log(test, logger, "The record is tagged as compound: "+taggedAsCompound);
			assertTrue(taggedAsCompound);
			
			// Navigate to dictionary tab
			CommonFunctions.clickTabs("Dictionary");
			LogUltility.log(test, logger, "Navigate to dictionary tab");
			String cleanDic = lemmaValue.split("\\}")[1].split("~")[0];
			DictionaryTab.searchInDictionary(test, logger, "form", "equal", cleanDic, "all records");
			
			// Choose the record and modify semantic groups
			// Retrieve records
			List<String> dictionaryRecordsList = DictionaryTab.getValuesFromApp(DictionaryTab.lst_records);
			LogUltility.log(test, logger, "Records retreived: " + dictionaryRecordsList);

			// Pick a random record
//			String randomDic = recordsList.get(randomizer.nextInt(recordsList.size()));
			String randomDic = lemmaValue.split(" \\(")[0];
			CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomDic);
			LogUltility.log(test, logger, "Choose from record list: " + randomDic);
			
			// Get the chosen Semantic Groups list value
			List<String> chosen1lexicalGroupsListDic = DictionaryTab.getChosenValueInDropdown(DictionaryTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "chosen Semantic Groups list before change: " + chosen1lexicalGroupsListDic);
			
			// Change or Select New Semantic Groups
			// Get all the values from the Semantic Groups list
			List<String> lst_SemanticGroupsDic = DictionaryTab.getValuesFromApp(DictionaryTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "all the values from the Semantic Groups list: " + lst_SemanticGroupsDic);
			
			// Change or Select New Semantic Groups
			lst_SemanticGroupsDic.removeAll(chosen1lexicalGroupsListDic);
			String randomselectDic = lst_SemanticGroups.get(randomfield.nextInt(lst_SemanticGroupsDic.size()));
			CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_SemanticGroups, randomselectDic);
			LogUltility.log(test, logger, "Choose from Semantic Groups list: " + randomselectDic);
			
			// Navigate back to corpus
			CommonFunctions.clickTabs("Corpus");
			LogUltility.log(test, logger, "Navigate to corpus tab");	
			
			// For syncing
			CorpusTab.bt_prev.click();
			CorpusTab.bt_next.click();
			
			// Click on the compound beginning again
			CorpusTab.clickSentencesRecord(recordsList.get(0), 0);
			LogUltility.log(test, logger, "Click again on: "+recordsList.get(0));
			
			// Check if it is tagged as compound
			taggedAsCompound = false;
			if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Compound Key"))
				taggedAsCompound = true;
			
			LogUltility.log(test, logger, "The record is tagged as compound: "+taggedAsCompound);
			assertTrue(taggedAsCompound);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--722:select sentence that contain deleted compound - Bug DIC-1210
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_722_select_sentence_that_contain_deleted_compound_Bug_DIC_1210() throws InterruptedException, IOException, AWTException
		{
			test = extent.createTest("Corpus Tests - DIC_TC--722", "select sentence that contain deleted compound - Bug DIC-1210");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			CurrentPage = GetInstance(MonitorTab.class);
			MonitorTab MonitorTab = CurrentPage.As(MonitorTab.class);
			CurrentPage = GetInstance(CorpusTab.class);
			CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);	
			
			// Get text to use for search
			List<String> records = CompoundsTab.getRecordsFromCompoundsWithCompletedStatus();
			LogUltility.log(test, logger, "Getting clean records from compounds.txt ");		
			
			// Pick random compound record
			Random random = new Random();
			String randomRecord = records.get(random.nextInt(records.size()));
//			String randomRecord = "vitamin B 12";
			LogUltility.log(test, logger, "Compound to use: " + randomRecord);
			
			// Add new sentence with compound and tag it as compound to make sure that the compound can be found in corpus
			// Go to monitor and insert a new sentence with the compound
			MonitorTab.tabMonitor.click();
			LogUltility.log(test, logger, "Click on the Monitor tab");
			
			String sentenceToAdd = randomRecord +" "+ CorpusTab.getRandomSentence();
			LogUltility.log(test, logger, "Insert the following sentence: "+sentenceToAdd);
			MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
				
			// Press the new button
			LogUltility.log(test, logger, "Click the new button");
			MonitorTab.bt_new.click();
			LogUltility.log(test, logger, "Click the ok button");
			MonitorTab.bt_ok.click();
			
			// Get the sentence records
			List<String> recordsList = CorpusTab.getSentencesRecords();
			LogUltility.log(test, logger, "Records list: " + recordsList);
			
			// Tag the compound 
			WebElement element = CorpusTab.sentencesRecordElement(recordsList.get(0), 0);
			CorpusTab.chooseCompoundTaggingOption(randomRecord,element);
			
			// Navigate to compound tab and search for the used compound
			CommonFunctions.clickTabs("Compounds");
			LogUltility.log(test, logger, "Navigate to compounds tab");		
			CompoundsTab.searchInCompound(test, logger, "form", "equal", randomRecord, "all records");
			
			// Choose the record and modify semantic groups
			// Retrieve records
			List<String> compoundsRecordsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records retreived: " + compoundsRecordsList);

			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = compoundsRecordsList.get(randomizer.nextInt(compoundsRecordsList.size()));
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Delete the compound
			CompoundsTab.bt_delete.click();
			LogUltility.log(test, logger, "Click the delete button");
			
			// Click the yes button in the confirmation message 
			CompoundsTab.yesDelete.click();
			LogUltility.log(test, logger, "Press yes button");
			Thread.sleep(1000);
			CompoundsTab.btnOk.click();
			
			// Navigate back to corpus
			CommonFunctions.clickTabs("Corpus");
			LogUltility.log(test, logger, "Navigate to corpus tab");	
			
			// For syncing
			CorpusTab.bt_prev.click();
			CorpusTab.bt_next.click();
			
			// Click on the compound beginning again
			CorpusTab.clickSentencesRecord(recordsList.get(0), 0);
			LogUltility.log(test, logger, "Click again on: "+recordsList.get(0));
			
			// Check if it is tagged as compound
			boolean taggedAsDictionary = false;
			System.out.println(CorpusTab.ll_dictionary_key.getAttribute("Name"));
			if(CorpusTab.ll_dictionary_key.getAttribute("Name").equals("Dictionary Key"))
				taggedAsDictionary = true;
			
			LogUltility.log(test, logger, "The record is tagged as dictionary: "+taggedAsDictionary);
			assertTrue(taggedAsDictionary);
			
			// Click the revert button
			CorpusTab.bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CorpusTab.revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * DIC_TC--617:Verify the composition display in the corpus
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_617_Verify_the_composition_display_in_the_corpus() throws InterruptedException, IOException, AWTException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Corpus Tests - DIC_TC--617", "Verify the composition display in the corpus");
			 
			if(Setting.Language.equals("HE")) return;

			// Go to monitor Tab
			CurrentPage = GetInstance(MonitorTab.class);
			CurrentPage.As(MonitorTab.class).tabMonitor.click();
			LogUltility.log(test, logger, "Navigate to Monitor tab");
			
			//Insert the sentence in the field
			String sentenceToAdd = "edable food";
			CurrentPage.As(MonitorTab.class).tb_raw_sentence.sendKeys(sentenceToAdd);
			LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
			
			//Click new button in footer
			LogUltility.log(test, logger, "Click New, Ok buttons");
			CurrentPage.As(MonitorTab.class).bt_new.click();
			//Confirm and click ok
			CurrentPage.As(MonitorTab.class).bt_ok.click();
			
			// Get the words from sentence separately
			CurrentPage = GetInstance(CorpusTab.class);
			List<String> wordsInRecord = CurrentPage.As(CorpusTab.class).getSentencesRecords();	
			LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
			
			// Click on the composition
			CurrentPage.As(CorpusTab.class).clickSentencesRecord(wordsInRecord.get(0), 0);
			LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(0));
			
			// Get the composition element
			WebElement element = CurrentPage.As(CorpusTab.class).sentencesRecordElement(wordsInRecord.get(0), 0);
			
			// Choose second option from menu
			CurrentPage.As(CorpusTab.class).chooseWordOptionFromMenu(element,0);
			LogUltility.log(test, logger, "Choose the first tagging option from menu" );
						
			// Get all tagging options
			List<List<String>> taggingOptions = CurrentPage.As(CorpusTab.class).getTaggingRecords();
			LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
			
			// Random choose another option from the taggingOptions
			Random randomizer = new Random();
			List<String> randomTaggingOption = taggingOptions.get(randomizer.nextInt(taggingOptions.size()));
			LogUltility.log(test, logger, "choose random tagging option: " + randomTaggingOption);
			
			// Choose tagging option
			try {
				CurrentPage.As(CorpusTab.class).chooseTaggingOption(randomTaggingOption);
				LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
			} catch (Exception e) {
				System.out.println("exception: " + e);
			}
			
			String expectedMessage = "You have to select first the POS and Formtype of the composition In the purple line!";
			boolean popupDisplayed = CurrentPage.As(CorpusTab.class).popupWindowMessage(test, logger, "", expectedMessage);
			LogUltility.log(test, logger, "The popup for selecting POS and Formtype has been displayed: "+popupDisplayed);
			assertTrue(popupDisplayed);
			LogUltility.log(test, logger, "Click the Ok button");
			CurrentPage.As(CorpusTab.class).bt_ok_new.click();
			
			// Click the revert button
			CurrentPage.As(CorpusTab.class).bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CurrentPage.As(CorpusTab.class).revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--612:Test merge button
		 * @throws InterruptedException 
		 * @throws AWTException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_612_Test_merge_button() throws InterruptedException, AWTException, IOException
		{
			test = extent.createTest("Corpus Tests - DIC_TC--612", "Test merge button");

			CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
			MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
			MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);

			// Click on the Corpus tab
			CommonFunctions.clickTabs("Corpus");
			LogUltility.log(test, logger, "Navigate to corpus");
			
			// Search to work with less records
			String searchedWord = Setting.Language.equals("EN")? "hello" : "בדיקה";
			CorpusTab.searchInCorpus(test, logger, "text", "contains",searchedWord ,"all records");
			
			// Backup corpus file before making any changes
			String source = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
			String dest =  Setting.AppPath+"corpus.txt";
			LogUltility.log(test, logger, "Backup the corpus file to: "+dest);
			boolean backupStatus = CorpusTab.backupFile(source, dest);
			LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
			assertTrue(backupStatus);
			
			//Click the new button in the footer
			CorpusTab.bt_new.click();
			//Click ok 
			CorpusTab.bt_ok_new.click();
			
			//Insert the sentence in the field
			String sentenceToAdd = CorpusTab.getRandomSentence().trim();
			MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
			LogUltility.log(test, logger, "Add new sentence: "+sentenceToAdd);
			
			//Click new button in footer
			MonitorTab.bt_new.click();
			//Confirm and click ok
			MonitorTab.bt_ok.click();
			
			// Get sentence number
			String sentenceNumber = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_corpus_records).get(0).substring(0,6);
			LogUltility.log(test, logger, "The new sentence number is: "+sentenceNumber);
			
			//Click the save button
			CorpusTab.bt_save.click();
			CorpusTab.bt_ok_save.click();
			LogUltility.log(test, logger, "Click the Save button");
			
			try {
				// Search to focus
		   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
		   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				}
				 catch (Exception e) {
				e.printStackTrace();
				 	}
			
			// Copy the modified corpus file to input_corpus folder		
			  String input_corpus_path = Setting.AppPath + Setting.Language+"\\Input_Corpus";
			  File input_corpus_file = new File(input_corpus_path);
			  if(!input_corpus_file.exists())
				  input_corpus_file.mkdir();		  
			String toInputCorpus =input_corpus_path+"\\corpus.txt";
			LogUltility.log(test, logger, "Copying corpus file from:"+source+" ,to:"+toInputCorpus);
			backupStatus = CorpusTab.backupFile(source, toInputCorpus);
			LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
			assertTrue(backupStatus);
			
			// Return the original backedup file
			String newSource = dest;
			String newDest = source;
			LogUltility.log(test, logger, "Copying corpus file from:"+newSource+" ,to:"+newDest);
			backupStatus = CorpusTab.backupFile(newSource, newDest);
			assertTrue(backupStatus);
			
			// Close the app
			MainDisplayWindow.closeSmorfetNo(test, logger);
			
			// Open smorfet
			MainDisplayWindow.LaunchSmorfetWait();
			LogUltility.log(test, logger, "Relaunch Smorfet");
			
			// Navigate to corpus tab
			CommonFunctions.clickTabs("Corpus");
			LogUltility.log(test, logger, "Navigate to corpus");
			
			// Click the merge button
			CorpusTab.bt_merge_files.click();
			LogUltility.log(test, logger, "Click 'merge' button");
			
			// Get list values - should be one value for now
			List<String> filesList = CommonFunctions.getValuesFromApp(CorpusTab.massToBeDD);
			LogUltility.log(test, logger, "Merge files list: "+filesList);
			
			// Choose the file
			LogUltility.log(test, logger, "Choose file: " +filesList.get(0));
			CommonFunctions.chooseValueInDropdown(CorpusTab.massToBeDD,filesList.get(0));
			
			// Click merge button
			CorpusTab.bt_utility_accept.click();
			LogUltility.log(test, logger, "Click convert button in window");
			
			// Click ok button
			Thread.sleep(20000);
			CorpusTab.bt_ok_save.click();
			LogUltility.log(test, logger, "Click ok button");
			
			//Click the save button
			CorpusTab.bt_save.click();
			CorpusTab.bt_ok_save.click();
			LogUltility.log(test, logger, "Click the Save button");
			
			try {
				// Search to focus
		   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
		   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				}
				 catch (Exception e) {
				e.printStackTrace();
				 	}
			
			// Close the app
			MainDisplayWindow.closeSmorfetNo(test, logger);
			
			// replace the existing corpus file with corpus.txt_converted
			String FromInputCorpus = Setting.AppPath+Setting.Language+"\\Input_Corpus\\corpus.txt_converted";
			LogUltility.log(test, logger, "Copying corpus file from:"+FromInputCorpus+" ,to:"+source);
			backupStatus = CorpusTab.backupFile(FromInputCorpus, source);
			assertTrue(backupStatus);
			
			// Open smorfet
			MainDisplayWindow.LaunchSmorfetWait();
			LogUltility.log(test, logger, "Relaunch Smorfet");
			
			// Navigate to corpus tab
			CommonFunctions.clickTabs("Corpus");
			LogUltility.log(test, logger, "Navigate to corpus");
	
			// Search to work with less records
			CorpusTab.bt_reset_search.click();
			CorpusTab.searchInCorpus(test, logger, "text", "contains", sentenceToAdd.split(" ")[0], "all records");
			
			//Insert value in the 'Go To' field
			CorpusTab.tb_goto_line.sendKeys(sentenceNumber.trim());
			LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
			
			//Click Enter
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			LogUltility.log(test, logger, "Click Enter");

			//Retrieve the selected sentence
			String selectedResult = CorpusTab.getChosenValueInDropdown(CorpusTab.lb_corpus_records).get(0);
			LogUltility.log(test, logger, "The retrieved sentence is: "+selectedResult);
			
			// Check that the sentence was added successfully
			boolean sentenceExists = selectedResult.contains(sentenceToAdd);
			LogUltility.log(test, logger, "Sentence has been added successfully: "+sentenceExists);
			assertTrue(sentenceExists);
			
			LogUltility.log(test, logger, "Test Case PASSED");
			
		}

		
		/**
		 * DIC_TC--737:Verify that user can select sentence with undefined word while focus is set - BUG 1372
		 * @throws InterruptedException 
		 * @throws AWTException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_737_Verify_that_user_can_select_sentence_with_undefined_word_while_focus_is_set_BUG_1372() throws InterruptedException, AWTException, IOException
		{
			test = extent.createTest("Corpus Tests - DIC_TC--737", "Verify that user can select sentence with undefined word while focus is set - BUG 1372");

			CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
			MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
			
			// Click on the Corpus tab
			CorpusTab.tabCorpus.click();
						
			//Click the new button in the footer
			CorpusTab.bt_new.click();
			//Click ok 
			CorpusTab.bt_ok_new.click();
			
			//Insert the sentence in the field , set the undefined word at the beginning
			String sentenceToAdd =  CorpusTab.RandomString(4).toLowerCase()+CorpusTab.getRandomSentence().toLowerCase() ;
			MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
			
			//Click new button in footer
			MonitorTab.bt_new.click();
			//Confirm and click ok
			MonitorTab.bt_ok.click();
			
			// Choose from focus dropdown
			CommonFunctions.chooseValueInDropdown(CorpusTab.cb_show, "Focus");
			LogUltility.log(test, logger, "Choose 'Focus' from the dropdown");
			
			CorpusTab.searchInCorpus(test, logger, "text", "contains", sentenceToAdd, "all records");
			
			// Get the chosen sentence
			String expectedSentence = CommonFunctions.getLastValueFromApp(CorpusTab.lb_corpus_records);	
			CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, expectedSentence);
			LogUltility.log(test, logger, "Click on the sentence: "+expectedSentence);

			// Verify that the sentence has been selected
			boolean isSelected = expectedSentence.substring(6).trim().equals(sentenceToAdd);
			LogUltility.log(test, logger, "The sentence has been chosen successfully: "+isSelected);
			assertTrue(isSelected);
			
			// Reset to default
			CommonFunctions.chooseValueInDropdown(CorpusTab.cb_show, "Grammar");
			LogUltility.log(test, logger, "Choose 'Grammar' from the dropdown");
			
			// Click the revert button
			CorpusTab.bt_revert.click();
			LogUltility.log(test, logger, "Click revert button" );
			
			// Click ok in popup
			CorpusTab.revertFileMsgOK.click();
			LogUltility.log(test, logger, "Click Ok" );

			LogUltility.log(test, logger, "Test Case PASSED");
			
		}

		
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Corpus",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
//			 DriverContext._Driver.quit();
			}

}

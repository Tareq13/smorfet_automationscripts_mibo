package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.DictionaryTab;

/**
 * 
 * All tests for Compounds search
 *
 */
public class Compounds_Coloring_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrry(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Compounds tab - Coloring");
	
		// Logger
		logger = LogManager.getLogger(CompoundsTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Compounds tab - Coloring");
		logger.info("Compounds tab Tests - FrameworkInitilize");
	
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			//Click the compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
//			CurrentPage.As(CompoundsTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Compounds");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * DIC_TC--375:Check the Origin drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_375_Check_the_Origin_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_375", "Check the Origin drop-down coloring");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the Origin dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpOrigin, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpOrigin);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the origin dropdown
		List<String> originList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpOrigin);
		originList.remove("unknown");
		String randomOrigin= originList.get(randomizer.nextInt(originList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpOrigin, randomOrigin);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new origin value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpOrigin);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--373:Check the Style drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_373_Check_the_Style_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_373", "Check the Style drop-down coloring");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the style dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpStyle, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpStyle);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the style dropdown
		List<String> styleList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpStyle);
		styleList.remove("unknown");
		String randomStyle= styleList.get(randomizer.nextInt(styleList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpStyle, randomStyle);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new style value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpStyle);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--372:Check the Acronym drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_372_Check_the_Acronym_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_372", "Check the Acronym drop-down coloring");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the Acronym dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpAcronym, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpAcronym);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the acronym dropdown
		List<String> acronymList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpAcronym);
		acronymList.remove("unknown");
		String randomAcronym= acronymList.get(randomizer.nextInt(acronymList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpAcronym, randomAcronym);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new acronym value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpAcronym);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--371:Check the Register drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_371_Check_the_Register_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_371", "Check the Register drop-down coloring");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the register dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpRegister, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpRegister);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the register dropdown
		List<String> registerList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpRegister);
		registerList.remove("unknown");
		String randomRegister= registerList.get(randomizer.nextInt(registerList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpRegister, randomRegister);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new register value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpRegister);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--361:Check the unknown value Compound Type drop-down
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_361_Check_the_unknown_value_Compound_Type_drop_down() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_361", "Check the unknown value Compound Type drop-down");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the compounds type dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).dd_CompoundsType, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).dd_CompoundsType);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the compounds type dropdown
		List<String> compoundstTypeList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).dd_CompoundsType);
		compoundstTypeList.remove("unknown");
		String randomCompoundstType= compoundstTypeList.get(randomizer.nextInt(compoundstTypeList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).dd_CompoundsType, randomCompoundstType);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new compounds type value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).dd_CompoundsType);
		LogUltility.log(test, logger, "Color of a value should be beige: " + colorValue);
		assertEquals(colorValue, "Beige");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--349:Check the color of Etymology when there is no text saved
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_354_Check_the_color_of_Etymology_when_there_is_no_text_saved() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_349", "Check the color of Etymology when there is no text saved");

		// Focus on dictionary tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and delete the etymology from the first record
		CurrentPage.As(CompoundsTab.class).cleanDucomentControl(CurrentPage.As(CompoundsTab.class).Etymology_Text);
		LogUltility.log(test, logger, "Delete everything from the Etymology textbox");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of box is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).Etymology_Text);
		LogUltility.log(test, logger, "Color of the field should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Enter a text in the etymology field
		String randomText = CurrentPage.As(CompoundsTab.class).RandomString(10);
		CurrentPage.As(CompoundsTab.class).Etymology_Text.sendKeys(randomText);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new origin value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of the etymology field with text
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).Etymology_Text);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--355:Check the color of Definition when there is no text saved
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_355_Check_the_color_of_Definition_when_there_is_no_text_saved() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_355", "Check the color of Definition when there is no text saved");

		// Focus on dictionary tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and delete the definition from the first record
		CurrentPage.As(CompoundsTab.class).cleanDucomentControl(CurrentPage.As(CompoundsTab.class).lst_Definition);
		LogUltility.log(test, logger, "Delete everything from the Definition textbox");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of box is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).lst_Definition);
		LogUltility.log(test, logger, "Color of the field should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Enter a text in the definition field
		String randomText = CurrentPage.As(CompoundsTab.class).RandomString(10);
		CurrentPage.As(CompoundsTab.class).lst_Definition.sendKeys(randomText);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new origin value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of the definition field with text
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).lst_Definition);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--341:Check the semantic groups turn to white when the user select values
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_341_Check_the_semantic_groups_turn_to_white_when_the_user_select_values() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_341", "Check the semantic groups turn to white when the user select values");

		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CommonFunctions.clickTabs("Compounds");

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and remove any chosen value from the list
		CommonFunctions.unselectValuesFromList(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the empty list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of the empty list is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose value from the semantic groups list
		List<String> semanticGroupsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		semanticGroupsList.remove("unknown");
		String randomSemanticGroups= semanticGroupsList.get(randomizer.nextInt(semanticGroupsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups, randomSemanticGroups);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the semantic groups list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "Color of list with values should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--342:Check the semantic fields turn to white when the user select values
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_342_Check_the_semantic_fields_turn_to_white_when_the_user_select_values() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_342", "Check the semantic fields turn to white when the user select values");

		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CommonFunctions.clickTabs("Compounds");

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and remove any chosen value from the list
		CommonFunctions.unselectValuesFromList(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the empty list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of the empty list is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose value from the semantic fields list
		List<String> semanticFieldsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
		semanticFieldsList.remove("unknown");
		String randomSemanticFields= semanticFieldsList.get(randomizer.nextInt(semanticFieldsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields, randomSemanticFields);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the semantic fields list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
		LogUltility.log(test, logger, "Color of list with values should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--376:Check the Style drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_376_Check_the_Style_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_376", "Check the Style drop-down coloring");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the style dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpStyle, "unknown");
		LogUltility.log(test, logger, "Set style dropdown value to: unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpStyle);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the style dropdown
		List<String> styleList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpStyle);
		styleList.remove("unknown");
		String randomStyle= styleList.get(randomizer.nextInt(styleList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpStyle, randomStyle);
		LogUltility.log(test, logger, "Choose random style: " + randomStyle);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new style value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpStyle);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--377:Check the Acronym drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_377_Check_the_Acronym_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_377", "Check the Acronym drop-down coloring");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the acronym dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpAcronym, "unknown");
		LogUltility.log(test, logger, "Set acronym dropdown value to: unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpAcronym);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the acronym dropdown
		List<String> acronymList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpAcronym);
		acronymList.remove("unknown");
		String randomAcronym= acronymList.get(randomizer.nextInt(acronymList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpAcronym, randomAcronym);
		LogUltility.log(test, logger, "Choose random acronym: " + randomAcronym);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new acronym value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpAcronym);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--379:Check the Register drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_379_Check_the_Register_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_379", "Check the Register drop-down coloring");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		recordsList.remove(randomRecordFirst);
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the register dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpRegister, "unknown");
		LogUltility.log(test, logger, "Set register dropdown value to: unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpRegister);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the register dropdown
		List<String> registerList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpRegister);
		registerList.remove("unknown");
		String randomRegister= registerList.get(randomizer.nextInt(registerList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpRegister, randomRegister);
		LogUltility.log(test, logger, "Choose random register: " + randomRegister);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new register value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(CompoundsTab.class).cmpRegister);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Compound/Coloring",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			// DriverContext._Driver.quit();
			}

}

package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.MainDisplayWindow;
import smi.smorfet.test.pages.MonitorTab;

/**
 * 
 * All tests for Compounds search
 *
 */
public class Compounds_Syncing_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Compounds tab - Syncing");
	
		// Logger
		logger = LogManager.getLogger(CompoundsTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Compounds tab - Syncing");
		logger.info("Compounds tab Tests - FrameworkInitilize");
	
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			//Click the compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
//			CurrentPage.As(CompoundsTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Compounds");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
         

	/**
	 * DIC_TC--659:Check sync between compounds and internal corpus and external corpus when Changing semantic field
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_659_Check_sync_between_compounds_and_internal_corpus_and_external_corpus_when_Changing_semantic_field() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Compounds Tests - DIC_TC--659", "Check sync between compounds and internal corpus and external corpus when Changing semantic field");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);
		
		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> compoundKeys = CorpusTab.getTaggedDictionaryKeysInCorpusCompound();

		String searchedKey = compoundKeys.get(randomizer.nextInt(compoundKeys.size()));
		String  cleanKey = searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ");
		CorpusTab.searchInCorpus(test,logger,"text","contains",cleanKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose random sentence
		String randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		int wordIndex = CorpusTab.getSentenceCompouundRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		
		// search for the word
		CompoundsTab.searchInCompound(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, searchedKey);
						
		// Get semantic fields values and remove the chosen ones in order to choose a new value
		List<String> availableSemanticFields = CompoundsTab.getValuesFromApp(CompoundsTab.lb_compounds_semantic_fields);
		List<String> markedSemanticFields = CompoundsTab.getChosenValueInDropdown(CompoundsTab.lb_compounds_semantic_fields);
		for(int i=0;i<markedSemanticFields.size();i++)
			availableSemanticFields.remove(markedSemanticFields.get(i));

		// Choose semantic field value
		String chosenSF = availableSemanticFields.get(randomizer.nextInt(availableSemanticFields.size()));
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lb_compounds_semantic_fields, chosenSF);
		LogUltility.log(test, logger, "Choose semantic field value: "+chosenSF);
		
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
//		try {
//			// Change the last filter value, in case it was changed by other TC
//	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
//	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
//		} catch (Exception e) {
//			e.printStackTrace();
//			}
		
		try {
			CommonFunctions.clickTabs("Compounds");
			
		}catch (Exception e) {
			// TODO: handle exception
		}
	
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		boolean foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		boolean SFdisplayed = false;
		String SFfromCorpus="";
		chosenSF=chosenSF.replaceAll("&", "and");
		chosenSF=chosenSF.replaceAll(" ", "_");
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Semantic Fields").contains(chosenSF))
				{
					SFfromCorpus = recordInfo.get(i).get("Semantic Fields");
					SFdisplayed = true;
				}
		LogUltility.log(test, logger, "The new semantic field is displayed: "+SFdisplayed);
		assertTrue(SFdisplayed);
				
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Semantic field from grammar indices file
		String SFfromGIfile = getGrammarIndexInfo.get(7).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The Semantic fields from grammar_indices.txt: "+SFfromGIfile);
		LogUltility.log(test, logger, "The Semantic fields from corpus GUI: "+SFfromCorpus);
		boolean sameGI = SFfromCorpus.equals(SFfromGIfile);
		LogUltility.log(test, logger, "The Semantic fields are the same: "+sameGI);
		assertTrue(sameGI);
		
	
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		

		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
				
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		
		//Insert value in the 'Go To' field
	    CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
	    LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		SFdisplayed = false;
		SFfromCorpus="";
		chosenSF=chosenSF.replaceAll("&", "and");
		chosenSF=chosenSF.replaceAll(" ", "_");
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Semantic Fields").contains(chosenSF))
				{
					SFfromCorpus = recordInfo.get(i).get("Semantic Fields");
					SFdisplayed = true;
				}
		LogUltility.log(test, logger, "The new semantic field is displayed: "+SFdisplayed);
		assertTrue(SFdisplayed);
				
		// Get the grammar index of the word
		grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Semantic field from grammar indices file
		SFfromGIfile = getGrammarIndexInfo.get(7).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The Semantic fields from grammar_indices.txt: "+SFfromGIfile);
		LogUltility.log(test, logger, "The Semantic fields from corpus GUI: "+SFfromCorpus);
		sameGI = SFfromCorpus.equals(SFfromGIfile);
		LogUltility.log(test, logger, "The Semantic fields are the same: "+sameGI);
		assertTrue(sameGI);
		
				
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
		
	/**
	 * DIC_TC--658:Check sync between compounds and internal corpus and external corpus when Changing semantic groups
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_658_Check_sync_between_compounds_and_internal_corpus_and_external_corpus_when_Changing_semantic_groups() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Corpus Tests - DIC_TC--658", "Check sync between compounds and internal corpus and external corpus when Changing semantic groups");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);
	
		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
//		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
//		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
//		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> compoundKeys = CorpusTab.getTaggedDictionaryKeysInCorpusCompound();

		// For Hebrew
		List<ArrayList<String>> hebrewCompoundKeysRepresintation = CorpusTab.hebrewCompoundsInCorpus();
		int hebIndex = randomizer.nextInt(hebrewCompoundKeysRepresintation.size());
		
		String searchedKey = Setting.Language.equals("EN")? compoundKeys.get(randomizer.nextInt(compoundKeys.size())) : hebrewCompoundKeysRepresintation.get(hebIndex).get(0);
		String  cleanKey = Setting.Language.equals("EN")? searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ") : hebrewCompoundKeysRepresintation.get(hebIndex).get(1);
		CorpusTab.searchInCorpus(test,logger,"text","contains",cleanKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose random sentence
		String randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		int wordIndex = CorpusTab.getSentenceCompouundRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		
		// search for the word
		CompoundsTab.searchInCompound(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, searchedKey);
						
		// Get semantic fields values and remove the chosen ones in order to choose a new value
		List<String> availableSemanticGroups = CompoundsTab.getValuesFromApp(CompoundsTab.lst_SemanticGroups);
		List<String> markedSemanticGroups = CompoundsTab.getChosenValueInDropdown(CompoundsTab.lst_SemanticGroups);
		for(int i=0;i<markedSemanticGroups.size();i++)
			availableSemanticGroups.remove(markedSemanticGroups.get(i));

		// Choose semantic field value
		String chosenSG = availableSemanticGroups.get(randomizer.nextInt(availableSemanticGroups.size()));
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_SemanticGroups, chosenSG);
		LogUltility.log(test, logger, "Choose semantic group value: "+chosenSG);
		
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		boolean foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		boolean SGdisplayed = false;
		String SGfromCorpus="";
		chosenSG=chosenSG.replaceAll("&", "and");
		chosenSG=chosenSG.replaceAll(" ", "_");
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Semantic Group").contains(chosenSG))
				{
					SGfromCorpus = recordInfo.get(i).get("Semantic Group");
					SGdisplayed = true;
				}
		LogUltility.log(test, logger, "The new semantic group is displayed: "+SGdisplayed);
		assertTrue(SGdisplayed);
				
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Semantic group from grammar indices file
		String SGfromGIfile = getGrammarIndexInfo.get(6).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The Semantic groups from grammar_indices.txt: "+SGfromGIfile);
		LogUltility.log(test, logger, "The Semantic group from corpus GUI: "+SGfromCorpus);
		boolean sameGI =SGfromGIfile.contains(SGfromCorpus) ;
		LogUltility.log(test, logger, "The Semantic groups are the same: "+sameGI);
		assertTrue(sameGI);
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
				
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		//Insert value in the 'Go To' field
	    CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
	    LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");

		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new semantic field is displayed 
		SGdisplayed = false;
		SGfromCorpus="";
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Semantic Group").contains(chosenSG))
				{
					SGfromCorpus = recordInfo.get(i).get("Semantic Group");
					SGdisplayed = true;
				}
		LogUltility.log(test, logger, "The new semantic group is displayed: "+SGdisplayed);
		assertTrue(SGdisplayed);
				
		// Get the grammar index of the word
		grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// Semantic field from grammar indices file
		SGfromGIfile = getGrammarIndexInfo.get(6).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The Semantic group from grammar_indices.txt: "+SGfromGIfile);
		LogUltility.log(test, logger, "The Semantic group from corpus GUI: "+SGfromCorpus);
		sameGI =SGfromGIfile.contains(SGfromCorpus) ;
		LogUltility.log(test, logger, "The Semantic fields are the same: "+sameGI);
		assertTrue(sameGI);
		
				
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
		
	
	/**
	 * DIC_TC--660:Changing frames in a tagged compound
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_660_Changing_frames_in_a_tagged_compound() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Compounds Tests - DIC_TC--660", "Changing frames in a tagged compound");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);

		//************************************
//		String x = "3970";
//		String y = "HE{j}שים_לב~0";
//		int f = CorpusTab.getSentenceCompouundRecordIndex(x,y);
//		System.out.println();
		
		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> compoundKeys = CorpusTab.getTaggedDictionaryKeysInCorpusCompound();
		
//		// For Hebrew
		List<ArrayList<String>> hebrewCompoundKeysRepresintation = CorpusTab.hebrewCompoundsInCorpus();
		
		// Get hebrew dictionary keys
		List<String> hebrewCompoundKeys = new ArrayList<String>();
		for(int i=0;i<hebrewCompoundKeysRepresintation.size();i++)
			hebrewCompoundKeys.add(hebrewCompoundKeysRepresintation.get(i).get(0));
				
		// Get compound keys with frames
		List<String> framesCompoundKeys = CompoundsTab.getCompoundKeysWithFrames();
		// Intersection to get the corpus tagged compounds with frames
		compoundKeys.retainAll(framesCompoundKeys);
		hebrewCompoundKeys.retainAll(framesCompoundKeys);
		
		String searchedKey = Setting.Language.equals("EN")? compoundKeys.get(randomizer.nextInt(compoundKeys.size())): hebrewCompoundKeys.get(randomizer.nextInt(hebrewCompoundKeys.size()));
		String  cleanKey = searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ");
		
		// Get clean key for Hebrew
		if(Setting.Language.equals("HE"))	for(int i=0;i<hebrewCompoundKeysRepresintation.size();i++)	if(hebrewCompoundKeysRepresintation.get(i).get(0).equals(searchedKey))
					{cleanKey = hebrewCompoundKeysRepresintation.get(i).get(1);break;}
		
		CorpusTab.searchInCorpus(test,logger,"text","contains",cleanKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose random sentence
		
		boolean hasCompound = false;
		String randomRecord = "";
		do {
			randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			sentencesList.remove(randomRecord);
			hasCompound = CorpusTab.containsCompound(randomRecord,searchedKey);
		}
		while(!hasCompound);
			
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		int wordIndex = CorpusTab.getSentenceCompouundRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));

		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		
		// search for the word
		 cleanKey = searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ");
		CompoundsTab.searchInCompound(test, logger, "form", "contains",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, searchedKey);
							
		// Get frames values and remove the chosen ones in order to choose a new value
		List<String> availableFrames = CompoundsTab.getValuesFromApp(CompoundsTab.lst_frames);
		List<String> markedFrames = CompoundsTab.getChosenValueInDropdown(CompoundsTab.lst_frames);
		for(int i=0;i<markedFrames.size();i++)
			availableFrames.remove(markedFrames.get(i));
		availableFrames.remove("no frame");

		// Choose new frame value
		String chosenFrame = availableFrames.get(randomizer.nextInt(availableFrames.size()));
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_frames, chosenFrame);
		LogUltility.log(test, logger, "Choose frame value: "+chosenFrame);
		
		
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		boolean foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap(searchedKey);
		LogUltility.log(test, logger, searchedKey+" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		
		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new frame is displayed 
		boolean Framedisplayed = false;
		String FramesfromCorpus="";
//		chosenFrame=chosenFrame.replaceAll("&", "and");
		chosenFrame=chosenFrame.replaceAll(" ", "_");
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Frames").contains(chosenFrame))
				{
					FramesfromCorpus = recordInfo.get(i).get("Frames");
					Framedisplayed = true;
				}
		LogUltility.log(test, logger, "The new Frame is displayed: "+Framedisplayed);
		assertTrue(Framedisplayed);
				
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// frame from grammar indices file
		String FramesfromGIfile = getGrammarIndexInfo.get(4).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The frames from grammar_indices.txt: "+FramesfromGIfile);
		LogUltility.log(test, logger, "The frames from corpus GUI: "+FramesfromCorpus);
		boolean sameGI = FramesfromCorpus.equals(FramesfromGIfile);
		LogUltility.log(test, logger, "The Semantic groups are the same: "+sameGI);
		assertTrue(sameGI);
	
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
//		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		

		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
				
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");
	
		//Insert value in the 'Go To' field
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
	
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new frame is displayed 
		 Framedisplayed = false;
		 FramesfromCorpus="";
//		chosenFrame=chosenFrame.replaceAll("&", "and");
		chosenFrame=chosenFrame.replaceAll(" ", "_");
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Frames").contains(chosenFrame))
				{
					FramesfromCorpus = recordInfo.get(i).get("Frames");
					Framedisplayed = true;
				}
		LogUltility.log(test, logger, "The new Frame is displayed: "+Framedisplayed);
		assertTrue(Framedisplayed);
				
		// Get the grammar index of the word
		grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// frame from grammar indices file
		FramesfromGIfile = getGrammarIndexInfo.get(4).replaceAll("\\|", ",");
		LogUltility.log(test, logger, "The frames from grammar_indices.txt: "+FramesfromGIfile);
		LogUltility.log(test, logger, "The frames from corpus GUI: "+FramesfromCorpus);
		sameGI = FramesfromCorpus.equals(FramesfromGIfile);
		LogUltility.log(test, logger, "The Semantic groups are the same: "+sameGI);
		assertTrue(sameGI);

		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	/**
	 * DIC_TC--661:Changing compound�s POS in a tagged compound
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_661_Changing_compounds_POS_in_a_tagged_compound() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Compounds Tests - DIC_TC--661", "Changing compound�s POS in a tagged compound");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);	
		
		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
//		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
//		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
//		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> compoundKeys = CorpusTab.getTaggedDictionaryKeysInCorpusCompound();

		// For Hebrew
		List<ArrayList<String>> hebrewCompoundKeysRepresintation = CorpusTab.hebrewCompoundsInCorpus();
		int hebIndex = randomizer.nextInt(hebrewCompoundKeysRepresintation.size());
		
		String searchedKey = Setting.Language.equals("EN")? compoundKeys.get(randomizer.nextInt(compoundKeys.size())) : hebrewCompoundKeysRepresintation.get(hebIndex).get(0);
		String  cleanKey = Setting.Language.equals("EN")? searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ") : hebrewCompoundKeysRepresintation.get(hebIndex).get(1);
		CorpusTab.searchInCorpus(test,logger,"text","contains",cleanKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose random sentence
		boolean hasCompound = false;
		String randomRecord = "";
		do {
			randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			sentencesList.remove(randomRecord);
			hasCompound = CorpusTab.containsCompound(randomRecord);
		}
		while(!hasCompound);
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		int wordIndex = CorpusTab.getSentenceCompouundRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		
		// search for the word
		CompoundsTab.searchInCompound(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, searchedKey);
							
		// Get existing POS
		String POSB4Change = CompoundsTab.getChosenValueInDropdown(CompoundsTab.cmpPOS).get(0);
		LogUltility.log(test, logger, "POS before change: " + POSB4Change);
		
		// Get random POS
		List<String> posList0 = CompoundsTab.checkPOSValidity(POSB4Change);
		List<String> posList = new ArrayList<String>();
		for(int i=0;i<posList0.size();i++)
			posList.add(posList0.get(i).replaceAll("_"," "));
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random pos
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);	
		
		// Press yes
		CompoundsTab.btnYes.click();
		LogUltility.log(test, logger, "Press 'Yes' button");
		
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
			
		// Convert the chosen new pos to code
		String newPOScode = DictionaryTab.POSConverter(randompos);
//		String oldPOScode = DictionaryTab.POSConverter(POSB4Change);
		
		String newCompoundKey =  searchedKey.split("\\{")[0]+"{"+newPOScode+"}"+searchedKey.split("\\}")[1];
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		boolean foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap(searchedKey,newCompoundKey);
		LogUltility.log(test, logger, newCompoundKey+" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new compound key is set
		boolean newCompound = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(newCompoundKey))
				newCompound = true;
		LogUltility.log(test, logger, "The new compound POS is displayed: "+newCompound);
		assertTrue(newCompound);
				
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// frame from grammar indices file
		String POSfromGIfile = getGrammarIndexInfo.get(1);
		LogUltility.log(test, logger, "The POS from grammar_indices.txt: "+POSfromGIfile);
		LogUltility.log(test, logger, "The POS from corpus GUI: "+randompos);
		boolean sameGI = randompos.equals(POSfromGIfile);
		LogUltility.log(test, logger, "The POS is the same: "+sameGI);
		assertTrue(sameGI);
	
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		

		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
				
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		//Insert value in the 'Go To' field
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
	recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the new compound key is set
		 newCompound = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(newCompoundKey))
				newCompound = true;
		LogUltility.log(test, logger, "The new compound POS is displayed: "+newCompound);
		assertTrue(newCompound);
				
		// Get the grammar index of the word
		 grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// frame from grammar indices file
		POSfromGIfile = getGrammarIndexInfo.get(1);
		LogUltility.log(test, logger, "The POS from grammar_indices.txt: "+POSfromGIfile);
		LogUltility.log(test, logger, "The POS from corpus GUI: "+randompos);
		sameGI = randompos.equals(POSfromGIfile);
		LogUltility.log(test, logger, "The POS is the same: "+sameGI);
		assertTrue(sameGI);
		

		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--663:Deleting a tagged compound
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_663_Deleting_a_tagged_compound() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Compounds Tests - DIC_TC--663", "Deleting a tagged compound");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);	

		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
//		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
//		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> compoundKeys = CorpusTab.getTaggedDictionaryKeysInCorpusCompound();

		// For Hebrew
		List<ArrayList<String>> hebrewCompoundKeysRepresintation = CorpusTab.hebrewCompoundsInCorpus();
		int hebIndex = randomizer.nextInt(hebrewCompoundKeysRepresintation.size());
		
		String searchedKey = Setting.Language.equals("EN")? compoundKeys.get(randomizer.nextInt(compoundKeys.size())) : hebrewCompoundKeysRepresintation.get(hebIndex).get(0);
		String  cleanKey = Setting.Language.equals("EN")? searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ") : hebrewCompoundKeysRepresintation.get(hebIndex).get(1);
		CorpusTab.searchInCorpus(test,logger,"text","contains",cleanKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose random sentence
		
		boolean hasCompound = false;
		String randomRecord = "";
		do {
			randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			sentencesList.remove(randomRecord);
			hasCompound = CorpusTab.containsCompound(randomRecord);
		}
		while(!hasCompound);
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		int wordIndex = CorpusTab.getSentenceCompouundRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		
		// search for the word
		CompoundsTab.searchInCompound(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, searchedKey);
							
		// Press 'delete' button
		CompoundsTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the delete button");	
		
		// Press yes
		CompoundsTab.yesDelete.click();
		LogUltility.log(test, logger, "Press 'Yes' button");
		
		// Press ok 
		Thread.sleep(1000);
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Press 'Ok' button");
		
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
						
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the compound has been deleted
		boolean deleted = false;
		for(int i=0;i<recordInfo.size();i++)
			if(!recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				deleted = true;
		LogUltility.log(test, logger, "The compound isn't tagged and the key is not displayed in corpus: "+deleted);
		assertTrue(deleted);
				
		// Get the currently chosen tag
		List<String> chosenTagAfter = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagAfter);
		boolean noTag = chosenTagAfter.isEmpty();
		LogUltility.log(test, logger, "The word is not tagged after deleting the compound: " + noTag);
		assertTrue(noTag);
				
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Grammar index is empty
		boolean noGrammarIndex = grammarIndex.equals("");
		LogUltility.log(test, logger, "There is no grammar index applied: "+noGrammarIndex);
		assertTrue(noGrammarIndex);
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
//		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
//		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
//		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
//		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
//		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		
		//Insert value in the 'Go To' field
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the compound has been deleted
		deleted = false;
		for(int i=0;i<recordInfo.size();i++)
			if(!recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				deleted = true;
		LogUltility.log(test, logger, "The compound isn't tagged and the key is not displayed in corpus: "+deleted);
		assertTrue(deleted);
				
		// Get the currently chosen tag
		chosenTagAfter = CorpusTab.getRecordTaggingFromCorpusFile(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Current tagging: " + chosenTagAfter);
		noTag = chosenTagAfter.isEmpty();
		LogUltility.log(test, logger, "The word is not tagged after deleting the compound: " + noTag);
		assertTrue(noTag);
				
		// Get the grammar index of the word
		grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Grammar index is empty
		noGrammarIndex = grammarIndex.equals("");
		LogUltility.log(test, logger, "There is no grammar index applied: "+noGrammarIndex);
		assertTrue(noGrammarIndex);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	/**
	 * DIC_TC--662:Changing compound�s form in a tagged compound
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_662_Changing_compounds_form_in_a_tagged_compound() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Compounds Tests - DIC_TC--662", "Changing compounds form in a tagged compound");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);	
		
		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");
	
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus");
		
		// Get fully tagged sentence to choose a tagged word	
		// Search in corpus and that in order to have less number of sentences to work with
		Random randomizer = new Random();
		List<String> compoundKeys = CorpusTab.getTaggedDictionaryKeysInCorpusCompound();
		
		//*********************************************
		// For Hebrew
				List<ArrayList<String>> hebrewCompoundKeysRepresintation = CorpusTab.hebrewCompoundsInCorpus();
				
				// Get hebrew dictionary keys
				List<String> hebrewCompoundKeys = new ArrayList<String>();
				for(int i=0;i<hebrewCompoundKeysRepresintation.size();i++)
					hebrewCompoundKeys.add(hebrewCompoundKeysRepresintation.get(i).get(0));
						
				// Get compound keys with frames
				List<String> framesCompoundKeys = CompoundsTab.getCompoundKeysWithFrames();
				// Intersection to get the corpus tagged compounds with frames
				compoundKeys.retainAll(framesCompoundKeys);
				hebrewCompoundKeys.retainAll(framesCompoundKeys);
				
				String searchedKey = Setting.Language.equals("EN")? compoundKeys.get(randomizer.nextInt(compoundKeys.size())): hebrewCompoundKeys.get(randomizer.nextInt(hebrewCompoundKeys.size()));
//				String searchedKey = "EN{v}air_fry~0";
				String  cleanKey = searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ");
				
				// Get clean key for Hebrew
				if(Setting.Language.equals("HE"))	for(int i=0;i<hebrewCompoundKeysRepresintation.size();i++)	if(hebrewCompoundKeysRepresintation.get(i).get(0).equals(searchedKey))
							{cleanKey = hebrewCompoundKeysRepresintation.get(i).get(1);break;}
		//*********************************************

			
		
		// Get verb compounds
		List<String> verbCompoundKeys =	CompoundsTab.getVerbCompounds();
		// Intersection , get all tagged compounds in corpus with verb POS, since verb compounds has more than one form type
		compoundKeys.retainAll(verbCompoundKeys);
		compoundKeys.remove("EN{v}spell_check~0");
		
//		String searchedKey = compoundKeys.get(randomizer.nextInt(compoundKeys.size()));
//		String  cleanKey = searchedKey.split("}")[1].split("~")[0].replaceAll("_", " ");
		CorpusTab.searchInCorpus(test,logger,"text","contains",cleanKey,"all records");
		
		// Add filter to get only fully tagged sentences
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose fully tagged sentences
  		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose random sentence
		
		boolean hasCompound = false;
		String randomRecord = "";
		do {
			randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
			sentencesList.remove(randomRecord);
			hasCompound = CorpusTab.containsCompound(randomRecord,searchedKey);
		}
		while(!hasCompound);
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose random sentence from the sentences list: " + randomRecord);
		
   		// Get the words from sentence separately
		List<String> wordsInRecord = CorpusTab.getSentencesRecords();	
		LogUltility.log(test, logger, "Words in sentence: "+wordsInRecord);
		
		List<String> wordsInRecordLowerCase = new ArrayList<String>();
		// convert to lower case
		for(int i=0;i<wordsInRecord.size();i++)
			wordsInRecordLowerCase.add(wordsInRecord.get(i).toLowerCase());
		
		// Click on the word
		int wordIndex = CorpusTab.getSentenceCompouundRecordIndex(randomRecord.substring(0,6).trim(), searchedKey);
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
		
		// Get current Form Type
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		String formTypeB4Change = "";
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				formTypeB4Change = recordInfo.get(i).get("Form Type");
		
		LogUltility.log(test, logger, "Form Type before changing: "+formTypeB4Change);
		
		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		
		// search for the word
		CompoundsTab.searchInCompound(test, logger, "form", "equal",cleanKey, "all records");
		
		// choose from the list
		LogUltility.log(test, logger, "Choose from the list: "+searchedKey);
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, searchedKey);
							
		// Get number of words in compound
		int len = cleanKey.split(" ").length;
		
		// Search for the correct column number
		String newFormType = "";
		for(int i=0;i<len;i++)
		{
			// Get the displayed POS
			String pos = CompoundsTab.getSelectedTaggingValue("POS",i);
			if(pos.equals("verb"))
			{
				// Get the displayed Form Type
				String ft = CompoundsTab.getSelectedTaggingValue("Form Type",i);
				if(ft.equals(formTypeB4Change))
				{
					// This record form type determines the compounds form type, so change it
					List<String> FormTypeValues = CompoundsTab.getTaggingValues("Form Type", i);
					LogUltility.log(test, logger, "Available Form Types:"+FormTypeValues);
					FormTypeValues.remove(0);
					FormTypeValues.remove(formTypeB4Change);
					newFormType = FormTypeValues.get(randomizer.nextInt(FormTypeValues.size()));
					LogUltility.log(test, logger, "The new chosen form type is: "+newFormType);
					CompoundsTab.chooseTaggingValue("Form Type", i, newFormType);
					break;
				}
			}
		}
			
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
//			e.printStackTrace();
			}
						
		// doesn't work now
		// Check if the compounds Key was added to compounds_Sync_map.txt file
//		boolean foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap(searchedKey);
//		LogUltility.log(test, logger, searchedKey+" found in compounds_Sync_map.txt file: "+foundInSyncMap);
//		assertTrue(foundInSyncMap);
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		//Insert value in the 'Go To' field
		String sentenceNumber = randomRecord.substring(0,6).trim();
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the compound form type has been changed
		boolean changedFT = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Form Type").equals(newFormType))
					changedFT = true;
		LogUltility.log(test, logger, "The compound Form Type has been changed in corpus: "+changedFT);
		assertTrue(changedFT);
			
		// Get the grammar index of the word
		String grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		 List<String> getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// frame from grammar indices file
		String FormTypefromGIfile = getGrammarIndexInfo.get(2).replaceAll("_"," ");
		LogUltility.log(test, logger, "The Form Type from grammar_indices.txt: "+FormTypefromGIfile);
		LogUltility.log(test, logger, "The Form Type from corpus GUI: "+newFormType);
		boolean sameGI = newFormType.equals(FormTypefromGIfile);
		LogUltility.log(test, logger, "The Form Type is the same: "+sameGI);
		assertTrue(sameGI);
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Click save
		DictionaryTab.btnSave.click();
		DictionaryTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(DictionaryTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		//Insert value in the 'Go To' field
		CorpusTab.tb_goto_line.sendKeys(sentenceNumber);
		LogUltility.log(test, logger, "Inserting number: " + sentenceNumber);
		
		//Click Enter
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		LogUltility.log(test, logger, "Click Enter");
		
		// Click on the word
		CorpusTab.clickSentencesRecord(wordsInRecord.get(wordIndex), wordIndex);
		LogUltility.log(test, logger, "Click on: "+wordsInRecord.get(wordIndex));
	
		// Get info from app after removing the attached form
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();	
		
		// Check if the compound form type has been changed
		changedFT = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(searchedKey))
				if(recordInfo.get(i).get("Form Type").equals(newFormType))
					changedFT = true;
		LogUltility.log(test, logger, "The compound Form Type has been changed in corpus: "+changedFT);
		assertTrue(changedFT);
			
		// Get the grammar index of the word
		grammarIndex = CorpusTab.getRecordGrammarIndex(randomRecord, wordsInRecord.get(wordIndex));
		LogUltility.log(test, logger, "Grammar index of the record: "+grammarIndex);
		
		// Get grammar index info from grammar_indices.txt file
		getGrammarIndexInfo = CorpusTab.getGrammarIndexInfo(grammarIndex) ;
		LogUltility.log(test, logger, "Grammar index info: "+getGrammarIndexInfo);

		// frame from grammar indices file
		FormTypefromGIfile = getGrammarIndexInfo.get(2).replaceAll("_"," ");
		LogUltility.log(test, logger, "The Form Type from grammar_indices.txt: "+FormTypefromGIfile);
		LogUltility.log(test, logger, "The Form Type from corpus GUI: "+newFormType);
		sameGI = newFormType.equals(FormTypefromGIfile);
		LogUltility.log(test, logger, "The Form Type is the same: "+sameGI);
		assertTrue(sameGI);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--688:Changing restrictions on a tagged compound- Plural for words without S
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_688_Changing_restrictions_on_a_tagged_compound_Plural_for_words_without_S() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Compounds Tests - DIC_TC--688", "Changing restrictions on a tagged compound- Plural for words without S");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);	
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);	
		
		if(Setting.Language.equals("HE")) return;
		
//		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);

//		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");

		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
	
		// Click the new button
		CompoundsTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CompoundsTab.btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText="strong_man";
		CompoundsTab.txtKeyForm.sendKeys(randomText);
		LogUltility.log(test, logger, "New record: " + randomText);
		
		// Select POS
		String posValue = "noun";
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpPOS, posValue);
		LogUltility.log(test, logger, "Choose from pos list: " + posValue);
		
		// Choose first lemma
		Random randomizer = new Random();
		List<String> firstLemmasValues = CompoundsTab.getTaggingValues("Lemma", 0);
		firstLemmasValues.remove(0);
		String chosenLemma = firstLemmasValues.get(randomizer.nextInt(firstLemmasValues.size()));
		CompoundsTab.chooseTaggingValue("Lemma", 0, chosenLemma);
		LogUltility.log(test, logger, "The chosen lemma is: "+chosenLemma);
		
		// Choose second column POS
		CompoundsTab.chooseTaggingValue("POS", 1, "noun");
		LogUltility.log(test, logger, "Choose POS: noun");
		
		// Choose second lemma
//		chosenLemma = "EN{n}man~0 (";
		chosenLemma = "EN{n}man~0 (/mˈæn/): ";
		CompoundsTab.chooseTaggingValue("Lemma", 1, chosenLemma);
		LogUltility.log(test, logger, "The chosen lemma is: "+chosenLemma);
		
		// Choose stress
		LogUltility.log(test, logger, "Choosing Stress");
		CompoundsTab.chooseTaggingValue("Stress", 0, "secondary_stress");
		CompoundsTab.chooseTaggingValue("Stress", 1, "primary_stress");
		
		// Set plural to allowed
		LogUltility.log(test, logger, "Set 'Plural' to allowed");
		CompoundsTab.chooseTaggingValue("Plural", 1, "allowed");
		
		// For syncing
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpVolumePOS, "title");
		CompoundsTab.searchInCompound(test, logger, "form","equal", "strong man", "all records");
		
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Get compounds list and choose the compound for syncing
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, "EN{n}strong_man~0");
		
		// Navigate to monitor
		CommonFunctions.clickTabs("Monitor");
		LogUltility.log(test, logger, "Navigate to monitor tab");
		
		// Add new sentence
		String singleSentence = "He is a strong man";
		LogUltility.log(test, logger, "Insert the following sentence: "+singleSentence);
		MonitorTab.tb_raw_sentence.sendKeys(singleSentence);
			
		// Press the new button
		LogUltility.log(test, logger, "Click the new button");
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		MonitorTab.bt_ok.click();
	
		// Get the sentence records
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Tag the compound 
		WebElement element = CorpusTab.sentencesRecordElement(recordsList.get(3),3);
		CorpusTab.chooseCompoundTaggingOption(randomText.replaceAll("_"," "),element);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
		// Get the displayed form type
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		boolean expectedFormType = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Form Type").equals("single"))
				expectedFormType = true;	
		LogUltility.log(test, logger, "The form type is single: "+expectedFormType);	
		assertTrue(expectedFormType);
		
		// Tag it as plural
		// Navigate to monitor
		CommonFunctions.clickTabs("Monitor");
		LogUltility.log(test, logger, "Navigate to monitor tab");
		
		// Add new sentence
		String pluralSentence = "They are strong men";
		LogUltility.log(test, logger, "Insert the following sentence: "+pluralSentence);
		MonitorTab.tb_raw_sentence.sendKeys(pluralSentence);
			
		// Press the new button
		LogUltility.log(test, logger, "Click the new button");
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		MonitorTab.bt_ok.click();
	
		// Get the sentence records
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Tag the compound 
		element = CorpusTab.sentencesRecordElement(recordsList.get(2),2);
		String pluralCompound = "strong man";
		CorpusTab.chooseCompoundTaggingOption(pluralCompound,element);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(2),2);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(2));
		
		// Get the displayed form type
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		expectedFormType = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Form Type").equals("plural"))
				expectedFormType = true;	
		LogUltility.log(test, logger, "The form type is plural: "+expectedFormType);	
		assertTrue(expectedFormType);
		
		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
	
		// Set plural to prohibited
		LogUltility.log(test, logger, "Set 'Plural' prohibited");
		CompoundsTab.chooseTaggingValue("Plural", 1, "prohibited");
		
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		boolean foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap("EN{n}strong_man~0");
		LogUltility.log(test, logger, "EN{n}strong_man~0"+" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
				
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");	
		CorpusTab.searchInCorpus(test, logger, "text", "contains",pluralSentence, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose sentence
		String randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose sentence from the sentences list: " + randomRecord);
		
		// Click on the "strong" record
		recordsList = CorpusTab.getSentencesRecords();
		CorpusTab.clickSentencesRecord(recordsList.get(2),2);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(2));
		
		// Check that the compound is no longer tagged after setting plural value = prohibited
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		boolean notTagged = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals("EN{a}strong~0"))
				notTagged = true;		
		LogUltility.log(test, logger, "The plural form compound is not tagged anymore: " + notTagged);
		assertTrue(notTagged);
		
		
		// Check that the 'single' form compound is still tagged
		CorpusTab.searchInCorpus(test, logger, "text", "contains",singleSentence, "all records");
		
		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records,40);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
	
		// Choose sentence
		randomRecord = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecord);
		LogUltility.log(test, logger, "Choose sentence from the sentences list: " + randomRecord);
			
		// Click on the "strong" record
		recordsList = CorpusTab.getSentencesRecords();
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
		// Check that the compound is no longer tagged after setting plural value = prohibited
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		boolean tagged = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals("EN{n}strong_man~0"))
				tagged = true;		
		LogUltility.log(test, logger, "The single form compound is still tagged : " + tagged);
		assertTrue(tagged);
				
		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Add the sentences again to corpus after replacing the corpus.txt file
		// Navigate to monitor
		CommonFunctions.clickTabs("Monitor");
		LogUltility.log(test, logger, "Navigate to monitor tab");
		
		// Add new sentence
		LogUltility.log(test, logger, "Insert the following sentence: "+singleSentence);
		MonitorTab.tb_raw_sentence.sendKeys(singleSentence);	
		
		// Press the new button
		LogUltility.log(test, logger, "Click the new button");
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		MonitorTab.bt_ok.click();
	
		// Get the sentence records
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Tag the compound 
		element = CorpusTab.sentencesRecordElement(recordsList.get(3),3);
		CorpusTab.chooseCompoundTaggingOption(randomText.replaceAll("_"," "),element);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
		// Get the displayed form type
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		expectedFormType = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Form Type").equals("single"))
				expectedFormType = true;	
		LogUltility.log(test, logger, "The form type is single: "+expectedFormType);	
		assertTrue(expectedFormType);
			
		// Navigate to monitor
		CommonFunctions.clickTabs("Monitor");
		LogUltility.log(test, logger, "Navigate to monitor tab");
		
		// Add new sentence
		LogUltility.log(test, logger, "Insert the following sentence: "+pluralSentence);
		MonitorTab.tb_raw_sentence.sendKeys(pluralSentence);
			
		// Press the new button
		LogUltility.log(test, logger, "Click the new button");
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		MonitorTab.bt_ok.click();
	
		// Get the sentence records
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Click on the "strong" record
		CorpusTab.clickSentencesRecord(recordsList.get(2),2);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(2));
		
		// Check that the compound is no longer tagged after setting plural value = prohibited
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		notTagged = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals("EN{a}strong~0"))
				notTagged = true;		
		LogUltility.log(test, logger, "The plural form compound is not tagged anymore: " + notTagged);
		assertTrue(notTagged);
			
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap("EN{n}strong_man~0");
		LogUltility.log(test, logger, "EN{n}strong_man~0"+" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--664:Changing restrictions on a tagged compound- Plural for words with S
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_664_Changing_restrictions_on_a_tagged_compound_Plural_for_words_with_S() throws InterruptedException, AWTException, IOException
	{
		
		test = extent.createTest("Compounds Tests - DIC_TC--664", "Changing restrictions on a tagged compound- Plural for words with S");

		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		MainDisplayWindow MainDisplayWindow = GetInstance(MainDisplayWindow.class).As(MainDisplayWindow.class);
		CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);	
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);	

		if(Setting.Language.equals("HE")) return;

//		// Close smorfet and click No
//		CommonFunctions.clickTabs("Dictionary");
//		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Backup corpus,grammar indices,grammar windows,partial grammar indices files before making any changes
		String corpusSource = Setting.AppPath + Setting.Language+"\\Data\\corpus.txt";
		String corpusDest = Setting.AppPath+"corpus.txt";
		String giSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_indices.txt";
		String giDest = Setting.AppPath+"grammar_indices.txt";
		String gwSource = Setting.AppPath + Setting.Language+"\\Data\\grammar_windows.txt";
		String gwDest = Setting.AppPath+"grammar_windows.txt";
		String pgSource = Setting.AppPath + Setting.Language+"\\Data\\partial_grammar_indices.txt";
		String pgDest = Setting.AppPath+"partial_grammar_indices.txt";
		
		// corpus file
		LogUltility.log(test, logger, "Backup the corpus file to: "+ corpusDest);
		boolean backupStatus = CorpusTab.backupFile(corpusSource, corpusDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giSource, giDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwSource, gwDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgSource, pgDest);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);

//		// Open smorfet
//		MainDisplayWindow.LaunchSmorfetWait();
//		LogUltility.log(test, logger, "Relaunch Smorfet");

		
		// Navigate to monitor
		CommonFunctions.clickTabs("Monitor");
		LogUltility.log(test, logger, "Navigate to monitor tab");
		
		// Add new sentence
		String singleSentence = "I have a bank account";
		LogUltility.log(test, logger, "Insert the following sentence: "+singleSentence);
		MonitorTab.tb_raw_sentence.sendKeys(singleSentence);
			
		// Press the new button
		LogUltility.log(test, logger, "Click the new button");
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		MonitorTab.bt_ok.click();
	
		// Get the sentence records
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Tag the compound 
		WebElement element = CorpusTab.sentencesRecordElement(recordsList.get(3),3);
		CorpusTab.chooseCompoundTaggingOption("bank account",element);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
		// Get the displayed form type
		ArrayList<HashMap<String, String>> recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		boolean expectedFormType = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Form Type").equals("single"))
				expectedFormType = true;	
		LogUltility.log(test, logger, "The form type is single: "+expectedFormType);	
		assertTrue(expectedFormType);

		// Plural sentence
		// Navigate to monitor
		CommonFunctions.clickTabs("Monitor");
		LogUltility.log(test, logger, "Navigate to monitor tab");
		
		// Add new sentence
		String pluralSentence = "I have different bank accounts";
		LogUltility.log(test, logger, "Insert the following sentence: "+pluralSentence);
		MonitorTab.tb_raw_sentence.sendKeys(pluralSentence);
			
		// Press the new button
		LogUltility.log(test, logger, "Click the new button");
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		MonitorTab.bt_ok.click();
	
		// Get the sentence records
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Tag the compound 
		element = CorpusTab.sentencesRecordElement(recordsList.get(3),3);
		String pluralCompound = "";
		CorpusTab.chooseCompoundTaggingOption(pluralCompound,element);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
//		// Get the displayed form type
//		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
//		expectedFormType = false;
//		for(int i=0;i<recordInfo.size();i++)
//			if(recordInfo.get(i).get("Form Type").equals("plural"))
//				expectedFormType = true;	
//		LogUltility.log(test, logger, "The form type is plural: "+expectedFormType);	
//		assertTrue(expectedFormType);
		
		// Get the displayed tagging menu text
		String attachPlace = element.getAttribute("ClickablePoint");
 		String[] XY = attachPlace.split(",");
 		Robot robot = new Robot();
 	    int XPoint = 0, YPoint = 0;
 	    XPoint = Integer.parseInt(XY[0]);
 	    YPoint = Integer.parseInt(XY[1]);
 	
 	    // Right click on the element and choose the compound option
        robot.mouseMove(XPoint, YPoint);
        robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
        
        WebElement mainMenu = CorpusTab.desktopRoot.findElement(By.xpath("./*[contains(@ControlType,'ControlType.Menu')]"));
        CorpusTab.takeElementScreenshot(mainMenu);
		// Right click on the compound and read the tagging option values
		List<String> readMenuText = CorpusTab.readTextFromImage();
		LogUltility.log(test, logger, "Read from menu:"+readMenuText);
		
		// Check that there is 3 values in the tagging menu
		boolean is3Options = readMenuText.size() == 3;
		LogUltility.log(test, logger, "There is 3 options in the tagging menu: "+is3Options);
		assertTrue(is3Options);

		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		CompoundsTab.searchInCompound(test, logger, "form", "equal", "bank account", "all records");
		
		// Choose the record
		String compoundKey = "EN{n}bank_account~0";
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records,compoundKey );
	
		// Set plural to prohibited
		LogUltility.log(test, logger, "Set 'Plural' = prohibited");
		CompoundsTab.chooseTaggingValue("Plural", 1, "prohibited");
		
		// For syncing
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpVolumePOS, "title");
		CompoundsTab.searchInCompound(test, logger, "form","equal", "bank account", "all records");
				
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		boolean foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap(compoundKey);
		LogUltility.log(test, logger, compoundKey +" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		// Get compounds list and choose the compound for syncing
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, "EN{n}bank_account~0");
		
		// Navigate to corpus tab
		CommonFunctions.clickTabs("Corpus");
		LogUltility.log(test, logger, "Navigate to corpus tab");

		// Search for the sentence
		CorpusTab.searchInCorpus(test, logger,"text", "contains",singleSentence, "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose the last sentence
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, sentencesList.get(sentencesList.size()-1));
		LogUltility.log(test, logger, "Choose sentence from the sentences list: " + singleSentence);
		
		// Get the sentence records
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
//		// Get the displayed form type
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		boolean expectedKey = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(compoundKey))
				expectedKey = true;	
		LogUltility.log(test, logger, "The single sentence still tagged: "+expectedKey);	
		assertTrue(expectedKey);
		
		// Check the plural isn't tagged anymore
		// Search for the sentence
		CorpusTab.searchInCorpus(test, logger,"text", "contains",pluralSentence, "all records");
		
		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose the last sentence
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, sentencesList.get(sentencesList.size()-1));
		LogUltility.log(test, logger, "Choose sentence from the sentences list: " + pluralSentence);
		
		// Get the sentence records
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
		// Get the displayed form type
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		expectedKey = true;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Dictionary Key").equals(compoundKey))
				expectedFormType = false;	
		LogUltility.log(test, logger, "The plural sentence IS NOT tagged: "+expectedKey);	
		assertTrue(expectedKey);

		// Close smorfet and click No
		MainDisplayWindow.closeSmorfetNo(test, logger);
		
		// Copy external corpus and grammar files to original path En\Data
		LogUltility.log(test, logger, "Backup the external corpus file to: "+ corpusSource);
		backupStatus = CorpusTab.backupFile(corpusDest,corpusSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar indices file
		LogUltility.log(test, logger, "Backup the external grammar indices file to: "+ giDest);
		backupStatus = CorpusTab.backupFile(giDest,giSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// grammar windows file
		LogUltility.log(test, logger, "Backup the external grammar windows file to: "+ gwDest);
		backupStatus = CorpusTab.backupFile(gwDest,gwSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// partial grammar indices file
		LogUltility.log(test, logger, "Backup the external partial grammar indices file to: "+ pgDest);
		backupStatus = CorpusTab.backupFile(pgDest,pgSource);
		LogUltility.log(test, logger, "File has been backedup successfully: "+backupStatus);
		assertTrue(backupStatus);
		
		// Open smorfet
		MainDisplayWindow.LaunchSmorfetWait();
		LogUltility.log(test, logger, "Relaunch Smorfet");
		
		// Navigate to monitor
		CommonFunctions.clickTabs("Monitor");
		LogUltility.log(test, logger, "Navigate to monitor tab");
		
		// Add new sentence
		LogUltility.log(test, logger, "Insert the following sentence: "+singleSentence);
		MonitorTab.tb_raw_sentence.sendKeys(singleSentence);
			
		// Press the new button
		LogUltility.log(test, logger, "Click the new button");
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		MonitorTab.bt_ok.click();
	
		// Get the sentence records
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Tag the compound 
		element = CorpusTab.sentencesRecordElement(recordsList.get(3),3);
		CorpusTab.chooseCompoundTaggingOption("bank account",element);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
		// Get the displayed form type
		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
		expectedFormType = false;
		for(int i=0;i<recordInfo.size();i++)
			if(recordInfo.get(i).get("Form Type").equals("single"))
				expectedFormType = true;	
		LogUltility.log(test, logger, "The form type is single: "+expectedFormType);	
		assertTrue(expectedFormType);

		// Plural sentence
		// Navigate to monitor
		CommonFunctions.clickTabs("Monitor");
		LogUltility.log(test, logger, "Navigate to monitor tab");
		
		// Add new sentence
		LogUltility.log(test, logger, "Insert the following sentence: "+pluralSentence);
		MonitorTab.tb_raw_sentence.sendKeys(pluralSentence);
			
		// Press the new button
		LogUltility.log(test, logger, "Click the new button");
		MonitorTab.bt_new.click();
		LogUltility.log(test, logger, "Click the ok button");
		MonitorTab.bt_ok.click();
	
		// Get the sentence records
		recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);
		
		// Tag the compound 
		element = CorpusTab.sentencesRecordElement(recordsList.get(3),3);
		CorpusTab.chooseCompoundTaggingOption(pluralCompound,element);
		
		// Click on the compound beginning again
		CorpusTab.clickSentencesRecord(recordsList.get(3),3);
		LogUltility.log(test, logger, "Click again on: "+recordsList.get(3));
		
//		// Get the displayed form type
//		recordInfo = CorpusTab.getTaggingRecordsRawAllInfo();
//		expectedFormType = false;
//		for(int i=0;i<recordInfo.size();i++)
//			if(recordInfo.get(i).get("Form Type").equals("plural"))
//				expectedFormType = true;	
//		LogUltility.log(test, logger, "The form type is plural: "+expectedFormType);	
//		assertTrue(expectedFormType);
		
		//****************************************
		
		// Get the displayed tagging menu text
		 attachPlace = element.getAttribute("ClickablePoint");
 		XY = attachPlace.split(",");
 	    XPoint = Integer.parseInt(XY[0]);
 	    YPoint = Integer.parseInt(XY[1]);
 	
 	    // Right click on the element and choose the compound option
        robot.mouseMove(XPoint, YPoint);
        robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
        
        mainMenu = CorpusTab.desktopRoot.findElement(By.xpath("./*[contains(@ControlType,'ControlType.Menu')]"));
        CorpusTab.takeElementScreenshot(mainMenu);
		// Right click on the compound and read the tagging option values
        readMenuText = CorpusTab.readTextFromImage();
		LogUltility.log(test, logger, "Read from menu:"+readMenuText);
		
		// Check that there is 2 values in the tagging menu when plural is set to prohibited
		boolean is2Options = readMenuText.size() == 2;
		LogUltility.log(test, logger, "There is 2 options in the tagging menu: "+is2Options);
		assertTrue(is2Options);

		// Navigate to compounds
		CommonFunctions.clickTabs("Compounds");
		LogUltility.log(test, logger, "Navigate to compounds tab");
		CompoundsTab.searchInCompound(test, logger, "form", "equal", "bank account", "all records");
		
		// Choose the record
		CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records,compoundKey );
	
		// Set plural to prohibited
		LogUltility.log(test, logger, "Set 'Plural' = prohibited");
		CompoundsTab.chooseTaggingValue("Plural", 1, "prohibited");
		
		// For syncing
		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpVolumePOS, "title");
		CompoundsTab.searchInCompound(test, logger, "form","equal", "bank account", "all records");
				
		// Click save
		CompoundsTab.btnSave.click();
		CompoundsTab.btnOk.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		try {
			// Change the last filter value, in case it was changed by other TC
	   		CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
	   		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		} catch (Exception e) {
			e.printStackTrace();
			}
		
		// Check if the compounds Key was added to compounds_Sync_map.txt file
		foundInSyncMap = CompoundsTab.checkIfFoundInCompoundSyncMap(compoundKey);
		LogUltility.log(test, logger, compoundKey +" found in compounds_Sync_map.txt file: "+foundInSyncMap);
		assertTrue(foundInSyncMap);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
@AfterMethod(alwaysRun = true)
public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
{		
	
	int testcaseID = 0;
	if (enableReportTestlink == true)
		testcaseID=rpTestLink.GetTestCaseIDByName("Compound/Syncing Tests",method.getName());
	
//	System.out.println("tryCount: " + tryCount);
//	System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));

	if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
			&& Integer.parseInt(Setting.RetryFailed)!=0) {
		extent.removeTest(test);
		
        // Close popups to get back to clean app
		 System.out.println("Test Case Failed");
		 CurrentPage = GetInstance(DictionaryTab.class);
		 if(Setting.closeEveryWindow.equals("true"))
			 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
	}
	else if(result.getStatus() == ITestResult.FAILURE)
	    {
		 	tryCount = 0;
	        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
	        test.fail(result.getThrowable());
	        String screenShotPath = Screenshot.captureScreenShot();
	        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
	        // Send result to testlink
	        if (enableReportTestlink == true){
	        	try {
	        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
	            	LogUltility.log(test, logger, "Report to testtlink: " + response);
				} catch (Exception e) {
					LogUltility.log(test, logger, "Testlink Error: " + e);
				}
	        	}
	        
	        // Close popups to get back to clean app
//	        if (Integer.parseInt(Setting.RetryFailed) != 0) {
	         System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//	        }
	    }
	    else if(result.getStatus() == ITestResult.SUCCESS)
	    {
	    	tryCount = 0;
	        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
	        // Send result to testlink
	        if (enableReportTestlink == true){
	        	try {
	        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
	            	LogUltility.log(test, logger, "Report to testtlink: " + response);
	    		} catch (Exception e) {
	    			LogUltility.log(test, logger, "Testlink Error: " + e);
	    		}
	        	
	        	}
	    }
	    else
	    {
	        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
	        test.skip(result.getThrowable());
	    }
	
	    extent.flush();
	    
		// Count how many a test was processed
		tryCount++;
		}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			// DriverContext._Driver.quit();
			}

}

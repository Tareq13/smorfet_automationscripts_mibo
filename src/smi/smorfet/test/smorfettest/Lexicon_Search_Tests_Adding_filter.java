package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.LexiconTab;

/**
 * 
 * All tests for Lexicon search
 *
 */
public class Lexicon_Search_Tests_Adding_filter extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Lexicon tab");
	
		// Logger
		logger = LogManager.getLogger(LexiconTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Lexicon tab");
		logger.info("Lexicon tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(LexiconTab.class);
//		CurrentPage.As(LexiconTab.class).Start_Window.click();
//		CurrentPage.As(LexiconTab.class).bt_abort.click();
		
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(LexiconTab.class);
//			CurrentPage.As(LexiconTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Lexicon");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * DIC_TC--197:Verify user can add filter to the search 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_197_Verify_user_can_add_filter_to_the_search() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Lexicon Search Test - DIC_TC_197", "Verify user can add filter to the search");
		
//		// Get records from the Lexicon file
//		CurrentPage = GetInstance(LexiconTab.class);
//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
//		
//		// Pick a random record
//		Random randomizer = new Random();
//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));

		//Press the Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).LexiconTab.click();

		// Focus on Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();

		// Choose "form" from the first search dropdown
//		String firstDropdownValue = null;
		String randomrecord = null;
		String randomrecord2 = null;
		
		if (Setting.Language.equals("EN")) {
//			firstDropdownValue = "form";
			randomrecord ="100";
			randomrecord2 ="th";
		}
		else if (Setting.Language.equals("HE")) {
//			firstDropdownValue = "presentations";
			randomrecord ="אא";
			randomrecord2 ="אס";
		}
			
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

//		// Change the last filter to "all records", in case it was changed by other TC
//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");

		// Type in the search field
//		String randomrecord ="100";
		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		
		// add filter , change the " all record" to " add filter"  cmpCondition_all_next
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "add filter");
		LogUltility.log(test, logger, "Change the all record value to add filter");
		LogUltility.log(test, logger, "dropdown now displaying : add filter ");
		
		// Type in the search field to add filter
//		String randomrecord2 ="th";
		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord2);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord2);
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList2 = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList2);
		
		// Check that retrieved records do contain the searched record
		if (recordsList2.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList2) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(randomrecord)&& s.toLowerCase().contains(randomrecord2)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		assertTrue(containResult);
		LogUltility.log(test, logger, "Every results contain the 2 searched values 100 and th "+ recordsList2   + " and the value should be true: " + containResult);
		
		// Erase the  filter dropdown to  " all records"
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "Set the dropdown to all records");
		LogUltility.log(test, logger, "dropdown now displaying : all records ");
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

/**
	 * DIC_TC--198:Verify user can change filter while searching
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_198_Verify_user_can_change_filter_while_searching() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Lexicon Search Test - DIC_TC_198", "Verify user can change filter while searching");
		
//		// Get records from the Lexicon file
//		CurrentPage = GetInstance(LexiconTab.class);
//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
//		
//		// Pick a random record
//		Random randomizer = new Random();
//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));

		//Press the Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).LexiconTab.click();
		
		// Focus on Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		
		// Choose "form" from the first search dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
//		// Change the last filter to "all records", in case it was changed by other TC
//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Choose "form" from the first search dropdown
//		String firstDropdownValue = null;
		String randomrecord = null;
		String randomrecord2 = null;
		String randomrecord3 = null;
		
		if (Setting.Language.equals("EN")) {
//			firstDropdownValue = "form";
			randomrecord ="100";
			randomrecord2 ="th";
			randomrecord3 ="s";
		}
		else if (Setting.Language.equals("HE")) {
//			firstDropdownValue = "presentations";
			randomrecord ="אא";
			randomrecord2 ="אס";
			randomrecord3 ="רו";
		}
		
		// Type in the search field
//		String randomrecord ="90";
		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		
		// add filter , change the " all record" to " add filter"  cmpCondition_all_next
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "add filter");
		LogUltility.log(test, logger, "Change the all record value to add filter");
		LogUltility.log(test, logger, "dropdown now displaying : add filter ");
		
		
		// Type in the search field to add filter
//		String randomrecord2 ="th";
		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord2);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord2);
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList2 = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList2);
		
		// Check that retrieved records do contain the searched record
		if (recordsList2.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList2) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(randomrecord)&& s.toLowerCase().contains(randomrecord2)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		assertTrue(containResult);
		LogUltility.log(test, logger, "Every results contain the 2 searched values 100 and th "+ recordsList2   + " and the value should be true: " + containResult);
		
		
		// add filter , change the " all record" to " change filter"  cmpCondition_all_next
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "change filter");
		LogUltility.log(test, logger, "Change the all record value to change filter");
		LogUltility.log(test, logger, "dropdown now displaying : change filter ");
		
		// Type in the search field to add filter
//		String randomrecord3 ="s";
		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord3);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord3);
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList3 = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList3);
		
		
		
		// Check that retrieved records do contain the searched record
		if (recordsList2.isEmpty()) assertTrue(false);
		
		boolean containResult2 = true;
		for (String s : recordsList3) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(randomrecord)&& s.toLowerCase().contains(randomrecord3)) continue;
		       	else {
		       		containResult2 = false;
		           break;
		       	}
		    }
		assertTrue(containResult2);
		LogUltility.log(test, logger, "Every results contain the 2 searched values 100 and th "+ recordsList3   + " and the value should be true: " + containResult2);

		// Erase the  filter dropdown to  " all records"
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "Set the dropdown to all records");
		LogUltility.log(test, logger, "dropdown now displaying : all records ");
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Lexicon/Search/Adding filter",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}

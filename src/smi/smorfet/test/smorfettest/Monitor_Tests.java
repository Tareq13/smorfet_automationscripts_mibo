package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.MonitorTab;

/**
 * 
 * All tests for Corpus
 *
 */
public class Monitor_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Monitor tab");
	
		// Logger
		logger = LogManager.getLogger(MonitorTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite = Monitor tab");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}

		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(MonitorTab.class);
//			CurrentPage.As(MonitorTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Monitor");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * DIC_TC--15:Check the process of adding new phrase to the corpus
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_15_Check_the_process_of_adding_new_phrase_to_the_corpus() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Monitor Tests - DIC_TC--15", "Check the process of adding new phrase to the corpus");

		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();
					
		//Insert the sentence in the field
//		String sentenceToAdd = "I like to go to the ocean";
		String sentenceToAdd = CorpusTab.getRandomSentence();
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
		
		//Click new button in footer
		MonitorTab.bt_new.click();
		//Confirm and click ok
		MonitorTab.bt_ok.click();
				
		//Click the save button
		CorpusTab.bt_save.click();
		CorpusTab.bt_ok_save.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		// Search in corpus and that in order to have less number of sentences to work with
//		String searchWord = "ocean";
		sentenceToAdd = CommonFunctions.cleanStringNikud(sentenceToAdd);
		CorpusTab.searchInCorpus(test,logger,"text","contains",sentenceToAdd,"all records");
				
		/*Check if the new sentence is added to the records display window*/
			
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		//Retrieve the last added sentence including it's serial number
		String newSentence = sentencesList.get(sentencesList.size()-1);
		//Eliminate the serial number 
		String sentence = newSentence.substring(6);
		LogUltility.log(test, logger, "Retrieve the new sentence from the display window: " + sentence);
		boolean result = sentence.equals(sentenceToAdd);
		LogUltility.log(test, logger, "Comparation result: " + result);
		assertTrue(result);
		
		/*check if the new sentence saved in the database file*/	
		List<String> corpusData = CorpusTab.getRecordsFromCorpus();
		LogUltility.log(test, logger, "Retrieve the new sentence from the corpus file: " + corpusData.get(corpusData.size()-1));
		
		result = corpusData.get(corpusData.size()-1).equals(sentenceToAdd);
		LogUltility.log(test, logger, "Comparation result: " + result);
		assertTrue(result);
		 
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 * DIC_TC--265:Check the Remove SSML tags button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_265_Check_the_Remove_SSML_tags_button() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Monitor Tests - DIC_TC--265", "Check the Remove SSML tags button");

		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();
		
		// Click view tab
		MonitorTab.viewTab.click();
		LogUltility.log(test, logger, "Click view tab");
		
		// Click show remove tags checkbox
		if(!MonitorTab.cbx_show_removeTags.isSelected())
			MonitorTab.cbx_show_removeTags.click();
		LogUltility.log(test, logger, "Click the 'Show Remove Tags' Checkbox");
		
		// Insert the sentence in the field
		String startTag = "<speak>";
		String endTag = "</speak>";
		String sentence = CorpusTab.getRandomSentence();
		String sentenceToAdd = startTag.concat(sentence).concat(endTag);
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
		
		// Click the 'Disambiguate' button
		MonitorTab.bt_disambiguate.click();
		LogUltility.log(test, logger, "Click the 'Disambiguate' button");
		
		// Get text from the 'Remove Tags' text area
		String removedTagsText = MonitorTab.tb_SSML_tags_removed.getText();
		
		// Check if the tags are not in the sentence after removal
		boolean startTagRemoved = !removedTagsText.contains(startTag);
		boolean endTagRemoved = !removedTagsText.contains(endTag);		
		boolean result = startTagRemoved & endTagRemoved;
		LogUltility.log(test, logger, "Comparation result: " + result);
	
		assertTrue(result);
		 
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 * DIC_TC--266:Check the Normalize button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_266_Check_the_Normalize_button() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Monitor Tests - DIC_TC--266", "Check the Normalize button");

		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();
					
		// Insert the sentence in the field
		String sentenceToAdd = "10-5-2016";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);

		
		// Click the 'Disambiguate' button
		MonitorTab.bt_disambiguate.click();
		LogUltility.log(test, logger, "Click the 'Disambiguate' button");
		
		// Get text from the 'Normalize' text area
		String normalizedSentence = MonitorTab.tb_normalized.getText();
		LogUltility.log(test, logger, "Normalized sentence: " + normalizedSentence);
		
		// Expected result
		String expectedSentence ="";
		expectedSentence = Setting.Language.equals("EN")? "October fifth , twenty sixteen" : "עֲשָׂרָה בּמָאי אַלְפַּיִם ושֵׁשׁ עֶשְׂרֵה";
		
		// Check if the expected sentence equals the actual sentence
		boolean result = normalizedSentence.contains(expectedSentence);
		LogUltility.log(test, logger, "Comparation result: " + result);
	
		assertTrue(result);
		 
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 * DIC_TC--279:Check the Space Punctuation button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_279_Check_the_Space_Punctuation_button() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Monitor Tests - DIC_TC--279", "Check the Space Punctuation button");


		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();
		
		// Click view tab
		MonitorTab.viewTab.click();
		LogUltility.log(test, logger, "Click view tab");
		
		// Click show space punctuation
		if(!MonitorTab.cbx_show_spacePunctuation.isSelected())
			MonitorTab.cbx_show_spacePunctuation.click();
		LogUltility.log(test, logger, "Click the 'Show Space Punctuation' Checkbox");
		
		// Build Punctuation mark set
		ArrayList<Character> punctuationSet = new ArrayList<Character>();
		for(int i=33;i<48;i++)
			punctuationSet.add((char)i);
		for(int i=58;i<65;i++)
			punctuationSet.add((char)i);
		for(int i=91;i<97;i++)
			punctuationSet.add((char)i);
//		for(int i=123;i<127;i++)
//			punctuationSet.add((char)i);
		
		// Build sentence with three punctuation marks
		Random randomizer = new Random();
		char firstChar = punctuationSet.get(randomizer.nextInt(punctuationSet.size()));
		char secondChar =  punctuationSet.get(randomizer.nextInt(punctuationSet.size()));
		char thirdChar =  punctuationSet.get(randomizer.nextInt(punctuationSet.size()));
		
		
		// Insert the sentence in the field
//		String sentenceFirstPart = "Amazing The fa";
//		String sentenceSecondPart = "rmer speaks Latin";
		String sentenceFirstPart = CorpusTab.getRandomSentence();
		String sentenceSecondPart = CorpusTab.getRandomSentence();
		String sentenceToAdd = firstChar+sentenceFirstPart+secondChar+sentenceSecondPart+thirdChar;
		
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);	
		LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
	
		// Click the 'Disambiguate' button
		MonitorTab.bt_disambiguate.click();
		LogUltility.log(test, logger, "Click the 'Disambiguate' button");
		
		// Get text from the 'Space punctuation' text area
		String puncSentence = MonitorTab.tb_space_punctuation.getText();
		LogUltility.log(test, logger, "Space punctuation sentence: " + puncSentence);
		
		// Check if the sentence has space before and after each punctuation mark
		boolean spaceFlag = true;
		for(int i=0;i<puncSentence.length();i++)
		{
			// Punctuation mark at first index
			if(i==0 && puncSentence.charAt(i) == firstChar)
				if(!(puncSentence.charAt(i+1) == ' '))
				{
					spaceFlag = false;
					break;
				}
			// Punctuation mark at last index
			if(i==puncSentence.length()-1  && puncSentence.charAt(i) == thirdChar)
				if(!(puncSentence.charAt(i-1)== ' '))
				{
					spaceFlag = false;
					break;
				}
			// Punctuation mark in other indexes
			if(puncSentence.charAt(i) == secondChar && i!=puncSentence.length()-1 && i!=0)
				if(! (puncSentence.charAt(i-1) == ' ' && puncSentence.charAt(i+1) == ' '))
				{
					spaceFlag = false;
					break;
				}
		}
		
		LogUltility.log(test, logger, "Comparation result: " + spaceFlag);
		assertTrue(spaceFlag);

		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 * DIC_TC--643:Space Punctuation, prefixes with hyphen
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_643_Space_Punctuation_prefixes_with_hyphen() throws InterruptedException
	{

		test = extent.createTest("Monitor Tests - DIC_TC--643", "Space Punctuation, prefixes with hyphen");
		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();

		// Click view tab
		MonitorTab.viewTab.click();
		LogUltility.log(test, logger, "Click view tab");
		
		// Click show space punctuation
		if(!MonitorTab.cbx_show_spacePunctuation.isSelected())
			MonitorTab.cbx_show_spacePunctuation.click();
		LogUltility.log(test, logger, "Click the 'Show Space Punctuation' Checkbox");
		
		// Insert the sentence in the field
		String sentenceToAdd = Setting.Language.equals("EN")? "is she your ex-wife" : "אבו-ערב";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);

		// Click the 'Disambiguate' button
		MonitorTab.bt_disambiguate.click();
		LogUltility.log(test, logger, "Click the 'Disambiguate' button");
		
		// Get text from the 'Space punctuation' text area
		String puncSentence = MonitorTab.tb_space_punctuation.getText();
		LogUltility.log(test, logger, "Space punctuation sentence: " + puncSentence);
		
		boolean spaces = false;
		String prefix = Setting.Language.equals("EN")? "ex" : "אבו";
		
		// Look for hyphen then check if it has the required prefix before it
		// In English there shouldn't be spaces
		if(Setting.Language.equals("EN"))
		{
		for(int i=0;i<puncSentence.length();i++)
			if(puncSentence.charAt(i) == '-')
			{
				String subStr = puncSentence.substring(i-prefix.length(), i);
				if(subStr.equals(prefix))
					if(puncSentence.charAt(i+1)!=' ' && puncSentence.charAt(i-1)!=' ')
						spaces = true;
			}
		}
		else
			if(Setting.Language.equals("HE"))
			
		{
			// In Hebrew, there should be spaces
			for(int i=0;i<puncSentence.length();i++)
				if(puncSentence.charAt(i) == '-')
				{
					String subStr = puncSentence.substring(i-prefix.length()-1,i-1);
					if(subStr.equals(prefix))
						if(puncSentence.charAt(i+1)==' ' && puncSentence.charAt(i-1)==' ')
							spaces = true;
				}
		}
		LogUltility.log(test, logger, "Comparation result: " + spaces);
		assertTrue(spaces);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	

	/**
	 * DIC_TC--280:Check the Break Morphology button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_280_Check_the_Break_Morphology_button() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Monitor Tests - DIC_TC--280", "Check the Break Morphology button");

		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();
					
		// Insert the sentence in the field
		String sentenceToAdd = "He is sleepaholic";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
		
		// Click new button in footer
		MonitorTab.bt_new.click();
		//Confirm and click ok
		MonitorTab.bt_ok.click();
				
		// Check that sleepaholic is displayed as sleep+aholic in corpus
		List<String> sentenceRecords = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "The sentence records are: "+sentenceRecords);
		
		// Check that the suffix is displayed as expected
		String expectedRepresentation = "sleep + aholic";
		LogUltility.log(test, logger, "The expected representation is: "+expectedRepresentation);
		boolean asExpected = sentenceRecords.contains(expectedRepresentation);
		LogUltility.log(test, logger, "The suffix is represented as expected: "+asExpected);
		assertTrue(asExpected);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--439:Check the domain dropdown
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_439_Check_the_domain_dropdown() throws InterruptedException 
	{
		test = extent.createTest("Monitor Tests - DIC_TC--439", "Check the domain dropdown");

		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();
					
		// Get dropdown values
		List<String> domains = MonitorTab.getValuesFromApp(MonitorTab.domainDropdown);
		LogUltility.log(test, logger, "The domain dropdown values: "+domains);
		boolean notEmpty = !domains.isEmpty();
		LogUltility.log(test, logger, "The dropdown isn't empty: "+notEmpty);
		assertTrue(notEmpty);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 * DIC_TC--441:Check the user can select any domain by typing the domain tags
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_441_Check_the_user_can_select_any_domain_by_typing_the_domain_tags() throws InterruptedException, IOException 
	{
		test = extent.createTest("Monitor Tests - DIC_TC--441", "Check the user can select any domain by typing the domain tags");

		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();
					
		// Get dropdown values
		List<String> domains = MonitorTab.getValuesFromApp(MonitorTab.domainDropdown);
		LogUltility.log(test, logger, "The domain dropdown values: "+domains);
	
		// Choose random domain
		Random random = new Random();
		String chosenDomain = domains.get(random.nextInt(domains.size()));
		LogUltility.log(test, logger, "Chosen domain: "+chosenDomain);
		
		// Tag template
		String tagTemp = "<domain=\""+chosenDomain+"\">";
		String intputToField = tagTemp+" "+CorpusTab.getRandomSentence();
		LogUltility.log(test, logger, "Insert the following sentence: "+intputToField);
		MonitorTab.tb_raw_sentence.sendKeys(intputToField);
		
		// Click disambiguate
		MonitorTab.bt_disambiguate.click();
		LogUltility.log(test, logger, "Click the disambiguate button");
		
//		try {
//			boolean res = MonitorTab.popupWindowMessage(test,logger,"","Domain not found in DLL.\nDefault domain will be used for WAV generation.");
//			if(res)
//				MonitorTab.bt_ok_domain.click();
//		}
//		catch(NoSuchElementException e)
//			{	}
		
		String chosenDDdomain = MonitorTab.getChosenValueInDropdown(MonitorTab.domainDropdown).get(0);
		LogUltility.log(test, logger, "The chosen dropdown after disambiguating: "+chosenDDdomain);
		
		boolean same = chosenDDdomain.equals(chosenDomain);
		LogUltility.log(test, logger, "The input field tag domain has been chosen: "+same);
		assertTrue(same);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--442:Check the disambiguate button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_442_Check_the_disambiguate_button() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Monitor Tests - DIC_TC--442", "Check the disambiguate button");

		MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
		CorpusTab CorpusTab = GetInstance(CorpusTab.class).As(CorpusTab.class);
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		
		// Click on the Monitor tab
		MonitorTab.tabMonitor.click();
		
		// Insert the sentence in the field
		String sentenceToAdd = CorpusTab.getRandomSentence();
//		String sentenceToAdd = "doggone epiphyte Bardo Gujerat Thew ovate bipartisanship ";
		MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
		LogUltility.log(test, logger, "Insert sentence: " + sentenceToAdd);
		
		// Get each word alone
		String[] inputWords = sentenceToAdd.split(" ");
		
		// Click the 'Disambiguate' button
		MonitorTab.bt_disambiguate.click();
		LogUltility.log(test, logger, "Click the 'Disambiguate' button");
		
		// Get the displayed table data
		HashMap<String, List<String>> tableData = MonitorTab.getDataTable();
		List<String> words = tableData.get("[DM] Word");
		List<String> pos = tableData.get("[DM] POS");
		List<String> formType = tableData.get("[DM] FormType");

		// Check if the displayed words in the table matches the input
		boolean asExpected = true;
		for(int i=0;i<words.size();i++)
		{
			String posCode = DictionaryTab.POSConverter(pos.get(i));
			String wordTemp = "EN{"+posCode+"}"+words.get(i);
			String recordForms =  DictionaryTab.getRecordForms(wordTemp);
			String[] splitted = recordForms.split(formType.get(i));
			String spelling = splitted[1].split("~")[1].split("\\|")[0];
			LogUltility.log(test, logger, "Input word: "+words.get(i)+", Expected word: "+spelling);
			if(!inputWords[i].equalsIgnoreCase(spelling))
				{asExpected = false;break;}
			
		}
	
		LogUltility.log(test, logger, "All words are displayed as expected: "+asExpected);
		assertTrue(asExpected);
		 
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Monitor",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}
		
	

}

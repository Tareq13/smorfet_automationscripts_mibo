package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.MonitorTab;

/**
 * 
 * All tests for Dictionary search
 *
 */
public class Corpus_Coloring_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Corpus tab - Coloring");
	
		// Logger
		logger = LogManager.getLogger(CorpusTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite = Corpus tab - Coloring");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}

		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(CorpusTab.class);
//			CurrentPage.As(CorpusTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Corpus");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * DIC_TC--380:Check the Audience drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_380_Check_the_Audience_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--380", "Check the Audience drop-down coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and choose unknown in the Audience dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_audience, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.dd_audience);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the Audience dropdown
		List<String> audienceList = CommonFunctions.getValuesFromApp(CorpusTab.dd_audience);
		audienceList.remove("unknown");
		String randomAudience= audienceList.get(randomizer.nextInt(audienceList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_audience, randomAudience);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new audience value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.dd_audience);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--381:Check the Complexity drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_381_Check_the_Complexity_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--381", "Check the Complexity drop-down coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and choose unknown in the Complexity dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_complexity, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.dd_complexity);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the Complexity dropdown
		List<String> complexityList = CommonFunctions.getValuesFromApp(CorpusTab.dd_complexity);
		complexityList.remove("unknown");
		String randomComplexity= complexityList.get(randomizer.nextInt(complexityList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_complexity, randomComplexity);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new complexity value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.dd_complexity);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--383:Check the formality drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_383_Check_the_formality_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--383", "Check the formality drop-down coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and choose unknown in the formality dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_formality, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.dd_formality);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the formality dropdown
		List<String> formalityList = CommonFunctions.getValuesFromApp(CorpusTab.dd_formality);
		formalityList.remove("unknown");
		String randomformality= formalityList.get(randomizer.nextInt(formalityList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_formality, randomformality);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new formality value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.dd_formality);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--384:Check the �medium� drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_384_Check_the_medium_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--384", "Check the �medium� drop-down coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and choose unknown in the medium dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_medium, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.dd_medium);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the medium dropdown
		List<String> mediumList = CommonFunctions.getValuesFromApp(CorpusTab.dd_medium);
		mediumList.remove("unknown");
		String randomedium= mediumList.get(randomizer.nextInt(mediumList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_medium, randomedium);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new medium value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.dd_medium);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--385:Check the �social status� selection box coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_385_Check_the_social_status_selection_box_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--385", "Check the �social status� selection box coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");

		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and unselect all values in the list
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_social_status);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.lb_social_status);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");

		// Choose another value than unknown from the social dropdown
		List<String> socialList = CommonFunctions.getValuesFromApp(CorpusTab.lb_social_status);
		socialList.remove("unknown");
		String randoSocial= socialList.get(randomizer.nextInt(socialList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_social_status, randoSocial);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new social value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.lb_social_status);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--386:Check the �sentence type� selection box coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_386_Check_the_sentence_type_selection_box_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--386", "Check the �sentence type� selection box coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");

		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and unselect all values in the list
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_sentence_type);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.lb_sentence_type);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");

		// Choose another value than unknown from the sentence type list
		List<String> sentenceTypelList = CommonFunctions.getValuesFromApp(CorpusTab.lb_sentence_type);
		sentenceTypelList.remove("unknown");
		String randomSentenceType= sentenceTypelList.get(randomizer.nextInt(sentenceTypelList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_sentence_type, randomSentenceType);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new sentence type value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.lb_sentence_type);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--387:Check the �Speech act� selection box coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_387_Check_the_Speech_act_selection_box_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--387", "Check the �Speech act� selection box coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");

		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and unselect all values in the list
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_speech_act);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.lb_speech_act);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");

		// Choose another value than unknown from the speech Act list
		List<String> speechActList = CommonFunctions.getValuesFromApp(CorpusTab.lb_speech_act);
		speechActList.remove("unknown");
		String speechActType= speechActList.get(randomizer.nextInt(speechActList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_speech_act, speechActType);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new sentence type value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.lb_speech_act);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--388:Check the �Lingo� selection box coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_388_Check_the_Lingo_selection_box_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--388", "Check the �Lingo� selection box coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");

		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and unselect all values in the list
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_lingo);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.lb_lingo);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");

		// Choose another value than unknown from the Lingo list
		List<String> lingoList = CommonFunctions.getValuesFromApp(CorpusTab.lb_lingo);
		lingoList.remove("unknown");
		String lingoType= lingoList.get(randomizer.nextInt(lingoList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_lingo, lingoType);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new sentence type value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.lb_lingo);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--389:Check the �mood� selection box coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_389_Check_the_mood_selection_box_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--389", "Check the �mood� selection box coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");

		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and unselect all values in the list
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_mood);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.lb_mood);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");

		// Choose another value than unknown from the Mood list
		List<String> moodList = CommonFunctions.getValuesFromApp(CorpusTab.lb_mood);
		moodList.remove("unknown");
		String moodType= moodList.get(randomizer.nextInt(moodList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_mood, moodType);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new sentence type value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.lb_mood);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--390:Check the �Dialect� selection box coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_390_Check_the_Dialect_selection_box_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--390", "Check the �Dialect� selection box coloring");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field, "text");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");

		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");

		//Type in the search field
		String searchWord = "ocean";
		CorpusTab.cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");

		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CorpusTab.btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Choose random record from the first 20 records in the displayed list
		sentencesList = sentencesList.subList(0, 20 > sentencesList.size() ? sentencesList.size() : 20);

		// Pick first random sentence
		Random randomizer = new Random();
		String randomRecordFirst = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first sentence from the sentences list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = sentencesList.get(randomizer.nextInt(sentencesList.size()));
		
		// First go and unselect all values in the list
		CommonFunctions.unselectValuesFromList(CorpusTab.lb_dialect);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the setnences list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first sentence again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CorpusTab.lb_dialect);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");

		// Choose another value than unknown from the Mood list
		List<String> dialectList = CommonFunctions.getValuesFromApp(CorpusTab.lb_dialect);
		dialectList.remove("unknown");
		String dialectType= dialectList.get(randomizer.nextInt(dialectList.size()));
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_dialect, dialectType);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second sentence from the sentences list to sync changes: " + randomRecordSecond);

		// Choose back the first sentence and then check the color of the new sentence type value
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of new value
		String colorValue = CommonFunctions.getColor(CorpusTab.lb_dialect);
		LogUltility.log(test, logger, "Color of a value should be White: " + colorValue);
		assertEquals(colorValue, "White");

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--451:Check editing record to not exist record in the lexicon will displays it as black record
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_451_Check_editing_record_to_not_exist_record_in_the_lexicon_will_displays_it_as_black_record() throws InterruptedException, AWTException, FileNotFoundException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--451", "Check editing record to not exist record in the lexicon will displays it as black record");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();		
		
		// Search in corpus and that in order to have less number of sentences to work with
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "ocean", "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
        LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Pick random number to search for
		Random randomizer = new Random();
		String randomSentence = sentencesList.get(randomizer.nextInt(sentencesList.size()));		
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence: " + randomSentence);

		// Get list of the records for the chosen sentence
		List<String> recordsList = CorpusTab.getSentencesRecords();
		LogUltility.log(test, logger, "Records list: " + recordsList);

		// Choose and click on random record
		String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		LogUltility.log(test, logger, "Click random record " + randomRecord);
		int recordNumber = recordsList.indexOf(randomRecord);
		CorpusTab.clickSentencesRecord(randomRecord, recordNumber);
		
		
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_END);
//		Thread.sleep(1000);
		r.delay(100);
		r.keyRelease(KeyEvent.VK_END);
		
		// Add text
		String str = CorpusTab.RandomString(5);
//		CorpusTab.keyType(test, logger, str);
		CorpusTab.keyType(test, logger, str);
//	    r.keyPress(str);
//		endClick.sendKeys(str);
		
		// Click the update button
		CorpusTab.bt_update.click();
		LogUltility.log(test, logger, "Click update button");
		
		// Choose the word again after updating
		recordsList = CorpusTab.getSentencesRecords();
		randomRecord = recordsList.get(recordNumber);
		CorpusTab.clickSentencesRecord(randomRecord, recordNumber);
		WebElement recordElement = CorpusTab.sentencesRecordElement(randomRecord, recordNumber);
		LogUltility.log(test, logger, "Click on the updated word");
	
//		// Click on the word's end
//		r.keyPress(KeyEvent.VK_END);
//		r.delay(100);
//		r.keyRelease(KeyEvent.VK_END);
//		
//		// Delete the word using backspace	
//		LogUltility.log(test, logger, "Press backspace to delete the word");
//		
//		int size = randomRecord.length();
//		while(size-- > 0)
//			{CorpusTab.keyType(test, logger, "\b");}
				
		// Get color
		String actualColor = CommonFunctions.getColor(recordElement);
		String expectedColor = "Black";
		
		// Compare result
		boolean sameColor = actualColor.equals(expectedColor);
		LogUltility.log(test, logger, "Expected color: "+expectedColor+" ,Actual color: "+actualColor+" they are the same: "+sameColor);
		assertTrue(sameColor);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	

/**
 * DIC_TC--620:Verify the undefined compounds display in the corpus
 * @throws InterruptedException 
 * @throws IOException 
 * @throws AWTException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_620_Verify_the_undefined_compounds_display_in_the_corpus() throws InterruptedException, IOException, AWTException 
{
	test = extent.createTest("Corpus Tests - DIC_TC--620", "Verify the undefined compounds display in the corpus");
	
	CurrentPage = GetInstance(CorpusTab.class);
	CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
	MonitorTab MonitorTab = GetInstance(MonitorTab.class).As(MonitorTab.class);
	CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);
	
	// Click on the Corpus tab
	CorpusTab.tabCorpus.click();		
	
	// Search in corpus and that in order to have less number of sentences to work with
	CorpusTab.searchInCorpus(test, logger, "text", "contains", "hello", "all records");
	
	// Choose value from sort by dropdown
	Random randomizer = new Random();
	
	// Click the new button in the footer
	CorpusTab.bt_new.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click ok 
	CorpusTab.bt_ok_new.click();
	LogUltility.log(test, logger, "Click the ok button");
	
	// Insert random sentence in the field
	List<String> undefiendCompounds = CompoundsTab.getUndefinedCompoundsSet(10);
	String undefiendCompound = undefiendCompounds.get(randomizer.nextInt(undefiendCompounds.size()));
	String randomSentence = CorpusTab.getRandomSentence();
	String sentenceToAdd = randomSentence + " " + undefiendCompound;
	MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
	LogUltility.log(test, logger, "Insert sentence: "+sentenceToAdd);
	
	// Click new button in footer
	MonitorTab.bt_new.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Confirm and click ok
	MonitorTab.bt_ok.click();
	LogUltility.log(test, logger, "Click the ok button");
	
	// Click on the last part of the compound
	List<String> recordsInSentence = CorpusTab.getSentencesRecords();
	String record = recordsInSentence.get(recordsInSentence.size()-1);
	int number =  recordsInSentence.size()-1;
	CorpusTab.clickSentencesRecord(record,number);
	LogUltility.log(test, logger, "Click on the record");
	
	// Get the color
	WebElement recordElement = CorpusTab.sentencesRecordElement(record, number);
	String color = CommonFunctions.getColor(recordElement);
	boolean checkColor = color.equals("Dark Green") || color.equals("Dark Green2") || color.equals("Dark Green3");
	LogUltility.log(test, logger,"The color should be dark green: " +checkColor);
				
	assertTrue(checkColor);
	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	/**
	 * DIC_TC--403:Check the corpus list box colors - part 1 selected sentence color
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_403_Check_the_corpus_list_box_colors_part1_selected_sentence_color() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--403", "Check the corpus list box colors - part 1 selected sentence color");
	
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
	
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		CommonFunctions.clickTabs("Corpus");
	
		// Search in corpus 
		CorpusTab.searchInCorpus(test, logger, "text", "contains", "test", "all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		
		// Pick random number to search for
		Random randomizer = new Random();
		int index = randomizer.nextInt(sentencesList.size());
		String randomSentence = sentencesList.get(index);
		CommonFunctions.chooseValueInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		LogUltility.log(test, logger, "Chosen sentence from Corpus tab: " + randomSentence);
		
		// Get color of selected sentence
		WebElement sentenceElement = CorpusTab.getWebElementInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		String chosenSentenceColor = CommonFunctions.getColor(sentenceElement);
		String expectedColor = "Light Blue";
		LogUltility.log(test, logger, "The color is:"+chosenSentenceColor+" ,The expected color is: "+expectedColor);
		boolean isBlue = chosenSentenceColor.equals(expectedColor);
		LogUltility.log(test, logger, "The color is correct:"+isBlue);
		assertTrue(isBlue);
	
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--403:Check the corpus list box colors - part 2 fully tagged sentence color
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_403_Check_the_corpus_list_box_colors_part2_fully_tagged_sentence_color() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--403", "Check the corpus list box colors - part 2 fully tagged sentence color");
	
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
	
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		CommonFunctions.clickTabs("Corpus");
	
		// Search in corpus 
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,"status");
   		LogUltility.log(test, logger, "status is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "equal");
   		LogUltility.log(test, logger, "equal is chosen from the condition dropdown");

   		// Choose value in dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, "fully tagged");
   		LogUltility.log(test, logger, "Set dropdown to: fully tagged");
   		
   		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: all records");
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Move the mouse to the list location
		String attachPlace = CorpusTab.lb_corpus_records.getAttribute("ClickablePoint");
		String[] XY = attachPlace.split(",");
		Robot robot = new Robot();
	    int XPoint =  Integer.parseInt(XY[0]), YPoint =  Integer.parseInt(XY[1]);
	    robot.mouseMove(XPoint, YPoint);    
        String color = CommonFunctions.getColor(CorpusTab.lb_corpus_records);
        String expectedColor = "White";
        LogUltility.log(test, logger, "The fully tagged sentence color: "+color+", The expected color: "+expectedColor);
        
        // Check if it is white
        boolean same = color.equals(expectedColor);
        LogUltility.log(test, logger, "The color are the same: "+same);
        assertTrue(same);
        
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--403:Check the corpus list box colors - part 3 partially tagged sentence color
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_403_Check_the_corpus_list_box_colors_part3_partially_tagged_sentence_color() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--403", "Check the corpus list box colors - part 3 partially tagged sentence color");
	
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
	
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		CommonFunctions.clickTabs("Corpus");
	
		// Search in corpus 
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,"status");
   		LogUltility.log(test, logger, "status is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "equal");
   		LogUltility.log(test, logger, "equal is chosen from the condition dropdown");

   		// Choose value in dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, "partially tagged");
   		LogUltility.log(test, logger, "Set dropdown to: partially tagged");
   		
   		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: all records");
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
		 		
   		// Retrieve sentences
   		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
   			
		// Pick random number to search for
   		sentencesList.remove(0);
		Random randomizer = new Random();
		int index = randomizer.nextInt(sentencesList.size());
		String randomSentence = sentencesList.get(index);

		// Move the mouse to the list location
   		CorpusTab.ScrollLargeIncrement(CorpusTab.lb_corpus_records);
   		
		// Get color of selected sentence
		WebElement sentenceElement = CorpusTab.getWebElementInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		String chosenSentenceColor = CommonFunctions.getColor(sentenceElement);
		
        String expectedColor = "Red";
        LogUltility.log(test, logger, "The partially tagged sentence color: "+chosenSentenceColor+", The expected color: "+expectedColor);
        
        // Check if it is red
        boolean same = chosenSentenceColor.equals(expectedColor);
        LogUltility.log(test, logger, "The color are the same: "+same);
        assertTrue(same);
        
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--403:Check the corpus list box colors - part 4 not tagged sentence color
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_403_Check_the_corpus_list_box_colors_part4_not_tagged_sentence_color() throws InterruptedException, AWTException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--403", "Check the corpus list box colors - part 4 not tagged sentence color");
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
	
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		CommonFunctions.clickTabs("Corpus");
	
		// Search in corpus 
		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,"status");
   		LogUltility.log(test, logger, "status is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "equal");
   		LogUltility.log(test, logger, "equal is chosen from the condition dropdown");

   		// Choose value in dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, "not tagged");
   		LogUltility.log(test, logger, "Set dropdown to: partially tagged");
   		
   		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: all records");
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
		 		
   		// Retrieve sentences
   		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
   			
		// Pick random number to search for
   		sentencesList.remove(0);
		Random randomizer = new Random();
		int index = randomizer.nextInt(sentencesList.size());
		String randomSentence = sentencesList.get(index);

		// Move the mouse to the list location
   		CorpusTab.ScrollLargeIncrement(CorpusTab.lb_corpus_records);
   		
		// Get color of selected sentence
		WebElement sentenceElement = CorpusTab.getWebElementInDropdown(CorpusTab.lb_corpus_records, randomSentence);
		String chosenSentenceColor = CommonFunctions.getColor(sentenceElement);
		
        String expectedColor = "Red";
        LogUltility.log(test, logger, "The partially tagged sentence color: "+chosenSentenceColor+", The expected color: "+expectedColor);
        
        // Check if it is red
        boolean same = chosenSentenceColor.equals(expectedColor);
        LogUltility.log(test, logger, "The color are the same: "+same);
        assertTrue(same);
        
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Corpus/Coloring",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}

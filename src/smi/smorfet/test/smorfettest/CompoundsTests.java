package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.MonitorTab;

/**
 * 
 * All tests for Compounds search
 *
 */
public class CompoundsTests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Compounds tab");
	
		// Logger
		logger = LogManager.getLogger(CompoundsTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Compounds tab");
		logger.info("Compounds tab Tests - FrameworkInitilize");
	
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			//Click the compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
//			CurrentPage.As(CompoundsTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Compounds");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
         
	/**
	 * DIC_TC--54:verify user can Revert changes
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_54_verify_user_can_Revert_changes() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_54", "verify user can Revert changes");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		//Get the text from the Definition textbox
		String beforeClear= CurrentPage.As(CompoundsTab.class).lst_Definition.getText(); 
		LogUltility.log(test, logger, "the text that populated in the Definition textbox before clear:" + beforeClear);
		
		//Click into the Definition textbox
		CurrentPage.As(CompoundsTab.class).lst_Definition.click();
		
		//Mark all the text into the Definition textbox
		CurrentPage.As(CompoundsTab.class).lst_Definition.sendKeys(Keys.chord(Keys.CONTROL, "a"));
	                                                     
		// Clear the Definition textbox
		CurrentPage.As(CompoundsTab.class).lst_Definition.clear();
		LogUltility.log(test, logger, "Clear the Definition textbox");
		
		//Get the text from the Definition textbox
		String AfterClear= CurrentPage.As(CompoundsTab.class).lst_Definition.getText(); 
		LogUltility.log(test, logger, "the text that populated in the Definition textbox after clear should be empty:" + AfterClear);

		// Click the revert button
		CurrentPage.As(CompoundsTab.class).bt_revert.click();
		LogUltility.log(test, logger, "Click the revert button");
		
		// Click Yes on the confirmation message
		CurrentPage.As(CompoundsTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
		// Choose the record again
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose the record again");
		
		// Check the Text is back after revert the changes 
		String AfterRevert= CurrentPage.As(CompoundsTab.class).lst_Definition.getText(); 
		LogUltility.log(test, logger, "the text that populated in the Definition textbox after Revert should NOT be empty:" + AfterRevert);
		
		Boolean textBack= beforeClear.contains(AfterRevert);
		LogUltility.log(test, logger, "the text that populated in the Definition textbox after Revert is the same as before revert:" + textBack);
		assertTrue(textBack);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--83:Check adding a new record to the compounds
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_83_check_adding_a_new_record_to_the_Compounds() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_83", "Check adding a new record to the compounds");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Click the new button
		CurrentPage.As(CompoundsTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(CompoundsTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText="";
		if (Setting.Language.equals("EN"))
			randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomString(5);
		else if (Setting.Language.equals("HE"))
			randomText= CurrentPage.As(CompoundsTab.class).RandomHEString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomHEString(5);

		CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test, logger, "New record: " + randomText);
		
		
		// Get random POS
		List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random pos
		Random randomizer = new Random();
		String randompos = posList.get(randomizer.nextInt(posList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Search for the new added record
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");
	   		 
	   		 
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(removeRight[0].toLowerCase().equals(randomText)) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		
		LogUltility.log(test, logger, "Is new record found: " + equalResult);
		assertTrue(equalResult);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--109:Verify user can change/select Semantic Fields
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_109_verify_user_can_change_select_Semantic_Fields() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_109", "Verify user can change/select Semantic Fields");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
		// Only take the first 40 results in order to click on them
		//recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Semantic Fields list value
		List<String> chosen1lexicalFieldsList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
		LogUltility.log(test, logger, "chosen Semantic Fields list before change: " + chosen1lexicalFieldsList);
	//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		// Change or Select New Semantic Fields
		
		// Get all the values from the Semantic fields list
		List<String> lst_Lexical_Fields = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
		LogUltility.log(test, logger, "all the values from the Semantic fields list: " + lst_Lexical_Fields);
		
		// Change or Select New Semantic Fields
		
		Random randomfield = new Random();
		String randomselect = lst_Lexical_Fields.get(randomfield.nextInt(lst_Lexical_Fields.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields, "art");
		LogUltility.log(test, logger, "Choose from Semantic Fields: " + randomselect);
		
		// Click the Right arrow
		 CurrentPage.As(CompoundsTab.class).Next_button.click();
		  
		// Click the Left arrow
		 CurrentPage.As(CompoundsTab.class).Previous_button.click();
	
		// Get the chosen Semantic Fields list value after changing the selection 
		List<String> chosen2lexicalFieldsList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
		LogUltility.log(test, logger, "chosen Semantic Fields list after the change: " + chosen2lexicalFieldsList);
		
		//verify that the the Semantic field match the searched one
		boolean notSameFields = chosen1lexicalFieldsList.equals(chosen2lexicalFieldsList);
		assertFalse(notSameFields);
		LogUltility.log(test, logger, "Check that the choosen Semantic fields list before the change do NOT equal the final choosen Semantic Fields. and the value should be false: " + notSameFields);
		assertFalse(notSameFields);
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--110: Part one - Verify user can select Semantic Groups
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_110_Part_one_verify_user_can_change_select_Semantic_Groups() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_110", "Part one - Verify user can change/select Semantic Groups");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
		// Only take the first 40 results in order to click on them
		//recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Semantic Groups list value
		List<String> chosen1lexicalGroupsList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen Semantic Groups list before change: " + chosen1lexicalGroupsList);
	//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		// Change or Select New Semantic Groups
		
		// Get all the values from the Semantic Groups list
		List<String> lst_SemanticGroups = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "all the values from the Semantic Groups list: " + lst_SemanticGroups);
		
		// Change or Select New Semantic Groups
		
		Random randomfield = new Random();
		String randomselect = lst_SemanticGroups.get(randomfield.nextInt(lst_SemanticGroups.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups, "age");
		LogUltility.log(test, logger, "Choose from Semantic Groups list: " + randomselect);
		
		// Click the Right arrow
		 CurrentPage.As(CompoundsTab.class).Next_button.click();
		  
		// Click the Left arrow
		 CurrentPage.As(CompoundsTab.class).Previous_button.click();
	
		// Get the chosen Semantic Groups list value after changing the selection 
		List<String> chosen2lexicalGroupsList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen Semantic Groups list after the change: " + chosen2lexicalGroupsList);
		
		//verify that the the Semantic Groups match the searched one
		boolean notSameFields = chosen1lexicalGroupsList.equals(chosen2lexicalGroupsList);
		assertFalse(notSameFields);
		LogUltility.log(test, logger, "Check that the chosen Semantic group before the change do NOT equal the final chosen Semantic group. and the value should be false: " + notSameFields);
		assertFalse(notSameFields);
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--110:Part two - Verify user can change/unselect Semantic Groups
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_110_Part_two_verify_user_can_change_unselect_Semantic_Groups() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_110", "Part two - Verify user can change/unselect Semantic Groups");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
				
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "Semantic Groups" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "semantic groups");
		LogUltility.log(test, logger, "semantic groups is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomSemanticGroups = "v body functions";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomSemanticGroups);
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomSemanticGroups);
		
		// Click the Retrieve button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
//		//Check if the Too many retrieved record message appear then press the OK button  
//		try {
//			boolean isWindowDisplayed = CurrentPage.As(CompoundsTab.class).window_too_many_records.isDisplayed();
//			if (isWindowDisplayed) CurrentPage.As(CompoundsTab.class).btnOKPopupWindow.click();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		

		// Retrieve records
		LogUltility.log(test, logger, "Before getting records list");
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
		// Only take the first 40 results in order to click on them
		//recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Semantic Groups list value
		List<String> chosen1lexicalGroupsList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen list: " + chosen1lexicalGroupsList);
	//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		// Change or Select New Semantic Groups
		
		// Get all the values from the Semantic Groups list
		List<String> lst_SemanticGroups = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "Records retreived: " + lst_SemanticGroups);
		
		// UnSelect the searched Semantic Groups by clicking it
		
		Random randomfield = new Random();
		String randomselect = lst_SemanticGroups.get(randomfield.nextInt(lst_SemanticGroups.size()));
		randomselect = "animal noises";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups, randomselect);
		LogUltility.log(test, logger, "Choose from record list: " + randomselect);
		
//		Random randomUnSelectfield = new Random();
		String randomUNselect = lst_SemanticGroups.get(randomfield.nextInt(lst_SemanticGroups.size()));
		randomUNselect = chosen1lexicalGroupsList.get(0);
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups, randomUNselect);
		LogUltility.log(test, logger, "Choose from record list: " + randomUNselect);
		
		
		// Click the Right arrow
		 CurrentPage.As(CompoundsTab.class).Next_button.click();
		  
		// Click the Left arrow
		 CurrentPage.As(CompoundsTab.class).Previous_button.click();
	
		// Get the chosen Semantic Groups list value after changing the selection 
		List<String> chosen2lexicalGroupsList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen list: " + chosen2lexicalGroupsList);
		
		// Verify that the specific Semantic Groups was unselected
		boolean unselectGroup = !chosen2lexicalGroupsList.contains(randomUNselect);
		assertTrue(unselectGroup);
		LogUltility.log(test, logger, "Check that the retrieved Semantic group list do not include the searched Semantic group since was unselected. the value should be True: " + unselectGroup);
		//verify that the the Semantic Groups match the searched one
		boolean notSameFields = chosen1lexicalGroupsList.equals(chosen2lexicalGroupsList);
		assertFalse(notSameFields);
		LogUltility.log(test, logger, "Check that the retrieved Semantic group list do include the searched Semantic group: " + notSameFields);
		assertFalse(notSameFields);
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 *  DIC_TC--127:Check Cancel button in "Save" pop up
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  DIC_TC_127_Check_Cancel_button_in_Save_pop_up() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_127", "Check Cancel button in Save pop up");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		//Select Value from the Volume POS dropdown 
		String randomPOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, randomPOS);
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomPOS);
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Type a record name in the key form field
		String randomText = CurrentPage.As(CompoundsTab.class).RandomString(3);
		CurrentPage.As(CompoundsTab.class).txtKeyForm.click();
		String newRandomText = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText() + randomText;
		CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(newRandomText);
		LogUltility.log(test, logger, "New record: " + randomText + " ,include the searched Spellings: " + newRandomText);
		
		// Click the Save button for the first time before the key field message
		CurrentPage.As(CompoundsTab.class).btnSave.click();
		LogUltility.log(test, logger, "Click the Save button");
		
		//Press yes on key field message 
		CurrentPage.As(CompoundsTab.class).yesKeyFieldMessage.click();
		LogUltility.log(test, logger, "Click Yes button on the key field message");
		
		// Click the Save button
		CurrentPage.As(CompoundsTab.class).btnSave.click();
		LogUltility.log(test, logger, "Click the Save button for the second time");
		
		// Click Cancel button on the confirmation message button btnCancelSave
		CurrentPage.As(CompoundsTab.class).btnCancelSave.click();
		LogUltility.log(test, logger, "Click the Cancel button in the save confirmation message");
		
		//Check no changes done on the Compounds file
		
		List<String> recordsFromFile =  CurrentPage.As(CompoundsTab.class).getRecordsFromcompounds();
		boolean newRecordexist = recordsFromFile.contains(newRandomText.replace("_", " "));
		LogUltility.log(test, logger, "Is the new record exist in the file after canceling the save? must be False:" + newRecordexist);
		assertFalse(newRecordexist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	

	/**
	 *  DIC_TC--128:Check Cancel button in "New" pop up
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  DIC_TC_128_Check_Cancel_button_in_New_pop_up() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_128", "Check Cancel button in New pop up");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Click the New button to add new record
		CurrentPage.As(CompoundsTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the New button");
		
		//Press No on the adding new record message 
		CurrentPage.As(CompoundsTab.class).btnNoNew.click();
		LogUltility.log(test, logger, "Click No button on the adding new record message to cancel adding new compound");
		
		//Get the KeyForm field value
		
		String keyForm= CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
		LogUltility.log(test, logger, "Value into the KeyForm field: " + keyForm);
		
		//Verify that the KeyForm field is NOT empty since the operation cancelled
		boolean isNotEmpty =! keyForm.isEmpty();
		LogUltility.log(test, logger, "The fuction New was cancelled and KeyForm field is not empty that`s mean the value should be True:" + isNotEmpty);
		assertTrue(isNotEmpty);

		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 *  DIC_TC--129:Check Cancel button in "Revert" pop up
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  DIC_TC_129_Check_Cancel_button_in_Revert_pop_up() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_129", "Check Cancel button in Revert pop up");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		//Select Value from the Volume POS dropdown 
		String randomPOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, randomPOS);
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomPOS);
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose the first record from the record list
		// Pick a first record
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, recordsList.get(0));
		LogUltility.log(test, logger, "Choose from record list: " + recordsList.get(0));


		// get the KeyForm value
		String firstkeyForm= CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
		LogUltility.log(test, logger, "Value into the KeyForm field: " + firstkeyForm);
		
		// Type a record name in the key form field
		String randomText = CurrentPage.As(CompoundsTab.class).RandomString(3);
		CurrentPage.As(CompoundsTab.class).txtKeyForm.click();
		String newRandomText = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText() + randomText;
		CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(newRandomText);
		LogUltility.log(test, logger, "New record: " + randomText + " ,include the searched Spellings: " + newRandomText);
		
		// Click the Revert button 
		CurrentPage.As(CompoundsTab.class).bt_revert.click();
		LogUltility.log(test, logger, "Click the Revert button");
		
		//Press yes on key field message 
		CurrentPage.As(CompoundsTab.class).yesKeyFieldMessage.click();
		LogUltility.log(test, logger, "Click Yes button on the key field message");
		
		// Click the Revert button  button
		CurrentPage.As(CompoundsTab.class).bt_revert.click();
		LogUltility.log(test, logger, "Click the Revert button for the second time");
		
		// Click Cancel button on the Revert confirmation message for
		CurrentPage.As(CompoundsTab.class).btnCancelRevert.click();
		LogUltility.log(test, logger, "Click the Cancel button in the Revert confirmation message");
		
		//Check that change was NOT reverted and Keyform still with the last change 
		String secondkeyForm= CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
		LogUltility.log(test, logger, "Value into the KeyForm field: " + secondkeyForm);
		
		boolean newKeyForm = !firstkeyForm.equals(secondkeyForm);
		LogUltility.log(test, logger, "Verify that: "+ firstkeyForm +" is different than: "+ secondkeyForm +" and value must be True:" + newKeyForm);
		assertTrue(newKeyForm);
		
		// Search for the new Record to verify that the change not reverted 
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(newRandomText);
		LogUltility.log(test, logger, "word is typed in the search field: " + newRandomText);
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList1 = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList1);
		
		// Check that retrieved records do contain the searched record
		if (recordsList1.isEmpty()) assertTrue(false);
		
		boolean equalResult = true;
		for (String s : recordsList1) {
			 String[] removeLeft = s.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");
	   		 
	   		 
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
	   		LogUltility.log(test, logger, "Record after split: " + removeRight[0]);
	   		LogUltility.log(test, logger, "New random text: " + newRandomText);
		       if(removeRight[0].equals(newRandomText)) continue;
		       	else {
		       		equalResult = false;
		           break;
		       	}
		    }
		
		LogUltility.log(test, logger, "Is new record found after cancelling the revert: " + equalResult);
		assertTrue(equalResult);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 *  DIC_TC--130:Check Cancel button in "Delete" pop up
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void  DIC_TC_130_Check_Cancel_button_in_Delete_pop_up() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_130", "Check Cancel button in Delete pop up");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		//Select Value from the Volume POS dropdown 
		String randomPOS = "pronoun";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, randomPOS);
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomPOS);
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

		// Click the Delete button 
		CurrentPage.As(CompoundsTab.class).bt_delete.click();
		LogUltility.log(test, logger, "Click the Delete button");
		
		//Press No on the Deleting record message 
		CurrentPage.As(CompoundsTab.class).noDelete.click();
		LogUltility.log(test, logger, "Click No button on the Delete message to cancel Deleting the selectedcompound");
		
//		//Get the KeyForm field value
//
//		String keyForm= CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
//		LogUltility.log(test, logger, "Value into the KeyForm field: " + keyForm);
//		
//		//Verify that the KeyForm field is NOT empty since the operation cancelled
//		boolean isNotEmpty =! keyForm.isEmpty();
//		LogUltility.log(test, logger, "The fuction New was cancelled and KeyForm field is not empty that`s mean the value should be True:" + isNotEmpty);
//		assertTrue(isNotEmpty);
		
		//Check no changes done on the Compounds file
		
		List<String> recordsFromFile =  CurrentPage.As(CompoundsTab.class).getRecordsFromcompounds();
		LogUltility.log(test, logger, "clean record: " + CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomrecord));
		boolean recordexist = recordsFromFile.contains(CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomrecord));
		LogUltility.log(test, logger, "Is the record exist in the file after canceling the Deleting? must be True:" + recordexist);
		assertTrue(recordexist);
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	/**
	 * DIC_TC--131:Check Cancel button in "Duplicate" pop up
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_131_Check_Cancel_button_in_Duplicate_popup() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_131", "Check Cancel button in Duplicate popup");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "preposition";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Click the duplicate button
		CurrentPage.As(CompoundsTab.class).bt_duplicate.click();
		LogUltility.log(test, logger, "Click the duplicate button");
		
		// Click NO on the confirmation message
		CurrentPage.As(CompoundsTab.class).btnNoDuplicate.click();
		LogUltility.log(test, logger, "Click No on the Duplicate confirmation message");
		
//		// Refresh the list and check that duplicated record is found
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
//		LogUltility.log(test, logger, "Refresh voluem POS dropdown: " + volumePOS);
//		
//		// Increment the duplicated record and then search if it does exist in the records list
//		String[] help = randomrecord.split("~");
//		String duplicatedRecord =  help[0] + "~" + (Integer.parseInt(help[1])+1);
//		LogUltility.log(test, logger, "DuplicatedRecord: " + duplicatedRecord);
//		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
//		boolean doesContain = recordsList.contains(duplicatedRecord);
//		LogUltility.log(test, logger, "Does record list contain the duplicated record: " + doesContain);
//		assertFalse(doesContain);
		
//		// Click the revert button
//		CurrentPage.As(CompoundsTab.class).bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//		
//		// Click Yes on the confirmation message
//		CurrentPage.As(CompoundsTab.class).btnOk.click();
//		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
		// Refresh the list and check that duplicated record is removed
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Refresh voluem POS dropdown: " + volumePOS);
		
		// Get the records list and check that the duplicate record does not exist
		String[] help = randomrecord.split("~");
		String duplicatedRecord =  help[0] + "~" + (Integer.parseInt(help[1])+1);
		LogUltility.log(test, logger, "DuplicatedRecord: " + duplicatedRecord);
		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		LogUltility.log(test, logger, "check that the duplicate record does not exist");
		boolean doesContain = recordsList.contains(duplicatedRecord);
		doesContain = recordsList.contains(duplicatedRecord);
		LogUltility.log(test, logger, "Does record list contain the duplicated record: " + doesContain);
		assertFalse(doesContain);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--132: Check Cancel button in "Merge" pop up
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_132_Check_Cancel_button_in_Merge_popup() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_132", "Check Cancel button in Merge pop up");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Click on the merge button
		CurrentPage.As(CompoundsTab.class).btn_merge_files.click();
		LogUltility.log(test, logger, "Click the merge button");
		
		
		//Check that Merge window is displayed 
		boolean popupDisplayed = CurrentPage.As(CompoundsTab.class).generalPopUp.isDisplayed();
		LogUltility.log(test, logger, "Check that the Merge popup is Displayed, value should be True: " + popupDisplayed);
		assertTrue(popupDisplayed);
		
		// Click the cancel button in the merge popup
		CurrentPage.As(CompoundsTab.class).btnpopupCancelWindow.click();
		LogUltility.log(test, logger, "Click cancel in the merge window");
		
		//Check if the Merge popup still opened or closed
	
	try {
		popupDisplayed = CurrentPage.As(CompoundsTab.class).generalPopUp.isDisplayed();
		LogUltility.log(test, logger, "Merge pop up window still open");
		assertFalse(true);
	} catch (Exception e) {
		LogUltility.log(test, logger, "Merge pop up window is closed");
	}
	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	

	/**
	 * DIC_TC--134: Check Cancel button in "Mass Attribute" pop up
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_134_Check_Cancel_button_in_Mass_Attribute_popup() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_134", "Check Cancel button in Mass Attribute pop up");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		//Click the mass attribute button
		CurrentPage.As(CompoundsTab.class).bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the mass attribute button");
		
		//Check that Mass attribute window is displayed 
		boolean popupDisplayed = CurrentPage.As(CompoundsTab.class).generalPopUp.isDisplayed();
		LogUltility.log(test, logger, "Check that the Mass attribute popup Is displayed, value should be True: " + popupDisplayed);
		assertTrue(popupDisplayed);
		
		
		// Click the cancel button in the mass attribute popup
		CurrentPage.As(CompoundsTab.class).btnCancelMass.click();
		LogUltility.log(test, logger, "Click cancel in the Mass attribute window");

		//Check if the Mass Attribute popup still opened or closed
		
		try {
			popupDisplayed = CurrentPage.As(CompoundsTab.class).generalPopUp.isDisplayed();
			LogUltility.log(test, logger, "Mass attriibute pop up window still open");
			assertFalse(true);
		} catch (Exception e) {
			LogUltility.log(test, logger, "Mass attriibute pop up window is closed");
		}
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--147:Verify user can "Mass attribute" successfully
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_147_Verify_user_can_Mass_attribute_successfully() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_147", "Verify user can Mass Attribute successfully");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "Form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "Form is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, " Contains is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomForm = "פעם";
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
		//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomForms);
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomForm);
		LogUltility.log(test, logger, "chosen search value: " + randomForm);
		
		// Click the Retrieve button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
					
		//Click the mass attribute button
		CurrentPage.As(CompoundsTab.class).bt_mass_attribute.click();
		LogUltility.log(test, logger, "Click the mass attribute button");
		
		//Check that Mass attribute window is displayed 
		boolean popupDisplayed = CurrentPage.As(CompoundsTab.class).generalPopUp.isDisplayed();
		LogUltility.log(test, logger, "Check that the Mass attribute popup Is displayed, value should be True: " + popupDisplayed);
		assertTrue(popupDisplayed);
		
		// choose the origin  form the semantic Groups dropdowns
		String semanticGroups = "origin";
		//String Value = "Hebrew";
		
		String DDvalue = CurrentPage.As(CompoundsTab.class).chooseValuesInMassWindow(semanticGroups, "random");
		
		//
//		String semanticGroups = "origin";
//		CurrentPage.As(CompoundsTab.class).dd_open.click();
//		CurrentPage.As(CompoundsTab.class).dd_open.click();
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).dd_SemanticGroups, semanticGroups);
		LogUltility.log(test, logger, "Choose value in first dropdown: " + semanticGroups + " and in the second dropdown :" +DDvalue );
		
		
//		//Press Yes button on mass Conversion popup  bt_utility_accept bt_Change
//		CurrentPage.As(CompoundsTab.class).bt_Change.click();
//		Thread.sleep(30000);
//		LogUltility.log(test, logger, "Click Yes button on the mass Conversion popup");
//		
//		// choose the Hebrew form the Value dropdowns
	//String Value = "Hebrew";
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).dd_ToBe, Value);
//		LogUltility.log(test, logger, "Choose value in Value dropdown: " + Value);
		
//		// Click the Change button in the mass attribute popup
//		CurrentPage.As(CompoundsTab.class).bt_Change.click();
//		LogUltility.log(test, logger, "Click Change in the Mass attribute window");
		
		// select random record and verify that the origin changed to selected value
		
	    Thread.sleep(60000);
	    
	    // click the save button
	    CurrentPage.As(CompoundsTab.class).btnSave.click();
	    LogUltility.log(test, logger, "Save button clicked");
	    
	    //Click the OK button on the saving popup
	    CurrentPage.As(CompoundsTab.class).btnOKSave.click();
	    LogUltility.log(test, logger, "OK button clicked on the saving popup");
	    
	    try {
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
			LogUltility.log(test, logger, "Click compounds to refresh");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Search for the same list
		// Choose "Form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "Form is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, " Contains is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomForm2 = "פעם";
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
		//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomForms);
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomForm2);
		LogUltility.log(test, logger, "chosen search value: " + randomForm2);
		
		// Click the Retrieve button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		 
		// Get the chose value from the origin dropdown
		String Value = DDvalue;
		List<String> originList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpOrigin);
		boolean found = Value.equals(originList.get(0));
		LogUltility.log(test, logger, "Is the searched word:"+Value + " are the same as the selected value from the dropdown: "+ originList.get(0)+ " if yes , the value should be True :  " + found);
		assertTrue(found);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	/**
	 * DIC_TC--148:Verify user can duplicate records successfully
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_148_Verify_user_can_duplicate_records_successfully() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_148", "Verify user can duplicate records successfully");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "preposition";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Click the duplicate button
		CurrentPage.As(CompoundsTab.class).bt_duplicate.click();
		LogUltility.log(test, logger, "Click the duplicate button");
		
		// Click Yes on the confirmation message
		CurrentPage.As(CompoundsTab.class).btnYesDuplicate.click();
		LogUltility.log(test, logger, "Click Yes on the Duplicate confirmation message");
		
//		// Refresh the list and check that duplicated record is found
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
//		LogUltility.log(test, logger, "Refresh voluem POS dropdown: " + volumePOS);
//		
//		// Increment the duplicated record and then search if it does exist in the records list
//		String[] help = randomrecord.split("~");
//		String duplicatedRecord =  help[0] + "~" + (Integer.parseInt(help[1])+1);
//		LogUltility.log(test, logger, "DuplicatedRecord: " + duplicatedRecord);
//		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
//		boolean doesContain = recordsList.contains(duplicatedRecord);
//		LogUltility.log(test, logger, "Does record list contain the duplicated record: " + doesContain);
//		assertFalse(doesContain);
		
//		// Click the revert button
//		CurrentPage.As(CompoundsTab.class).bt_revert.click();
//		LogUltility.log(test, logger, "Click the revert button");
//		
//		// Click Yes on the confirmation message
//		CurrentPage.As(CompoundsTab.class).btnOk.click();
//		LogUltility.log(test, logger, "Click Yes on the confirmation message");
		
		// Refresh the list and check that duplicated record is removed
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Refresh voluem POS dropdown: " + volumePOS);
		
		// Get the records list and check that the duplicate record does exist
		String[] help = randomrecord.split("~");
		String duplicatedRecord =  help[0] + "~" + (Integer.parseInt(help[1])+1);
		LogUltility.log(test, logger, "DuplicatedRecord: " + duplicatedRecord);
		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		LogUltility.log(test, logger, "check that the duplicate record does exist in the retrived records");
		boolean doesContain = recordsList.contains(duplicatedRecord);
		//doesContain = recordsList.contains(duplicatedRecord);
		LogUltility.log(test, logger, "Does record list contain the duplicated record? value should be True: " + doesContain);
		assertTrue(doesContain);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--218:Verify DIC create new version when user creating exist record
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_218_Verify_DIC_create_new_version_when_user_creating_exist_record() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_218", "Verify DIC create new version when user creating exist record");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		//New record we plan to add
		String randomText=null;
		if (Setting.Language.equals("EN"))
			randomText = "a_ok";
		else if (Setting.Language.equals("HE"))
			randomText = "קצר_רוח";
		
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
//		List<String> allFoundRecords = new ArrayList<>();
		int version=0;
		for (String record : recordsList) {
			boolean foundRecord= CurrentPage.As(CompoundsTab.class).getOnlyRecordName(record).equals(randomText.replace("_", " "));
			if(foundRecord){
				String[] splittedRecord = record.split("~");
				if(Integer.parseInt(splittedRecord[1]) > version)
					version= Integer.parseInt(splittedRecord[1]);
			}
			
		}
		
		LogUltility.log(test, logger, "the version value is: " + version);
		
		// Click the new button
		CurrentPage.As(CompoundsTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(CompoundsTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type an exist record name in the key form field
		//String randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomString(5);
		//String randomText = "a_few";
		CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test, logger, "New record:" + randomText);
		
		// Get random POS
		List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick the same POS for the exist record
		//Random randomizer = new Random();
		//String randompos = posList.get(randomizer.nextInt(posList.size()));
		String randompos = ("adjective");
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Click Yes on the popup 
		CurrentPage.As(CompoundsTab.class).btnYesDuplicate.click();
		
//		// Search for the new added record
//		// Choose "form" from the first search dropdown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
//		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//		
//		// Choose "equal" from the condition dropdown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
//		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
//		
//		//Type in the search field
//		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys("a_few");
//		LogUltility.log(test, logger, "word is typed in the search field:  a_few");
//		
//		// Click the Retreive button
//		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
//		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose a value from Volume[POS] dropdown
		//String volumePOS = "adjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
		
		// Retrieve records
		List<String> newRecordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + newRecordsList);
		
		boolean found=false;
		version+=1;
		for (String record : newRecordsList) {
			boolean foundRecord= CurrentPage.As(CompoundsTab.class).getOnlyRecordName(record).equals(randomText.replace("_", " "));
			if(foundRecord){
				String[] splittedRecord = record.split("~");
				if(Integer.parseInt(splittedRecord[1]) == version){
					found = true;
					break;
				}
				else found = false;
					
			}
			
		}
		LogUltility.log(test, logger, "the version value is: " + version);
		LogUltility.log(test, logger, "Does new record list contain the duplicated record? should be True: " + found);
		assertTrue(found);
		LogUltility.log(test, logger, "Test Case PASSED");

	}
	
	/**
	 * DIC_TC--220:Verify user can cancel creating an exist record
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_220_Verify_user_can_cancel_creating_an_exist_record() throws InterruptedException, IOException
	{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_220", "Verify user can cancel creating an exist record");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
				
		//Type in the search field
		String searchWord = null;
		if (Setting.Language.equals("EN"))
			searchWord = "a_few";
		else if (Setting.Language.equals("HE"))
			searchWord = "אי_זוגי";
		
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
		assertTrue(containResult);
		
		// Save the number of the returned record
		int recordNumber = recordsList.size();
		LogUltility.log(test, logger, "Number of retreived Records is: " + recordNumber);
				
					
		//New record we plan to add
		String randomText = searchWord;
		
//		// Choose a value from Volume[POS] dropdown
//		String volumePOS = "adjective";
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
//		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
//
//		// Retrieve records
//		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
//		
////		List<String> allFoundRecords = new ArrayList<>();
//		int version=0;
//		for (String record : recordsList) {
//			boolean foundRecord= CurrentPage.As(CompoundsTab.class).getOnlyRecordName(record).equals(randomText.replace("_", " "));
//			if(foundRecord){
//				String[] splittedRecord = record.split("~");
//				if(Integer.parseInt(splittedRecord[1]) > version)
//					version= Integer.parseInt(splittedRecord[1]);
//			}
//			
//		}
//		
//		LogUltility.log(test, logger, "the version value is: " + version);
		
		// Click the new button
		CurrentPage.As(CompoundsTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(CompoundsTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type an exist record name in the key form field
		//String randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomString(5);
		//String randomText = "a_few";
		CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test, logger, "New record:" + randomText);
		
		// Get random POS
		List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick the same POS for the exist record
		//Random randomizer = new Random();
		//String randompos = posList.get(randomizer.nextInt(posList.size()));
		String randompos = ("adjective");
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		// Click Cancel on the popup 
		CurrentPage.As(CompoundsTab.class).btnCancelSave.click();
		LogUltility.log(test, logger, "Click Cancel button in the popup");
		
//		// Verify that the create function was cancelled and the previous value appear on the Keyform field
//		String keyFormValue = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
//		LogUltility.log(test, logger, "since the Creating was cancelled, Value in the Keyform is the previous one : " + keyFormValue);
//		LogUltility.log(test, logger, "the entered keyform is : " + randomText);
//		//Verify that the entered keyform is not the same one after cancelling the creation of new record
//		boolean notSameKF = !keyFormValue.equals(randomText);
//		LogUltility.log(test, logger, "since the Creating was cancelled, Value in the Keyform is the previous one : " + keyFormValue +" not the same as the entered one: "+ randomText + " and the value should be True: " + notSameKF);
//		assertTrue(notSameKF);
		
		//Search to verify that no new record addred
		// Choose "form" from the first search dropdown
		LogUltility.log(test, logger, "Search again for the same record to verify that no new record added");
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
				
		//Type in the search field
		String searchWord1 = searchWord;
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord1);
		LogUltility.log(test, logger, "word is typed in the search field:"+ searchWord1);
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList2 = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList2);
		
		// Check that retrieved records do contain the searched record
		if (recordsList2.isEmpty()) assertTrue(false);
		
		boolean containResult2 = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord1)) continue;
		       	else {
		       		containResult2 = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult2);
		assertTrue(containResult2);
		
		// Save the number of the returned record
		int recordNumber2 = recordsList2.size();
		LogUltility.log(test, logger, "Number of retreived Records is: " + recordNumber2);
		
		// check if the same number of the retrieved records
		boolean smaeNumber = recordNumber2 == recordNumber;
		assertTrue(smaeNumber);
		LogUltility.log(test, logger, "Check that the : "+ recordNumber2 + " is equal to the : " + recordNumber +" and the value should be True" +smaeNumber);
		

		LogUltility.log(test, logger, "Test Case PASSED");
		
		
//		// Search for the new added record
//		// Choose "form" from the first search dropdown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
//		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//		
//		// Choose "equal" from the condition dropdown
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
//		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
//		
//		//Type in the search field
//		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys("a_few");
//		LogUltility.log(test, logger, "word is typed in the search field:  a_few");
//		
//		// Click the Retreive button
//		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
//		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Choose a value from Volume[POS] dropdown
		//String volumePOS = "adjective";
//		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
//		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
//		
//		// Retrieve records
//		List<String> newRecordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + newRecordsList);
//		
//		boolean found=false;
//		version+=1;
//		for (String record : newRecordsList) {
//			boolean foundRecord= CurrentPage.As(CompoundsTab.class).getOnlyRecordName(record).equals(randomText.replace("_", " "));
//			if(foundRecord){
//				String[] splittedRecord = record.split("~");
//				if(Integer.parseInt(splittedRecord[1]) == version){
//					found = true;
//					break;
//				}
//				else found = false;
//					
//			}
//			
//		}
//		LogUltility.log(test, logger, "the version value is: " + version);
//		LogUltility.log(test, logger, "Does new record list contain the duplicated record? should be True: " + found);
//		assertTrue(found);

	}
	
	

/**
 * DIC_TC--224:Verify user can edit semantic relations, lexical relations - part 1 semantic relations
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 * @throws AWTException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_224_Verify_user_can_edit_semantic_relations_lexical_relations_part1_semantic_relations() throws InterruptedException, FileNotFoundException, AWTException
{
	test = extent.createTest("compounds Search Test - DIC_TC_224", "Verify user can edit semantic relations, lexical relations - part 1 semantic relations");
	
	if (Setting.Language.equals("HE")) {
		LogUltility.log(test, logger, "This Test was skipped because Hebrew records doesn`t have semantic relations ");
		return;
	}
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).CompoundsTab.click();

	// get list of records that have Semantic Relations 
	Map<String, String> recordsWithSR = CurrentPage.As(CompoundsTab.class).availableRelationsForCompounds("semantic", 20);
	LogUltility.log(test, logger, "Semantic relation from file: " +recordsWithSR );
	
	// Choose a random record that have semantic relations as compounds
	Random randomizer = new Random();
	Object[] values = recordsWithSR.keySet().toArray();
	
	String randomRecord = (String) values[randomizer.nextInt(values.length)];
	String relationValue = recordsWithSR.get(randomRecord);
	LogUltility.log(test, logger, "Random record: " + randomRecord + ", relation value: " + relationValue);
	
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "Equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "Equal is chosen in the condition dropdown");
	
	//Type in the search field
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
	LogUltility.log(test, logger, "word is typed in the search field:" +CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
	
	// Click the Retrieve button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");

	if (Setting.Language.equals("HE")) {
		LogUltility.log(test, logger, "This Test was skipped because Hebrew records doesn`t have Spellings to attach ");
		return;
	}
	
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
	
	// Get the Semantic Relation list value
	List<String> relationB4Change = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticRelations);
	LogUltility.log(test, logger, "Relation list BEFORE modifying: " + relationB4Change);
	
	// Modify random relation
	CurrentPage.As(CompoundsTab.class).editSemanticRelation(test,logger);
		
	// Get the Semantic Relation list value
	List<String> relationAfterChange = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticRelations);
	LogUltility.log(test, logger, "Relation list AFTER modifying: " + relationAfterChange);
	
	// Verify that the lists are different
	Collections.sort(relationAfterChange);
	Collections.sort(relationB4Change);
	boolean relationChanged = !relationB4Change.equals(relationAfterChange);
	LogUltility.log(test, logger, "The value has been modified: "+relationChanged);	
	assertTrue(relationChanged);
	
	// Click the revert button
	CurrentPage.As(CompoundsTab.class).bt_revert.click();
	LogUltility.log(test, logger, "Click the revert button");
	
	// Click Yes on the confirmation message
	CurrentPage.As(CompoundsTab.class).btnOk.click();
	LogUltility.log(test, logger, "Click Yes on the confirmation message");
	
	LogUltility.log(test, logger, "Test Case PASSED");	
}

///**
// * DIC_TC-226:Verify user can attach records from Compounds to Dictionary
// * @throws InterruptedException 
// * @throws AWTException 
// */
//@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
//public void DIC_TC_226_Verify_user_can_attach_records_from_Compounds_to_Dictionary() throws InterruptedException, AWTException
//{
//	//Thread.sleep(5000);
//	test = extent.createTest("Compounds Tests - DIC_TC_226", "Verify user can attach records from Compounds to Dictionary");
//	
//	// Focus on Compounds tab
//	CurrentPage = GetInstance(CompoundsTab.class);
//	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
//	LogUltility.log(test, logger, "Compounds tab opened and Records retreived");
//
//	// Retrieve records
//	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
//	
//	// Choose random record from the middle of the displayed list
//	recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
//	LogUltility.log(test, logger, "Choose random record from the middle of the displayed list");
//
//	// Pick a random record
//	Random randomizer = new Random();
//	String randomRecord = recordsList.get(randomizer.nextInt(recordsList.size()));
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecord);
//	LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
//	
//	// Click the attach button
//	CurrentPage.As(CompoundsTab.class).bt_attach.click();
//	LogUltility.log(test, logger, "Click the attach button");
//	
//	// Click OK on the confirmation message
//	CurrentPage.As(CompoundsTab.class).btnOKPopupWindow.click();
//	LogUltility.log(test, logger, "Click OK on the confirmation window");
//	
//	// Focus on dictionary tab
//	CurrentPage = GetInstance(DictionaryTab.class);
//	CurrentPage.As(DictionaryTab.class).dictionaryTab.click();
//	
//	// Choose another record to attach to
//	String randomRecordSecond;
//	do {
//		randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
//	} while (randomRecordSecond.equals(randomRecord));
//	
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
//	LogUltility.log(test, logger, "Choose from record list: " + randomRecordSecond);
//	
//	// Click the attach button again
//	CurrentPage.As(DictionaryTab.class).bt_attach.click();
//	LogUltility.log(test, logger, "Click the attach button again");
//	
//	// Choose a key value from the dropdown
//	String keyValue = "standard 1"; 
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpPopupValue, keyValue);
//	
//	// Click OK on the confirmation message
////Need to verify records with Spellings		CurrentPage.As(CompoundsTab.class).bt_utility_accept.click();
//	LogUltility.log(test, logger, "Click attach button");
////	 The above click does not work, trying another physical way
////	String attachPlace = CurrentPage.As(CompoundsTab.class).bt_utility_accept.getAttribute("ClickablePoint"); 
////	String[] XY = attachPlace.split(",");
//
////      Robot robot = new Robot();
////    robot.mouseMove(Integer.parseInt(XY[0]),Integer.parseInt(XY[1]));
////    robot.mousePress(InputEvent.BUTTON1_MASK);
////    robot.mouseRelease(InputEvent.BUTTON1_MASK);
//
//	// Check that we have attach and attached to record names in the spellings list
//	List<String> spellingList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lb_dictionary_spellings);
//	//remove the values and keep only the records
//	for (String value : spellingList) {
//		String[] split = value.split("~");
//		spellingList.remove(value);
//		spellingList.add(split[1]);
//	}
//	LogUltility.log(test, logger, "Values in spelling list: " + spellingList);
//	
//	String firstRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecord);
//	String secondRecord = CurrentPage.As(DictionaryTab.class).getOnlyRecordName(randomRecordSecond);
//	
//	boolean bothExist = spellingList.contains(firstRecord) && spellingList.contains(secondRecord);
//	LogUltility.log(test, logger, "Are first and second records exist in the spelling list box: " + bothExist);
//	assertTrue(bothExist);
//	
//	LogUltility.log(test, logger, "Test Case PASSED");
//}

/**
 * DIC_TC--235:Check the semantic relations between the records
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_235_Check_the_semantic_relations_between_the_records() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("compounds Search Test - DIC_TC_235", "Check the semantic relations between the records");
	
	if (Setting.Language.equals("HE")) {
		LogUltility.log(test, logger, "This Test was skipped because Hebrew records doesn`t have semantic relations ");
		return;
	}
	
	// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
		
		// get list of records that have semantic relations
//		List<String> recordsWithLR = CurrentPage.As(CompoundsTab.class).availableRelationsForCompounds("lexical", 30);
		Map<String, String> recordsWithLR = CurrentPage.As(CompoundsTab.class).availableRelationsForCompounds("semantic", 30);
		LogUltility.log(test, logger, "List of records that have lexical relations:" +recordsWithLR);
		 
		// Choose a random record that have semantic relations as compounds
		Random randomizer = new Random();
//		String randomRecord = recordsWithLR.get(randomizer.nextInt(recordsWithLR.size()));
		Object[] values = recordsWithLR.keySet().toArray();
		
		String randomRecord = (String) values[randomizer.nextInt(values.length)];
		String relationValue = recordsWithLR.get(randomRecord);
		LogUltility.log(test, logger, "Random record: " + randomRecord + ", relation value: " + relationValue);

		//Clean records
//		String cleanRecord = CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord); 
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "form is chosen in the first search dropdown");
		
		// Choose "Equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "Equal is chosen in the condition dropdown");
		
		//Type in the search field
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
		LogUltility.log(test, logger, "word is typed in the search field:" +CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
		
		// Click the Retrieve button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
//		// Choose a random record from the record list
//		String randomResult = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
		
		// Get the chosen Semantic Relation list value
		List<String> chosenLexicalRelationList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticRelations);
		LogUltility.log(test, logger, "chosen lexical relation list: " + chosenLexicalRelationList);
//		String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
		//getChosenValueInDropdown
		
		String randomLexicalRelation1 = relationValue;
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticRelations, randomLexicalRelation1);
		LogUltility.log(test, logger, "Choose from semantic realation list: " + randomLexicalRelation1);
		
		// Get the chosen Semantic Relation list value
		List<String> chosenLexicalRelationList2 = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticRelations);
		LogUltility.log(test, logger, "chosen Lexical  relation list: " + chosenLexicalRelationList2);
		
		//Check that the semantic relation list does contain the First searched record
		boolean ExistRecord = false;
		for (String value : chosenLexicalRelationList2) {
			if (value.split("¬")[1].equals(randomRecord))
				ExistRecord = true;
		}
		
//		boolean ExistRecord= chosenLexicalRelationList2.contains(randomRecord);
		LogUltility.log(test, logger, "Check that  semantic relation list: " + chosenLexicalRelationList2 + " Does include the record:" +randomRecord);
		assertTrue(ExistRecord);
		LogUltility.log(test, logger, "Test Case PASSED");
}


/**
 * DIC_TC--236:Check the lexical relations between the records
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_236_Check_the_lexical_relations_between_the_records() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("compounds Search Test - DIC_TC_235", "Check the lexica relations between the records");
	
	if (Setting.Language.equals("HE")) {
		LogUltility.log(test, logger, "This Test was skipped because Hebrew records doesn`t have lexical relations ");
		return;
	}
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
	
	// get list of records that have lexical relations
//	List<String> recordsWithLR = CurrentPage.As(CompoundsTab.class).availableRelationsForCompounds("lexical", 30);
	Map<String, String> recordsWithLR = CurrentPage.As(CompoundsTab.class).availableRelationsForCompounds("lexical", 30);
	LogUltility.log(test, logger, "List of records that have lexical relations:" +recordsWithLR);
	 
	// Choose a random record that have semantic relations as compounds
	Random randomizer = new Random();
//	String randomRecord = recordsWithLR.get(randomizer.nextInt(recordsWithLR.size()));
	Object[] values = recordsWithLR.keySet().toArray();
	
	String randomRecord = (String) values[randomizer.nextInt(values.length)];
	String relationValue = recordsWithLR.get(randomRecord);
	LogUltility.log(test, logger, "Random record: " + randomRecord + ", relation value: " + relationValue);

	//Clean records
//	String cleanRecord = CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord); 
	
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "Equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "Equal is chosen in the condition dropdown");
	
	//Type in the search field
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
	LogUltility.log(test, logger, "word is typed in the search field:" +CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
	
	// Click the Retrieve button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	//Thread.sleep(2000);
	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	
	// Check that retrieved records do contain the searched record
	if (recordsList.isEmpty()) assertTrue(false);
	
//	// Choose a random record from the record list
//	String randomResult = recordsList.get(randomizer.nextInt(recordsList.size()));
	
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
	
	// Get the chosen Lexical Relation list value
	List<String> chosenLexicalRelationList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_Lexical_relations);
	LogUltility.log(test, logger, "chosen lexical relation list: " + chosenLexicalRelationList);
//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
	//getChosenValueInDropdown
	
	String randomLexicalRelation1 = relationValue;
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_Lexical_relations, randomLexicalRelation1);
	LogUltility.log(test, logger, "Choose from lexical relation list: " + randomLexicalRelation1);
	
	// Get the chosen Semantic Relation list value
	List<String> chosenLexicalRelationList2 = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_Lexical_relations);
	LogUltility.log(test, logger, "chosen Lexical  relation list: " + chosenLexicalRelationList2);
	
	//Check that the semantic relation list does contain the First searched record
	boolean ExistRecord = false;
	for (String value : chosenLexicalRelationList2) {
		if (value.split("�")[1].equals(randomRecord))
			ExistRecord = true;
	}
//	boolean ExistRecord= chosenLexicalRelationList2.contains(randomRecord);
	LogUltility.log(test, logger, "Check that  semantic relation list: " + chosenLexicalRelationList2 + " Does does include the record:" +randomRecord);
	assertTrue(ExistRecord);
	LogUltility.log(test, logger, "Test Case PASSED");	
}
	

/**
 * DIC_TC--239:Verify merge button works successfully
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_239_Verify_merge_button_works_successfully() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_239", "Verify merge button works successfully");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Create a file for the Compounds to merge if it does not exist
	File dest = new File(Setting.AppPath + Setting.Language+ "\\Data\\" + "Compounds for merge.txt");
	File source = new File(".\\Compounds for merge.txt");
	if(!dest.exists() || dest.isDirectory()) { 
		FileUtils.copyFile(source, dest);
	}
	
	// Click on the merge button
	CurrentPage.As(CompoundsTab.class).btn_merge_files.click();
	LogUltility.log(test, logger, "Click the merge button");
	
	
	//Check that Merge window is displayed 
	boolean popupDisplayed = CurrentPage.As(CompoundsTab.class).generalPopUp.isDisplayed();
	LogUltility.log(test, logger, "Check that the Merge popup is Displayed, value should be True: " + popupDisplayed);
	assertTrue(popupDisplayed);
	
	// Click the Dropdown in the merge popup
	CurrentPage.As(CompoundsTab.class).dd_Merge.click();
	LogUltility.log(test, logger, "Click to open the dropdown in the merge window");
	
	
	//Select the merge file from the dropdown 
	
	// Retrieve files names
	List<String> filesList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).dd_Merge);
	LogUltility.log(test, logger, "Files names retreived: " + filesList);
			
	// Choose a file in the merge dropdown
	LogUltility.log(test, logger, "filesList.get(0): " + filesList.get(0));
	CurrentPage.As(CompoundsTab.class).dd_Merge.click();
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).dd_Merge, filesList.get(0));
	LogUltility.log(test, logger, "Choose the file from dropdown:"+ filesList.get(0));
	
	//CurrentPage.As(CompoundsTab.class).dd_Merge.click();
	
	// Click the Merge button on the popup
	CurrentPage.As(CompoundsTab.class).btn_popup_Merge.click();
	LogUltility.log(test, logger, "Click the merge button in the poup");
	
	//Verify the success message appear TXTMergedSuccess and press the OK button
	Thread.sleep(5000);
	boolean isWindowDisplayed= false;
	if (Setting.Language.equals("EN"))
		isWindowDisplayed= CurrentPage.As(CompoundsTab.class).TXTMergedSuccess.isDisplayed();
	else if (Setting.Language.equals("HE"))
		isWindowDisplayed= CurrentPage.As(CompoundsTab.class).HETXTMergedSuccess.isDisplayed();
    assertTrue(isWindowDisplayed);
	LogUltility.log(test, logger, "Is the success message appear: " + isWindowDisplayed);
	Thread.sleep(2000);
	//if (isWindowDisplayed) 
	//CurrentPage.As(CompoundsTab.class).TXTMergedSuccess.click();
	CurrentPage.As(CompoundsTab.class).btnOk.click(); 
	LogUltility.log(test, logger, "Click OK in the success message popup");
	
	// Press yes for all the records
	for(int i=0;i<4;i++)
		CurrentPage.As(CompoundsTab.class).mergeWarningYes.click();
	
    // click the cancel button 
	CurrentPage.As(CompoundsTab.class).mergeWarningCancel.click();
	
	Thread.sleep(3000);
	
	//Verify the success message appear TXTMergedSuccess and press the OK button
	
	boolean isMergeEnded = CurrentPage.As(CompoundsTab.class).mergeEnded.isDisplayed();
	LogUltility.log(test, logger, "Is the success message appear: " + isMergeEnded);
	Thread.sleep(2000);
	CurrentPage.As(CompoundsTab.class).btnOk.click();
	
	LogUltility.log(test, logger, "Merge ended successfully poup closed");
	
	
   LogUltility.log(test, logger, "Test Case PASSED");
}


	
/**
 * DIC_TC--245:Check the new added record info are saved correctly in the database file
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_245_Check_the_new_added_record_info_are_saved_correctly_in_the_database_file() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_245", "Check the new added record info are saved correctly in the database file");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
		
	// Type a record name in the key form field
	String randomText="";
	if (Setting.Language.equals("EN"))
		randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomString(5);
	else if (Setting.Language.equals("HE"))
		randomText= CurrentPage.As(CompoundsTab.class).RandomHEString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomHEString(5);

	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	 
	// Pick a random pos
	Random randomizer = new Random();
	String randompos = "unknown";
	while(randompos == "unknown"){
		randompos = posList.get(randomizer.nextInt(posList.size()));
	}
	   
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// Get random Compound Type
	List<String> compoundTypeList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpType);
	LogUltility.log(test, logger, "Compound Type list: " + compoundTypeList);
	
	// Pick a random Compound Type
	Random randomizer6 = new Random();
	String randomCompoundType = compoundTypeList.get(randomizer6.nextInt(compoundTypeList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpType, randomCompoundType);
	LogUltility.log(test, logger, "Choose from Compound Type list: " + randomCompoundType);
	
	// Get random Origin
	List<String> originList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpOrigin);
	LogUltility.log(test, logger, "Origin list: " + originList);
	
	// Pick a random Origin
	Random randomizer1 = new Random();
	String randomOrigin = originList.get(randomizer1.nextInt(originList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpOrigin, randomOrigin);
	LogUltility.log(test, logger, "Choose from Origin list: " + randomOrigin);
	
	// Get random Style
	List<String> styleList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpStyle);
	LogUltility.log(test, logger, "Style list: " + styleList);
	
	// Pick a random Style
	Random randomizer2 = new Random();
	String randomStyle = styleList.get(randomizer2.nextInt(styleList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpStyle, randomStyle);
	LogUltility.log(test, logger, "Choose from Style list: " + randomStyle);
	
	// Get random Acronym
	List<String> acronymList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpAcronym);
	LogUltility.log(test, logger, "Acronym list: " + acronymList);
	
	// Pick a random Acronym
	Random randomizer3 = new Random();
	String randomAcronym = acronymList.get(randomizer3.nextInt(acronymList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpAcronym, randomAcronym);
	LogUltility.log(test, logger, "Choose from Acronym list: " + randomAcronym);
	
	// Get random Register
	List<String> registerList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpRegister);
	LogUltility.log(test, logger, "Register list: " + registerList);
	
	// Pick a random Register
	Random randomizer4 = new Random();
	String randomRegister = registerList.get(randomizer4.nextInt(registerList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpRegister, randomRegister);
	LogUltility.log(test, logger, "Choose from Register list: " + randomRegister);
	
	
	// Get random Frequency
	List<String> frequencyList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpFrequency);
	LogUltility.log(test, logger, "Frequency list: " + frequencyList);
	
	// Pick a random Frequency
	Random randomizer5 = new Random();
	String randomFrequency = frequencyList.get(randomizer5.nextInt(frequencyList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpFrequency, randomFrequency);
	LogUltility.log(test, logger, "Choose from Frequency list: " + randomFrequency);
	

	// click the save button
	CurrentPage.As(CompoundsTab.class).btnSave.click();
	LogUltility.log(test, logger, "Click Save button ");
	
	
	// click the OK button in the saving popup
	CurrentPage.As(CompoundsTab.class).btnOKSave.click();
	LogUltility.log(test, logger, "Click ok on the Saving popup ");
	
	
	// get the new record info from the compounds.txt file
	Thread.sleep(5000);
	String recordInfo = CurrentPage.As(CompoundsTab.class).getRecordsInfoFromcompounds(randomText);
	LogUltility.log(test, logger, "New record info returned from the Compounds.txt file: " + recordInfo);
	
	//Verify that the line contain all the selected info
	
	 boolean  name = recordInfo.contains(randomText);
	 assertTrue(name);
	 LogUltility.log(test, logger, "name exist in the returned line from the compound.txt. should be True : " + name);
	 // verify the selected POS
	 String name1 = CurrentPage.As(CompoundsTab.class).POSConverter(randompos);
	 LogUltility.log(test, logger, "The selected POS after converting is : " + name1);
	 String fullName = Setting.Language +"{"+name1+"}"+randomText+"~0";
	 LogUltility.log(test, logger, "The full name after converting is : " + fullName);
	 boolean  POS = recordInfo.contains(fullName.trim());
	 assertTrue(POS);
	 LogUltility.log(test, logger, "POS exist in the returned line from the compound.txt.and it`s :" + fullName +"and the value should be True : " + POS);
	 boolean  Origin = recordInfo.contains(randomOrigin);
	 assertTrue(Origin);
	 LogUltility.log(test, logger, "Origin exist in the returned line from the compound.txt. should be True : " + Origin);
	 Thread.sleep(3000);
	 boolean  Style = recordInfo.contains(randomStyle.replace(" ", "_"));
	 assertTrue(Style);
	 LogUltility.log(test, logger, "Style exist in the returned line from the compound.txt. should be True : " + Style);
	 Thread.sleep(3000);
	 boolean  Acronym = recordInfo.contains(randomAcronym.replace(" ", "_"));
	 assertTrue(Acronym);
	 LogUltility.log(test, logger, "Acronym exist in the returned line from the compound.txt. should be True : " + Acronym);
	 Thread.sleep(3000);
	 boolean  Register = recordInfo.contains(randomRegister.replace(" ", "_"));
	 assertTrue(Register);
	 LogUltility.log(test, logger, "Register exist in the returned line from the compound.txt. should be True : " + Register);
	 Thread.sleep(3000);
      boolean  Frequency = recordInfo.contains(randomFrequency.replace(" ", "_"));
	 assertTrue(Frequency);
	 LogUltility.log(test, logger, "Frequency exist in the returned line from the compound.txt. should be True : " + Frequency);
	 Thread.sleep(3000);
	 boolean  CompoundType = recordInfo.contains(randomCompoundType);
	 assertTrue(CompoundType);
	 LogUltility.log(test, logger, "Compound Type exist in the returned line from the compound.txt. should be True : " + CompoundType);
//	// Search for the new added record
//	// Choose "form" from the first search dropdown
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
//	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//	
//	// Choose "equal" from the condition dropdown
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
//	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
//	
//	//Type in the search field
//	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
//	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
//	
//	// Click the Retreive button
//	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
//	LogUltility.log(test, logger, "Click the Retreive button");
//	
//	// Retrieve records
//	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
//	
//	// Check that retrieved records do contain the searched record
//	if (recordsList.isEmpty()) assertTrue(false);
//	
//	boolean equalResult = true;
//	for (String s : recordsList) {
//		 String[] removeLeft = s.split("}");
//   		 String[] removeRight = removeLeft[1].split("~");
//   		 
//   		 
//		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
//	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
//	       	else {
//	       		equalResult = false;
//	           break;
//	       	}
//	    }
//	
//	LogUltility.log(test, logger, "Is new record found: " + equalResult);
//	assertTrue(equalResult);

	LogUltility.log(test, logger, "Test Case PASSED");
}
	

/**
 * DIC_TC--251:Verify user can delete an existed record
 * @throws InterruptedException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_251_Verify_user_can_delete_an_existed_record() throws InterruptedException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_251", "Verify user can delete an existed record");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

	// Click the delete button
	CurrentPage.As(CompoundsTab.class).bt_delete.click();
	LogUltility.log(test, logger, "Click the delete button");
	// Click the yes button in the confirmation message 
	CurrentPage.As(CompoundsTab.class).yesDelete.click();

	LogUltility.log(test, logger, "Record is deleted");
	
//	try {
		// try to get the text
		//String mibo = CurrentPage.As(CompoundsTab.class).text_removed_record_confirmation.getText();
	
		// Click OK on the confirmation message
	    Thread.sleep(5000);
		CurrentPage.As(CompoundsTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click the OK button on the Confirmation popup");
		
//	} catch (Exception e) {
//		LogUltility.log(test, logger, "Confirmation popup didn't appear");
//	}
//	
	// Choose a value from Volume[POS] dropdown
	String volumePOS2 = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS2);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown for the second time: " + volumePOS2);
		
	// Retrieve the records list again and check that the deleted record does not exist
	recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	//LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	boolean doContain = recordsList.contains(randomrecord);
	LogUltility.log(test, logger, "Does list contain the removed record, should be false: " + doContain);
	assertFalse(doContain);
	

	LogUltility.log(test, logger, "Test Case PASSED");
}
	

/**
 * DIC_TC--252:Check the deleted record info are saved correctly in the database file
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_252_Check_the_deleted_record_info_are_saved_correctly_in_the_database_file() throws InterruptedException, FileNotFoundException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_252", "Check the deleted record info are saved correctly in the database file");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

	// Click the delete button
	CurrentPage.As(CompoundsTab.class).bt_delete.click();
	LogUltility.log(test, logger, "Click the delete button");
	// Click the yes button in the confirmation message 
	CurrentPage.As(CompoundsTab.class).yesDelete.click();

	LogUltility.log(test, logger, "Record is deleted");
	
//	try {
		// try to get the text
		//String mibo = CurrentPage.As(CompoundsTab.class).text_removed_record_confirmation.getText();
	
		// Click OK on the confirmation message
	    Thread.sleep(5000);
		CurrentPage.As(CompoundsTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click the OK button on the Confirmation popup");
//	} catch (Exception e) {
//		LogUltility.log(test, logger, "Confirmation popup didn't appear");
//	}
//	
	// Retrieve the records list again and check that the deleted record does not exist
	recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	//LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	boolean doContain = recordsList.contains(randomrecord);
	LogUltility.log(test, logger, "Does list contain the removed record, should be false: " + doContain);
	assertFalse(doContain);
	
// click the save button
   CurrentPage.As(CompoundsTab.class).btnSave.click();
   LogUltility.log(test, logger, "Click Save button ");
		
		
// click the OK button in the saving popup
	CurrentPage.As(CompoundsTab.class).btnOKSave.click();
	LogUltility.log(test, logger, "Click ok on the Saving popup ");
	
// get the Deleted record info from the compounds.txt file
	Thread.sleep(5000);
	String recordInfo = CurrentPage.As(CompoundsTab.class).getRecordsInfoFromcompounds(randomrecord);
	LogUltility.log(test, logger, "New record info returned from the Compounds.txt file: " + recordInfo);
		
//Verify that the line does not contain all the selected info
		
	 boolean  name = recordInfo.contains(randomrecord);
	 assertFalse(name);
	 LogUltility.log(test, logger, "Name DOES NOT exist in the returned line from the compound.txt. should be False : " + name);
		

	LogUltility.log(test, logger, "Test Case PASSED");
}
	


/**
 * DIC_TC--254:check the "Look In Corpus" button
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_254_check_the_Look_In_Corpus_button() throws InterruptedException, FileNotFoundException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_252", "check the Look In Corpus button");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "contains" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	
	//Type in the search field
	String searchWord = null;
	if (Setting.Language.equals("EN"))
		searchWord = "supreme_court";
	else if (Setting.Language.equals("HE"))
		searchWord = "על_גבי";
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord);
	LogUltility.log(test, logger, "word is typed in the search field:"+ searchWord);
	
	// Click the Retrieve button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
    LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
//	// Choose random record from the middle of the displayed list
//	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
//	Random randomizer = new Random();
//	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size())); supreme_court
	String randomrecord=null;
	if (Setting.Language.equals("EN"))
	    randomrecord = "EN{n}supreme_court~1";
	else if (Setting.Language.equals("HE"))
		randomrecord = "HE{p}על_גבי~1";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the value from the first POS drop down in the tagging area  taggingPOS
	List<String> taggingPOSList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).taggingPOS);
	
//	boolean found = searchValue.equals(taggingPOSList.get(0));
	LogUltility.log(test, logger, "the selected value from the tagging dropdown is searched word  " + taggingPOSList);
//	assertTrue(found);

	// Click the Look In Corpus  button
	CurrentPage.As(CompoundsTab.class).btnLookCorpus.click();
	LogUltility.log(test, logger, "Click the Look in Corpus button");
	
	//Verify that the Look in Corpus popup displayed 
	boolean lookPopupDisplayed = CurrentPage.As(CompoundsTab.class).matchCorpusFound.isDisplayed();
	LogUltility.log(test, logger, "Check that the Look in corpus popup is Displayed, value should be True: " + lookPopupDisplayed);
	assertTrue(lookPopupDisplayed);
	
	// Click the yes button in the confirmation message 
	CurrentPage.As(CompoundsTab.class).lookPopupYes.click();

	LogUltility.log(test, logger, "Yes selected in the look in corpus popup");
	
	// get the value from the first POS drop down in the tagging area  taggingPOS
	List<String> taggingPOSList1 = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).taggingPOS);
	LogUltility.log(test, logger, "the selected value from the tagging dropdown is searched word  " + taggingPOSList1);


	
	boolean differentvalues = !taggingPOSList.equals(taggingPOSList1);
	LogUltility.log(test, logger, "Does list contain the removed record, should be True: " + differentvalues);
	assertTrue(differentvalues);
	


	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	

/**
 * DIC_TC--256:Verify user can edit key form in Compounds
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_256_Verify_user_can_edit_key_form_in_Compounds() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_256", "Verify user can edit key form in Compounds");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	

	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
    
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	
	
	// add random text in the key form field
	String randomText=null;
	if (Setting.Language.equals("EN"))
		randomText = CurrentPage.As(CompoundsTab.class).RandomString(5)+ removeRight[0];
    else if (Setting.Language.equals("HE"))
    	randomText = CurrentPage.As(CompoundsTab.class).RandomHEString(5)+ removeRight[0];
	
	//CurrentPage.As(CompoundsTab.class).txtKeyForm.click();
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	//Get the new edited Key Form
	String editedKeyForm=  CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
	LogUltility.log(test, logger, "New record after adding Text to the Key Form : " + editedKeyForm);
	
	//Press the Save button
	CurrentPage.As(CompoundsTab.class).btnSave.click();
	LogUltility.log(test, logger, "Click Save button ");
	
	Thread.sleep(3000);
	
	// click the OK button on the change key form popup
	CurrentPage.As(CompoundsTab.class).yesKeyFieldMessage.click();
	LogUltility.log(test, logger, "Click yes button on the change key form popup ");
	
	//Press the Save button
	CurrentPage.As(CompoundsTab.class).btnSave.click();
	LogUltility.log(test, logger, "Click Save button ");
	
    // click the OK button in the saving popupyesKeyFieldMessage
    CurrentPage.As(CompoundsTab.class).btnOKSave.click();
    LogUltility.log(test, logger, "Click ok on the Saving popup ");
	

	// Search for the edited record
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "Equal is chosen in the condition dropdown");
	
	//Type in the search field
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(editedKeyForm);
	LogUltility.log(test, logger, "word is typed in the search field: " + editedKeyForm);
	
	// Click the Retreive button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList1 = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived: " + recordsList1);
	
	// get a clean records list
	
	// get a clean records list
	List<String> recordsList2 = new ArrayList<>();
	  for (String cleanRecord : recordsList1) {
	    	String[] removeLeft1 = cleanRecord.split("}");
			String[] removeRight1 = removeLeft1[1].split("~");
			recordsList2.add(removeRight1[0]);
	  }
			LogUltility.log(test, logger, "New clean record list: " + recordsList2);
	  

	// Check that retrieved records do contain the searched record
    boolean equalResult = recordsList2.contains(editedKeyForm);
	LogUltility.log(test, logger, "Is edited record found? should be True: " + equalResult);
	assertTrue(equalResult);

	
	//Check the changes done on the Compounds file
	
	List<String> recordsFromFile =  CurrentPage.As(CompoundsTab.class).getRecordsFromcompounds();
	String editedKeyFormClean = editedKeyForm.replace("_", " ");
	editedKeyFormClean = editedKeyFormClean.replace("-", " ");
	LogUltility.log(test, logger, "change the: " + editedKeyForm + " ,to the new clean without the _ or - : " + editedKeyFormClean);
//	Thread.sleep(5000);
	boolean newRecordexist = recordsFromFile.contains(editedKeyFormClean);
	LogUltility.log(test, logger, "Is the new record exist in the Compounds.txt file after editing the record? must be True: " + newRecordexist);
	assertTrue(newRecordexist);

	LogUltility.log(test, logger, "Test Case PASSED");
}



/**
 * DIC_TC--257:Verify user can save changes in Compounds
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_257_Verify_user_can_save_changes_in_Compounds() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_257", "Verify user can save changes in Compounds");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
	
	// get the dropdowns values before changing 
	List<String> oldOrigin = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpOrigin);
	List<String> oldFrequency = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpFrequency);
	List<String> oldPOS = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS);
	List<String> oldSemanticGroups = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
	List<String> oldSemanticFields = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
	
	LogUltility.log(test, logger, "First Origin is: " + oldOrigin + " , first Frequency is: " + oldFrequency+ " ,and first POS is: " + oldPOS+ " ,and first Semantic Groups are: " + oldSemanticGroups+ " ,and first Semantic Fields are: " + oldSemanticFields);

	List<String> validPOS = CurrentPage.As(CompoundsTab.class).checkPOSValidity("adjective"); 
	LogUltility.log(test, logger, "POS List that user can change to : " + validPOS);
	// Choose also a POS value that does exist in the Volume[POS} dropdown in order to have values in Semantic Groups
	List<String> volumeDropdownValues = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpVolumePOS);
	validPOS.retainAll(volumeDropdownValues);
	
//	// Get random POS
//	List<String> validPOS2 = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
//	LogUltility.log(test, logger, "POS list: " + validPOS);
	
	
	// Pick a random pos
	LogUltility.log(test, logger, "Start with changing the dropdowns values ");
	Random randomizer1 = new Random();
//	String randompos = "unknown";
//	while(randompos == "unknown"){
	String randompos = validPOS.get(randomizer1.nextInt(validPOS.size())).replace("_", " ");
	LogUltility.log(test, logger, "Selected new random POS is : " + randompos);	
//	}
	
	// get a clean records list
//	String[] removeLeft1 = randompos.split("}");
//	String[] removeRight1 = removeLeft1[1].split("~");
	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " +randompos);
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos.replace("_", " "));
	LogUltility.log(test, logger, "Choose from pos list the clean POS: " + randompos.replace("_", " "));
	
	
	// click Yes button in the Key popup message yesKeyFieldMessage
	CurrentPage.As(CompoundsTab.class).yesKeyFieldMessage.click();
	LogUltility.log(test, logger, "Click Yes button in the Key changed popup message ");
	
	// Record converted after changing the POS
	String name1 = CurrentPage.As(CompoundsTab.class).POSConverter(randompos);
	LogUltility.log(test, logger, "The selected POS after converting is : " + name1);
	String fullName = Setting.Language+"{"+name1+"}"+removeRight[0]+"~0";
	LogUltility.log(test, logger, "The full name after converting is : " + fullName);
	
	
	// Search for the record after changing the POS
	LogUltility.log(test, logger, "Search for the record after changing the POS, since the page refreshed ");
	// Choose "Form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	
	// Choose a value from the third dropdown
	String searchValue = removeRight[0];
	
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchValue);
	LogUltility.log(test, logger, "Chosen search value: " + searchValue);
	
	
	// Click the Retreive button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");

	// Pick a random record
	String randomrecord1 = fullName;
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord1);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);

	// Get random Origin
	List<String> originList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpOrigin);
	LogUltility.log(test, logger, "Origin list: " + originList);
	originList.removeAll(oldOrigin);
	LogUltility.log(test, logger, "Origin list: " + originList);
	
	// Pick a random Origin
	Random randomizer2 = new Random();
	String randomOrigin = "unknown";
	while(randomOrigin == "unknown"){
		randomOrigin = originList.get(randomizer2.nextInt(originList.size()));
	}
	LogUltility.log(test, logger, "Choose from Origin list: " + randomOrigin);
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpOrigin, randomOrigin);

	// Get random Frequency
	List<String> frequencyList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpFrequency);
	LogUltility.log(test, logger, "Frequency list: " + frequencyList);
	frequencyList.removeAll(oldFrequency);
	
	// Pick a random Frequency
	Random randomizer5 = new Random();
	String randomFrequency = "unknown";
	while(randomFrequency == "unknown"){
		randomFrequency = frequencyList.get(randomizer5.nextInt(frequencyList.size()));
	}
	LogUltility.log(test, logger, "Choose from Frequency list: " + randomFrequency);
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpFrequency, randomFrequency);
	
	// Get random semantic Group
	Thread.sleep(4000);
	List<String> semanticGroupList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
	LogUltility.log(test, logger, "Semantic Group list: " + semanticGroupList);

	if(!oldSemanticGroups.isEmpty())
		semanticGroupList.removeAll(oldSemanticGroups);
	LogUltility.log(test, logger, "Semantic Group list: " + semanticGroupList);
	
	Random randomizer6 = new Random();
	String randomSemanticGroup = "unknown";
	while(randomSemanticGroup == "unknown"){
		randomSemanticGroup = semanticGroupList.get(randomizer6.nextInt(semanticGroupList.size()));
	}
	LogUltility.log(test, logger, "Choose from Semantic Group list: " + randomSemanticGroup);
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups, randomSemanticGroup);
	
	// Get random  semantic field
	List<String> semanticfieldList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
	LogUltility.log(test, logger, "Semantic field list: " + semanticfieldList);
	
	if(!oldSemanticFields.isEmpty())
		semanticfieldList.removeAll(oldSemanticFields);
	LogUltility.log(test, logger, "Semantic field list: " + semanticfieldList);
	
	Random randomizer7 = new Random();
	String randomSemanticField = "unknown";
	while(randomSemanticField == "unknown"){
		randomSemanticField = semanticfieldList.get(randomizer7.nextInt(semanticfieldList.size()));
	}
	LogUltility.log(test, logger, "Choose from Semantic field list: " + randomSemanticField);
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields, randomSemanticField);

	// Get the New values for the dropdowns
	// get the dropdowns values before changing 
	List<String> newOrigin = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpOrigin);
	List<String> newFrequency = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpFrequency);
	List<String> newPOS = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS);
	List<String> newSemanticGroup = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
	List<String> newSemanticfield = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
	LogUltility.log(test, logger, "New Origin is: " + newOrigin + " , New Frequency is: " + newFrequency+ " ,and new POS is: " + newPOS+ " ,and new Semantic Group is: " + newSemanticGroup+ " ,and new Semantic field is: " + newSemanticfield);
	
	//check that the dropdowns were changed 
	boolean theSameOrgin = !oldOrigin.equals(newOrigin);
	LogUltility.log(test, logger, "Origin value should be changed from: " + oldOrigin + " to the : " + newOrigin+ "  and the value should be True: "+theSameOrgin);
	assertTrue(theSameOrgin);
	
	boolean theSamePOS = !oldPOS.equals(newPOS);
	LogUltility.log(test, logger, "POS value should be changed from: " + oldPOS + " to the : " + newPOS+ "  and the value should be True: "+theSamePOS);
	assertTrue(theSamePOS);
	
	boolean theSameFrequency = !oldFrequency.equals(newFrequency);
	LogUltility.log(test, logger, "Frequency value should be changed from: " + oldFrequency + " to the : " + newFrequency+ "  and the value should be True: "+theSameFrequency);
	assertTrue(theSameFrequency);
	
	
	// click the save button
	CurrentPage.As(CompoundsTab.class).btnSave.click();
	LogUltility.log(test, logger, "Click Save button ");
	
	
	// click the OK button in the saving popup
	CurrentPage.As(CompoundsTab.class).btnOKSave.click();
	LogUltility.log(test, logger, "Click ok on the Saving popup ");
	
	
	// get the new record info from the compounds.txt file
	Thread.sleep(20000);
	//public String getRegularRecordsInfoFromcompounds(String searchedRecord)
	String recordInfo = CurrentPage.As(CompoundsTab.class).getRegularRecordsInfoFromcompounds(randomrecord1);
	LogUltility.log(test, logger, "New record info returned from the Compounds.txt file: " + recordInfo);
	
	//Verify that the line contain all the selected info
	Thread.sleep(20000);
	 boolean  name = recordInfo.contains(randomrecord1);
	 assertTrue(name);
	 LogUltility.log(test, logger, "name exist in the returned line from the compound.txt. should be True : " + name);
	// verify the selected POS
	 String searchValue2 = removeRight[0];
	 String name2 = CurrentPage.As(CompoundsTab.class).POSConverter(randompos);
	 LogUltility.log(test, logger, "The selected POS after converting is : " + name2);
	 String fullName1 = Setting.Language+"{"+name2+"}"+searchValue2+"~0";
	 LogUltility.log(test, logger, "The full name after converting is : " + fullName1);
	 boolean  POS = recordInfo.contains(fullName1);
     assertTrue(POS);
	 boolean  Origin = recordInfo.contains(randomOrigin);
	 assertTrue(Origin);
	 LogUltility.log(test, logger, "Origin exist in the returned line from the compound.txt. should be True : " + Origin);
	 Thread.sleep(3000);
      boolean  Frequency = recordInfo.contains(randomFrequency.replace(" ", "_"));
	 assertTrue(Frequency);
	 LogUltility.log(test, logger, "Frequency exist in the returned line from the compound.txt. should be True : " + Frequency);
	 
	 // Check that the record with the orginal POS not appear in the compounds file
		String recordInfo2 = CurrentPage.As(CompoundsTab.class).getRegularRecordsInfoFromcompounds(randomrecord);
		LogUltility.log(test, logger, "New record info returned from the Compounds.txt file: " + recordInfo2);
		//assertEquals(recordInfo2, " ");
		assertEquals(recordInfo2, "");

	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	
	

/**
 * DIC_TC--336:Check the tagged compounds counter in the compounds tab
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_336_Check_the_tagged_compounds_counter_in_the_compounds_tab() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_336", "Check the tagged compounds counter in the compounds tab");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Verify that the tagged compounds counter displayed
	
	boolean isCounterDisplayed = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.isDisplayed();
	assertTrue(isCounterDisplayed);
	LogUltility.log(test, logger, "Check that the Tagged compounds number displayed . should be True : "  + isCounterDisplayed);
	
	// get the counter text
	String counterText = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.getAttribute("Name");
	LogUltility.log(test, logger, "the counter text is : "  + counterText);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	


/**
 * DIC_TC--337:Check the number of all compounds will change when the user adds compounds
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_337_Check_the_number_of_all_compounds_will_change_when_the_user_adds_compounds() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_337", "Check the number of all compounds will change when the user adds compounds");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Verify that the tagged compounds counter displayed
	
	boolean isCounterDisplayed = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.isDisplayed();
	assertTrue(isCounterDisplayed);
	LogUltility.log(test, logger, "Check that the Tagged compounds number displayed . should be True : "  + isCounterDisplayed);
	
	// get the counter text
	String counterText = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.getAttribute("Name");
	LogUltility.log(test, logger, "the counter text is : "  + counterText);
	
    // get the number of all compounds
	String[] splittedText = counterText.split(" ");
	int compoundsNumber = Integer.parseInt(splittedText[4]);
	LogUltility.log(test, logger, "the compounds number in the counter is : "  + compoundsNumber);
	
	LogUltility.log(test, logger, "Adding new record");
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");

	// get the counter text after adding new record
	String counterText2 = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.getAttribute("Name");
	LogUltility.log(test, logger, "the counter text after adding new record is : "  + counterText2);
	
    // get the number of all compounds
	String[] splittedText2 = counterText2.split(" ");
	int compoundsNumber2 = Integer.parseInt(splittedText2[4]);
	LogUltility.log(test, logger, "the compounds number in the counter after adding new record is : "  + compoundsNumber2);
	
	boolean results = compoundsNumber2 == compoundsNumber +1;
	assertTrue(results);
	LogUltility.log(test, logger, "the compounds number in the counter before adding new record is : "  + compoundsNumber+  "  then compounds number in the counter after adding new record is + 1 : " + compoundsNumber2+ "  and the value should be True : "  +results);
	
	// exist from creating new record mode
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown , to exist the creating mode: " + volumePOS);
	
	//Press the cancel button in the bad key message
	CurrentPage.As(CompoundsTab.class).cancelBadKey.click();
	LogUltility.log(test, logger, "Cancel button Clicked in the bad key message and the warning closed");

	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	

/**
 * DIC_TC--338:Check the number of all compounds will change when the user deletes compounds
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_338_Check_the_number_of_all_compounds_will_change_when_the_user_deletes_compounds() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_338", "Check the number of all compounds will change when the user deletes compounds");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Verify that the tagged compounds counter displayed
	
	boolean isCounterDisplayed = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.isDisplayed();
	assertTrue(isCounterDisplayed);
	LogUltility.log(test, logger, "Check that the Tagged compounds number displayed . should be True : "  + isCounterDisplayed);
	
	// get the counter text
	String counterText = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.getAttribute("Name");
	LogUltility.log(test, logger, "the counter text is : "  + counterText);
	
    // get the number of all compounds
	String[] splittedText = counterText.split(" ");
	int compoundsNumber = Integer.parseInt(splittedText[4]);
	LogUltility.log(test, logger, "the compounds number in the counter is : "  + compoundsNumber);
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	
	LogUltility.log(test, logger, "Now we need to delete one record");
	
	// Click the Delete button
	CurrentPage.As(CompoundsTab.class).bt_delete.click();
	LogUltility.log(test, logger, "Click the Delete button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).yesDelete.click();
	LogUltility.log(test, logger, "Click the Yes button in the delete confirmation message");
	
	// close the massage : record was successfully removed press the OK button btnOKremoved
	Thread.sleep(3000);
	CurrentPage.As(CompoundsTab.class).btnOKremoved.click();
	LogUltility.log(test, logger, "close the massage : record was successfully removed press the OK button");

	// get the counter text after adding new record
	String counterText2 = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.getAttribute("Name");
	LogUltility.log(test, logger, "the counter text after adding new record is : "  + counterText2);
	
    // get the number of all compounds
	String[] splittedText2 = counterText2.split(" ");
	int compoundsNumber2 = Integer.parseInt(splittedText2[4]);
	LogUltility.log(test, logger, "the compounds number in the counter after adding new record is : "  + compoundsNumber2);
	
	boolean results = compoundsNumber2 == compoundsNumber -1;
	assertTrue(results);
	LogUltility.log(test, logger, "the compounds number in the counter before adding new record is : "  + compoundsNumber+  "  then compounds number in the counter after adding new record is  -1 : " + compoundsNumber2+ "  and the value should be True : "  +results);
	
	
	LogUltility.log(test, logger, "Test Case PASSED");
}



	
/**
 * DIC_TC--345: Check the semantic relation have multiple groups of values in each POS
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_345_Check_the_semantic_relation_have_multiple_groups_of_values_in_each_POS() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_345", "Check the semantic relation have multiple groups of values in each POS");
	
	if (Setting.Language.equals("HE")) {
		LogUltility.log(test, logger, "This Test was skipped because Hebrew records doesn`t have semantic relations ");
		return;
	}
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Verify that the tagged compounds counter displayed
	
	boolean isCounterDisplayed = CurrentPage.As(CompoundsTab.class).numberOfTaggedCompounds.isDisplayed();
	assertTrue(isCounterDisplayed);
	LogUltility.log(test, logger, "Check that the Tagged compounds number displayed . should be True : "  + isCounterDisplayed);
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> semanticRelationListAdj = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
	LogUltility.log(test, logger, "The Semantic relation list when select adjective  : " + semanticRelationListAdj);
	
	LogUltility.log(test, logger, "Now we changing the Volume POS ,to check if we get a different Semantic Relations list");
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS2 = "verb";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS2);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS2);

	// Retrieve records
	List<String> semanticRelationListverb = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
	LogUltility.log(test, logger, "The Semantic relation list when select verb  : " + semanticRelationListverb);
	
	LogUltility.log(test, logger, "Now we need to compare between the Adjective semantic relation list and the Verb Semantic Relations list ,they should be different ");
	
	boolean sameSRList = !semanticRelationListAdj.equals(semanticRelationListverb);
	assertTrue(sameSRList);
	LogUltility.log(test, logger, "After comparring , the semantic relation list for adjective is differnt than for verb. value should be true, and it`s   : "  + sameSRList);
	
	LogUltility.log(test, logger, "Now we changing the Volume POS again to a new value ,to check if we get a different Semantic Relations list");
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS3 = "noun";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS3);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS3);

	// Retrieve records
	List<String> semanticRelationListnoun = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
	LogUltility.log(test, logger, "The Semantic relation list when select noun  : " + semanticRelationListnoun);
	
	boolean differentSRList = !semanticRelationListnoun.equals(semanticRelationListverb);
	assertTrue(differentSRList);
	LogUltility.log(test, logger, "After comparring , the semantic relation list for Noun is differnt than for verb. value should be true, and it`s    : "  + differentSRList);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	

/**
 * DIC_TC--351:Check the Definition is editable
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_351_Check_the_Definition_is_editable() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_351", "Check the Definition is editable");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "interjection";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	
	// Get the definition value  
	String orginalDefinition =CurrentPage.As(CompoundsTab.class).lst_Definition.getText();
	LogUltility.log(test, logger, "Orginal Definition is : " + orginalDefinition);
	
	// Type a new value in the Definition field
	LogUltility.log(test, logger, "Change the Orginal Definition.");
	String randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + " " + CurrentPage.As(CompoundsTab.class).RandomString(5);
	CurrentPage.As(CompoundsTab.class).lst_Definition.click();
	CurrentPage.As(CompoundsTab.class).lst_Definition.sendKeys(randomText);
	LogUltility.log(test, logger, "New Definition is : " + randomText);

	
	// Get the definition value before saving changes 
	String beforeSavedDefinition =CurrentPage.As(CompoundsTab.class).lst_Definition.getText();
	LogUltility.log(test, logger, "Definition before saving is : " + beforeSavedDefinition);
	
	//Press the Save button
	CurrentPage.As(CompoundsTab.class).btnSave.click();
	LogUltility.log(test, logger, "Click Save button ");
	
    // click the OK button in the saving popupyesKeyFieldMessage
    CurrentPage.As(CompoundsTab.class).btnOKSave.click();
    LogUltility.log(test, logger, "Click ok on the Saving popup ");
	
	// Get the definition value after saving changes 
	String savedDefinition =CurrentPage.As(CompoundsTab.class).lst_Definition.getText();
	LogUltility.log(test, logger, "Orginal Definition is : " + savedDefinition);
	
	// Check that the saved Definition is different than the original one
    boolean lastDefinition = !savedDefinition.equals(orginalDefinition);
	LogUltility.log(test, logger, "the saved Definition is different than the original one? should be True: " + lastDefinition);
	assertTrue(lastDefinition);


	LogUltility.log(test, logger, "Test Case PASSED");
}

	


/**
 * DIC_TC--353:Check the Etymology is editable
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_353_Check_the_Etymology_is_editable() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_353", "Check the Etymology is editable");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "interjection";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	
	// Get the Etymology value  
	String orginalEtymology =CurrentPage.As(CompoundsTab.class).Etymology_Text.getText();
	LogUltility.log(test, logger, "Orginal Etymology is : " + orginalEtymology);
	
	// Type a new value in the Etymology field
	LogUltility.log(test, logger, "Change the Orginal Etymology.");
	String randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + " " + CurrentPage.As(CompoundsTab.class).RandomString(5);
	CurrentPage.As(CompoundsTab.class).Etymology_Text.click();
	CurrentPage.As(CompoundsTab.class).Etymology_Text.sendKeys(randomText);
	LogUltility.log(test, logger, "New Etymology is : " + randomText);

	
	// Get the Etymology value before saving changes 
	String beforeSavedEtymology =CurrentPage.As(CompoundsTab.class).Etymology_Text.getText();
	LogUltility.log(test, logger, "Etymology before saving is : " + beforeSavedEtymology);
	
	//Press the Save button
	CurrentPage.As(CompoundsTab.class).btnSave.click();
	LogUltility.log(test, logger, "Click Save button ");
	
    // click the OK button in the saving popupyesKeyFieldMessage
    CurrentPage.As(CompoundsTab.class).btnOKSave.click();
    LogUltility.log(test, logger, "Click ok on the Saving popup ");
	
	// Get the Etymology value after saving changes 
	String savedEtymology =CurrentPage.As(CompoundsTab.class).Etymology_Text.getText();
	LogUltility.log(test, logger, "Saved Etymology is : " + savedEtymology);
	
	// Check that the saved Etymology is different than the original one
    boolean lastEtymology = !savedEtymology.equals(orginalEtymology);
	LogUltility.log(test, logger, "the saved Etymology:    "   + savedEtymology+   " is different than the original one :  "  + orginalEtymology+ "  and the value should be True: " + lastEtymology);
	assertTrue(lastEtymology);


	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	

/**
 * DIC_TC--363: Check the user cannot choose unknown value in the Part of Speech drop-down
 * @throws InterruptedException 
 * @throws IOException 
 * @throws AWTException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_363_Check_the_user_cannot_choose_unknown_value_in_the_Part_of_Speech_drop_down() throws InterruptedException, IOException, AWTException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_363", "Check the user cannot choose unknown value in the Part of Speech drop-down");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
//	// get a clean records list
//	String[] removeLeft = randomrecord.split("}");
//	String[] removeRight = removeLeft[1].split("~");
//	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
//	
//	// get the dropdowns values before changing 
//	List<String> oldOrigin = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS);
//	List<String> oldFrequency = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpFrequency);
//	List<String> oldPOS = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS);
//	List<String> oldSemanticGroups = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
//	List<String> oldSemanticFields = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_Lexical_Fields);
//	
//	LogUltility.log(test, logger, "First Origin is: " + oldOrigin + " , first Frequency is: " + oldFrequency+ " ,and first POS is: " + oldPOS+ " ,and first POS is: " + oldSemanticGroups+ " ,and first POS is: " + oldSemanticFields);


//	// Get random POS
//	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
//	LogUltility.log(test, logger, "POS list: " + posList);
//	
//	// Pick a random pos
//	Random randomizer1 = new Random();
//	String randompos = "unknown";
//	while(randompos != "unknown"){
//		randompos = posList.get(randomizer1.nextInt(posList.size()));
//	}
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
//	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	
	// Click POS dropdown then click the U button to choose unknown
	  CurrentPage.As(CompoundsTab.class).cmpPOS.click();
	  Robot r = new Robot();
	  r.keyPress(KeyEvent.VK_U);
	  r.keyRelease(KeyEvent.VK_U);
	
	

//	CurrentPage.As(CompoundsTab.class).scrollUpArrow.click();
//	CurrentPage.As(CompoundsTab.class).scrollUpArrow.click();
//	CurrentPage.As(CompoundsTab.class).scrollUpArrow.click();
//	CurrentPage.As(CompoundsTab.class).scrollUpArrow.click();
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
//	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// verify that the message "Illegal POS selected! Value will be reverted"  is displayed
	boolean illegalDisplayed = CurrentPage.As(CompoundsTab.class).illegalText.isDisplayed();
	assertTrue(illegalDisplayed);
	LogUltility.log(test, logger, "Check if the Illegal POS selected message displayed, Value should be True :  " +illegalDisplayed);
	
	// Press the OK button on the Illegal POS message to close it 
	Thread.sleep(5000);
    CurrentPage.As(CompoundsTab.class).btnOKIllegal.click();
    CurrentPage.As(CompoundsTab.class).btnOKIllegal.click();
    LogUltility.log(test, logger, "Click ok on the Illegal POS message to close it ");
    
	// verify that the message "Illegal POS selected! Value will be reverted" closed after press the OK button
    //Thread.sleep(3000);
    boolean illegalClosed = false;
    try {
    	illegalClosed = CurrentPage.As(CompoundsTab.class).illegalText.isDisplayed();
	} catch (Exception e) {
		illegalClosed = false;
	}
    assertFalse(illegalClosed);
	LogUltility.log(test, logger, "Check if the Illegal POS selected message displayed, Value should be false :  " +illegalClosed);
	assertFalse(illegalClosed);
	
	
	LogUltility.log(test, logger, "Test Case PASSED");
}	
	



/**
 * DIC_TC--382-Part1:Check the new button in specific scenario - Cancel button
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_382_Part1_Check_the_new_button_in_specific_scenario_Cancel_button() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_382-Part1", "Check the new button in specific scenario - Cancel button");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "interjection";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	
	String randomText = null;
	if (Setting.Language.equals("EN"))
		randomText = "uh_oh";
	else if (Setting.Language.equals("HE"))
		randomText = "איך_היה";
	
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	//Random randomizer1 = new Random();
	String randompos = "interjection";
//	while(randompos == "unknown"){
//		randompos = posList.get(randomizer.nextInt(posList.size()));
//	}
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	
	// Verify the Similar key exists message appear
	boolean similarPopup = CurrentPage.As(CompoundsTab.class).similarKeyMessage.isDisplayed();
	LogUltility.log(test, logger, "The Similar key exists message displayed, Value should be True : "+similarPopup);
	
//	// Press the No button in the similar popup
//	CurrentPage.As(CompoundsTab.class).noSimilarKeyMessage.click();
//	LogUltility.log(test, logger, "Click the No button on the Similar message.");
//	
//	
//	// Press the Yes button in the similar popup
//	CurrentPage.As(CompoundsTab.class).yesSimilarKeyMessage.click();
//	LogUltility.log(test, logger, "Click the Yes button on the Similar message.");
//	
	// Press the Cancel button in the similar popup
	Thread.sleep(2000);
	CurrentPage.As(CompoundsTab.class).cancelSimilarKeyMessage.click();
	LogUltility.log(test, logger, "Click the Cancel button on the Similar message.");
	
	//Verify that the Key form field returned with the last value
	String lastValue = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
	LogUltility.log(test, logger, "The Key form field returned with the last value : " + lastValue);
	//check that the same value as before displayed in the Key form field 
	boolean sameKeyForm = lastValue.equals(removeRight[0]);
	
	
	LogUltility.log(test, logger, "The same value as before " + removeRight[0]+ " displayed in the Key form field as the new one : " + lastValue+" and the value should be true "+sameKeyForm);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	

/**
 * DIC_TC--382-Part2:Check the new button in specific scenario - No button
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_382_Part2_Check_the_new_button_in_specific_scenario_No_button() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_382-Part2", "Check the new button in specific scenario - No button");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "interjection";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText = null;
	if (Setting.Language.equals("EN"))
		randomText = "uh_oh";
	else if (Setting.Language.equals("HE"))
		randomText = "איך_היה";
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	// Get the  record version
	String version1 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
	LogUltility.log(test, logger, " First Record version is : " + version1);
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	//Random randomizer1 = new Random();
	String randompos = "interjection";
//	while(randompos == "unknown"){
//		randompos = posList.get(randomizer.nextInt(posList.size()));
//	}
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	
	// Verify the Similar key exists message appear
	boolean similarPopup = CurrentPage.As(CompoundsTab.class).similarKeyMessage.isDisplayed();
	LogUltility.log(test, logger, "The Similar key exists message displayed, Value should be True : "+similarPopup);
	
//	// Press the No button in the similar popup
//	CurrentPage.As(CompoundsTab.class).noSimilarKeyMessage.click();
//	LogUltility.log(test, logger, "Click the No button on the Similar message.");
//	
//	
//	// Press the Yes button in the similar popup
//	CurrentPage.As(CompoundsTab.class).yesSimilarKeyMessage.click();
//	LogUltility.log(test, logger, "Click the Yes button on the Similar message.");
//	
	// Press the no button in the similar popup
	Thread.sleep(2000);
	CurrentPage.As(CompoundsTab.class).noSimilarKeyMessage.click();
	LogUltility.log(test, logger, "Click the no button on the Similar message.");
	
//	// Get random POS for the second time
//	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
//	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	//Random randomizer1 = new Random();
	String randompos2 = "interjection";
//	while(randompos == "unknown"){
//		randompos = posList.get(randomizer.nextInt(posList.size()));
//	}
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos2);
	LogUltility.log(test, logger, "Choose from pos list for the second time: " + randompos2);
	
	// Verify the Similar key exists message appear
	boolean similarPopup2 = CurrentPage.As(CompoundsTab.class).similarKeyMessage.isDisplayed();
	LogUltility.log(test, logger, "The Similar key exists message displayed for the second time, Value should be True : "+similarPopup2);
	
	// Verify that the record version doesn`t changed
	String version2 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
	LogUltility.log(test, logger, " First Record version is : " + version2);
	boolean sameVersion = version2.equals(version1);
	assertTrue(sameVersion);
	LogUltility.log(test, logger, "Record version doesn`t changed , Before click No was "  + version1+ " and after click no still the same  : " + version2);
	
	// Press the Cancel button to close the popup
	CurrentPage.As(CompoundsTab.class).cancelSimilarKeyMessage.click();
	LogUltility.log(test, logger, "Click the Cancel button on the Similar message to close the popup.");
	
	
	// verify that the message "Illegal POS selected! Value will be reverted" closed after press the OK button
    //Thread.sleep(3000);
    boolean similarClosed = false;
    try {
    	similarClosed = CurrentPage.As(CompoundsTab.class).similarKeyMessage.isDisplayed();
	} catch (Exception e) {
		similarClosed = false;
	}
    assertFalse(similarClosed);
    LogUltility.log(test, logger, "The Similar key exists message closed, Value should be false : "+similarClosed);
	
//	//Verify that the Key form field returned with the last value
//	String lastValue = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
//	LogUltility.log(test, logger, "The Key form field returned with the last value : " + lastValue);
//	//check that the same value as before displayed in the Key form field 
//	boolean sameKeyForm = lastValue.equals(removeRight[0]);
	
	
//	LogUltility.log(test, logger, "The same value as before " + removeRight[0]+ " displayed in the Key form field as the new one : " + lastValue+" and the value should be true "+sameKeyForm);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	
/**
 * DIC_TC--382-Part3:Check the new button in specific scenario - yes button
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_382_Part3_Check_the_new_button_in_specific_scenario_Yes_button() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_382-Part3", "Check the new button in specific scenario - yes button");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "interjection";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText = null;
	if (Setting.Language.equals("EN"))
		randomText = "uh_oh";
	else if (Setting.Language.equals("HE"))
		randomText = "איך_היה";
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	// Get the  record version
	String version1 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
	LogUltility.log(test, logger, " First Record version is : " + version1);
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	//Random randomizer1 = new Random();
	String randompos = "interjection";
//	while(randompos == "unknown"){
//		randompos = posList.get(randomizer.nextInt(posList.size()));
//	}
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	
	// Verify the Similar key exists message appear
	boolean similarPopup = CurrentPage.As(CompoundsTab.class).similarKeyMessage.isDisplayed();
	LogUltility.log(test, logger, "The Similar key exists message displayed, Value should be True : "+similarPopup);
	

//	
	// Press the Yes button in the similar popup
	Thread.sleep(2000);
	CurrentPage.As(CompoundsTab.class).yesSimilarKeyMessage.click();
	LogUltility.log(test, logger, "Click the Yes button on the Similar message.");
	
	// Verify that the record version changed
	String version2 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
	LogUltility.log(test, logger, " New Record version is : " + version2);
	boolean sameVersion = !version2.equals(version1);
	assertTrue(sameVersion);
	LogUltility.log(test, logger, "The version before Clicking the Yes button was : "  + version1+  "  then after clicking the yes button become + 1 : " + version2+ "  and the value should be True : "  +sameVersion);

	
	//Verify that the Key form field doesn`t changed
	String lastValue = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
	LogUltility.log(test, logger, "The Key form field returned with the last value : " + lastValue);
	//check that the same value entered by user still displayed in the Key form field 
	boolean sameKeyForm = lastValue.equals(randomText);
	
	
	LogUltility.log(test, logger, "The same value entered by User " + randomText+ " displayed in the Key form field as the new one : " + lastValue+" and the value should be true "+sameKeyForm);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	


/**
 * DIC_TC--364:check the user can edit tags
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_364_check_the_user_can_edit_tags() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_364", "check the user can edit tags");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "interjection";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText = null;
	if (Setting.Language.equals("EN"))
		randomText = "uh_oh";
	else if (Setting.Language.equals("HE"))
		randomText = "איך_היה";
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	// Get the  record version
	String version1 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
	LogUltility.log(test, logger, " First Record version is : " + version1);
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	//Random randomizer1 = new Random();
	String randompos = "interjection";
//	while(randompos == "unknown"){
//		randompos = posList.get(randomizer.nextInt(posList.size()));
//	}
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	
	// Verify the Similar key exists message appear
	boolean similarPopup = CurrentPage.As(CompoundsTab.class).similarKeyMessage.isDisplayed();
	LogUltility.log(test, logger, "The Similar key exists message displayed, Value should be True : "+similarPopup);
	

//	
	// Press the Yes button in the similar popup
	Thread.sleep(2000);
	CurrentPage.As(CompoundsTab.class).yesSimilarKeyMessage.click();
	LogUltility.log(test, logger, "Click the Yes button on the Similar message.");
	
	// Verify that the record version changed
	String version2 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
	LogUltility.log(test, logger, " New Record version is : " + version2);
	boolean sameVersion = !version2.equals(version1);
	assertTrue(sameVersion);
	LogUltility.log(test, logger, "The version before Clicking the Yes button was : "  + version1+  "  then after clicking the yes button become + 1 : " + version2+ "  and the value should be True : "  +sameVersion);

	
	//Verify that the Key form field doesn`t changed
	String lastValue = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
	LogUltility.log(test, logger, "The Key form field returned with the last value : " + lastValue);
	//check that the same value entered by user still displayed in the Key form field 
	boolean sameKeyForm = lastValue.equals(randomText);
	
	
	LogUltility.log(test, logger, "The same value entered by User " + randomText+ " displayed in the Key form field as the new one : " + lastValue+" and the value should be true "+sameKeyForm);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}



/**
 * DIC_TC--366:Check the stress drop-down must have value
 * @throws InterruptedException 
 * @throws IOException 
 * @throws AWTException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_366_Check_the_stress_dropdown_must_have_value() throws InterruptedException, IOException, AWTException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_366", "Check the stress drop-down must have value");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "interjection";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
	
	// Get selected stress from the dropdowns
	String firstStress = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Stress", 0);
	LogUltility.log(test, logger, "the value that selected in the first stress dropdown :  " + firstStress);
	String secondStress = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Stress", 1);
	LogUltility.log(test, logger, "the value that selected in the second stress dropdown :  " + secondStress);
	
	// Get values from the stress dropdowns 
	List<String> stressValues1 = CurrentPage.As(CompoundsTab.class).getTaggingValues("Stress", 0);
	LogUltility.log(test, logger, "Stress values from the first dropdown : " + stressValues1);
	List<String> stressValues2 = CurrentPage.As(CompoundsTab.class).getTaggingValues("Stress", 1);
	LogUltility.log(test, logger, "Stress values from the second dropdown : " + stressValues2);
	
	//remove the choosen value from the list
	stressValues1.remove(firstStress);
	stressValues1.remove("");
	LogUltility.log(test, logger, "First stress list after remove : " + stressValues1);
	stressValues2.remove(secondStress);
	stressValues2.remove("");
	LogUltility.log(test, logger, "Second stress list after remove  : " + stressValues2);
	
	// Pick a random Stress from the first  dropdown
	Random randomizer1 = new Random();
	String randomStress = stressValues1.get(randomizer1.nextInt(stressValues1.size()));
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomStress);
	LogUltility.log(test, logger, "Choose Random stress from the first dropdown: " + randomStress);
	// Select random stress from the first dropdown 
	CurrentPage.As(CompoundsTab.class).chooseTaggingValue("Stress", 0, randomStress);
	LogUltility.log(test, logger, "Selected stress on the first dropdown: " + randomStress);
	
	// Pick a random Stress from the second  dropdown
	Random randomizer2 = new Random();
	String randomStress2 = stressValues2.get(randomizer2.nextInt(stressValues2.size()));
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomStress2);
	Thread.sleep(3000);
	LogUltility.log(test, logger, "Choose Random stress from the second dropdown: " + randomStress2);
	// Select random stress from the first dropdown 
	CurrentPage.As(CompoundsTab.class).chooseTaggingValue("Stress", 1, randomStress2);
	LogUltility.log(test, logger, "Selected stress on the second dropdown: " + randomStress2);
		
	//Click the save button 
	CurrentPage.As(CompoundsTab.class).btnSave.click();
	LogUltility.log(test, logger, "Click save button ");
	//Click the OK button on the after the save button 
	CurrentPage.As(CompoundsTab.class).btnOKSave.click();
	LogUltility.log(test, logger, "Click OK after save button ");
	
	
	// Get selected stress from the dropdowns after changes
	String firstStress2 = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Stress", 0);
	LogUltility.log(test, logger, "the value that selected in the first stress dropdown :  " + firstStress2);
	String secondStress2 = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Stress", 1);
	LogUltility.log(test, logger, "the value that selected in the second stress dropdown :  " + secondStress2);
	
	boolean firstcolumn= !firstStress.equals(firstStress2);
	assertTrue(firstcolumn);
	LogUltility.log(test, logger, "the value that selected in the first stress dropdown before change was : " +firstStress +" and after changes is  :  " + firstStress2 +" and the value should be True :" +firstcolumn);
	
	boolean secondcolumn= !secondStress.equals(secondStress2);
	assertTrue(secondcolumn);
	LogUltility.log(test, logger, "the value that selected in the first stress dropdown before change was : " +secondStress +" and after changes is  :  " + secondStress2 +" and the value should be True :" +secondcolumn);
	
	
//	
//	//Verify that the Key form field doesn`t changed
//	String lastValue = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
//	LogUltility.log(test, logger, "The Key form field returned with the last value : " + lastValue);
//	//check that the same value entered by user still displayed in the Key form field 
//	boolean sameKeyForm = lastValue.equals(randomText);
//	
//	
//	LogUltility.log(test, logger, "The same value entered by User " + randomText+ " displayed in the Key form field as the new one : " + lastValue+" and the value should be true "+sameKeyForm);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}





/**
 * DIC_TC--393_Part1: Check creating a new compound using (-) 
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_393_Part1_Check_creating_a_new_compound_using_dash () throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_393_Part1", "Check creating a new compound using (-) ");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText="";
	if (Setting.Language.equals("EN"))
		randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + "-" + CurrentPage.As(CompoundsTab.class).RandomString(5);
	else if (Setting.Language.equals("HE"))
		randomText= CurrentPage.As(CompoundsTab.class).RandomHEString(5) + "-" + CurrentPage.As(CompoundsTab.class).RandomHEString(5);

	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	Random randomizer = new Random();
	String randompos = posList.get(randomizer.nextInt(posList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// Search for the new added record
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	
	//Type in the search field
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
	
	// Click the Retreive button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Check that retrieved records do contain the searched record
	if (recordsList.isEmpty()) assertTrue(false);
	
	boolean equalResult = true;
	for (String s : recordsList) {
		 String[] removeLeft = s.split("}");
   		 String[] removeRight = removeLeft[1].split("~");
   		 
   		 
		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
	       	else {
	       		equalResult = false;
	           break;
	       	}
	    }
	
	LogUltility.log(test, logger, "Is new record found: " + equalResult);
	assertTrue(equalResult);

	LogUltility.log(test, logger, "Test Case PASSED");
}
	



/**
 * DIC_TC--393_Part2: Check creating a new compound using underscore
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_393_Part2_Check_creating_a_new_compound_using_underscore() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_393_Part2", "Check creating a new compound using underscore");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText="";
	if (Setting.Language.equals("EN"))
		randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomString(5);
	else if (Setting.Language.equals("HE"))
		randomText= CurrentPage.As(CompoundsTab.class).RandomHEString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomHEString(5);

	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	Random randomizer = new Random();
	String randompos = posList.get(randomizer.nextInt(posList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// Search for the new added record
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	
	//Type in the search field
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
	
	// Click the Retreive button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Check that retrieved records do contain the searched record
	if (recordsList.isEmpty()) assertTrue(false);
	
	boolean equalResult = true;
	for (String s : recordsList) {
		 String[] removeLeft = s.split("}");
   		 String[] removeRight = removeLeft[1].split("~");
   		 
   		 
		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
	       	else {
	       		equalResult = false;
	           break;
	       	}
	    }
	
	LogUltility.log(test, logger, "Is new record found: " + equalResult);
	assertTrue(equalResult);

	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	
	
	
	
	
/**
 * DIC_TC--396:Check the dictionary index (Lemma) in compounds, pointers to dictionary words array selection
 * @throws InterruptedException 
 * @throws IOException 
 * @throws AWTException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_396_Check_the_dictionary_index_Lemma_in_compounds_pointers_to_dictionary_words_array_selection() throws InterruptedException, IOException, AWTException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_396", "Check the dictionary index (Lemma) in compounds, pointers to dictionary words array selection");
	
	if (Setting.Language.equals("HE")) {
		LogUltility.log(test, logger, "This Test was skipped because Hebrew records doesn`t have semantic relations ");
		return;
	}
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	//Click the compounds tab
	CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
	
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "interjection";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//		LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
	
	// Pick a random record  EN{j}Christ_'s_sake~0
//	Random randomizer = new Random();
//	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	
	String randomrecord = null;
	if (Setting.Language.equals("EN"))
		randomrecord = "EN{j}Christ_'s_sake~0";
	else if (Setting.Language.equals("HE"))
		randomrecord = "HE{j}אין_לו_מושג_ירוק~0";
	
	//String randomrecord = "EN{j}Christ_'s_sake~0";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
	
    // get the first word
	String[] firstWord = removeLeft[1].split("_");
	LogUltility.log(test, logger, "First word from the randomrecord : " + firstWord[0]);
	
	// Get selected Lemma from the dropdowns
	String firstLemma = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 0);
	LogUltility.log(test, logger, "the value that selected in the first Lemma dropdown :  " + firstLemma);
	String secondLemma = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 1);
	LogUltility.log(test, logger, "the value that selected in the second Lemma dropdown :  " + secondLemma);
	
	// Get values from the Lemma dropdowns 
	List<String> lemmaValues1 = CurrentPage.As(CompoundsTab.class).getTaggingValues("Lemma", 0);
	LogUltility.log(test, logger, "Lemma values from the first dropdown : " + lemmaValues1);
	List<String> lemmaValues2 = CurrentPage.As(CompoundsTab.class).getTaggingValues("Lemma", 1);
	LogUltility.log(test, logger, "Lemma values from the second dropdown : " + lemmaValues2);
	
	// Create list that contain only the keyforms
	List<String> keyForms = new ArrayList<String>();
	for (String value : lemmaValues1) {
		if (value.isEmpty()) continue;
		keyForms.add(value.split(" ")[0]);
		
	}
	// get the keyforms list
	LogUltility.log(test, logger, "Lemma values names only from the first dropdown : " + keyForms);
	
	// Navigate to Dictionary tab and search for the first word
	// Focus on dictionary tab
//	CurrentPage = GetInstance(DictionaryTab.class);
//	CurrentPage.As(DictionaryTab.class).tabDictionary.click();
	
	CurrentPage.As(CompoundsTab.class).dictionaryTab.click();
	
	// Choose "spellings" from the first search dropdown
	CurrentPage = GetInstance(DictionaryTab.class);
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "form is chosen in the first search dropdown");
	
	// Choose "contains" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
	
	//Type in the search field
	String searchWord = firstWord[0];
	CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(searchWord);
	LogUltility.log(test, logger, "word is typed in the search field:" +searchWord);
	
	// Click the Retreive button
	CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList1 = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived: " + recordsList1);
	
	// verify that the list from the Dictionary include list from the Lemma
	Collections.sort(recordsList1);
	Collections.sort(keyForms);
	boolean sameList = keyForms.containsAll(recordsList1);
	//boolean sameList = recordsList1.equals(keyForms);
	LogUltility.log(test, logger, "The list from the dictionary: " +keyForms +  " contains the list from the Lemma: "  +recordsList1 + " and the value shhould be true : " + sameList);
	assertTrue(sameList);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	
	
	


/**
* DIC_TC--397:Check the form type in compounds, pointers to dictionary words array selection box
* @throws InterruptedException 
* @throws IOException 
* @throws AWTException 
*/
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_397_Check_the_form_type_in_compounds_pointers_to_dictionary_words_array_selection_box() throws InterruptedException, IOException, AWTException
{
//Thread.sleep(5000);
test = extent.createTest("Compounds Tests - DIC_TC_397", "Check the form type in compounds, pointers to dictionary words array selection box");

//String var="STD~איפ(ו)ה";
//
//boolean result= (var.contains("("));
//boolean result1= (var.contains("[(]"));
//LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + result + " and:"+result1);

// Focus on Compounds tab
CurrentPage = GetInstance(CompoundsTab.class);
CurrentPage.As(CompoundsTab.class).tabcompounds.click();

//Click the compounds tab
CurrentPage.As(CompoundsTab.class).CompoundsTab.click();

//
//String firstLemma1 = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 0);
//LogUltility.log(test, logger, "the value that selected in the first Lemma dropdown :  " + firstLemma1);
//String secondLemma1 = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 1);
//LogUltility.log(test, logger, "the value that selected in the second Lemma dropdown :  " + secondLemma1);
//
//
//


// Choose a value from Volume[POS] dropdown
String volumePOS = "interjection";
CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

// Retrieve records
List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);

// Choose random record from the middle of the displayed list
recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

// Pick a random record
//Random randomizer = new Random();
//String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));

String randomrecord =  null;
if (Setting.Language.equals("EN"))
	randomrecord = "EN{j}holy_mackerel~0";
else if (Setting.Language.equals("HE"))
	randomrecord = "HE{j}איפה_טעינו~0";

//String randomrecord ="EN{j}holy_mackerel~0";
CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

// get a clean records list
String[] removeLeft = randomrecord.split("}");
String[] removeRight = removeLeft[1].split("~");
LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);

// get the first word
String[] firstWord = removeLeft[1].split("_");
LogUltility.log(test, logger, "First word from the randomrecord : " + firstWord[0]);

// Get selected Form Type from the dropdowns
String firstLemma = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 0);
LogUltility.log(test, logger, "the value that selected in the first Lemma dropdown :  " + firstLemma);
String secondLemma = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 1);
LogUltility.log(test, logger, "the value that selected in the second Lemma dropdown :  " + secondLemma);

//// First Lemma as a clean dictionary record
//String searchedWord= firstLemma.split("(")[0];
//LogUltility.log(test, logger, "the clean search word : " + searchedWord);

// Get values from the Form Type dropdowns 
List<String> formTypeValues1 = CurrentPage.As(CompoundsTab.class).getTaggingValues("Form Type", 0);
formTypeValues1.remove(0);
LogUltility.log(test, logger, "Form Type values from the first dropdown : " + formTypeValues1);
List<String> formTypeValues2 = CurrentPage.As(CompoundsTab.class).getTaggingValues("Form Type", 1);
formTypeValues2.remove(0);
LogUltility.log(test, logger, "Form Type values from the second dropdown : " + formTypeValues2);

//// Create list that contain only the keyforms
//List<String> keyForms = new ArrayList<>();
//for (String value : formTypeValues1) {
//	if (value.isEmpty()) continue;
//	keyForms.add(value.split(" ")[0]);
//	
//}
//// get the keyforms list
//LogUltility.log(test, logger, "Form Type values from the Form Type dropdown : " + keyForms);

// Navigate to Dictionary tab and search for the first word
// Focus on dictionary tab
//CurrentPage = GetInstance(DictionaryTab.class);
//CurrentPage.As(DictionaryTab.class).tabDictionary.click();

CurrentPage.As(CompoundsTab.class).dictionaryTab.click();

// Choose "form" from the first search dropdown
CurrentPage = GetInstance(DictionaryTab.class);
CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
LogUltility.log(test, logger, "from is chosen in the first search dropdown");

// Choose "equal" from the condition dropdown
CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
LogUltility.log(test, logger, "equal is chosen in the condition dropdown");

//Type in the search field
String searchWord = firstWord[0];
CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(searchWord);
LogUltility.log(test, logger, "word is typed in the search field:" +searchWord);

// Click the Retreive button
CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
LogUltility.log(test, logger, "Click the Retreive button");

//// Choose random record from the middle of the displayed list
//recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

// Pick a random record
//Random randomizer1 = new Random();
String randomrecord1 = firstLemma;
CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord1);
LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);

List<String> lst_Forms = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).Forms_list);
LogUltility.log(test, logger, "Forms retreived: " + lst_Forms);


boolean sameWord = false;
for (String form : formTypeValues1) {
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).Forms_list, form);
	List<String> spellingValues = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
	for (String spelling : spellingValues) {
		String removeLeftPart = spelling.split("~")[1];
		String leftSide = "";
		String RightSide= "";
		if (removeLeftPart.contains("(")) {
			 leftSide= removeLeftPart.split("\\(")[0];
		     RightSide= removeLeftPart.split("\\)")[1];
		     removeLeftPart= leftSide+RightSide;
		     }
		sameWord = removeLeftPart.equals(firstWord[0]);
		LogUltility.log(test, logger, "compare the word:"  + firstWord[0] + " with the selected spelling : "+ removeLeftPart+ " and the value should be true : "  + sameWord);
		if (sameWord) break;
	}
	assertTrue(sameWord);
	
}

////// Retrieve records
////List<String> recordsList1 = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
////LogUltility.log(test, logger, "Records retreived: " + recordsList1);
//
//// verify that the list from the Lemma is the same as from the Dictionary 
//boolean sameFormsList = formTypeValues1.equals(lst_Forms);
//assertTrue(sameFormsList);
//LogUltility.log(test, logger, "The list from the dictionary: " +formTypeValues1 +  " equals to the list from the Lemma: "  +lst_Forms + " and the value shhould be true : " + sameFormsList);

LogUltility.log(test, logger, "Test Case PASSED");
}
	
	


/**
* DIC_TC--398:Check the POS in compounds, pointers to dictionary words array selection box
* @throws InterruptedException 
* @throws IOException 
* @throws AWTException 
*/
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_398_Check_the_POS_in_compounds_pointers_to_dictionary_words_array_selection_box() throws InterruptedException, IOException, AWTException
{
//Thread.sleep(5000);
test = extent.createTest("Compounds Tests - DIC_TC_398", "Check the POS in compounds, pointers to dictionary words array selection box");

if (Setting.Language.equals("HE")) {
	LogUltility.log(test, logger, "This Test was skipped because Hebrew records doesn`t have semantic relations ");
	return;
}

// Focus on Compounds tab
CurrentPage = GetInstance(CompoundsTab.class);
CurrentPage.As(CompoundsTab.class).tabcompounds.click();


//
//String firstLemma1 = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 0);
//LogUltility.log(test, logger, "the value that selected in the first Lemma dropdown :  " + firstLemma1);
//String secondLemma1 = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 1);
//LogUltility.log(test, logger, "the value that selected in the second Lemma dropdown :  " + secondLemma1);
//
//
//


// Choose a value from Volume[POS] dropdown
String volumePOS = "preposition";
CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

// Retrieve records
List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);

// Choose random record from the middle of the displayed list
recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

// Pick a random record
Random randomizer = new Random();
String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
//String randomrecord ="EN{j}holy_mackerel~0";
CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

// get a clean records list
String[] removeLeft = randomrecord.split("}");
String[] removeRight = removeLeft[1].split("~");
LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);

// get the first word
String[] firstWord = removeLeft[1].split("_");
LogUltility.log(test, logger, "First word from the randomrecord : " + firstWord[0]);

// Get selected Lemma from the dropdowns
String firstLemma = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 0);
LogUltility.log(test, logger, "the value that selected in the first Lemma dropdown :  " + firstLemma);
String secondLemma = CurrentPage.As(CompoundsTab.class).getSelectedTaggingValue("Lemma", 1);
LogUltility.log(test, logger, "the value that selected in the second Lemma dropdown :  " + secondLemma);

//// First Lemma as a clean dictionary record
//String searchedWord= firstLemma.split("(")[0];
//LogUltility.log(test, logger, "the clean search word : " + searchedWord);

// Get values from the POS dropdowns 
List<String> POSValues1 = CurrentPage.As(CompoundsTab.class).getTaggingValues("POS", 0);
POSValues1.remove(0);
LogUltility.log(test, logger, "POS values from the first dropdown : " + POSValues1);
List<String> POSValues2 = CurrentPage.As(CompoundsTab.class).getTaggingValues("POS", 1);
POSValues2.remove(0);
LogUltility.log(test, logger, "POS values from the second dropdown : " + POSValues2);

// Convert the POS list to letters


//Convert the POS list to letters
List<String> cleanLettersCompounds = new ArrayList<String>();
for (String POS : POSValues1) {
	;
	cleanLettersCompounds.add(CurrentPage.As(CompoundsTab.class).POSConverter(POS));
}
LogUltility.log(test, logger, "POS values converted to letters for the first POS dropdown : " + cleanLettersCompounds);

// Sort the list 
Collections.sort(cleanLettersCompounds);
LogUltility.log(test, logger, "POS values converted to letters after sort : " + cleanLettersCompounds);

//// Create list that contain only the keyforms
//List<String> keyForms = new ArrayList<>();
//for (String value : formTypeValues1) {
//	if (value.isEmpty()) continue;
//	keyForms.add(value.split(" ")[0]);
//	
//}
//// get the keyforms list
//LogUltility.log(test, logger, "Form Type values from the Form Type dropdown : " + keyForms);

// Navigate to Dictionary tab and search for the first word
// Focus on dictionary tab
//CurrentPage = GetInstance(DictionaryTab.class);
//CurrentPage.As(DictionaryTab.class).tabDictionary.click();

CurrentPage.As(CompoundsTab.class).dictionaryTab.click();
LogUltility.log(test, logger, "Navigate to the Dictionary tab");
// Choose "form" from the first search dropdown
CurrentPage = GetInstance(DictionaryTab.class);
CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_field, "form");
LogUltility.log(test, logger, "form is chosen in the first search dropdown");

// Choose "equal" from the condition dropdown
CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpCondition_operation, "equal");
LogUltility.log(test, logger, "equal is chosen in the condition dropdown");

//Type in the search field
String searchWord = firstWord[0];
CurrentPage.As(DictionaryTab.class).cmpCondition_value.sendKeys(searchWord);
LogUltility.log(test, logger, "word is typed in the search field:" +searchWord);

// Click the Retreive button
CurrentPage.As(DictionaryTab.class).btnRetrieve_same.click();
LogUltility.log(test, logger, "Click the Retreive button");

//Retrieve records
List<String> recordsList1 = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
//Get only the first visible 40 records ONLY IN ORDER TO MAKE THE TEST FINISH FAST
//recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
LogUltility.log(test, logger, "Records retreived: " + recordsList1);
List<String> POScodesDictionary = new ArrayList<>();
for (String record : recordsList1) {
	// get a clean records list
		String[] removeFirst = record.split("\\{");
		LogUltility.log(test, logger, "Records retreived: " + removeFirst);
		String[] removeSecond = removeFirst[1].split("}");
		LogUltility.log(test, logger, "Records retreived: " + removeSecond);
		POScodesDictionary.add(removeSecond[0]);
		LogUltility.log(test, logger, "Clean record after splliting the randomrecord, get the following letters : " + removeSecond[0]);
}

//Get the POS letters for the retrieved records
//Sort the list
Collections.sort(POScodesDictionary);
LogUltility.log(test, logger, "From the retrived records get only the POS letter Sorted  : " + POScodesDictionary);
Set<String> uniquePOSCodesDictionary = new TreeSet<String>(POScodesDictionary); 
LogUltility.log(test, logger, "From the retrived records get only the POS letter sorted A-Z  : " + uniquePOSCodesDictionary);

//Sort the list
//Collections.EMPTY_LIST(POScodesDictionary);
//POScodesDictionary = Collections.<String>emptyList();
//POScodesDictionary.addAll(uniquePOSCodesDictionary);
Iterator<String> iter = POScodesDictionary.iterator();
while (iter.hasNext()){
	iter.next();
	iter.remove();
}
//for (String str : POScodesDictionary)  
//	POScodesDictionary.remove(str);
for (String str : uniquePOSCodesDictionary)  
	POScodesDictionary.add(str);

boolean sameLetters= POScodesDictionary.equals(cleanLettersCompounds);
LogUltility.log(test, logger, "compare the letters form compound:"  + cleanLettersCompounds + " with the letters from dictionary : "+ POScodesDictionary+ " and the value should be true : "  + sameLetters);
assertTrue(sameLetters);

LogUltility.log(test, logger, "Test Case PASSED");
}
	


////// Choose random record from the middle of the displayed list
////recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
//
//// Pick a random record
////Random randomizer1 = new Random();
//String randomrecord1 = firstLemma;
//CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomrecord1);
//LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
//
//List<String> lst_Forms = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).Forms_list);
//LogUltility.log(test, logger, "Forms retreived: " + lst_Forms);
//
//
//boolean sameWord = false;
//for (String form : POSValues1) {
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).Forms_list, form);
//	List<String> spellingValues = CurrentPage.As(DictionaryTab.class).getSpellinigValues();
//	for (String spelling : spellingValues) {
//		sameWord = spelling.split("~")[1].equals(firstWord[0]);
//		LogUltility.log(test, logger, "compare the word:"  + firstWord[0] + " with the selected spelling : "+ spelling.split("~")[1]+ " and the value should be true : "  + sameWord);
//		if (sameWord) break;
//	}
//	assertTrue(sameWord);
//	
//}

////// Retrieve records
////List<String> recordsList1 = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
////LogUltility.log(test, logger, "Records retreived: " + recordsList1);
//
//// verify that the list from the Lemma is the same as from the Dictionary 
//boolean sameFormsList = formTypeValues1.equals(lst_Forms);
//assertTrue(sameFormsList);
//LogUltility.log(test, logger, "The list from the dictionary: " +formTypeValues1 +  " equals to the list from the Lemma: "  +lst_Forms + " and the value shhould be true : " + sameFormsList);


/**
 * DIC_TC--406:Check clicking the new button several times then saving
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_406_Check_clicking_the_new_button_several_times_then_saving() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_406", "Check clicking the new button several times then saving");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Click the New button again
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button for the second time");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button for the second time");
	
	//Verify that the "Bad Key" warning appear 
	boolean badKeyWarning = CurrentPage.As(CompoundsTab.class).newTwice.isDisplayed();
	LogUltility.log(test, logger, "verify that the bad key warning displayed, and the value should be True : " + badKeyWarning);
	assertTrue(badKeyWarning);
	
	//Press the cancel button on the bad key warning: cancelBadKey
	LogUltility.log(test, logger, "Click the Cancel in the bad key warning");
	CurrentPage.As(CompoundsTab.class).cancelBadKey.click();
	
	//Verify that the "Bad Key" warning closed
	boolean badKeyWarningClosed = CurrentPage.As(CompoundsTab.class).checkNoPopupWindow();
	LogUltility.log(test, logger, "verify that the bad key warning closed, and no more poups appear, and the value should be True : " + badKeyWarningClosed);
	assertTrue(badKeyWarningClosed);
	
	LogUltility.log(test, logger, "Test Case PASSED");
}
	




/**
 * DIC_TC--431:Click in an empty record box should not give exception 
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_431_Click_in_an_empty_record_box_should_not_give_exception() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("compounds Search Test - DIC_TC_431", "Click in an empty record box should not give exception");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
	
	// Focus on compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose "Form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "Form is chosen in the first search dropdown");
	
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, " Contains is chosen in the condition dropdown");
	
	//Select Value from the dropdown 
	//String randomForm = "coal";
	//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
	//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomForms);
	//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
	String randomText = CurrentPage.As(CompoundsTab.class).RandomString(12);
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
	LogUltility.log(test, logger, "chosen search value: " + randomText);
	
	// Click the Retrieve button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	
//	boolean isWindowDisplayed = CurrentPage.As(CompoundsTab.class).window_too_many_records.isDisplayed();
//	if (isWindowDisplayed) CurrentPage.As(CompoundsTab.class).window_too_many_records_ok.click();
	
	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
	// Check that retrieved records do contain the searched record
	if (!recordsList.isEmpty()) assertTrue(false);
	LogUltility.log(test, logger, "Result list should be empty: " + recordsList);
	
	// Click on the empty results area
	CurrentPage.As(CompoundsTab.class).lst_records.click();
	LogUltility.log(test, logger, "Click on the empty list" );
	
	// Verify that no errrors or any popup appear
	boolean badKeyWarningClosed = CurrentPage.As(CompoundsTab.class).checkNoPopupWindow();
	LogUltility.log(test, logger, "verify that No errors appear, and the value should be True : " + badKeyWarningClosed);
	assertTrue(badKeyWarningClosed);

	LogUltility.log(test, logger, "Test Case PASSED");	
}




/**
 * DIC_TC--508:Check editing the form then clicking the record box then saving
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_508_Check_editing_the_form_then_clicking_the_record_box_then_saving() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_256", "Verify user can edit key form in Compounds");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	

	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Choose random record from the middle of the displayed list
	recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get a clean records list
    
	String[] removeLeft = randomrecord.split("}");
	String[] removeRight = removeLeft[1].split("~");
	
	
	// add random text in the key form field
	String randomText = CurrentPage.As(CompoundsTab.class).RandomHEString(5)+ removeRight[0];
	//CurrentPage.As(CompoundsTab.class).txtKeyForm.click();
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	//Get the new edited Key Form
	String editedKeyForm=  CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
	LogUltility.log(test, logger, "New record after adding Text to the Key Form : " + editedKeyForm);
	
	//Press on the Definition box
	CurrentPage.As(CompoundsTab.class).lst_Definition.click();
	LogUltility.log(test, logger, "Click on the Definition box");
	
	Thread.sleep(3000);
	
	// click the OK button on the change key form popup
	CurrentPage.As(CompoundsTab.class).yesKeyFieldMessage.click();
	LogUltility.log(test, logger, "Click yes button on the change key form popup ");
	
//	//Press the Save button
//	CurrentPage.As(CompoundsTab.class).btnSave.click();
//	LogUltility.log(test, logger, "Click Save button ");
//	
//    // click the OK button in the saving popupyesKeyFieldMessage
//    CurrentPage.As(CompoundsTab.class).btnOKSave.click();
//    LogUltility.log(test, logger, "Click ok on the Saving popup ");
	

	// Search for the edited record
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "form is chosen in the first search dropdown");
	
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "Equal is chosen in the condition dropdown");
	
	//Type in the search field
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(editedKeyForm);
	LogUltility.log(test, logger, "word is typed in the search field: " + editedKeyForm);
	
	// Click the Retreive button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList1 = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived: " + recordsList1);
	
	// get a clean records list
	
	// get a clean records list
	List<String> recordsList2 = new ArrayList<>();
	  for (String cleanRecord : recordsList1) {
	    	String[] removeLeft1 = cleanRecord.split("}");
			String[] removeRight1 = removeLeft1[1].split("~");
			recordsList2.add(removeRight1[0]);
	  }
			LogUltility.log(test, logger, "New clean record list: " + recordsList2);
	  

	// Check that retrieved records do contain the searched record
    boolean equalResult = recordsList2.contains(editedKeyForm);
	LogUltility.log(test, logger, "Is edited record found? should be True: " + equalResult);
	assertTrue(equalResult);

	
//	//Check the changes done on the Compounds file
//	
//	List<String> recordsFromFile =  CurrentPage.As(CompoundsTab.class).getRecordsFromcompounds();
//	String editedKeyFormClean = editedKeyForm.replace("_", " ");
//	editedKeyFormClean = editedKeyFormClean.replace("-", " ");
//	LogUltility.log(test, logger, "change the: " + editedKeyForm + " ,to the new clean without the _ or - : " + editedKeyFormClean);
////	Thread.sleep(5000);
//	boolean newRecordexist = recordsFromFile.contains(editedKeyFormClean);
//	LogUltility.log(test, logger, "Is the new record exist in the Compounds.txt file after editing the record? must be True: " + newRecordexist);
//	assertTrue(newRecordexist);

	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	
/**
 * DIC_TC--512:check creating new records and using the arrows right after
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_512_check_creating_new_records_and_using_the_arrows_right_after() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_512", "check creating new records and using the arrows right after");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText = CurrentPage.As(CompoundsTab.class).RandomHEString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomHEString(5);
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
//	// Get random POS
//	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
//	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
//	Random randomizer = "adverb";
//	//Random randomizer = new Random();
//	String randompos = posList.get(randomizer.nextInt(posList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, "adverb");
	LogUltility.log(test, logger, "Choose from pos list: adverb");
	
	// click the save button
	CurrentPage.As(CompoundsTab.class).btnSave.click();
	LogUltility.log(test, logger, "Click Save button ");
	
	
	// click the OK button in the saving popup
	CurrentPage.As(CompoundsTab.class).btnOKSave.click();
	LogUltility.log(test, logger, "Click ok on the Saving popup ");
	
	// wait until record list refreshed 
	Thread.sleep(5000);
	
	// Get the selected record in the results area
	List<String> chosenRecord= CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "The selected Record is : " + chosenRecord);
	
	LogUltility.log(test, logger, "Failed because opened bug ");
	
//	// Focus on Compounds tab
//	CurrentPage = GetInstance(CompoundsTab.class);
//	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
//	
	//Press the arrows
	// Click the Right arrow
	 CurrentPage.As(CompoundsTab.class).Next_button.click();
	 CurrentPage.As(CompoundsTab.class).Next_button.click();
	 LogUltility.log(test, logger, "Click the Next arrow");
	// Get the selected record in the results area after press the next button
	List<String> chosenRecordNext= CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "The selected Record after press the next arrow is: " + chosenRecordNext);
	 boolean nextRecord = !chosenRecord.equals(chosenRecordNext);
	 assertTrue(nextRecord);
	 LogUltility.log(test, logger, "The selected Record before press the next button is : "+ chosenRecord +" and the new one after press the next arrow is: " + chosenRecordNext+  " and the value should be true :" +nextRecord);
	// Click the Left arrow
	 CurrentPage.As(CompoundsTab.class).Previous_button.click();
	 LogUltility.log(test, logger, "Click the Previous arrow");
	 //Get the selected record in the results area after press the Previous button
	 List<String> chosenRecordPrevious= CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records);
	 LogUltility.log(test, logger, "The selected Record after press the Previous arrow is: " + chosenRecordPrevious);
	 boolean previousRecord = !chosenRecordPrevious.equals(chosenRecordNext);
	 assertTrue(previousRecord);
	 LogUltility.log(test, logger, "The selected Record before press the next button is : "+ chosenRecordPrevious +" and the new one after press the next arrow is: " + chosenRecordNext+  " and the value should be true :" +previousRecord);
	
	
	
//	// Search for the new added record
//	// Choose "form" from the first search dropdown
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
//	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//	
//	// Choose "equal" from the condition dropdown
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
//	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
//	
//	//Type in the search field
//	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
//	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
//	
//	// Click the Retreive button
//	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
//	LogUltility.log(test, logger, "Click the Retreive button");
//	
//	// Retrieve records
//	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
//	
//	// Check that retrieved records do contain the searched record
//	if (recordsList.isEmpty()) assertTrue(false);
//	
//	boolean equalResult = true;
//	for (String s : recordsList) {
//		 String[] removeLeft = s.split("}");
//   		 String[] removeRight = removeLeft[1].split("~");
//   		 
//   		 
//		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
//	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
//	       	else {
//	       		equalResult = false;
//	           break;
//	       	}
//	    }
//	
//	LogUltility.log(test, logger, "Is new record found: " + equalResult);
//	assertTrue(equalResult);

	LogUltility.log(test, logger, "Test Case PASSED");
}

	
/**
 * DIC_TC--515:check look in corpus in new versions of existing records
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_515_check_look_in_corpus_in_new_versions_of_existing_records() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_515", "check look in corpus in new versions of existing records");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	//New record we plan to add
			String randomText="";
			if (Setting.Language.equals("EN"))
				randomText = "a_lot";
			else if (Setting.Language.equals("HE"))
				randomText= "אין_סופי";

			CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
			LogUltility.log(test, logger, "New record: " + randomText);
//	String randomText = "אין_סופי";
	
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
//	List<String> allFoundRecords = new ArrayList<>();
	int version=0;
	for (String record : recordsList) {
		boolean foundRecord= CurrentPage.As(CompoundsTab.class).getOnlyRecordName(record).equals(randomText.replace("_", " "));
		if(foundRecord){
			String[] splittedRecord = record.split("~");
			if(Integer.parseInt(splittedRecord[1]) > version)
				version= Integer.parseInt(splittedRecord[1]);
		}
		
	}
	
	LogUltility.log(test, logger, "the version value is: " + version);
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type an exist record name in the key form field
	//String randomText = CurrentPage.As(CompoundsTab.class).RandomString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomString(5);
	//String randomText = "a_few";
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record:" + randomText);
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick the same POS for the exist record
	//Random randomizer = new Random();
	//String randompos = posList.get(randomizer.nextInt(posList.size()));
	String randompos = ("adjective");
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// Click Yes on the popup 
	CurrentPage.As(CompoundsTab.class).btnYesDuplicate.click();
	
////	// Search for the new added record
////	// Choose "form" from the first search dropdown
////	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
////	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
////	
////	// Choose "equal" from the condition dropdown
////	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
////	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
////	
////	//Type in the search field
////	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys("a_few");
////	LogUltility.log(test, logger, "word is typed in the search field:  a_few");
////	
////	// Click the Retreive button
////	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
////	LogUltility.log(test, logger, "Click the Retreive button");
//	
//	// Choose a value from Volume[POS] dropdown
//	//String volumePOS = "adjective";
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
//	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
//	
//	// Retrieve records
//	List<String> newRecordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + newRecordsList);
//	
//	boolean found=false;
//	version+=1;
//	for (String record : newRecordsList) {
//		boolean foundRecord= CurrentPage.As(CompoundsTab.class).getOnlyRecordName(record).equals(randomText.replace("_", " "));
//		if(foundRecord){
//			String[] splittedRecord = record.split("~");
//			if(Integer.parseInt(splittedRecord[1]) == version){
//				found = true;
//				break;
//			}
//			else found = false;
//				
//		}
//		
//	}
//	LogUltility.log(test, logger, "the version value is: " + version);
//	LogUltility.log(test, logger, "Does new record list contain the duplicated record? should be True: " + found);
//	assertTrue(found);
	//Click the Look in Corpus button
	CurrentPage.As(CompoundsTab.class).btnLookCorpus.click();
	LogUltility.log(test, logger, "Click the Look in Corpus button");
	CurrentPage.As(CompoundsTab.class).lookPopupYes.click();
	LogUltility.log(test, logger, "Click the yes button in the Look in Corpus message");
	LogUltility.log(test, logger, "Test Case PASSED");

}

	

/**
 * DIC_TC--519:check specific scenario of adding records
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_519_check_specific_scenario_of_adding_records() throws InterruptedException, IOException
{
		//Thread.sleep(5000);
		test = extent.createTest("Compounds Tests - DIC_TC_382-Part3", "Check the new button in specific scenario - yes button");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "interjection";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//			LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// get a clean records list
		String[] removeLeft = randomrecord.split("}");
		String[] removeRight = removeLeft[1].split("~");
		LogUltility.log(test, logger, "Clean record after splliting the randomrecord : " + removeRight[0]);
		
		// Click the new button
		CurrentPage.As(CompoundsTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(CompoundsTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText = "אל_תשאל";
		CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
		LogUltility.log(test, logger, "New record: " + randomText);
		
		// Get the  record version
		String version1 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
		LogUltility.log(test, logger, " First Record version is : " + version1);
		
		// Get random POS
		List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList);
		
		// Pick a random pos
		//Random randomizer1 = new Random();
		String randompos = "interjection";
//		while(randompos == "unknown"){
//			randompos = posList.get(randomizer.nextInt(posList.size()));
//		}
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos);
		
		
		// Verify the Similar key exists message appear
		boolean similarPopup = CurrentPage.As(CompoundsTab.class).similarKeyMessage.isDisplayed();
		LogUltility.log(test, logger, "The Similar key exists message displayed, Value should be True : "+similarPopup);
		

	//	
		// Press the No button in the similar popup
		Thread.sleep(2000);
		CurrentPage.As(CompoundsTab.class).noSimilarKeyMessage.click();
		LogUltility.log(test, logger, "Click the No button on the Similar message.");
		
		// Verify that the record version not changed
		String version2 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
		LogUltility.log(test, logger, " New Record version is : " + version2);
		boolean sameVersion = version2.equals(version1);
		assertTrue(sameVersion);
		LogUltility.log(test, logger, "The version before Clicking the NO button was : "  + version1+  "  and after clicking the NO button still the same : " + version2+ "  and the value should be True : "  +sameVersion);

		
		//Verify that the Key form field doesn`t changed
		String lastValue = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
		LogUltility.log(test, logger, "The Key form field returned with the last value : " + lastValue);
		//check that the same value entered by user still displayed in the Key form field 
		boolean sameKeyForm = lastValue.equals(randomText);
		
		
		LogUltility.log(test, logger, "The same value entered by User " + randomText+ " displayed in the Key form field as the new one : " + lastValue+" and the value should be true:  " +sameKeyForm);
		
        // Click the new button
		CurrentPage.As(CompoundsTab.class).btnNew.click();
		LogUltility.log(test, logger, "Click the new button");
		
		// Click yes to the confirmation button
		CurrentPage.As(CompoundsTab.class).btnYes.click();
		LogUltility.log(test, logger, "Click the Yes button");
		
		// Type a record name in the key form field
		String randomText2 = "אל_תשאל";
		CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText2);
		LogUltility.log(test, logger, "New record: " + randomText2);
		
		// Get the  record version
		String version3 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
		LogUltility.log(test, logger, " First Record version is : " + version3);
		
		// Get random POS
		List<String> posList1 = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
		LogUltility.log(test, logger, "POS list: " + posList1);
		
		// Pick a random pos
		//Random randomizer1 = new Random();
		String randompos1 = "interjection";
//				while(randompos == "unknown"){
//					randompos = posList.get(randomizer.nextInt(posList.size()));
//				}
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos1);
		LogUltility.log(test, logger, "Choose from pos list: " + randompos1);
		
		
		// Verify the Similar key exists message appear
		boolean similarPopup2 = CurrentPage.As(CompoundsTab.class).similarKeyMessage.isDisplayed();
		LogUltility.log(test, logger, "The Similar key exists message displayed, Value should be True : "+similarPopup2);
		

	//	
		// Press the Cancel button in the similar popup
		Thread.sleep(2000);
		CurrentPage.As(CompoundsTab.class).cancelSimilarKeyMessage.click();
		LogUltility.log(test, logger, "Click the Cancel button on the Similar message.");
		
		// Verify that the record version not changed
		String version4 = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
		LogUltility.log(test, logger, " New Record version is : " + version4);
		boolean sameVersion1 = version4.equals(version3);
		assertTrue(sameVersion1);
		LogUltility.log(test, logger, "The version before Clicking the Cancel button was : "  + version3+  "  and after clicking the Cancel button still the same : " + version4+ "  and the value should be True : "  +sameVersion1);

		
		//Verify that the Key form field doesn`t changed
		String lastValue2 = CurrentPage.As(CompoundsTab.class).txtKeyForm.getText();
		LogUltility.log(test, logger, "The Key form field returned with the last value : " + lastValue2);
		//check that the same value entered by user still displayed in the Key form field 
		boolean sameKeyForm2 = ! lastValue2.equals(randomText2);
		assertTrue(sameKeyForm2);
		LogUltility.log(test, logger, "The value entered by User " + randomText2+ " Doesn`t displayed in the Key form field after cancelling the process : " + lastValue2+" and the value should be true "+sameKeyForm2);
		
		boolean noPopupAppear = CurrentPage.As(CompoundsTab.class).checkNoPopupWindow();
		assertTrue(noPopupAppear);
		LogUltility.log(test, logger, "all popups closed and no popup appear? should be true : " + noPopupAppear);
	
		
        LogUltility.log(test, logger, "Test Case PASSED");
     }
	
	
	

/**
 * DIC_TC--690:Check the message :Form not in right format
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_690_Check_the_message_Form_not_in_right_format() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_690", "Check the message :Form not in right format");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText = CurrentPage.As(CompoundsTab.class).RandomHEString(5);
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
//	// Get random POS
//	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
//	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
//	Random randomizer = "adverb";
//	//Random randomizer = new Random();
//	String randompos = posList.get(randomizer.nextInt(posList.size()));
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, "adverb");
//	LogUltility.log(test, logger, "Choose from pos list:  adverb");
//	
//	//Remove one of the words
//	// split the record
//	String[] removeLeft = randomText.split("_");
//	//String[] removeRight = removeLeft[1].split("_");
//	
//	//change the keyform with the first word only 
//	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(removeLeft[0]);
//	LogUltility.log(test, logger, "New record: " + randomText);
	
	
	// click  on the definition field  
	CurrentPage.As(CompoundsTab.class).lst_Definition.click();
	LogUltility.log(test, logger, "Click on the Definition field to verify synch ");
	
	
	// Verify that the "Form not in right format" popup
	boolean popupDisplayed = CurrentPage.As(CompoundsTab.class).popupWindowMessage(test, logger, "", "You entered only one word in the form. This should be a dictionary record!");
	LogUltility.log(test, logger, "Is the Form not in right format message displayed? the value should be True : " + popupDisplayed);
	assertTrue(popupDisplayed);
	
	// Press the OK button to close the "Form not in right format" popup
	LogUltility.log(test, logger, "Press the OK button on the popup ");
	CurrentPage.As(CompoundsTab.class).notRightFormatOK.click();
	LogUltility.log(test, logger, "Form not in right format popup closed ");
	
	//Select one of the results
	
//	CurrentPage.As(CompoundsTab.class).btnOKSave.click();
//	LogUltility.log(test, logger, "Click ok on the Saving popup ");
//	
//	// Get the selected record in the results area
//	List<String> chosenRecord= CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "The selected Record is : " + chosenRecord);
//	

	
//	// Search for the new added record
//	// Choose "form" from the first search dropdown
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
//	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//	
//	// Choose "equal" from the condition dropdown
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
//	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
//	
//	//Type in the search field
//	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
//	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
//	
//	// Click the Retreive button
//	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
//	LogUltility.log(test, logger, "Click the Retreive button");
//	
//	// Retrieve records
//	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
//	
//	// Check that retrieved records do contain the searched record
//	if (recordsList.isEmpty()) assertTrue(false);
//	
//	boolean equalResult = true;
//	for (String s : recordsList) {
//		 String[] removeLeft = s.split("}");
//   		 String[] removeRight = removeLeft[1].split("~");
//   		 
//   		 
//		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
//	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
//	       	else {
//	       		equalResult = false;
//	           break;
//	       	}
//	    }
//	
//	LogUltility.log(test, logger, "Is new record found: " + equalResult);
//	assertTrue(equalResult);
	 // search to get the regular screen
	
	//Press the cancel button on the bad key popup
	// Choose a value from Volume[POS] dropdown
	String volumePOS = "adjective";
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
	LogUltility.log(test, logger, "Bad key popup appear");
	CurrentPage.As(CompoundsTab.class).closeBadKey.click();
	//CurrentPage.As(CompoundsTab.class).cancelBadKey.click();
	LogUltility.log(test, logger, "Press the close button on the bad key popup");
	LogUltility.log(test, logger, "Test Case PASSED");
}



/**
 * DIC_TC--525:check when update record to wrong format
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_525_check_when_update_record_to_wrong_format() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_525", "check when update record to wrong format");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText = CurrentPage.As(CompoundsTab.class).RandomHEString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomHEString(5);
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
//	// Get random POS
//	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
//	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
//	Random randomizer = "adverb";
//	//Random randomizer = new Random();
//	String randompos = posList.get(randomizer.nextInt(posList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, "adverb");
	LogUltility.log(test, logger, "Choose from pos list:  adverb");
	
	//Remove one of the words
	// split the record
	String[] removeLeft = randomText.split("_");
	//String[] removeRight = removeLeft[1].split("_");
	
	//change the keyform with the first word only 
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(removeLeft[0]);
	LogUltility.log(test, logger, "New record: " + removeLeft[0]);
	
	
	// click  on the definition field  
	CurrentPage.As(CompoundsTab.class).lst_Definition.click();
	LogUltility.log(test, logger, "Click on the Definition field to verify synch ");
	
	
	// Verify that the "Form not in right format" popup
	boolean popupDisplayed = CurrentPage.As(CompoundsTab.class).popupWindowMessage(test, logger, "", "You entered only one word in the form. This should be a dictionary record!");
	LogUltility.log(test, logger, "Is the Form not in right format message displayed? the value should be True : " + popupDisplayed);
	assertTrue(popupDisplayed);
	
	
	
	// Press the OK button to close the "Form not in right format" popup
	// Press the OK button to close the "Form not in right format" popup
	CurrentPage.As(CompoundsTab.class).notRightFormatOK.click();
	LogUltility.log(test, logger, "Form not in right format popup closed ");
	
//	CurrentPage.As(CompoundsTab.class).btnOKSave.click();
//	LogUltility.log(test, logger, "Click ok on the Saving popup ");
//	
//	// Get the selected record in the results area
//	List<String> chosenRecord= CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "The selected Record is : " + chosenRecord);
//	

	
//	// Search for the new added record
//	// Choose "form" from the first search dropdown
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
//	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//	
//	// Choose "equal" from the condition dropdown
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
//	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
//	
//	//Type in the search field
//	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
//	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
//	
//	// Click the Retreive button
//	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
//	LogUltility.log(test, logger, "Click the Retreive button");
//	
//	// Retrieve records
//	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
//	LogUltility.log(test, logger, "Records retreived: " + recordsList);
//	
//	// Check that retrieved records do contain the searched record
//	if (recordsList.isEmpty()) assertTrue(false);
//	
//	boolean equalResult = true;
//	for (String s : recordsList) {
//		 String[] removeLeft = s.split("}");
//   		 String[] removeRight = removeLeft[1].split("~");
//   		 
//   		 
//		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
//	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
//	       	else {
//	       		equalResult = false;
//	           break;
//	       	}
//	    }
//	
//	LogUltility.log(test, logger, "Is new record found: " + equalResult);
//	assertTrue(equalResult);

	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	


/**
 * DIC_TC--683:Create new record with different steps part 1
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_683_Create_new_record_with_different_steps_part_1() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_683", "Create new record with different steps part 1");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Type a record name in the key form field
	String randomText = CurrentPage.As(CompoundsTab.class).RandomHEString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomHEString(5);
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	Random randomizer = new Random();
	String randompos = posList.get(randomizer.nextInt(posList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// Search for the new added record
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	
	//Type in the search field
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
	
	// Click the Retreive button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Check that retrieved records do contain the searched record
	if (recordsList.isEmpty()) assertTrue(false);
	
	boolean equalResult = true;
	for (String s : recordsList) {
		 String[] removeLeft = s.split("}");
   		 String[] removeRight = removeLeft[1].split("~");
   		 
   		 
		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
	       	else {
	       		equalResult = false;
	           break;
	       	}
	    }
	
	LogUltility.log(test, logger, "Is new record found: " + equalResult);
	assertTrue(equalResult);

	LogUltility.log(test, logger, "Test Case PASSED");
}
	



/**
 * DIC_TC--684:Create new record with different steps part 2
 * @throws InterruptedException 
 * @throws IOException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_684_Create_new_record_with_different_steps_part_2() throws InterruptedException, IOException
{
	//Thread.sleep(5000);
	test = extent.createTest("Compounds Tests - DIC_TC_684", "Create new record with different steps part 2");
	
	// Focus on Compounds tab
	CurrentPage = GetInstance(CompoundsTab.class);
	CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	// Click the new button
	CurrentPage.As(CompoundsTab.class).btnNew.click();
	LogUltility.log(test, logger, "Click the new button");
	
	// Click yes to the confirmation button
	CurrentPage.As(CompoundsTab.class).btnYes.click();
	LogUltility.log(test, logger, "Click the Yes button");
	
	// Get random POS
	List<String> posList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPOS);
	LogUltility.log(test, logger, "POS list: " + posList);
	
	// Pick a random pos
	Random randomizer = new Random();
	String randompos = posList.get(randomizer.nextInt(posList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPOS, randompos);
	LogUltility.log(test, logger, "Choose from pos list: " + randompos);
	
	// Type a record name in the key form field
	String randomText = CurrentPage.As(CompoundsTab.class).RandomHEString(5) + "_" + CurrentPage.As(CompoundsTab.class).RandomHEString(5);
	CurrentPage.As(CompoundsTab.class).txtKeyForm.sendKeys(randomText);
	LogUltility.log(test, logger, "New record: " + randomText);

	
	// Search for the new added record
	// Choose "form" from the first search dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
	LogUltility.log(test, logger, "from is chosen in the first search dropdown");
	
	// Choose "equal" from the condition dropdown
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
	LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
	
	//Type in the search field
	CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
	LogUltility.log(test, logger, "word is typed in the search field: " + randomText);
	
	// Click the Retreive button
	CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
	LogUltility.log(test, logger, "Click the Retreive button");
	
	// Retrieve records
	List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	// Check that retrieved records do contain the searched record
	if (recordsList.isEmpty()) assertTrue(false);
	
	boolean equalResult = true;
	for (String s : recordsList) {
		 String[] removeLeft = s.split("}");
   		 String[] removeRight = removeLeft[1].split("~");
   		 
   		 
		//System.out.println("s: " + s + " -- recordsList: " + recordsList);
	       if(removeRight[0].toLowerCase().equals(randomText)) continue;
	       	else {
	       		equalResult = false;
	           break;
	       	}
	    }
	
	LogUltility.log(test, logger, "Is new record found: " + equalResult);
	assertTrue(equalResult);

	LogUltility.log(test, logger, "Test Case PASSED");
}
	

		/**
		 * DIC_TC--219:Verify application mark the part of speech dropdown if the user created an exist record
		 * @throws InterruptedException 
		 * @throws AWTException 
		 * @throws IllegalArgumentException 
		 * @throws NoSuchFieldException 
		 * @throws IllegalAccessException 
		 * @throws SecurityException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_219_Verify_application_mark_the_part_of_speech_dropdown_if_the_user_created_an_exist_record() throws InterruptedException, AWTException, NoSuchFieldException, IllegalArgumentException, SecurityException, IllegalAccessException, IOException
		{
			test = extent.createTest("Corpus Tests - DIC_TC--219", "Verify application mark the part of speech dropdown if the user created an exist record");
			
			// Initialize current page
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			
			// Click compounds tab
			CompoundsTab.CompoundsTab.click();
			
			// Get records names with version zero
			List<String> records = CompoundsTab.getRecordsFromWithVersionZero();
			
			// Choose random record to search for
			Random randomizer = new Random();
			String searchValue = records.get(randomizer.nextInt(records.size()));

			// Search for value
			// Choose "form" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_field, "form");
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
					
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
					
			//Type in the search field
			CompoundsTab.cmpCondition_value.sendKeys(searchValue);
			LogUltility.log(test, logger, "word is typed in the search field");
			
			// Change the last filter value, in case it was changed by other TC
				CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
				LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
			// Click the Retreive button
				CompoundsTab.btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Get records list
			List<String> retrivedRecords = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Retrived records: " + retrivedRecords);
			
			// Choose record
			String chosenValue = retrivedRecords.get(randomizer.nextInt(retrivedRecords.size()));
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records , chosenValue);
			LogUltility.log(test, logger, "Choose record: "+chosenValue);
			
			// Get record name from key form textbox
			String recordName = CompoundsTab.txtKeyForm.getText();
			
			// Get record POS 
			String posValue = CompoundsTab.getChosenValueInDropdown(CompoundsTab.cmpPOS).get(0);
			
			// Click the new button
			CompoundsTab.btnNew.click();
			LogUltility.log(test, logger, "Click the new button");
			
			// Click yes to the confirmation button
			CompoundsTab.btnYes.click();
			LogUltility.log(test, logger, "Click the Yes button");
			
			// Type a record name in the key form field
			CompoundsTab.txtKeyForm.sendKeys(recordName);
			LogUltility.log(test,logger,"Insert Key Form: "+recordName);
			
			// Choose POS
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpPOS, posValue);
			LogUltility.log(test,logger,"Choose POS: "+posValue);
			
			boolean popupDisplayed = CompoundsTab.popupWindowMessage(test,logger,"Warning: similar key exists:","It looks like there is already an entry with same language, p.o.s. and form.\r\rClick YES to proceed - a new version of the key will be created\rClick NO  to change the value of one of the key fields\rClick CANCEL to abort\r");
			assertTrue(popupDisplayed);
			
			// Click No
			CompoundsTab.No.click();
			LogUltility.log(test,logger,"Click No");
			
			// Compare if highlighted
			String actualColor = CommonFunctions.getColor(CompoundsTab.cmpPOS);
			boolean colorMatch = actualColor.equals("Blue");
			LogUltility.log(test,logger,"The actual color: "+actualColor+" Matchs the expected: Blue");
			assertTrue(colorMatch);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--243:Check adding a value to a listbox with the mass attribute part 1 No
		 * @throws InterruptedException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_243_Check_adding_a_value_to_a_listbox_with_the_mass_attribute_part1_No() throws InterruptedException, AWTException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC--243", "Check adding a value to a listbox with the mass attribute part 1 No");
		
			// Initialize current page
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			
			// Focus on Compounds tab
			CompoundsTab.CompoundsTab.click();
			
			// Get random POS
			List<String> posList = CompoundsTab.getValuesFromApp(CompoundsTab.cmpVolumePOS);
			LogUltility.log(test, logger, "POS list: " + posList);
			
			// Pick a random POS
			Random randomizer = new Random();
			//String randompos = posList.get(randomizer.nextInt(posList.size()));
			String randompos = "adverb";
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpVolumePOS, randompos);
			LogUltility.log(test, logger, "Choose from pos list: " + randompos);
					
			// Get POS symbol
			String posSymbol = CompoundsTab.POSConverter(randompos);
			LogUltility.log(test, logger, "POS symbol: " + posSymbol);
			
			// Click the mass attribute button
			CompoundsTab.bt_mass_attribute.click();
			LogUltility.log(test, logger, "Click the mass attribute button");
			
			// Choose semantic groups
			String category = "semantic groups";
			CommonFunctions.chooseValueInDropdown(CompoundsTab.massChangeItsDD,category);
			LogUltility.log(test, logger, "Set change its dropdown to: "+category);
			
			// Get all values in to be dropdown
			List<String> massListValues = CompoundsTab.getValuesFromApp(CompoundsTab.massToBeDD);
			List<String> availableSGvalues = new ArrayList<String>();
			
			// Get relevant POS values
			for(int i=0;i<massListValues.size();i++)
				if(massListValues.get(i).charAt(0) == posSymbol.toLowerCase().charAt(0))
					availableSGvalues.add(massListValues.get(i));
			
			// Choose new semantic group value
			String chosenSG = availableSGvalues.get(randomizer.nextInt(availableSGvalues.size()));
			CommonFunctions.chooseValueInDropdown(CompoundsTab.massToBeDD,chosenSG);
			LogUltility.log(test, logger, "Set to be dropdown to: "+chosenSG);
			
			// Click change
			CompoundsTab.btn_popup_Merge.click();
			LogUltility.log(test, logger, "Click change button ");
			
			// Click No button
			CompoundsTab.mergeWarningNo.click();
			LogUltility.log(test, logger, "Click No button ");
			
			try {
				// Click again
				CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpVolumePOS, randompos);
				LogUltility.log(test, logger, "click to refresh");
			} catch (Exception e) {
				e.printStackTrace();
			}		
			
			// Retrieve selected semantic groups
			List<String> selectedValues = CompoundsTab.getChosenValueInDropdown(CompoundsTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "Selected values: " + selectedValues);
			
			// Compare result
			boolean onlyOneSG = selectedValues.size() == 1;
			LogUltility.log(test, logger, "Only one semantic group value: " + onlyOneSG);
			assertTrue(onlyOneSG);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		
		/**
<<<<<<< HEAD
=======
		 * DIC_TC--243:Check adding a value to a listbox with the mass attribute part 2 Yes
		 * @throws InterruptedException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_243_Check_adding_a_value_to_a_listbox_with_the_mass_attribute_part2_Yes() throws InterruptedException, AWTException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC--243", "Check adding a value to a listbox with the mass attribute part 2 Yes");
		
			// Initialize current page
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			
			// Focus on dictionary tab
			CompoundsTab.CompoundsTab.click();
			
			// Get random POS
			List<String> posList = CompoundsTab.getValuesFromApp(CompoundsTab.cmpVolumePOS);
			LogUltility.log(test, logger, "POS list: " + posList);
			
			// Pick a random POS
			Random randomizer = new Random();
			//String randompos = posList.get(randomizer.nextInt(posList.size()));
			String randompos = "adverb";
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpVolumePOS, randompos);
			LogUltility.log(test, logger, "Choose from pos list: " + randompos);
					
			// Get POS symbol
			String posSymbol = CompoundsTab.POSConverter(randompos);
			LogUltility.log(test, logger, "POS symbol: " + posSymbol);
			
			// Get records list
			List<String> recordsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records list: " + recordsList);
			
			// Check mass effect on two records
			// Pick two random records
			int firstRandomIndex = randomizer.nextInt(recordsList.size());
			int secondRandomIndex;
			String randomRecord1 = recordsList.get(firstRandomIndex);
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomRecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomRecord1);
			
			// unselect all values
			CommonFunctions.unselectValuesFromList(CompoundsTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "unselect values for the first record");
			
			// select semantic group
			String chosenSG = "downtoning";
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_SemanticGroups,chosenSG);
			LogUltility.log(test, logger, "select value for the first record: "+chosenSG);
			
			// Make sure to pick two different records
			do {secondRandomIndex = randomizer.nextInt(recordsList.size());}
				while(firstRandomIndex == secondRandomIndex);
			
			String randomRecord2 = recordsList.get(secondRandomIndex);
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomRecord2);
			LogUltility.log(test, logger, "Choose from record list: " + randomRecord2);
			
			// unselect all values
			CommonFunctions.unselectValuesFromList(CompoundsTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "unselect values for the second record");
		
			// select semantic group
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_SemanticGroups,chosenSG);
			LogUltility.log(test, logger, "select value for the second record: "+chosenSG);
			
			// For sync purpose
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomRecord1);
			
			// Click save button
			CompoundsTab.btnSave.click();
			CompoundsTab.btnOk.click();
			
			try {
				// Click again
				CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpVolumePOS, randompos);
				LogUltility.log(test, logger, "click to refresh");
			} catch (Exception e) {
				e.printStackTrace();
			}	
			
			// Click the mass attribute button
			CompoundsTab.bt_mass_attribute.click();
			LogUltility.log(test, logger, "Click the mass attribute button");
			
			// Choose semantic groups
			String category = "semantic groups";
			CommonFunctions.chooseValueInDropdown(CompoundsTab.massChangeItsDD,category);
			LogUltility.log(test, logger, "Set change its dropdown to: "+category);
			
			// Get all values in to be dropdown
			List<String> massListValues = CompoundsTab.getValuesFromApp(CompoundsTab.massToBeDD);
			List<String> availableSGvalues = new ArrayList<String>();
			
			// Get relevant POS values
			for(int i=0;i<massListValues.size();i++)
				if(massListValues.get(i).charAt(0) == posSymbol.toLowerCase().charAt(0))
					if(!massListValues.get(i).contains(chosenSG))
						availableSGvalues.add(massListValues.get(i));
			
			// Choose new semantic group value
			String choseNewSG = availableSGvalues.get(randomizer.nextInt(availableSGvalues.size()));
			CommonFunctions.chooseValueInDropdown(CompoundsTab.massToBeDD,choseNewSG);
			LogUltility.log(test, logger, "Set to be dropdown to: "+choseNewSG);
			
			// Click change
			CompoundsTab.bt_Change.click();
			LogUltility.log(test, logger, "Click change button ");
			
			// Click yes button
			CompoundsTab.btnYes.click();
			LogUltility.log(test, logger, "Click Yes button ");
			
			try {
				// Click again
				CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpVolumePOS, randompos);
				LogUltility.log(test, logger, "click to refresh");
			} catch (Exception e) {
				e.printStackTrace();
			}		
			
			// Select first record again
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomRecord1);
			LogUltility.log(test, logger, "Choose from record list: " + randomRecord1);
			
			// Retrieve selected semantic groups
			List<String> selectedValues1 = CompoundsTab.getChosenValueInDropdown(CompoundsTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "Selected values: " + selectedValues1);
			
			// select second record again
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomRecord2);
			LogUltility.log(test, logger, "Choose from record list: " + randomRecord2);
			
			// Retrieve selected semantic groups
			List<String> selectedValues2 = CompoundsTab.getChosenValueInDropdown(CompoundsTab.lst_SemanticGroups);
			LogUltility.log(test, logger, "Selected values: " + selectedValues2);
			
			// Compare
			boolean foundInBoth = selectedValues1.contains(choseNewSG.substring(2)) && selectedValues2.contains(choseNewSG.substring(2));
			LogUltility.log(test, logger, "New semantic group value has been attatched for both records: " + foundInBoth);
			assertTrue(foundInBoth);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		

		
		/**
>>>>>>> 90b0dcb061842418aa5ddd51f8874d8a8cc4948e
		 * DIC_TC--135:verify Dropdown reset after use in merge window
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_135_verify_Dropdown_reset_after_use_in_merge_window() throws InterruptedException, IOException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC_135", "verify Dropdown reset after use in merge window");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
			
			// Click on the merge button
			CurrentPage.As(CompoundsTab.class).btn_merge_files.click();
			LogUltility.log(test, logger, "Click the merge button");
			
			// Create a file that for the dictionary to merge if it does not exist
			File yourFile = new File(Setting.AppPath + "EN\\Data\\" + "compounds_sync_map.txt");
			yourFile.createNewFile(); // if file already exists will do nothing 
			
			// Retrieve records
			List<String> filesList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpPopupValue);
			LogUltility.log(test, logger, "Files names retreived: " + filesList);
					
			// Choose a file in the merge dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPopupValue, filesList.get(0));
			LogUltility.log(test, logger, "Choose value in voluem POS dropdown: "+filesList.get(0));
			
			// Click the cancel button in the merge popup
			CurrentPage.As(CompoundsTab.class).btnpopupCancelWindow.click();
			LogUltility.log(test, logger, "Click cancel in the merge window");
			
			// Click the mass attribute button
			CurrentPage.As(CompoundsTab.class).bt_mass_attribute.click();
			LogUltility.log(test, logger, "Click the mass attribute button");
			
			// Check that no value is initially selected
			List<String> originList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpPopupValue);
			boolean isEmpty =  originList.isEmpty();
			LogUltility.log(test, logger, "Is no value is selected in the dropdown: " + isEmpty);
			assertTrue(isEmpty);

			// Close the mass attribute window
			CurrentPage.As(CompoundsTab.class).btn_popup_Merge.click();
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--672:Check the display of frames
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_672_Check_the_display_of_frames() throws InterruptedException, IOException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC--672", "Check the display of frames");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			
			// Click Compounds Tab
			CompoundsTab.CompoundsTab.click();
			LogUltility.log(test, logger, "Click the Compounds Tab ");
			
			// Search for value
			// Choose "frames" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_field, "frames");
			LogUltility.log(test, logger, "frames is chosen in the first search dropdown");
					
			// Choose "contains" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
					
			// choose frame value
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_value, "clausal complement");
			LogUltility.log(test, logger, "Choose frame value");
			
			// Change the last filter value, in case it was changed by other TC
			CommonFunctions.chooseValueInDropdown(CompoundsTab.cmpCondition_all_next,"all records");
			LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
					
			// Click the Retreive button
			CompoundsTab.btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Get records list
			List<String> recordsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records list: " + recordsList);
			
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
//			String randomrecord = "HE{v}שם_לב~0";
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get chosen frames value from App
			List<String> chosenAppFrames = CompoundsTab.getChosenValueInDropdown(CompoundsTab.lst_frames);
			LogUltility.log(test, logger, "App chosen frames list: " + chosenAppFrames);
			
			// Get chosen frames value from file
			List<String> chosenFileFrames = CompoundsTab.getAllFramesOfRecordFromcompounds(randomrecord);
			LogUltility.log(test, logger, "File chosen frames list: " + chosenFileFrames);
			
			// Compare lists
			boolean sameValues = chosenFileFrames.equals(chosenAppFrames);
			LogUltility.log(test, logger, "Same values appear in both App and file: " + sameValues);
			assertTrue(sameValues);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}

		/**
		 * DIC_TC--121:Check the case sensitive  button in the search form - part1 button disabled
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_121_Check_the_case_sensetive_button_in_the_search_form_part1() throws InterruptedException, IOException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC--12", "Check the case sensitive  button in the search form - part 1 button disabled");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			
			// Click Compounds Tab
			CompoundsTab.tabcompounds.click();
			LogUltility.log(test, logger, "Click the Compounds Tab ");
			
			// Get text to use for search
			List<String> records = CompoundsTab.getRecordsFromcompounds();
			LogUltility.log(test, logger, "Getting clean records from compounds.txt ");
			
			// Pick random record
			Random random = new Random();
			String randomRecord = records.get(random.nextInt(records.size()));
			LogUltility.log(test, logger, "Record to use: " + randomRecord);
			
			// Disable the case sensitivity button if needed
			if(CompoundsTab.isUppercaseActive())
			{
				LogUltility.log(test, logger, "Disable case sensitivity button ");
				CompoundsTab.bt_condition_uppercase.click();
			}
			
			// Search in dictionary
			CompoundsTab.searchInCompound(test,logger,"form","equal",randomRecord,"all records");
		
			// Get records list
			List<String> recordsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records list: " + recordsList);
			    
			// Get clean records from the app list
			ArrayList<String> cleanAppRecords = new ArrayList<String>();
			for(int i=0;i<recordsList.size();i++)
			{
				String[] removeLeft = recordsList.get(i).split("}");
			    String[] removeRight = removeLeft[1].split("~");
			    removeRight[0] = removeRight[0].replaceAll("_", " ");
			    cleanAppRecords.add(removeRight[0]);
			}
			
			
			// The case sensitivity button is set to 'a' ,it should ignorecase 
			boolean ignoringCase = true;
			for(int i=0;i<cleanAppRecords.size();i++)
				if(!randomRecord.equalsIgnoreCase(cleanAppRecords.get(i)))
					ignoringCase = false;

			assertTrue(ignoringCase);
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * DIC_TC--121:Check the case sensitive  button in the search form - part2 button enabled
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_121_Check_the_case_sensetive_button_in_the_search_form_part2() throws InterruptedException, IOException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC--121", "Check the case sensitive  button in the search form - part 2 button enabled");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			
			// Click Compounds Tab
			CompoundsTab.tabcompounds.click();
			LogUltility.log(test, logger, "Click the Compounds Tab ");
			
			// Get text to use for search
			List<String> records = CompoundsTab.getRecordsFromcompounds();
			LogUltility.log(test, logger, "Getting clean records from compounds.txt ");
			
			// Pick random record
			Random random = new Random();
			String randomRecord = records.get(random.nextInt(records.size()));
			LogUltility.log(test, logger, "Record to use: " + randomRecord);
			
			// Enable the case sensitivity button if needed
			if(!CompoundsTab.isUppercaseActive())
			{
				LogUltility.log(test, logger, "Enable case sensitivity button ");
				CompoundsTab.bt_condition_uppercase.click();
			}
			
			// Search in dictionary
			CompoundsTab.searchInCompound(test,logger,"form","equal",randomRecord,"all records");
		
			// Get records list
			List<String> recordsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records list: " + recordsList);
			    
			// Get clean records from the app list
			ArrayList<String> cleanAppRecords = new ArrayList<String>();
			for(int i=0;i<recordsList.size();i++)
			{
				String[] removeLeft = recordsList.get(i).split("}");
			    String[] removeRight = removeLeft[1].split("~");
			    removeRight[0] = removeRight[0].replaceAll("_", " ");
			    cleanAppRecords.add(removeRight[0]);
			}
			
			
			// The case sensitivity button is set to 'A' ,it should not ignore case 
			boolean caseSensitive = true;
			for(int i=0;i<cleanAppRecords.size();i++)
				if(!randomRecord.equals(cleanAppRecords.get(i)))
					caseSensitive = false;

			assertTrue(caseSensitive);
			LogUltility.log(test, logger, "Test Case PASSED");
		}

		
		
		/**
		 * DIC_TC--136:Verify get results when Searching
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_136_Verify_get_results_when_Searching() throws InterruptedException, IOException
		{
			test = extent.createTest("Compounds Tests - DIC_TC--136", "Verify get results when Searching");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			
			// Click Compounds Tab
			CompoundsTab.tabcompounds.click();
			LogUltility.log(test, logger, "Click the Compounds Tab ");
			
			// Get text to use for search
			List<String> records = CompoundsTab.getRecordsFromcompounds();
			LogUltility.log(test, logger, "Getting clean records from compounds.txt ");
			
			// Pick random record
			Random random = new Random();
			String randomRecord = records.get(random.nextInt(records.size()));
			LogUltility.log(test, logger, "Record to use: " + randomRecord);
				
			// Search in dictionary
			CompoundsTab.searchInCompound(test,logger,"form","equal",randomRecord,"all records");
		
			// Get records list
			List<String> recordsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records list: " + recordsList);

			// Check if results retrieved successfully
			// Get clean records from the app list
			ArrayList<String> cleanAppRecords = new ArrayList<String>();
			for(int i=0;i<recordsList.size();i++)
			{
				String[] removeLeft = recordsList.get(i).split("}");
			    String[] removeRight = removeLeft[1].split("~");
			    removeRight[0] = removeRight[0].replaceAll("_", " ");
			    cleanAppRecords.add(removeRight[0]);
			}
									
			boolean exists = cleanAppRecords.contains(randomRecord);
			LogUltility.log(test, logger, "Searched value exists: "+exists);
			assertTrue(exists);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--516:check look in corpus works for names
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_516_check_look_in_corpus_works_for_names() throws InterruptedException, IOException, AWTException
		{
			test = extent.createTest("Compounds Tests - DIC_TC--516", "check look in corpus works for names");
			
			if(Setting.Language.equals("HE")) return;
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			CurrentPage = GetInstance(MonitorTab.class);
			MonitorTab MonitorTab = CurrentPage.As(MonitorTab.class);
			CurrentPage = GetInstance(CorpusTab.class);
			smi.smorfet.test.pages.CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
			
			// Click Compounds Tab
			CompoundsTab.tabcompounds.click();
			LogUltility.log(test, logger, "Click the Compounds Tab ");
			
			// Get text to use for search
			List<String> records = CompoundsTab.getRecordsFromCompoundsWithCompletedStatus();
			LogUltility.log(test, logger, "Getting clean records from compounds.txt ");		
			
			// Pick random record
			Random random = new Random();
			String randomRecord = records.get(random.nextInt(records.size()));
//			String randomRecord = "pack of cards";
			LogUltility.log(test, logger, "Compound to use: " + randomRecord);
			
			// Add new sentence with compound and tag it as compound to make sure that the compound can be found in corpus
			// Go to monitor and insert a new sentence with the compound
			MonitorTab.tabMonitor.click();
			LogUltility.log(test, logger, "Click on the Monitor tab");
			
			String sentenceToAdd = randomRecord +" "+ CorpusTab.getRandomSentence();
			LogUltility.log(test, logger, "Insert the following sentence: "+sentenceToAdd);
			MonitorTab.tb_raw_sentence.sendKeys(sentenceToAdd);
				
			// Press the new button
			LogUltility.log(test, logger, "Click the new button");
			MonitorTab.bt_new.click();
			LogUltility.log(test, logger, "Click the ok button");
			MonitorTab.bt_ok.click();
			
			// Get the sentence records
			List<String> recordsList = CorpusTab.getSentencesRecords();
			LogUltility.log(test, logger, "Records list: " + recordsList);
			
			// Tag the compound and the records
			// Tag the compound first
			WebElement element = CorpusTab.sentencesRecordElement(recordsList.get(0), 0);
			CorpusTab.chooseCompoundTaggingOption(randomRecord,element);
			
			// Get the number of words in compound
			String[] splitter = randomRecord.split(" ");
			int compoundWordsLen = splitter.length;
			
			// Remove the already tagged part
			for(int i=0;i<compoundWordsLen;i++)
				recordsList.remove(i);
					
			// Tag the remaining part of the sentence	
			int recordNumber = 0;
			LogUltility.log(test, logger, "Records list: " + recordsList);
			for (String record : recordsList) {
				
				LogUltility.log(test, logger, "------------------------");
				LogUltility.log(test, logger, "Record to tag: " + record);
				
				// Choose a record
				CorpusTab.clickSentencesRecord(record, recordNumber++);
				System.out.println("Choose a record");

				// Get all tagging options
				List<List<String>> taggingOptions = CorpusTab.getTaggingRecords();
				LogUltility.log(test, logger, "Get tagging Options: " + taggingOptions);
						
				// Random choose another option from the taggingOptions
				List<String> randomTaggingOption = taggingOptions.get(random.nextInt(taggingOptions.size()));
				System.out.println("choose random tagging option: " + randomTaggingOption);
				
				// Choose tagging option
				try {
					CorpusTab.chooseTaggingOption(randomTaggingOption);
					LogUltility.log(test, logger, "New tagging: " + randomTaggingOption);
				} catch (Exception e) {
					System.out.println("exception: " + e);
				}
				
			}
			
			// Navigate to Compound tab
			CompoundsTab.CompoundsTab.click();
			LogUltility.log(test, logger, "Navigate to compounds tab");
			
			// Search in dictionary
			LogUltility.log(test, logger, "Search for the compound");
			CompoundsTab.searchInCompound(test,logger,"form","equal",randomRecord,"all records");
		
			// Retrieve records
			List<String> compoundsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records retreived: " + compoundsList);

			// Pick a random record
			Random randomizer = new Random();
			String compound = compoundsList.get(randomizer.nextInt(compoundsList.size()));
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, compound);
			LogUltility.log(test, logger, "Choose from record list: " + compound);
			
			// Press the look in corpus button
			CompoundsTab.btnLookCorpus.click();
			LogUltility.log(test, logger, "Click on the Look in Corpus button");
			
			// Check that the compound exists in corpus
			boolean foundInCorpus = !CompoundsTab.popupWindowMessage(test, logger, "", "Could not find a match in the corpus.");
			LogUltility.log(test, logger, "The compound was found in corpus: "+foundInCorpus);
			assertTrue(foundInCorpus);
			
			// Click the cancel button
			CompoundsTab.btnCancelRevert.click();
			LogUltility.log(test, logger, "Click the cancel button");
			
			// Click the revert button
			CompoundsTab.bt_revert.click();
			LogUltility.log(test, logger, "Click the revert button");
			
			// Click Yes on the confirmation message
			CompoundsTab.btnOk.click();
			LogUltility.log(test, logger, "Click Yes on the confirmation message");
			
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--607:Delete a record that exist in Compounds and replace it with other word
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_607_Delete_a_record_that_exist_in_Compounds_and_replace_it_with_other_word() throws InterruptedException, IOException, AWTException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC--607", "Delete a record that exist in Compounds and replace it with other word");
			
			// Initialize pages
			CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);
			DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
			
			// Navigate to dictionary tab
			DictionaryTab.tabDictionary.click();
			CommonFunctions.clickTabs("Dictionary");
			LogUltility.log(test, logger, "Open dictionary tab");
			
			// Get all the compounds with their dictionary records
			HashMap<String, List<String>> compoundsWithDictionaryRecrods = CompoundsTab.getAllCompoundsLemma();
	
			// Pick a random compound
			Random random = new Random();
			// Get all available compounds = keys of hashmap
			List<String> keys = new ArrayList<String>(compoundsWithDictionaryRecrods.keySet());	
			String randomCompound =keys.get(random.nextInt(keys.size()));
			LogUltility.log(test, logger, "Choose random compound: "+randomCompound);
			
			// Get the compound dictionary records
			List<String> dictionaryRecords = compoundsWithDictionaryRecrods.get(randomCompound);
			String randomDictionary = dictionaryRecords.get(random.nextInt(dictionaryRecords.size()));
			LogUltility.log(test, logger, "Choose random dictionary: "+randomDictionary);
			
			// Search for the dictionary value
			 String[] removeLeft = randomDictionary.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");
			 String cleanDictionary = removeRight[0];	 
			 DictionaryTab.searchInDictionary(test, logger, "form", "equal", cleanDictionary, "all records");
			 
			 // Choose the randomDictionary from the list
			 CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomDictionary);
			 LogUltility.log(test, logger, "Choose the record from the list: "+randomDictionary);
			 
			// Click the delete button
			 DictionaryTab.bt_delete.click();
			LogUltility.log(test, logger, "Click the delete button");
			// Click the yes button
			DictionaryTab.btnYes.click();

			boolean popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, randomDictionary);
			while(popupDisplayed)
			{
				// Choose random replacement value	
				List<String> listValues = DictionaryTab.getValuesFromApp(DictionaryTab.replacementWindow);
				LogUltility.log(test, logger, "Replacement values: "+listValues);
				String repValue = listValues.get(random.nextInt(listValues.size()));
		
				// Choose from dropdown
				LogUltility.log(test, logger, "Chosen value: "+repValue);
				DictionaryTab.cmpPopupValue.click();
				CommonFunctions.chooseValueInDropdown(DictionaryTab.replacementWindow, repValue);
				
				// Press replace
				DictionaryTab.bt_utility_accept.click();
				LogUltility.log(test, logger, "Click the 'Replace' button");
				
//				Thread.sleep(10000);
				// The successfully removed popup displayed
				boolean end = popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, "Record "+randomDictionary+" was successfully removed");
				if(end)
					break;
				else
					popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, randomDictionary);
			}
				
			// Click ok
			DictionaryTab.btnOk.click();
			LogUltility.log(test, logger, "Click the 'Ok' button");
			
			// Click Compounds Tab
			CompoundsTab.tabcompounds.click();
			CommonFunctions.clickTabs("Compounds");
			LogUltility.log(test, logger, "Navigate to Compounds Tab");
			
			 // Search for the compound
			 removeLeft = randomCompound.split("}");
	   		 removeRight = removeLeft[1].split("~");
			 String cleanCompound = removeRight[0];	 
			 CompoundsTab.searchInCompound(test, logger, "form", "equal", cleanCompound, "all records");
			
			 // Choose the compound from the list
			LogUltility.log(test, logger, "Chosen value: "+randomCompound);
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomCompound);
			
			// Get all the lemmas values from the columns
			List<String> lemmasAfterDelete = new ArrayList<String>();
			for(int i=0;i<dictionaryRecords.size();i++)
				lemmasAfterDelete.add(CompoundsTab.getSelectedTaggingValue("Lemma", i));
			
			LogUltility.log(test, logger, "The dictionary values of the compound after deleting is: "+lemmasAfterDelete);
			
			// Check that the value doesn't exist in the columns after deleting the dictionary record
			boolean isDeleted = !lemmasAfterDelete.contains(randomDictionary);
			LogUltility.log(test, logger, "The dictionary key:"+randomDictionary+" , doesn't exist in the list:"+isDeleted);
			assertTrue(isDeleted);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}

		
		/**
		 * DIC_TC--609:Delete a record that exist in Compounds and leave an empty replacement
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_609_Delete_a_record_that_exist_in_Compounds_and_leave_an_empty_replacement() throws InterruptedException, IOException, AWTException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC--609", "Delete a record that exist in Compounds and leave an empty replacement");
			
			// Initialize pages
			CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);
			DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);

			// Navigate to dictionary tab
			DictionaryTab.tabDictionary.click();
			CommonFunctions.clickTabs("Dictionary");
			LogUltility.log(test, logger, "Open dictionary tab");
			
			// Get all the compounds with their dictionary records
			HashMap<String, List<String>> compoundsWithDictionaryRecrods = CompoundsTab.getAllCompoundsLemma();
	
			// Pick a random compound
			Random random = new Random();
			// Get all available compounds = keys of hashmap
			List<String> keys = new ArrayList<String>(compoundsWithDictionaryRecrods.keySet());	
			String randomCompound =keys.get(random.nextInt(keys.size()));
			LogUltility.log(test, logger, "Choose random compound: "+randomCompound);
			
			// Get the compound dictionary records
			List<String> dictionaryRecords = compoundsWithDictionaryRecrods.get(randomCompound);
			String randomDictionary = dictionaryRecords.get(random.nextInt(dictionaryRecords.size()));
			LogUltility.log(test, logger, "Choose random dictionary: "+randomDictionary);
			
			// Search for the dictionary value
			 String[] removeLeft = randomDictionary.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");
			 String cleanDictionary = removeRight[0];	 
			 DictionaryTab.searchInDictionary(test, logger, "form", "equal", cleanDictionary, "all records");
			 
			 // Choose the randomDictionary from the list
			 CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomDictionary);
			 LogUltility.log(test, logger, "Choose the record from the list: "+randomDictionary);
			 
			// Click the delete button
			 DictionaryTab.bt_delete.click();
			LogUltility.log(test, logger, "Click the delete button");
			// Click the yes button
			DictionaryTab.btnYes.click();

			boolean popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, randomDictionary);
			while(popupDisplayed)
			{
				// Choose "Leave empty" as a replacement value	
				String repValue = "Leave empty";
		
				// Choose from dropdown
				LogUltility.log(test, logger, "Chosen value: "+repValue);
				DictionaryTab.cmpPopupValue.click();
				CommonFunctions.chooseValueInDropdown(DictionaryTab.replacementWindow, repValue);
				
				// Press replace
				DictionaryTab.bt_utility_accept.click();
				LogUltility.log(test, logger, "Click the 'Replace' button");
				
				// The successfully removed popup displayed
				boolean end = popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, "Record "+randomDictionary+" was successfully removed");
				if(end)
					break;
				else
					popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, randomDictionary);
			}
				
			// Click ok
			DictionaryTab.btnOk.click();
			LogUltility.log(test, logger, "Click the 'Ok' button");
			
			// Click Compounds Tab
			CompoundsTab.tabcompounds.click();
			CommonFunctions.clickTabs("Compounds");
			LogUltility.log(test, logger, "Navigate to Compounds Tab");
			
			 // Search for the compound
			 removeLeft = randomCompound.split("}");
	   		 removeRight = removeLeft[1].split("~");
			 String cleanCompound = removeRight[0];	 
			 CompoundsTab.searchInCompound(test, logger, "form", "equal", cleanCompound, "all records");
			
			 // Choose the compound from the list
			LogUltility.log(test, logger, "Chosen value: "+randomCompound);
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomCompound);
			
			// Get the compound status
			String compoundStatus = CompoundsTab.getChosenValueInDropdown(CompoundsTab.dd_compounds_status).get(0);
			boolean notCompleted = !compoundStatus.equals("completed");
			LogUltility.log(test, logger, "The compound status should not be 'Completed': "+notCompleted+", The record status is: "+compoundStatus);
			assertTrue(notCompleted);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		/**
		 * DIC_TC--644:Verify the tagging area appear for word that deleted
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_644_Verify_the_tagging_area_appear_for_word_that_deleted() throws InterruptedException, IOException, AWTException
		{
			//Thread.sleep(5000);
			test = extent.createTest("Compounds Tests - DIC_TC--644", "Verify the tagging area appear for word that deleted");
			
			// Initialize pages
			CompoundsTab CompoundsTab = GetInstance(CompoundsTab.class).As(CompoundsTab.class);
			DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
			
			// Navigate to dictionary tab
			DictionaryTab.tabDictionary.click();
			CommonFunctions.clickTabs("Dictionary");
			LogUltility.log(test, logger, "Open dictionary tab");
			
			// Get all the compounds with their dictionary records
			HashMap<String, List<String>> compoundsWithDictionaryRecrods = CompoundsTab.getAllCompoundsLemma();
	
			// Pick a random compound
			Random random = new Random();
			// Get all available compounds = keys of hashmap
			List<String> keys = new ArrayList<String>(compoundsWithDictionaryRecrods.keySet());	
			String randomCompound =keys.get(random.nextInt(keys.size()));
			LogUltility.log(test, logger, "Choose random compound: "+randomCompound);
			
			// Get the compound dictionary records
			List<String> dictionaryRecords = compoundsWithDictionaryRecrods.get(randomCompound);
			String randomDictionary = dictionaryRecords.get(random.nextInt(dictionaryRecords.size()));
			LogUltility.log(test, logger, "Choose random dictionary: "+randomDictionary);
			
			// Search for the dictionary value
			 String[] removeLeft = randomDictionary.split("}");
	   		 String[] removeRight = removeLeft[1].split("~");
			 String cleanDictionary = removeRight[0];	 
			 DictionaryTab.searchInDictionary(test, logger, "form", "equal", cleanDictionary, "all records");
			 
			 // Choose the randomDictionary from the list
			 CommonFunctions.chooseValueInDropdown(DictionaryTab.lst_records, randomDictionary);
			 LogUltility.log(test, logger, "Choose the record from the list: "+randomDictionary);
			 
			// Click the delete button
			 DictionaryTab.bt_delete.click();
			LogUltility.log(test, logger, "Click the delete button");
			// Click the yes button
			DictionaryTab.btnYes.click();

			boolean popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, randomDictionary);
			while(popupDisplayed)
			{
				// Choose "Leave empty" as a replacement value	
				String repValue = "Leave empty";
		
				// Choose from dropdown
				LogUltility.log(test, logger, "Chosen value: "+repValue);
				DictionaryTab.cmpPopupValue.click();
				CommonFunctions.chooseValueInDropdown(DictionaryTab.replacementWindow, repValue);
				
				// Press replace
				DictionaryTab.bt_utility_accept.click();
				LogUltility.log(test, logger, "Click the 'Replace' button");
				
				// The successfully removed popup displayed
				boolean end = popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, "Record "+randomDictionary+" was successfully removed");
				if(end)
					break;
				else
					popupDisplayed = DictionaryTab.popupWindowMessage(test, logger, randomDictionary);
			}
				
			// Click ok
			DictionaryTab.btnOk.click();
			LogUltility.log(test, logger, "Click the 'Ok' button");
			
			// Click Compounds Tab
			CompoundsTab.tabcompounds.click();
			CommonFunctions.clickTabs("Compounds");
			LogUltility.log(test, logger, "Navigate to Compounds Tab");
			
			 // Search for the compound
			 removeLeft = randomCompound.split("}");
	   		 removeRight = removeLeft[1].split("~");
			 String cleanCompound = removeRight[0];	 
			 CompoundsTab.searchInCompound(test, logger, "form", "equal", cleanCompound, "all records");
			
			 // Choose the compound from the list
			LogUltility.log(test, logger, "Chosen value: "+randomCompound);
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomCompound);
			
			// Check the the dictionary key doesn't exist and dropdown is empty
			boolean lemmaDeleted = true;
			for(int i=0;i<dictionaryRecords.size();i++)
			{
				List<String> lemma = CurrentPage.As(CompoundsTab.class).getTaggingValues("Lemma",i);
				if(lemma.contains(randomDictionary) && !lemma.get(i).isEmpty())
					lemmaDeleted = false;
			}
			
			// Verify that the dictionary key is not displayed
			LogUltility.log(test, logger, "The dictionary key has been removed and the element is visible: "+lemmaDeleted);
			assertTrue(lemmaDeleted);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--713:delete a record after selecting it in the Semantic Relations - bug DIC-726
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_713_delete_a_record_after_selecting_it_in_the_Semantic_Relations_bug_DIC_726() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test -  DIC_TC--713","Check the semantic relations between the records");
			
			if(Setting.Language.equals("HE")) return;
			
			// Focus on Compounds tab
				CurrentPage = GetInstance(CompoundsTab.class);
				CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
				
				// get list of records that have semantic relations
//				List<String> recordsWithLR = CurrentPage.As(CompoundsTab.class).availableRelationsForCompounds("lexical", 30);
				Map<String, String> recordsWithLR = CurrentPage.As(CompoundsTab.class).availableRelationsForCompounds("semantic", 30);
				LogUltility.log(test, logger, "List of records that have lexical relations:" +recordsWithLR);
				 
				// Choose a random record that have semantic relations as compounds
				Random randomizer = new Random();
//				String randomRecord = recordsWithLR.get(randomizer.nextInt(recordsWithLR.size()));
				Object[] values = recordsWithLR.keySet().toArray();
				
				String randomRecord = (String) values[randomizer.nextInt(values.length)];
				String relationValue = recordsWithLR.get(randomRecord);
				LogUltility.log(test, logger, "Random record: " + randomRecord + ", relation value: " + relationValue);

				//Clean records
//				String cleanRecord = CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord); 
				
				// Choose "form" from the first search dropdown
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
				LogUltility.log(test, logger, "form is chosen in the first search dropdown");
				
				// Choose "Equal" from the condition dropdown
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
				LogUltility.log(test, logger, "Equal is chosen in the condition dropdown");
				
				//Type in the search field
				CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
				LogUltility.log(test, logger, "word is typed in the search field:" +CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
				
				// Click the Retrieve button
				CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
				LogUltility.log(test, logger, "Click the Retreive button");
				
				//Thread.sleep(2000);
				// Retrieve records
				List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
				
				// Check that retrieved records do contain the searched record
				if (recordsList.isEmpty()) assertTrue(false);
				
//				// Choose a random record from the record list
//				String randomResult = recordsList.get(randomizer.nextInt(recordsList.size()));
				
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecord);
				LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
				
				// Get the chosen Semantic Relation list value
				List<String> chosenLexicalRelationList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticRelations);
				LogUltility.log(test, logger, "chosen lexical relation list: " + chosenLexicalRelationList);
//				String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
				//getChosenValueInDropdown
				
				// Click on random semantic relation value
				String randomSemanticRelation = chosenLexicalRelationList.get(randomizer.nextInt(chosenLexicalRelationList.size()));
				CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticRelations, randomSemanticRelation);
				LogUltility.log(test, logger, "Choose from semantic relation list: " + randomSemanticRelation);
				
				// Click the delete button
				CurrentPage.As(CompoundsTab.class).bt_delete.click();
				LogUltility.log(test, logger, "Click the delete button");
				// Click the yes button in the confirmation message 
				CurrentPage.As(CompoundsTab.class).yesDelete.click();
				LogUltility.log(test, logger, "Record is deleted");
				
//				try {
					// try to get the text
					//String mibo = CurrentPage.As(CompoundsTab.class).text_removed_record_confirmation.getText();
				
					// Click OK on the confirmation message
				    Thread.sleep(5000);
					CurrentPage.As(CompoundsTab.class).btnOk.click();
					LogUltility.log(test, logger, "Click the OK button on the Confirmation popup");
//				} catch (Exception e) {
//					LogUltility.log(test, logger, "Confirmation popup didn't appear");
//				}
				
				// click the save button
			   CurrentPage.As(CompoundsTab.class).btnSave.click();
			   LogUltility.log(test, logger, "Click Save button ");								
	     		
			   // click the OK button in the saving popup
				CurrentPage.As(CompoundsTab.class).btnOKSave.click();
				LogUltility.log(test, logger, "Click ok on the Saving popup ");
			
			   // For syncing
			   // Choose value from the first search dropdown
	     		CommonFunctions.chooseValueInDropdown( CurrentPage.As(CompoundsTab.class).cmpCondition_field,"form");
	     		LogUltility.log(test, logger, "form is chosen from the first search dropdown");
				
				// get the Deleted record info from the compounds.txt file		
				String deletedRecord = CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomSemanticRelation.split("¬")[1]).replaceAll(" ","_");
				String recordInfo = CurrentPage.As(CompoundsTab.class).getRecordsInfoFromcompounds(deletedRecord);
				LogUltility.log(test, logger, "Deleted record info returned from the Compounds.txt file: " + recordInfo);
					
				//Verify that the line does not contain all the selected info
				boolean  name = recordInfo.contains(deletedRecord);
				 assertFalse(name);
				 LogUltility.log(test, logger, "Name DOES NOT exist in the returned line from the compound.txt. should be False : " + name);
					
				LogUltility.log(test, logger, "Test Case PASSED");	
		}

		
		
		
		/**
		 * DIC_TC--729:Tagging – Form Type should display values related to the chosen POS value - bug 1061
		 * @throws InterruptedException 
		 * @throws IOException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_729_Tagging_Form_Type_should_display_values_related_to_the_chosen_POS_value_bug_1061() throws InterruptedException, IOException, AWTException
		{
			test = extent.createTest("Compounds Tests - DIC_TC--729", "Tagging – Form Type should display values related to the chosen POS value - bug 1061");
			
			// Focus on dictionary tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CompoundsTab CompoundsTab = CurrentPage.As(CompoundsTab.class);
			
			CurrentPage = GetInstance(DictionaryTab.class);
			DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
			
			// Get all the compounds with their dictionary records
			HashMap<String, List<String>> compoundsWithDictionaryRecrods = CompoundsTab.getAllCompoundsLemma();
	
			// Click Compounds Tab
			CompoundsTab.tabcompounds.click();
			LogUltility.log(test, logger, "Click the Compounds Tab ");
			
			// Get text to use for search
//			List<String> records = CompoundsTab.getRecordsFromcompounds();
//			LogUltility.log(test, logger, "Getting clean records from compounds.txt ");
//			
			// Pick a random compound
			Random random = new Random();
			// Get all available compounds = keys of hashmap
			List<String> keys = new ArrayList<String>(compoundsWithDictionaryRecrods.keySet());	
			String randomCompound =keys.get(random.nextInt(keys.size()));
			LogUltility.log(test, logger, "Choose random compound: "+randomCompound);
			
			
//			// Pick random record
//			Random random = new Random();
////			String randomRecord = records.get(random.nextInt(records.size()));
			String cleanRecord = "chief_justice";
//			LogUltility.log(test, logger, "Record to use: " + cleanRecord);
//				
			// Search in dictionary
			CompoundsTab.searchInCompound(test,logger,"form","equal",cleanRecord,"all records");
		
			// Get records list
			List<String> recordsList = CompoundsTab.getValuesFromApp(CompoundsTab.lst_records);
			LogUltility.log(test, logger, "Records list: " + recordsList);

			// Choose the record
			String randomRecord = recordsList.get(random.nextInt(recordsList.size()));
			CommonFunctions.chooseValueInDropdown(CompoundsTab.lst_records, randomRecord);
			LogUltility.log(test, logger, "Choose from the list"+randomRecord);
			
			// Change the first lemma value
			String lemmaValue = CompoundsTab.getSelectedTaggingValue("Lemma",0);
			LogUltility.log(test, logger, "The lemma value is: "+lemmaValue);
			
			// Get all available lemma values
			List<String> lemmas = CompoundsTab.getTaggingValues("Lemma", 0);
			LogUltility.log(test, logger, "Available lemmas:"+lemmas);
			
			// Remove the selected option and the first empty option
			lemmas.remove(0);
			lemmas.remove(lemmaValue);
			
			// Select new value
			String chosenLemma = lemmas.get(random.nextInt(lemmas.size()));
			CompoundsTab.chooseTaggingValue("Lemma", 0, chosenLemma);
			LogUltility.log(test, logger, "The new chosen lemma is: "+chosenLemma);
			
			// Get the record form types from file
			String formTypeFromFile = DictionaryTab.getRecordForms(chosenLemma.split(" ")[0]);
			LogUltility.log(test, logger, "The record form type from file: "+formTypeFromFile);
			
			// Get form type value from app
			String formTypeValue = CompoundsTab.getSelectedTaggingValue("Form Type",0);
			LogUltility.log(test, logger, "The form type value from app is: "+formTypeValue);
			
			// Check that the displayed app value exists in the file info
			boolean correctValue  = formTypeFromFile.contains(formTypeValue);
			LogUltility.log(test, logger, "The displayed form type in app matches the info from file: "+correctValue);
			assertTrue(correctValue);
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}
		
		
		/**
		 * DIC_TC--224:Verify user can edit semantic relations, lexical relations - part 2 lexical relations
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 * @throws AWTException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_224_Verify_user_can_edit_semantic_relations_lexical_relations_part2_lexical_relations() throws InterruptedException, FileNotFoundException, AWTException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_224", "Verify user can edit semantic relations, lexical relations - part 2 lexical relations");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();

			// get list of records that have lexical Relations 
			Map<String, String> recordsWithLR = CurrentPage.As(CompoundsTab.class).availableRelationsForCompounds("lexical", 20);
			LogUltility.log(test, logger, "Lexical relations from file:" +recordsWithLR );
			
			// Choose a random record that have semantic relations as compounds
			Random randomizer = new Random();
			Object[] values = recordsWithLR.keySet().toArray();
			
			String randomRecord = (String) values[randomizer.nextInt(values.length)];
			String relationValue = recordsWithLR.get(randomRecord);
			LogUltility.log(test, logger, "Random record: " + randomRecord + ", relation value: " + relationValue);
			
			// Choose "form" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
			LogUltility.log(test, logger, "from is chosen in the first search dropdown");
			
			// Choose "Equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "Equal is chosen in the condition dropdown");
			
			//Type in the search field
			CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
			LogUltility.log(test, logger, "word is typed in the search field:" +CurrentPage.As(CompoundsTab.class).getOnlyRecordName(randomRecord));
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");

			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomRecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomRecord);
			
			// Get the Semantic Relation list value
			List<String> relationB4Change = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_Lexical_relations);
			LogUltility.log(test, logger, "Relation list BEFORE modifying: " + relationB4Change);
			
			// Modify random relation
			CurrentPage.As(CompoundsTab.class).editLexicalcRelation(test,logger);
				
			// Get the Semantic Relation list value
			List<String> relationAfterChange = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_Lexical_relations);
			LogUltility.log(test, logger, "Relation list AFTER modifying: " + relationAfterChange);
			
			// Verify that the lists are different
			Collections.sort(relationAfterChange);
			Collections.sort(relationB4Change);
			boolean relationChanged = !relationB4Change.equals(relationAfterChange);
			LogUltility.log(test, logger, "The value has been modified: "+relationChanged);	
			assertTrue(relationChanged);
			
			// Click the revert button
			CurrentPage.As(CompoundsTab.class).bt_revert.click();
			LogUltility.log(test, logger, "Click the revert button");
			
			// Click Yes on the confirmation message
			CurrentPage.As(CompoundsTab.class).btnOk.click();
			LogUltility.log(test, logger, "Click Yes on the confirmation message");
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

		
@AfterMethod(alwaysRun = true)
public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
{		
	
	int testcaseID = 0;
	if (enableReportTestlink == true)
		testcaseID=rpTestLink.GetTestCaseIDByName("Compound",method.getName());
	
//	System.out.println("tryCount: " + tryCount);
//	System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));

	if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
			&& Integer.parseInt(Setting.RetryFailed)!=0) {
		extent.removeTest(test);
		
        // Close popups to get back to clean app
		 System.out.println("Test Case Failed");
		 CurrentPage = GetInstance(DictionaryTab.class);
		 if(Setting.closeEveryWindow.equals("true"))
			 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
	}
	else if(result.getStatus() == ITestResult.FAILURE)
	    {
		 	tryCount = 0;
	        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
	        test.fail(result.getThrowable());
	        String screenShotPath = Screenshot.captureScreenShot();
	        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
	        // Send result to testlink
	        if (enableReportTestlink == true){
	        	try {
	        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
	            	LogUltility.log(test, logger, "Report to testtlink: " + response);
				} catch (Exception e) {
					LogUltility.log(test, logger, "Testlink Error: " + e);
				}
	        	}
	        
	        // Close popups to get back to clean app
//	        if (Integer.parseInt(Setting.RetryFailed) != 0) {
	         System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//	        }
	    }
	    else if(result.getStatus() == ITestResult.SUCCESS)
	    {
	    	tryCount = 0;
	        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
	        // Send result to testlink
	        if (enableReportTestlink == true){
	        	try {
	        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
	            	LogUltility.log(test, logger, "Report to testtlink: " + response);
	    		} catch (Exception e) {
	    			LogUltility.log(test, logger, "Testlink Error: " + e);
	    		}
	        	
	        	}
	    }
	    else
	    {
	        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
	        test.skip(result.getThrowable());
	    }
	
	    extent.flush();
	    
		// Count how many a test was processed
		tryCount++;
		}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			// DriverContext._Driver.quit();
			}

}

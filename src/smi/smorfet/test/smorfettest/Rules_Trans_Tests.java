package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.RulesTab;

/**
 * 
 * All tests for Transcription Rules
 *
 */
public class Rules_Trans_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");

		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Rules tab - Transcription");
	
		// Logger
		logger = LogManager.getLogger(RulesTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Rules tab - Transcription");
		logger.info("Rules tab Tests - FrameworkInitilize");
		
	//con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	//	logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(RulesTab.class);
//		CurrentPage.As(RulesTab.class).Start_Window.click();
//		CurrentPage.As(RulesTab.class).bt_abort.click();
		
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(RulesTab.class);
//			CurrentPage.As(RulesTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Rules");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * DIC_TC--568:Verify user can add new rules
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_568_Verify_user_can_add_new_rules() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--568", "Verify user can add new rules");

		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the transcription tab 
		RulesTab.clickTabs("transcription");
		
		// get all the transcription rules as list
		List<String> transcriptionRulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesList);
		
		// Press the new button
		RulesTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button ");
		
		//get the rule Number form Number field  
		String ruleNumber = RulesTab.numberBox.getText();
		LogUltility.log(test, logger, "The new Rule number from the Number field: " +ruleNumber);
		
		// Type a record name in the key form field
		String randomText = RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) ;
		RulesTab.ruleName.click();
		String newRandomText = randomText;
		RulesTab.ruleName.sendKeys(newRandomText);
		LogUltility.log(test, logger, "New rule name: " + newRandomText);
		
        // get the text from the rule name field
		String textFromFieldB4Save = RulesTab.ruleName.getText();
		LogUltility.log(test, logger, "text from the rule name field before Save: " +textFromFieldB4Save);
		
		//Press the save button  
		RulesTab.btnSave.click();
		LogUltility.log(test, logger, "Click the Save button ");
		
		//Press the OK button in the saving Dialog
		//Thread.sleep(10000);
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
			
		// verify that the rule saved successfully on the file
		String ruleFile= "transcription_rules.txt";
		boolean isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, textFromFieldB4Save ,ruleFile);
		assertTrue(isRuleExist);
		LogUltility.log(test, logger, "Verify that the new created rule do  appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is : " +textFromFieldB4Save+" ,  and the value should be True: " +isRuleExist);
	
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--569:Verify user can make changes and save it in exists rules
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_569_Verify_user_can_make_changes_and_save_it_in_exists_rules() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--569", "Verify user can make changes and save it in exists rules");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the transcription tab 
		RulesTab.clickTabs("transcription");
		
		// get all the transcription rules as list
		List<String> transcriptionRulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesList);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = transcriptionRulesList.get(randomizer.nextInt(transcriptionRulesList.size()));
	//	CurrentPage.As(RulesTab.class).chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the rule name
		//Eliminate unwanted zeros from the rule Number e.g. 006 -> 6
		String ruleNumber = randomrecord.split(" : ")[0];
		String ruleNumber4Select = ruleNumber;
		ruleNumber = Integer.toString(Integer.parseInt(ruleNumber));
		String ruleName = randomrecord.split(" : ")[1];
		LogUltility.log(test, logger, "The random rule text is: " +ruleName);
		
		// Click the rule
		CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, randomrecord);
		LogUltility.log(test, logger, "Click the rule to select it ");
		
        // get the text from the rule name field
		String textFromFieldB4Change = RulesTab.ruleName.getText();
		LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldB4Change);
		
		// verify that the rule name is the same before change 
		boolean sameName = textFromFieldB4Change.equals(ruleName);
		LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldB4Change+" is equal for the text from the list : " +ruleName+ " and value should be true : "+sameName );
		assertTrue(sameName);
		
		// Type a record name in the key form field
		String randomText = RulesTab.RandomString(5);
		RulesTab.ruleName.click();
		String newRandomText = RulesTab.ruleName.getText() + randomText;
		RulesTab.ruleName.sendKeys(newRandomText);
		LogUltility.log(test, logger, "New rule name: " + newRandomText);
		
		//Press the save button  
		RulesTab.btnSave.click();
		LogUltility.log(test, logger, "Click the Save button ");
		
		//Press the OK button in the saving Dialog
		Thread.sleep(3000);
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
		
        // get the text from the rule name field
		String newRuleName = ruleNumber4Select+ " : " +newRandomText;
		CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, newRuleName);
		
		String textFromFieldafterChange = RulesTab.ruleName.getText();
		LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldafterChange);
		
		// verify that the rule name was changed 
		boolean differentName = !textFromFieldafterChange.equals(ruleName);
		LogUltility.log(test, logger, "text from the rule name field after change: " +textFromFieldafterChange+" is different than the text from the list : " +ruleName+ " and value should be true : "+differentName );
		assertTrue(differentName);

		// verify that the rule name changed successfully on the file
		String ruleFile= "transcription_rules.txt";
		boolean isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, newRandomText ,ruleFile);
		assertTrue(isRuleExist);
		LogUltility.log(test, logger, "Verify that the new rule name do  appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is : " +ruleName+" ,  and the value should be True: " +isRuleExist);
		
		// Click the rule
		CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, randomrecord);
		LogUltility.log(test, logger, "Click the rule to select it ");
				
		// Get the priority value before change
		//get the rule Priority form priority field 
		List<String> chosenPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
		LogUltility.log(test, logger, "Rule priority from the priority field: " +chosenPriority);
		
		//Get the priority list
		List<String> lst_Priority = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).priorityDD);
		LogUltility.log(test, logger, "priority valueslist: " + lst_Priority);
		lst_Priority.remove(chosenPriority.get(0));
		LogUltility.log(test, logger, "priority values list after removing the chosen value: " + lst_Priority);
		
		// Press the Priority dropdown and chose a random value 
		Random randomizerPriority = new Random();
		String randomPriority = lst_Priority.get(randomizerPriority.nextInt(lst_Priority.size()));
		CommonFunctions.chooseValueInDropdown(RulesTab.priorityDD, randomPriority);
		LogUltility.log(test, logger, "Choose from priority list: " + randomPriority);
		
		//Press the save button  
		RulesTab.btnSave.click();
		LogUltility.log(test, logger, "Click the Save button ");
		
		//Press the OK button in the saving Dialog
		Thread.sleep(3000);
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
		
		boolean differentPriority = !chosenPriority.get(0).equals(randomPriority);
		LogUltility.log(test, logger, "Priority after change: " +randomPriority+" is different from the priority before the change : " +chosenPriority.get(0)+ " and value should be true : "+differentPriority );
		assertTrue(differentPriority);
		
		// Verify that priority was changed in the rules file
		Hashtable<Integer,Integer> rulesPriority = RulesTab.getRecordsPriorityFromRules(ruleFile);
		boolean priorityFileChange = !rulesPriority.get(Integer.parseInt(ruleNumber)).equals(Integer.parseInt(chosenPriority.get(0)));
		LogUltility.log(test, logger, "Priority value changed in file and should be true: "+priorityFileChange);
		assertTrue(priorityFileChange);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--575:Check the two arrows in the footer
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_575_Check_the_two_arrows_in_the_footer() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--575", "Check the two arrows in the footer");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		// Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Press the transcription tab 
		RulesTab.clickTabs("transcription");
		
	
		
		// get all the transcription rules as list
		List<String> transcriptionRulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesList);
		
		// get the rules name from the list
		String firstName = transcriptionRulesList.get(0);
		String secondName = transcriptionRulesList.get(1);
		
		// Click the first rule
		CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, firstName);
		LogUltility.log(test, logger, "Click the first rule to select it ");
		
		// Press the right arrow 
		RulesTab.Next_button.click();
		LogUltility.log(test, logger, "Click the next (right) button ");
		
		// Get the chosen rules list
		List<String> chosenRules = RulesTab.getChosenValueInDropdown(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "Chosen rules list after pressing the right arrow :  " +chosenRules);
	     
		//Check that the chosen rule is the same as the second rule 
	     boolean sameSecondRule = chosenRules.get(0).equals(secondName);
	 	LogUltility.log(test, logger, "Verify that the second rule is selected: " +chosenRules.get(0)+ " and it`s equal to the second rule from the list : " +secondName + " and value should be true :  " +sameSecondRule);
	     assertTrue(sameSecondRule);
			
		// Press the left arrow 
		RulesTab.Previous_button.click();
		LogUltility.log(test, logger, "Click the previous (left) button ");
		
		
		// Get the chosen rules list
		List<String> chosenRules2 = RulesTab.getChosenValueInDropdown(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "Chosen rules list after press the right arrow :  " +chosenRules2);

	     boolean sameFirstRule = chosenRules2.get(0).equals(firstName);
	 	LogUltility.log(test, logger, "Verify that the first rule is selected: " +chosenRules2.get(0)+ " and it`s equal to the second rule from the list : " +firstName + " and value should be true :  " +sameFirstRule);
	     assertTrue(sameFirstRule);
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	/**
	 * DIC_TC--576:Check the rule number in the number field
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_576_Check_the_rule_number_in_the_number_field() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--576", "Check the rule number in the number field");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the transcription tab 
		RulesTab.clickTabs("transcription");
		
		// get all the transcription rules as list
		List<String> transcriptionRulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesList);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = transcriptionRulesList.get(randomizer.nextInt(transcriptionRulesList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// get the rule number 
		String ruleNumberFromName = randomrecord.split(" : ")[0];
		LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
		
		//Convert the rule number from the name  to integer
		int ruleNumberFromNameInt= Integer.parseInt(ruleNumberFromName);
		LogUltility.log(test, logger, "Convert the number to integer: " +ruleNumberFromNameInt);
		
        // get the text from the rule number field
		String RuleNumberFromField = RulesTab.numberBox.getText();
		LogUltility.log(test, logger, "text from the rule name field: " +RuleNumberFromField);
		
		//Convert the rule number from field to integer
		int RuleNumberFromFieldInt= Integer.parseInt(RuleNumberFromField);
		LogUltility.log(test, logger, "Convert the number to integer: " +ruleNumberFromNameInt);
		
		
		// verify that the rule number is the same in the field and in the name 
		boolean sameName = ruleNumberFromNameInt==RuleNumberFromFieldInt;
		LogUltility.log(test, logger, "Rule number from the rule name : " +ruleNumberFromNameInt+" is equal for the number from the field  : " +RuleNumberFromFieldInt+ " and value should be true : "+sameName );
		assertTrue(sameName);
	
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--570:Verify user can make changes and Revert the old rule versions in exists rules
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_570_Verify_user_can_make_changes_and_Revert_the_old_rule_versions_in_exists_rules() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--570", "Verify user can make changes and Revert the old rule versions in exists rules");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the transcription tab 
		RulesTab.clickTabs("transcription");
		
		// get all the transcription rules as list
		List<String> transcriptionRulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesList);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = transcriptionRulesList.get(randomizer.nextInt(transcriptionRulesList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the rule name
		//Eliminate unwanted zeros from the rule Number e.g. 006 -> 6
		String ruleNumber = randomrecord.split(" : ")[0];
		String ruleNumber4Select = ruleNumber;
		ruleNumber = Integer.toString(Integer.parseInt(ruleNumber));
		String ruleName = randomrecord.split(" : ")[1];
		LogUltility.log(test, logger, "The random rule text is: " +ruleName);
		
		// Click the rule
		CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, randomrecord);
		LogUltility.log(test, logger, "Click the rule to select it ");
		
        // get the text from the rule name field
		String textFromFieldB4Change = RulesTab.ruleName.getText();
		LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldB4Change);
		
		// verify that the rule name is the same before change 
		boolean sameName = textFromFieldB4Change.equals(ruleName);
		LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldB4Change+" is equal for the text from the list : " +ruleName+ " and value should be true : "+sameName );
		assertTrue(sameName);
		
		// Change the rule name
		String randomText = RulesTab.RandomString(5);
		RulesTab.ruleName.click();
		String newRandomText = RulesTab.ruleName.getText() + randomText;
		RulesTab.ruleName.sendKeys(newRandomText);
		LogUltility.log(test, logger, "New rule name: " + newRandomText);
		
		// Click the rule again to refresh
		CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, randomrecord);
		LogUltility.log(test, logger, "Click the rule to refresh it ");
		
        // get the text from the rule name field
		String newRuleName = ruleNumber4Select+ " : " +newRandomText;
		CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, newRuleName);
		
		// Get the priority value before reverting
		//get the rule Priority form priority field 
		List<String> chosenPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
		LogUltility.log(test, logger, "Rule priority from the priority field: " +chosenPriority);
		
		//Get the priority list
		List<String> lst_Priority = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).priorityDD);
		LogUltility.log(test, logger, "priority valueslist: " + lst_Priority);
		lst_Priority.remove(chosenPriority.get(0));
		LogUltility.log(test, logger, "priority values list after removing the chosen value: " + lst_Priority);
		
		// Press the Priority dropdown and chose a random value 
		Random randomizerPriority = new Random();
		String randomPriority = lst_Priority.get(randomizerPriority.nextInt(lst_Priority.size()));
		CommonFunctions.chooseValueInDropdown(RulesTab.priorityDD, randomPriority);
		LogUltility.log(test, logger, "Choose from priority list: " + randomPriority);
		
		//Click the revert button to revert the changes
		 RulesTab.bt_revert.click();
		 LogUltility.log(test, logger, "Press the revert button");
		 
		// Press the OK button on the revert popup message revertFileMsgOK
		 RulesTab.revertFileMsgOK.click();
		 LogUltility.log(test, logger, "Press the OK button on the revert message");
		 
		 // Get the text from the rule name filed after reverting
		 String textFromFieldafterRevert = RulesTab.ruleName.getText();
		 LogUltility.log(test, logger, "text from the rule name field before change: " +textFromFieldafterRevert);
		 
		 // Get the priority value after reverting
		 List<String> chosenPriorityAfterReverting =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
		LogUltility.log(test, logger, "Rule priority from the priority field: " +chosenPriorityAfterReverting);
		
		// Verify that the priority didn't change
		boolean samePriorityAfterRevert = chosenPriorityAfterReverting.get(0).equals(chosenPriority.get(0));
		LogUltility.log(test,logger,"Priority before reverting: "+chosenPriority.get(0)+" equals to the Priority after reverting: "+chosenPriorityAfterReverting.get(0)+" and the value should be true: "+samePriorityAfterRevert);
		
		// verify that the rule name didn't change
		boolean sameNameAfterRevert = textFromFieldafterRevert.equals(ruleName);
		LogUltility.log(test, logger, "text from the rule name field after revert: " +textFromFieldafterRevert+" equals to the text from the list : " +ruleName+ " and value should be true : "+sameNameAfterRevert );
		assertTrue(sameNameAfterRevert);
	
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

	
	/**
	 * DIC_TC--571:Verify user can Delete exist rules
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_571_Verify_user_can_Delete_exist_rules() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--571", "Verify user can Delete exist rules");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the transcription rules tab 
		RulesTab.clickTabs("transcription");
		
		// get all the transcription rules as list
		List<String> transcriptionRulesListB4Add= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesListB4Add);
				
		// Press the new button
		RulesTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button ");
		
		//get the rule Number form Number field  
		String ruleNumber = RulesTab.numberBox.getText();
		LogUltility.log(test, logger, "The new Rule number from the Number field: " +ruleNumber);
		
		// Type a record name in the key form field
		String randomText = RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) ;
		RulesTab.ruleName.click();
		String newRandomText = randomText;
		RulesTab.ruleName.sendKeys(newRandomText);
		LogUltility.log(test, logger, "New rule name: " + newRandomText);
		
	    // get the text from the rule name field
		String textFromFieldB4Save = RulesTab.ruleName.getText();
		LogUltility.log(test, logger, "text from the rule name field before Save: " +textFromFieldB4Save);
		
		//Press the save button  
		RulesTab.btnSave.click();
		LogUltility.log(test, logger, "Click the Save button ");
		
		//Press the OK button in the saving Dialog
		Thread.sleep(1000);
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
		
		// Verify that the rule saved successfully on the file
		final String ruleFile= "transcription_rules.txt";
		boolean isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, textFromFieldB4Save ,ruleFile);
		assertTrue(isRuleExist);
		LogUltility.log(test, logger, "Verify that the new created rule do  appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is : " +textFromFieldB4Save+" ,  and the value should be True: " +isRuleExist);

		// Click the rule again to refresh
		List<String> transcriptionRulesListAfterAdd= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		String newRule = transcriptionRulesListAfterAdd.get(transcriptionRulesListAfterAdd.size()-1);

		CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, newRule);
		LogUltility.log(test, logger, "Click the rule to refresh it ");
				
		// Press the Delete button 
		RulesTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the Delete button");
		
		// Click yes button on the Delete popup 
		RulesTab.yesDeleteRule_bt.click();
		LogUltility.log(test, logger, "Click the Yes button on the Delete popup");
		
		//Press the OK button in the saving Dialog
		//Thread.sleep(3000);
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
		
		//Verify that the deleted rule doesn`t appear in the list anymore 
		// get all the transcription rules as list
		List<String> rulesListAfterDelete= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesListAfterDelete);
		
		boolean ruleDeleted = ! rulesListAfterDelete.contains(newRule);
		assertTrue(ruleDeleted);
		LogUltility.log(test, logger, " Verify that the list " + rulesListAfterDelete+ " doesn`t contain the deleted rule " + newRule + " the value should be : " +ruleDeleted);

		// Verify that the rule deleted from the transcription rules files
		String deletedRule = newRule.split(" : ")[1];
		isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, newRule.split(" : ")[1] ,ruleFile);
		assertFalse(isRuleExist);
		LogUltility.log(test, logger, "Verify that the Deleted rule do not appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is " +deletedRule+"  and the value should be False: " +isRuleExist);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
	 * DIC_TC--574:Verify user can sort the rules in the rules list by different categories part 1 by number
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_574_Verify_user_can_sort_the_rules_in_the_rules_list_by_different_categories_part_1_by_number() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--574", "Verify user can sort the rules in the rules list by different categories part 1 by number");
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		// Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
					
		// Press the transcription rules tab 
		RulesTab.clickTabs("transcription");	
		
		// Press on the Sort by dropdown list
		LogUltility.log(test, logger, "Click on the Sort by dropdown list ");
		
		// Choose to sort by "Number" from the dropdown list
		CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Number");
		LogUltility.log(test, logger, "Click on Number from the dropdown list ");
		
		// Get all the transcription rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
		// Get the rules numbers from the rules list
		ArrayList<Integer> rulesNumbers = new ArrayList<Integer>() ;
		
		for(int i=0;i<rulesList.size();i++)
		{
			String ruleNumber = rulesList.get(i).split(" : ")[0];
			rulesNumbers.add(Integer.parseInt(ruleNumber));
		}

		// Check if the rules numbers are sorted in ascending order
		boolean isSorted = true;
		for(int i=0,j=1;j<rulesNumbers.size();i++,j++)
			if(rulesNumbers.get(i) > rulesNumbers.get(j))
				{isSorted = false;break;}
		
		assertTrue(isSorted);
		LogUltility.log(test,logger,"The value should be true: "+isSorted);

		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	
	/**
	 * DIC_TC--574:Verify user can sort the rules in the rules list by different categories part 2 by name
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_574_Verify_user_can_sort_the_rules_in_the_rules_list_by_different_categories_part2_by_name() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--574", "Verify user can sort the rules in the rules list by different categories part 2 by name");
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		// Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
					
		// Press the transcription rules tab 
		RulesTab.clickTabs("transcription");	
		
		// Press on the Sort by dropdown list
		LogUltility.log(test, logger, "Click on the Sort by dropdown list ");
		
		// Choose to sort by "Name" from the dropdown list
		CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Name");
		LogUltility.log(test, logger, "Click on Name from the dropdown list ");
		
		// Get all the transcription rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
		// Get the rules names from the rules list
		ArrayList<String> rulesNames = new ArrayList<String>() ;
		
		for(int i=0;i<rulesList.size();i++)
		{
			String ruleName = rulesList.get(i).split(" : ")[1];
			// if it's a letter
			if((ruleName.charAt(0)<91 && ruleName.charAt(0)>64)|| (ruleName.charAt(0)<123 && ruleName.charAt(0)>96))
				rulesNames.add(ruleName.toLowerCase());
		}

		// Check if the rules names are sorted in alphabetical order
		boolean isSorted = true;
		if(rulesNames.size()>1)
		{
			String previous = rulesNames.get(0);  
			rulesNames.remove(0);
			for (String current: rulesNames)
			{
				int currentAscii = current.charAt(0);
				int prevAscii = previous.charAt(0);
			    if (currentAscii < prevAscii)
			    	{isSorted =  false;break;}
			    previous = current;
			}
		}
		assertTrue(isSorted);
		LogUltility.log(test,logger,"The value should be true: "+isSorted);

		// Reset sort value to number
		LogUltility.log(test, logger, "RESET TO DEFAULT VALUES ");
		CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Number");
				
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	
	/**
	 * DIC_TC--574:Verify user can sort the rules in the rules list by different categories part 3 by priority
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_574_Verify_user_can_sort_the_rules_in_the_rules_list_by_different_categories_part3_by_priority() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--574", "Verify user can sort the rules in the rules list by different categories part 3 by priority");
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		// Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
					
		// Press the transcription rules tab 
		RulesTab.clickTabs("transcription");	
		
		// Press on the Sort by dropdown list
		LogUltility.log(test, logger, "Click on the Sort by dropdown list ");
		
		// Choose to sort by "priority" from the dropdown list
		CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Priority");
		LogUltility.log(test, logger, "Click on Priority from the dropdown list ");
		
		// Get all the transcription rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
		// Get the rules priorities 	
		String ruleFile= "transcription_rules.txt";
		Hashtable<Integer,Integer> rulesPriority = RulesTab.getRecordsPriorityFromRules(ruleFile);
		ArrayList<Integer> sortedPriority = new ArrayList<Integer>();
		
		for(int i=0;i<rulesList.size();i++)
		{
			int ruleNumber = Integer.parseInt(rulesList.get(i).split(" : ")[0]);
			sortedPriority.add(rulesPriority.get(ruleNumber));
		}
		
		LogUltility.log(test, logger, "sorted priority: " +sortedPriority);
		
		// Check if the rules numbers are sorted in ascending order
		boolean isSorted = true;
		for(int i=0,j=1;j<sortedPriority.size();i++,j++)
			if(sortedPriority.get(i) < sortedPriority.get(j))
				{isSorted = false;break;}
		
		assertTrue(isSorted);
		LogUltility.log(test,logger,"The value should be true: "+isSorted);

		// Reset sort value to number
		LogUltility.log(test, logger, "RESET TO DEFAULT VALUES ");
		CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Number");
		
		LogUltility.log(test, logger, "Test Case PASSED");
		
	}
	
	
	
	/**
	 * DIC_TC--577:Check the application don�t save an empty rule �without name�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_577_Check_application_dont_save_an_empty_rule() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Rules Test - DIC_TC--577", "Check application don't save an empty rule");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the transcription rules tab 
		RulesTab.clickTabs("transcription");
		
		// Press the new button
		RulesTab.btnNew.click();
		
		// Press the Save button
		RulesTab.btnSave.click();
		
		// Verify that the new rule message appear ruleNameMsg
		boolean ruleNameMsgappear = RulesTab.popupWindowMessage(test, logger, "Rule's name", "This rule's name already exists");
		assertTrue(ruleNameMsgappear);
		LogUltility.log(test, logger, "Is the exist rule name message appear : " +ruleNameMsgappear);
		
		// Press the cancel button on the rule`s name popup ruleNameCancelbtn
		RulesTab.ruleNameCancelbtn.click();
		LogUltility.log(test, logger, "Press the cancel button on the rule`s name popup");
		
		// Press the yes button on the new rule popup 
		RulesTab.newRuleYesbtn.click();
		LogUltility.log(test, logger, "Press the Yes button on the new rule  popup");
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	
	
/**
 * DIC_TC--590:Check creating new rule with rule name that was deleted
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_590_Check_creating_new_rule_with_rule_name_that_was_deleted() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--590", "Check creating new rule with rule name that was deleted");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Press the transcription rules tab 
	RulesTab.clickTabs("transcription");
	
	// get all the transcription rules as list
	List<String> transcriptionRulesListB4Add= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesListB4Add);
			
	// Press the new button
	RulesTab.btnNew.click();
	LogUltility.log(test, logger, "Click the new button ");
	
	//get the rule Number form Number field  
	String ruleNumber = RulesTab.numberBox.getText();
	LogUltility.log(test, logger, "The new Rule number from the Number field: " +ruleNumber);
	
	// Type a record name in the key form field
	String randomText = RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) ;
	RulesTab.ruleName.click();
	String newRandomText = randomText;
	RulesTab.ruleName.sendKeys(newRandomText);
	LogUltility.log(test, logger, "New rule name: " + newRandomText);
	
    // get the text from the rule name field
	String textFromFieldB4Save = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before Save: " +textFromFieldB4Save);
	
	//Press the save button  
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	//	Thread.sleep(10000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	// Verify that the rule saved successfully on the file
	final String ruleFile= "transcription_rules.txt";
	boolean isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, textFromFieldB4Save ,ruleFile);
	assertTrue(isRuleExist);
	LogUltility.log(test, logger, "Verify that the new created rule do  appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is : " +textFromFieldB4Save+" ,  and the value should be True: " +isRuleExist);

	// Click the rule again to refresh
	List<String> transcriptionRulesListAfterAdd= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	String newRule = transcriptionRulesListAfterAdd.get(transcriptionRulesListAfterAdd.size()-1);

	CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, newRule);
	LogUltility.log(test, logger, "Click the rule to refresh it ");
			
	// Press the Delete button 
	RulesTab.bt_delete.click();
	LogUltility.log(test, logger, "Click the Delete button");
	
	// Click yes button on the Delete popup 
	RulesTab.yesDeleteRule_bt.click();
	LogUltility.log(test, logger, "Click the Yes button on the Delete popup");
	
	//Press the OK button in the saving Dialog
	//Thread.sleep(3000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	//Verify that the deleted rule doesn`t appear in the list anymore 
	// get all the transcription rules as list
	List<String> rulesListAfterDelete= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesListAfterDelete);
	
	boolean ruleDeleted = ! rulesListAfterDelete.contains(newRule);
	assertTrue(ruleDeleted);
	LogUltility.log(test, logger, " Verify that the list " + rulesListAfterDelete+ " doesn`t contain the deleted rule " + newRule + " the value should be : " +ruleDeleted);

	// Verify that the rule deleted from the transcription rules files
	String deletedRule = newRule.split(" : ")[1];
	isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, newRule.split(" : ")[1] ,ruleFile);
	assertFalse(isRuleExist);
	LogUltility.log(test, logger, "Verify that the Deleted rule do not appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is " +deletedRule+"  and the value should be False: " +isRuleExist);
	
	LogUltility.log(test, logger, "Adding new rule with the same name as the old deleted rule ");
	
	// Press the new button again
	RulesTab.btnNew.click();
	LogUltility.log(test, logger, "Click the new button ");
	
	//get the rule Number form Number field  
	ruleNumber = RulesTab.numberBox.getText();
	LogUltility.log(test, logger, "The new Rule number from the Number field: " +ruleNumber);
	
	// Type the same record name in the key form field
	RulesTab.ruleName.sendKeys(newRandomText);
	LogUltility.log(test, logger, "New rule name equals the old deleted rule name: " + newRandomText);
	
    // get the text from the rule name field
	textFromFieldB4Save = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before Save: " +textFromFieldB4Save);
	
	//Press the save button  
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	//	Thread.sleep(10000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	// Verify that the rule saved successfully on the file
	isRuleExist= RulesTab.getRecordsFromRules(ruleNumber, textFromFieldB4Save ,ruleFile);
	assertTrue(isRuleExist);
	LogUltility.log(test, logger, "Verify that the new created rule do  appear in the rule file after saving ,Rule number is :"  +ruleNumber+ " and rule name is : " +textFromFieldB4Save+" ,  and the value should be True: " +isRuleExist);

	LogUltility.log(test, logger, "Test Case PASSED");	
}

	
	
/**
 * DIC_TC--581:Change the dropdown options in the conditions
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_581_Change_the_dropdown_options_in_the_conditions() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--481", "Check the drop down options in the conditions");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Press the transcription rules tab 
	RulesTab.clickTabs("transcription");

	// get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "002 : /z/ -> [s] / voiceless non-sibilant __";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);

	 // Get the amount of panes/condition for the rule
	 int panesAmount = RulesTab.getDisplayedRulePanesAmount();
	 LogUltility.log(test, logger, "The number of conditions the rule has is: "+panesAmount);
	 
	 // Get random index
	 int paneIndex = randomizer.nextInt(panesAmount);
	 LogUltility.log(test, logger, "Edit pane number: "+paneIndex);
	 
	// Get pane dropdown values
	List<String> paneValues = RulesTab.readRulesInputAreaValues(paneIndex);
	LogUltility.log(test, logger, "Displayed pane values: "+paneValues);
	
	// Get the first list values
	List<String> keys = RulesTab.readRulesInputAreaDropDownValues(paneIndex, 1);
	keys.remove(paneValues.get(1));
	String key = keys.get(randomizer.nextInt(keys.size()));
	LogUltility.log(test, logger, "Choose new key value: " + key);
	
	//Change the  first dropdown values
	WebElement dropdownElement = RulesTab.rulesInputArea(paneIndex,0,paneValues.get(1), key);
	List<String> fisrtdropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement);
	LogUltility.log(test, logger, "the first dropdown element is : " + fisrtdropdownvalue);
	
	// Get the second list values
	List<String> secDropdown = RulesTab.readRulesInputAreaDropDownValues(paneIndex, 2);
	secDropdown.remove(paneValues.get(2));
	String key2 = secDropdown.get(randomizer.nextInt(secDropdown.size()));
	LogUltility.log(test, logger, "Choose second dropdown value: " + key2);
		
	//Change the  second dropdown values
	WebElement dropdownElement2 =RulesTab.rulesInputArea(paneIndex,0,paneValues.get(2), key2);
	List<String> seconddropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement2);
	LogUltility.log(test, logger, "the second dropdown element is : " + seconddropdownvalue);
	
	
	// Get the third list values
	List<String> thirdDropdown = RulesTab.readRulesInputAreaDropDownValues(paneIndex, 3);
	String key3 = thirdDropdown.get(randomizer.nextInt(thirdDropdown.size()));
	LogUltility.log(test, logger, "Choose third dropdown value: " + key3);
	
	// Since changing the first dropdown will reset the third dropdown value
	List<String> paneValues2 = RulesTab.readRulesInputAreaValues(paneIndex);
		
	//Change the  third dropdown values
	WebElement dropdownElement3 =RulesTab.rulesInputArea(paneIndex,0,paneValues2.get(3),key3);
	List<String> thirddropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement3);
	LogUltility.log(test, logger, "the third dropdown element is : " + thirddropdownvalue);

	//Press the save button  
	Thread.sleep(3000);
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	Thread.sleep(3000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	//Select the same rule
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);	
	
	// Verify that the new values still appear in the dropdowns
	boolean FDD = fisrtdropdownvalue.get(0).equals(key);
	assertTrue(FDD);
	LogUltility.log(test, logger, "The value in the first dropdown is:"+key+" and value should be True: "+FDD );
	
	boolean SDD = seconddropdownvalue.get(0).equals(key2);
	assertTrue(SDD);
	LogUltility.log(test, logger, "The value in the second dropdown is:"+key2+" and value should be True: "+SDD );
	
	boolean THDD = thirddropdownvalue.get(0).equals(key3);
	assertTrue(THDD);
	LogUltility.log(test, logger, "The value in the third dropdown is:"+key3+" and value should be True: "+THDD );	

	LogUltility.log(test, logger, "Test Case PASSED");	
}
	
	

/**
 * DIC_TC--582:Priority value is not mixed in disambiguation, normalization and prosody and transcription tabs
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_582_Priority_value_is_not_mixed_in_disambiguation_normalization_and_prosody_and_transcription_tabs() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--582", "Priority value is not mixed in disambiguation, normalization and prosody and transcription tabs");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// Click the normalization rules tab
	RulesTab.normalizationTab.click();
	LogUltility.log(test, logger, "Click the normalization rules tab ");
	
	// get all the normalization rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.normalizationRulesList);
	LogUltility.log(test, logger, "get all the normalization rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).normalizationRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	//get the rule Priority form priority field 
	List<String> chosenNorPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
	//String ruleScore = RulesTab.ScoreDD.getText();
	LogUltility.log(test, logger, "Rule priority from the priority field For normalization rule: " +chosenNorPriority);
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	LogUltility.log(test, logger, "Navigate back to the transcription rules tab ");
	
	
	// get all the transcription rules as list
	List<String> transcriptionRulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesList);

	// Choose random record from the middle of the displayed list
	transcriptionRulesList = transcriptionRulesList.subList(0, 20 > transcriptionRulesList.size() ? transcriptionRulesList.size() : 20);

	// Pick a random record
	randomizer = new Random();
	String proRandomrecord = transcriptionRulesList.get(randomizer.nextInt(transcriptionRulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, proRandomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + proRandomrecord);

	//Get the priority list
	List<String> lst_Priority = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).priorityDD);
	LogUltility.log(test, logger, "priority valueslist: " + lst_Priority);
	
	// Remove the old value from the list to make sure to pick a new value
	lst_Priority.remove(chosenNorPriority.get(0));
	LogUltility.log(test, logger, "priority values list after removing the normalization record value: " + lst_Priority);
	
	// Press the Priority dropdown and chose a random value 
	Random randomizerPriority = new Random();
	String randomPriority = lst_Priority.get(randomizerPriority.nextInt(lst_Priority.size()));
	CommonFunctions.chooseValueInDropdown(RulesTab.priorityDD, randomPriority);
	LogUltility.log(test, logger, "Choose from priority list: " + randomPriority);	

	// Click the normalization rules tab
	RulesTab.normalizationTab.click();
	LogUltility.log(test, logger, "Navigate back to the normalization rules tab ");
	
	// Choose the same record again
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).normalizationRulesList, randomrecord);
	

	//get the rule Priority form priority field 
	List<String> chosenNorPriority2 =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
	//String ruleScore = RulesTab.ScoreDD.getText();
	LogUltility.log(test, logger, "Rule priority from the priority field For normalization rule: " +chosenNorPriority2.get(0));

	boolean noChange = chosenNorPriority.get(0).equals(chosenNorPriority2.get(0));
	assertTrue(noChange);
	
	LogUltility.log(test, logger, "The compartion value should be true: "+noChange);	
	LogUltility.log(test, logger, "Test Case PASSED");	
}

	
	

/**
 * DIC_TC--583:Check condition type
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_583_Check_condition_type() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--583", "Check condition type");

	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
//	RulesTab.rulesTab.click();
	CommonFunctions.clickTabs("Rules");
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "004 : to before a vowel";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	// Add two more conditions
	RulesTab.rulesInputArea();
	RulesTab.rulesInputArea();
	LogUltility.log(test, logger, "Add new panes");

	// Change the Word In Position value in the middle pane
	RulesTab.rulesInputArea(1, 1);
	LogUltility.log(test, logger, "Change Word In Position value in the middle pane");
	
	// Change the Word In Position value in the last pane
	RulesTab.rulesInputArea(2, 2);
	LogUltility.log(test, logger, "Change Word In Position value in the last pane");

	// Check that the condition type 
	boolean conditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Mandatory");
	LogUltility.log(test, logger, "Verify that the condition type is mandatory and the value should be True: " +conditionType);
	assertTrue(conditionType);

	//Click on the middle row to change Mandatory to Optional
	 RulesTab.rulesInputArea(1, "Mandatory");
	 LogUltility.log(test, logger, "Click on the middle row to change Mandatory to Optional");
	 
	// Check the condition type after press the Mandatory , it should become Optional
	boolean newConditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Optional");
	LogUltility.log(test, logger, "Verify that the condition type is Optional and the value should be True: " +newConditionType);
	assertTrue(newConditionType);
	 
	//Click on the middle row to change Optional to Mandatory  
	 RulesTab.rulesInputArea(1, "Optional");
	 LogUltility.log(test, logger, "Click on the middle row to change Optional to Mandatory");
	 
	// Check that the condition type become mandatory after press the optional button
	boolean firstConditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Mandatory");
	LogUltility.log(test, logger, "Verify that the condition type is mandatory and the value should be True: " +firstConditionType);

   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");
	 
//	 // Click yes 
//	 RulesTab.yesDeleteRule_bt.click();

	LogUltility.log(test, logger, "Test Case PASSED");	
}




/**
 * DIC_TC--584:Check the plus button in the gray box in each sub condition
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_584_Check_the_plus_button_in_the_gray_box_in_each_sub_condition() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--584", "Check the plus button in the gray box in each sub condition");

	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "004 : to before a vowel";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field: " +ruleName);

	 // Get number of conditions
	 int panesAmount = RulesTab.getDisplayedRulePanesAmount();
	 LogUltility.log(test, logger, "Number of conditions: "+panesAmount);
	 
	 // Choose random pane to delete sub condition from
	 int paneIndex = randomizer.nextInt(panesAmount);
	 LogUltility.log(test, logger, "Delete sub condition from pane: "+paneIndex);
	 	
	//Click on the right green plus + button
	 RulesTab.rulesInputArea(paneIndex, "add condition");
	 LogUltility.log(test, logger, "Click on the right green plus + button near the condition: "+paneIndex);
	 
	 // Get the amount of displayed sub conditions in a pane
	 int subAmount = RulesTab.getSubConditionsAmountinPane(paneIndex);
	 LogUltility.log(test, logger, "Number of sub conditions in the pane: "+subAmount);
		 
	 // Verify that a new subcondition added with operator OR
	 String buttonValue = RulesTab.rulesInputAreaCheckOR(paneIndex,subAmount-1,0);
	 LogUltility.log(test, logger, "Verify that the button value is or : " +buttonValue);
	 boolean buttonChanged = buttonValue.equals("or");
	 LogUltility.log(test, logger, "Verify that the button value is or, so the value should be tru: " +buttonChanged);
	 assertTrue(buttonChanged);
	 
   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	
}





/**
 * DIC_TC--585: Check the �or�, �and� values in the sub conditions
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_585_Check_the_or_and_values_in_the_sub_conditions() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--585", "Check the �or�, �and� values in the sub conditions");
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "002 : /z/ -> [s] / voiceless non-sibilant __";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	 // Get the amount of panes/condition for the rule
	 int panesAmount = RulesTab.getDisplayedRulePanesAmount();
	 LogUltility.log(test, logger, "The number of conditions the rule has is: "+panesAmount);
	 
	// Choose random pane to delete sub condition from
	 int paneIndex = randomizer.nextInt(panesAmount);
	 LogUltility.log(test, logger, "Use sub condition from pane: "+paneIndex);
	 
	 // Add sub condition
	 RulesTab.rulesInputArea(paneIndex, "add condition");
	 LogUltility.log(test, logger, "Add new sub condition in pane: "+paneIndex);
	 
	 // Get the amount of displayed sub conditions in a pane
	 int subAmount = RulesTab.getSubConditionsAmountinPane(paneIndex);
	 LogUltility.log(test, logger, "Number of sub conditions in the pane: "+subAmount);
	 
	// Choose random sub condition to delete
	 int subIndex = subAmount -1;

	//Click on the blue �or� button to change it to and
//	 RulesTab.rulesInputArea(paneIndex,subIndex,0);
	RulesTab.clickbtnBetweenSubCon(paneIndex,subIndex);
	 LogUltility.log(test, logger, "Click on the blue �or� button to change it to and , and appear instead of or");
	 
	 // Verify that the OR changed to and
	 String buttonValue = RulesTab.rulesInputAreaCheckOR(paneIndex,subIndex,0);
	 LogUltility.log(test, logger, "Verify that the button value is and : " +buttonValue);
	 boolean buttonChanged = buttonValue.equals("and");
	 LogUltility.log(test, logger, "Verify that the button value is and, so the value should be tru: " +buttonChanged);
	 assertTrue(buttonChanged);
 
	//Click on the blue �or� button to change it to and
//	 RulesTab.rulesInputArea(paneIndex,subIndex,0);
	 RulesTab.clickbtnBetweenSubCon(paneIndex,subIndex);
	 LogUltility.log(test, logger, "Click on the blue �and� button to change it to or , or appear instead of and");
	 
	 // Verify that the and changed to or
	 String buttonValue2time = RulesTab.rulesInputAreaCheckOR(paneIndex,subIndex,0);
	 LogUltility.log(test, logger, "Verify that the button value is and : " +buttonValue2time);
	 boolean buttonChanged2time = buttonValue2time.equals("or");
	 LogUltility.log(test, logger, "Verify that the button value is and, so the value should be tru: " +buttonChanged2time);
	 assertTrue(buttonChanged2time);
	 
  //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
 // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	
}


/**
 * DIC_TC--586:Check the user can delete rule�s conditions
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_586_Check_the_user_can_delete_rule_conditions() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--586", "Check the user can delete rule�s conditions");

	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "002 : /z/ -> [s] / voiceless non-sibilant __";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	 // Get the amount of panes/condition for the rule
	 int panesAmount = RulesTab.getDisplayedRulePanesAmount();
	 LogUltility.log(test, logger, "The number of conditions the rule has is: "+panesAmount);
	 
	// Choose random pane to delete sub condition from
	 int paneIndex = randomizer.nextInt(panesAmount);
	 LogUltility.log(test, logger, "Use sub condition from pane: "+paneIndex);
	 
	 // Add sub condition
	 RulesTab.rulesInputArea(paneIndex, "add condition");
	 LogUltility.log(test, logger, "Add new sub condition in pane: "+paneIndex);
	 
	 // Get the amount of displayed sub conditions in a pane
	 int subAmount = RulesTab.getSubConditionsAmountinPane(paneIndex);
	 LogUltility.log(test, logger, "Number of sub conditions in the pane: "+subAmount);
	 
	// Choose random sub condition to delete
	 int subIndex = subAmount -1;
 
	//Click on the right red X button to delete sub condition
	 RulesTab.rulesInputAreaDeleteSubCondition(paneIndex,subIndex);
	 LogUltility.log(test, logger, "Click on the right red X button to delete sub condition");
	
   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	
}




/**
 * DIC_TC--587:Verify user can delete conditions
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_587_Verify_user_can_delete_conditions() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--587", "Verify user can delete conditions");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "004 : to before a vowel";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// Add one more pane
//	RulesTab.rulesInputArea();
			
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	 int panesAmount = RulesTab.getDisplayedRulePanesAmount();
	 LogUltility.log(test, logger, "Number of conditions: "+panesAmount);
	 
	// Choose random pane to delete sub condition from
	 int paneIndex = randomizer.nextInt(panesAmount);
	 LogUltility.log(test, logger, "Delete sub condition from pane: "+paneIndex);
	 
	 // Add sub condition
	 RulesTab.rulesInputArea(paneIndex, "add condition");
	 LogUltility.log(test, logger, "Add new sub condition ");
	 
	 // Get the amount of displayed sub conditions in a pane
	int subAmount = RulesTab.getSubConditionsAmountinPane(paneIndex);
	 LogUltility.log(test, logger, "Number of sub conditions in the pane: "+subAmount);
	 
	 // Choose random sub condition to delete
	 int subIndex = randomizer.nextInt(subAmount);
		
	//Click on the red [x] in the left screen to delete the condition 
	 RulesTab.rulesInputAreaDeleteSubCondition(paneIndex,subIndex);
	 LogUltility.log(test, logger, "Click on the red [x] in the left screen to delete the sub condition: "+subIndex);
	 
   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	
}



/**
 * DIC_TC--588: Verify user can delete sub conditions in each rule
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_588_Verify_user_can_delete_sub_conditions_in_each_rules() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC_588", "Verify user can delete sub conditions in each rule");
	
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "001 : the before a consonant";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
	 // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	// Get the amount of panes/condition for the rule
	 int panesAmount = RulesTab.getDisplayedRulePanesAmount();
	 LogUltility.log(test, logger, "The number of conditions the rule has is: "+panesAmount);
	 
	 // Add new pane/condition
	 RulesTab.rulesInputArea();
	LogUltility.log(test, logger, "Add new condition/pane" );		
	RulesTab.rulesInputArea(panesAmount,0,"unknown", "article");
			
	//Click on the gray �or� button to add condition
	 RulesTab.rulesInputArea(panesAmount,0,0);
	 LogUltility.log(test, logger, "Click on the gray �or� button to add dropdown , New dropdown added");
	 
	 // Check that the dropdown removed after pressing the OR button for the second time
	 List<String> paneValues = RulesTab.readRulesInputAreaValues(panesAmount);
	 boolean DDExist =  RulesTab.rulesInputAreaIfExist(panesAmount, 0, "ComboBox", paneValues.get(4));
	 LogUltility.log(test, logger, "Verify that the Dropdown was added after press the 'OR' button for the first time. value should be true : " + DDExist);
	 assertTrue(DDExist);
	 
	 // Get values from the third dropdown
	 List<String> ddValues = RulesTab.readRulesInputAreaDropDownValues(panesAmount, 4);
	 ddValues.remove(paneValues.get(4));
	 String d3Val = ddValues.get(randomizer.nextInt(ddValues.size()));
	LogUltility.log(test, logger, "Choose new value: "+d3Val);
		
	 // verify that new dropdown added , by selecting new value than the unknown 
	WebElement dropdownElement3 =RulesTab.rulesInputArea(panesAmount,0,paneValues.get(4), d3Val);
	List<String> thirddropdownvalue = RulesTab.getChosenValueInDropdown(dropdownElement3);
	LogUltility.log(test, logger, "the third dropdown element is : " + thirddropdownvalue);
	LogUltility.log(test, logger, "in the second row change the :"+paneValues.get(4)+"  dropdown  to: "+d3Val);

	boolean THDD = thirddropdownvalue.get(0).equals(d3Val);
	assertTrue(THDD);
	LogUltility.log(test, logger, "The value in the first dropdown is:"+d3Val+" and value should be True: "+THDD );
	
	//Click on the gray �or� button to add condition
	 RulesTab.rulesInputArea(panesAmount,0,1);
	 LogUltility.log(test, logger, "Click on the gray �or� button to delete the last created dropdown , Last Dropdown removed");
	
	 // Check that the dropdown removed after pressing the OR button for the second time
	 boolean DDDeleted =  RulesTab.rulesInputAreaIfExist(panesAmount, 0, "ComboBox", paneValues.get(4));
	 LogUltility.log(test, logger, "Verify that the Dropdown was deleted after press the 'OR' button for the second time. value should be False : " + DDDeleted);
	 assertFalse(DDDeleted);
	
  //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
 // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	
}




/**
 * DIC_TC--589: Check the user gets cannot remove last condition popup
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_589_Check_the_user_gets_cannot_remove_last_condition_popup() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC_589", "Check the user gets cannot remove last condition popup");

	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "005 : adjust transcription of \"they're\"";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	// Get number of displayed panes
	int panesAmount = RulesTab.getDisplayedRulePanesAmount();
	LogUltility.log(test, logger, "Number of panes: " +panesAmount);
	
	// Choose random pane index
	int paneIndex = randomizer.nextInt(panesAmount);
	LogUltility.log(test, logger, "Pane index: " +paneIndex);
	
	//Click on the right red X button to delete sub condition
	 RulesTab.rulesInputAreaDeleteSubCondition(paneIndex,0);
	 LogUltility.log(test, logger, "Click on the right red X button to delete sub condition");
	 
	 //Verify that the Cannot remove last condition popup appear
	 boolean cannotRemoveLastConditionPopup =RulesTab.popupWindowMessage( test,  logger, "Cannot remove last condition", "Cannot remove last condition in the condition list");
	 LogUltility.log(test, logger, "Verify that the Cannot remove last condition popup appear, value should be true: " +cannotRemoveLastConditionPopup);
	 assertTrue(cannotRemoveLastConditionPopup);
	
	 //Press the OK button on the Cannot remove last condition popup to close it
	 RulesTab.cannotRemoveLastConditionpopupOK_btn.click();
	 LogUltility.log(test, logger, "Press the OK button to close the Cannot remove last condition popup");

   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	
}



/**
 * DIC_TC--592:Check Priority value does not reset after save process
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_592_Check_priority_value_does_not_reset_after_save_process() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--592", "Check Priority value does not reset after save process");
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	// Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
				
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// Get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	//Choose random record from the list
	CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Click the rule "+randomrecord);
	
	//get the rule Number form Number field  
	int ruleNumber = Integer.parseInt(RulesTab.numberBox.getText());
	LogUltility.log(test, logger, "The selected rule number from the Number field: " +ruleNumber);
	
	// Get the rules priorities 	
	final String ruleFile= "transcription_rules.txt";
	Hashtable<Integer,Integer> rulesPriority = RulesTab.getRecordsPriorityFromRules(ruleFile);
	// Get the rule priority before making a change
	int rulePriorityB4Change = rulesPriority.get(ruleNumber);
	LogUltility.log(test, logger, "Rule priority from file before change: " +rulePriorityB4Change);
	
	
	//Get the priority list
	List<String> lst_Priority = CurrentPage.As(RulesTab.class).getValuesFromApp(CurrentPage.As(RulesTab.class).priorityDD);
	LogUltility.log(test, logger, "priority valueslist: " + lst_Priority);
	
	// Remove the old value from the list to make sure to pick a new value
	lst_Priority.remove(Integer.toString(rulePriorityB4Change));
	LogUltility.log(test, logger, "priority values list after removing the chosen value: " + lst_Priority);
	
	// Press the Priority dropdown and chose a random value 
	Random randomizerPriority = new Random();
	String randomPriority = lst_Priority.get(randomizerPriority.nextInt(lst_Priority.size()));
	CommonFunctions.chooseValueInDropdown(RulesTab.priorityDD, randomPriority);
	LogUltility.log(test, logger, "Choose from priority list: " + randomPriority);

	//Press the save button  
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	//	Thread.sleep(10000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK button on the Saving button ");
			
	//Choose random record from the list
	CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Click the rule "+randomrecord);
	
	// Verify the priority is changed
	
	//get the rule Priority form priority field 
	List<String> chosenPriority =CurrentPage.As(RulesTab.class).getChosenValueInDropdown(CurrentPage.As(RulesTab.class).priorityDD);
	LogUltility.log(test, logger, "Rule new priority from the priority field: " +chosenPriority.get(0));
	boolean priorityChangedInDropdown = randomPriority.equals(chosenPriority.get(0));
	LogUltility.log(test,logger,"The choosen value: "+randomPriority +" equals the displayed priority: "+chosenPriority.get(0)+" The value should be true: " +priorityChangedInDropdown);
	assertTrue(priorityChangedInDropdown);
	
	// Verify changes in file
	rulesPriority = RulesTab.getRecordsPriorityFromRules(ruleFile);
	// Get the rule priority after making a change
	int rulePriorityAfterChange = rulesPriority.get(ruleNumber);
	LogUltility.log(test, logger, "Rule priority from file after change: " +rulePriorityAfterChange);
	
	boolean priorityChangedInFile = (rulePriorityAfterChange == Integer.parseInt(randomPriority));
	assertTrue(priorityChangedInFile);
	LogUltility.log(test, logger, "Changes in file value should be true: "+priorityChangedInFile);
	
	LogUltility.log(test, logger, "Test Case PASSED");
	
}




/**
 * DIC_TC--593:Check the description box
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_593_Check_the_description_box() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--593", "Check the description box");
	
//	// Get records from the Rules file
//	CurrentPage = GetInstance(RulesTab.class);
//	List<String> recordsFromFile = CurrentPage.As(RulesTab.class).getRecordsFromRules();
//	
//	// Pick a random record
//	Random randomizer = new Random();
//	String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// Get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	//Choose random record from the list
	CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Click the rule "+randomrecord);
	
	
	//get the rule Number form Number field  
	String ruleNumber = RulesTab.numberBox.getText();
	LogUltility.log(test, logger, "Rule number from the Number field: " +ruleNumber);
	
	// Get the text from the Description field 
	String ruleDescription = RulesTab.descriptionBox.getText();
	LogUltility.log(test, logger, "Rule description before change: " +ruleDescription);
	
	
	// Type a new description on the field 
	String randomText = RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5) + " " + RulesTab.RandomString(5);
	RulesTab.descriptionBox.click();
	String newRandomText = randomText;
	RulesTab.descriptionBox.sendKeys(newRandomText);
	LogUltility.log(test, logger, "New rule Description: " + newRandomText);
	
	
	//Press the save button  
	//Thread.sleep(3000);
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	//Thread.sleep(3000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	//Select the same rule
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	
	// Get the text from the Description field 
	String ruleDescriptionAchange = RulesTab.descriptionBox.getText();
	LogUltility.log(test, logger, "Rule description after change: " +ruleDescriptionAchange);
	
	
//	// Get the text from the Description field 
//	String ruleDescription = RulesTab.descriptionBox.getText();
//	LogUltility.log(test, logger, "Rule description before change: " +ruleDescription);
	
	
	//Verify that the rule have a new priority that different than the first one 
	boolean descriptionChanged = ! ruleDescription.equals(ruleDescriptionAchange);               
	assertTrue(descriptionChanged);
	LogUltility.log(test, logger, " Verify that the First Description : " + ruleDescription + " different than the new Description : " + ruleDescriptionAchange + " the value should be True : " +descriptionChanged);
	
//	//Close the application the reopen and navigate to the same rule and verify that the new discription appear
//	
//	//Select the same rule
//	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).disambiguationRulesList, randomrecord);
//	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
//	
//	// Get the text from the Description field 
//	String ruleDescriptionAreopen = RulesTab.descriptionBox.getText();
//	LogUltility.log(test, logger, "Rule description before change: " +ruleDescriptionAreopen);
//	
//	//Verify that the rule have a new priority that different than the first one 
//	boolean descriptionChangedAReopen = ruleDescriptionAchange.equals(ruleDescriptionAreopen);               
//	assertTrue(descriptionChangedAReopen);
//	LogUltility.log(test, logger, " Verify that the First Priority " + ruleDescriptionAchange + " different than the new Priority " + ruleDescriptionAreopen + " the value should be True : " +descriptionChangedAReopen);
//	
//	
	

	LogUltility.log(test, logger, "Test Case PASSED");	
}


	
/**
 * DIC_TC--594:Check the Overlapping condition lists popup
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_594_Check_the_Overlapping_condition_lists_popup() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--594", "Check the Overlapping condition lists popup");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// Get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	//Choose random record from the list
	CommonFunctions.chooseValueInDropdown(RulesTab.transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Click the rule "+randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	//Click on the right green plus + button on the upper right side 
	 RulesTab.rulesInputArea();
	// RulesTab.rulesInputArea();
	LogUltility.log(test, logger, "Click on the right green plus + button on the upper right side ");	 
	
	//Press the save button  
	//Thread.sleep(3000);
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");

	// Verify that the overlapping popup appear
	boolean overlappingPopupAppear = RulesTab.popupWindowMessage(test, logger, "Overlapping Condition Lists", "Your rule has more than one condition list per word, please correct that before continuing.");
	LogUltility.log(test, logger, "Is the Overlapping popup appear ? value should be True : " + overlappingPopupAppear);
	assertTrue(overlappingPopupAppear);
	
	// Click the OK button on the overlapping popup
	RulesTab.overlappingOK_btn.click();
	LogUltility.log(test, logger, "Click the OK button on the overlapping popup ");
	
   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	

}



/**
 * DIC_TC--595: Check changing condition for the first word
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_595_Check_changing_condition_for_the_first_word() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC_595", "Check changing condition for the first word");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// Get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	// Check that the condition type 
	boolean conditionType = RulesTab.rulesInputAreaMandatoryOrOptional(0,"Mandatory");
	LogUltility.log(test, logger, "Verify that the condition type is mandatory and the value should be True: " +conditionType);
	assertTrue(conditionType);

	//Click on the last row to change Mandatory to Optional
	 RulesTab.rulesInputArea(0, "Mandatory");
	 LogUltility.log(test, logger, "Click on the last row to change Mandatory to Optional");
	 
	// Verify that the condition List referring to word popup appear
	boolean conditionPopupAppear = RulesTab.popupWindowMessage(test, logger, "", "The condition List referring to word IN POSITION 0 cannot be optional!");
	LogUltility.log(test, logger, "Is The condition List referring to word IN POSITION 0 cannot be optional! popup appear ? value should be True : " + conditionPopupAppear);
	assertTrue(conditionPopupAppear);
	
	// press the OK button for the The condition List referring to word IN POSITION 0 cannot be optional! popup
	 RulesTab.conditionpopupOK_btn.click();
	 LogUltility.log(test, logger, "Press the OK button for the The condition List referring popup");
 
	// Check the condition type still Mandatory 
	boolean newConditionType = RulesTab.rulesInputAreaMandatoryOrOptional(0,"Mandatory");
	LogUltility.log(test, logger, "Verify that the condition type not changed and still Mandatory, and the value should be True: " +newConditionType);
	assertTrue(newConditionType);

   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	
}





/**
 * DIC_TC--596: Check changing condition for the last word
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_596_Check_changing_condition_for_the_last_word() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC_596", "Check changing condition for the last word");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// Get all the transcription rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);
	
	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "005 : adjust transcription of \"they're\"";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	
	// Get amount of panes
			int panesAmount = RulesTab.getDisplayedRulePanesAmount();
			// If there is only one condition add new one
			if(panesAmount == 1)
			{
				RulesTab.rulesInputArea();
				// Change its value
				RulesTab.rulesInputArea(panesAmount, 1);
			}
			
			int paneIndex = RulesTab.getEdgePaneIndex();

			// Check that the condition type 
			boolean conditionType = RulesTab.rulesInputAreaMandatoryOrOptional(paneIndex,"Mandatory");
			LogUltility.log(test, logger, "Verify that the condition type is mandatory and the value should be True: " +conditionType);
			assertTrue(conditionType);
			

	//Click on the last row to change Mandatory to Optional
	 RulesTab.rulesInputArea(paneIndex, "Mandatory");
	 LogUltility.log(test, logger, "Click on the last row to change Mandatory to Optional");
	 
	// Verify that the condition List referring to word.... popup appear
	boolean conditionPopupAppear = RulesTab.popupWindowMessage(test, logger, "", "The condition List referring to an EDGE word cannot be optional!");
	LogUltility.log(test, logger, "Is The condition List referring to an EDGE word cannot be optional! popup appear ? value should be True : " + conditionPopupAppear);
	assertTrue(conditionPopupAppear);
	
	// press the OK button for the The condition List referring to word IN POSITION 0 cannot be optional! popup
	 RulesTab.conditionpopupOK_btn.click();
	 LogUltility.log(test, logger, "Press the OK button for the The condition List referring popup");
	 
	// Check the condition type still Mandatory 
	boolean newConditionType = RulesTab.rulesInputAreaMandatoryOrOptional(1,"Mandatory");
	LogUltility.log(test, logger, "Verify that the condition type not changed and still Mandatory, and the value should be True: " +newConditionType);
	assertTrue(newConditionType);

   //Click the revert button to revert the changes
	 RulesTab.bt_revert.click();
	 LogUltility.log(test, logger, "Press the revert button");
	 
  // Press the OK button on the revert popup message revertFileMsgOK
	 RulesTab.revertFileMsgOK.click();
	 LogUltility.log(test, logger, "Press the OK button on the revert message");

	LogUltility.log(test, logger, "Test Case PASSED");	
}


/**
 * DIC_TC--711: Check the position of the word could be chagned and saved
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_711_Check_the_position_of_the_word_could_be_chagned_and_saved() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC_711", "Check the position of the word could be chagned and saved");

	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");

	
	// get all the transcriptionRulesList rules as list
	List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);

	// Choose random record from the middle of the displayed list
	rulesList = rulesList.subList(0, 20 > rulesList.size() ? rulesList.size() : 20);

	// Pick a random record
	Random randomizer = new Random();
//	String randomrecord = "001 : the before a consonant";
	String randomrecord = rulesList.get(randomizer.nextInt(rulesList.size()));
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the rule number 
	String ruleNumberFromName = randomrecord.split(" : ")[0];
	LogUltility.log(test, logger, "The selected rule number is: " +ruleNumberFromName);
	
    // get the text from the rule name field
	String ruleName = RulesTab.ruleName.getText();
	LogUltility.log(test, logger, "text from the rule name field before change: " +ruleName);
	// Get number of conditions
	int panesAmount = RulesTab.getDisplayedRulePanesAmount();
	LogUltility.log(test, logger, "The amount of conditions/panes: "+panesAmount);
	
	// Add new condition
	if(panesAmount == 1)
		RulesTab.rulesInputArea();
	
	// Choose random pane to work with
	int paneIndex = randomizer.nextInt(panesAmount);
	LogUltility.log(test, logger, "Change values for pane number: "+paneIndex);
	
	// Get pane dropdown values
	List<String> paneValues = RulesTab.readRulesInputAreaValues(paneIndex);
	LogUltility.log(test, logger, "Displayed pane values: "+paneValues);
	
	// get the selected Word in position in the required row
	String selectedWIP = paneValues.get(0);
	LogUltility.log(test, logger, "the selected word in position before the change is: "+selectedWIP );
	
	// Get the first list values
	List<String> WIPvalues = RulesTab.readRulesInputAreaDropDownValues(paneIndex, 0);
	WIPvalues.remove(selectedWIP);
	String newWIP = WIPvalues.get(randomizer.nextInt(WIPvalues.size()));
	LogUltility.log(test, logger, "Choose new key value: " + newWIP);
	
	// Choose a value from word in position dropdown
	RulesTab.rulesInputArea(paneIndex,Integer.parseInt(newWIP));
	
	//Press the save button  
	//Thread.sleep(3000);
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	Thread.sleep(1000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	//Select the same rule
	CommonFunctions.chooseValueInDropdown(CurrentPage.As(RulesTab.class).transcriptionRulesList, randomrecord);
	LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	// get the selected Word in position in the required row
	List<String> paneValuesAfter = RulesTab.readRulesInputAreaValues(paneIndex);
	LogUltility.log(test, logger, "Displayed pane values After change: "+paneValuesAfter);
	
	String selectedWIP2 = paneValuesAfter.get(0);
	LogUltility.log(test, logger, "the selected word in position After the change is: "+selectedWIP2 );
	
	String selectedWIPb4change=selectedWIP;
	String selectedWIPAfterchange=selectedWIP2;
	
	//Verify that the rule have a new score that different than the first one 
	boolean wipChanged = ! selectedWIPb4change.equals(selectedWIPAfterchange);               
	assertTrue(wipChanged);
	LogUltility.log(test, logger, " Verify that the First word in position  " + selectedWIPb4change+ " was changed and  different than the new word in position  " + selectedWIPAfterchange + " the value should be True : " +wipChanged);
	
	LogUltility.log(test, logger, "Test Case PASSED");	
}



/**
 * DIC_TC--572:Check the �All� button selects all the rules in the rules list
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_572_Check_the_All_button_selects_all_the_rules_in_the_rules_list() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--572", "Check the �All� button selects all the rules in the rules list");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// Click the All button
	RulesTab.allButton.click();
	LogUltility.log(test, logger, "Click the All button");

	//Press the save button  
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	Thread.sleep(1000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	// Get the checkbox list status from the file
	Hashtable<String, String> checkBoxList = RulesTab.checkboxMark("Trans");
	
	// get all the transcription rules as list
	List<String> transcriptionRulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesList);
			
	// Check if all checkboxes are active
	boolean markedCheckBoxes = true;
	for(int i=0;i<checkBoxList.size();i++)
	{
		String ruleName = transcriptionRulesList.get(i).split(" : ")[1];
		if(checkBoxList.get(ruleName).equals("disabled"))
			markedCheckBoxes = false;
	}

	LogUltility.log(test, logger, "All checkboxes are marked: "+markedCheckBoxes);	
	assertTrue(markedCheckBoxes);

	LogUltility.log(test, logger, "Test Case PASSED");	
}


/**
 * DIC_TC--573:Check the �None� button selects all the rules in the rules list
 * @throws InterruptedException 
 * @throws FileNotFoundException 
 */
@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
public void DIC_TC_573_Check_the_None_button_selects_all_the_rules_in_the_rules_list() throws InterruptedException, FileNotFoundException
{
	test = extent.createTest("Rules Test - DIC_TC--573", "Check the �None� button selects all the rules in the rules list");
	
	// define the page as variable 
	CurrentPage = GetInstance(RulesTab.class);
	RulesTab RulesTab = CurrentPage.As(RulesTab.class);
	
	//Press the Rules tab
	RulesTab.rulesTab.click();
	
	// Focus on Rules tab
	RulesTab.rulesFrame.click();
	
	// Click the transcription rules tab
	RulesTab.clickTabs("transcription");
	
	// Click the none button
	RulesTab.noneButton.click();
	LogUltility.log(test, logger, "Click the None button");

	//Press the save button  
	RulesTab.btnSave.click();
	LogUltility.log(test, logger, "Click the Save button ");
	
	//Press the OK button in the saving Dialog
	Thread.sleep(1000);
	RulesTab.OKSavingDisRuleDialog.click();
	LogUltility.log(test, logger, "Click the OK  button on the Saving button ");
	
	// Get the checkbox list status from the file
	Hashtable<String, String> checkBoxList = RulesTab.checkboxMark("Trans");
	
	// get all the transcription rules as list
	List<String> transcriptionRulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
	LogUltility.log(test, logger, "get all the transcription rules as list: " +transcriptionRulesList);
			
	// Check if all checkboxes are disabled
	boolean unmarkedCheckBoxes = true;
	for(int i=0;i<checkBoxList.size();i++)
	{
		String ruleName = transcriptionRulesList.get(i).split(" : ")[1];
		if(checkBoxList.get(ruleName).equals("active"))
			unmarkedCheckBoxes = false;
	}

	LogUltility.log(test, logger, "All checkboxes are unmarked: "+unmarkedCheckBoxes);	
	assertTrue(unmarkedCheckBoxes);

	LogUltility.log(test, logger, "Test Case PASSED");	
}

		
		/**
		 * DIC_TC--574:Verify user can sort the rules in the rules list by different categories part 4 by status
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_574_Verify_user_can_sort_the_rules_in_the_rules_list_by_different_categories_part4_by_status() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("Rules Test - DIC_TC--574", "Verify user can sort the rules in the rules list by different categories part 4 by Status");
			// define the page as variable 
			CurrentPage = GetInstance(RulesTab.class);
			RulesTab RulesTab = CurrentPage.As(RulesTab.class);
			
			// Press the Rules tab
			RulesTab.rulesTab.click();
			
			// Focus on Rules tab
			RulesTab.rulesFrame.click();
						
			// Press the transcription rules tab 
			RulesTab.clickTabs("transcription");	
			
			// Press on the Sort by dropdown list
			LogUltility.log(test, logger, "Click on the Sort by dropdown list ");
			
			// Choose to sort by "Status" from the dropdown list
			CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Status");
			LogUltility.log(test, logger, "Click on Status from the dropdown list ");
			
			// Get all the transcription rules as list
			List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
			LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
			
			// Get the checkbox list status from the file
			Hashtable<String, String> checkBoxList = RulesTab.checkboxMark("Trans");
				
			// Count how many checboxes are unmarked and insert the sorted rules names into list
			int counter = 0;
			ArrayList<String> ruleNames = new ArrayList<String>();
			for(int i=0;i<checkBoxList.size();i++)
			{
				String ruleName = rulesList.get(i).split(" : ")[1];
				counter = checkBoxList.get(ruleName).equals("disabled") ? counter+1 : counter;
				ruleNames.add(ruleName);
			}
			
			
			// Check if the rules are sorted
			boolean isSorted = true;
			for(int i=0;i<ruleNames.size();i++)
				if(i<counter)
				{
					if(!checkBoxList.get(ruleNames.get(i)).equals("disabled"))
							{isSorted = false;break;}
				}
				else
				{
					if(!checkBoxList.get(ruleNames.get(i)).equals("active"))
						{isSorted = false;break;}
				}
				
			
			assertTrue(isSorted);
			LogUltility.log(test,logger,"The value should be true: "+isSorted);
		
			// Reset sort value to number
			LogUltility.log(test, logger, "RESET TO DEFAULT VALUES ");
			CommonFunctions.chooseValueInDropdown(RulesTab.sortDD, "Number");
			
			LogUltility.log(test, logger, "Test Case PASSED");
			
		}
		
		/**
		 * DIC_TC--733:Get unneeded popup after deleting new rule immediately - BUG #1213
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_733_Get_unneeded_popup_after_deleting_new_rule_immediately_BUG_1213() throws InterruptedException, FileNotFoundException
		{
		test = extent.createTest("Rules Test - DIC_TC--733", "Get unneeded popup after deleting new rule immediately - BUG #1213");
		
		// define the page as variable 
		CurrentPage = GetInstance(RulesTab.class);
		RulesTab RulesTab = CurrentPage.As(RulesTab.class);
		
		//Press the Rules tab
		RulesTab.rulesTab.click();
		
		// Focus on Rules tab
		RulesTab.rulesFrame.click();
		
		// Press the prosody rules tab 
		RulesTab.transcriptionTab.click();
		
		// get all the transcription rules as list
		List<String> rulesList= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list: " +rulesList);
		
		// Press the new button
		RulesTab.btnNew.click();
		LogUltility.log(test, logger, "Click the new button ");
		
		// Press the Delete button 
		RulesTab.bt_delete.click();
		LogUltility.log(test, logger, "Click the Delete button");
		
		// Click yes button on the Delete poup 
		RulesTab.yesDeleteRule_bt.click();
		LogUltility.log(test, logger, "Click the Yes button on the Delete poup");
		
		//Press the OK button 
		RulesTab.OKSavingDisRuleDialog.click();
		LogUltility.log(test, logger, "Click the OK button");
			
		// Check that the following popup isn't being displayed
		String titleToCheck = "New Rule";
		String textToCheck = "You are in the middle of adding a new rule!\r\rwould you like to abort?";
		
		boolean noPopup = false;
		try {
		 noPopup = !RulesTab.popupWindowMessage(test, logger, titleToCheck, textToCheck);
		}
		
		catch (Exception e) {
			e.printStackTrace();
			noPopup = true;
		}
		
		LogUltility.log(test, logger, "There no additional/uneeded popup after deleting the rule: "+noPopup);
		assertTrue(noPopup);
		
		// get all the prosody rules as list
		List<String> rulesListAfter= RulesTab.getValuesFromApp(RulesTab.transcriptionRulesList);
		LogUltility.log(test, logger, "get all the transcription rules as list After: " +rulesList);
		
		// The rule was deleted successfully
		boolean successDelete = rulesListAfter.size() == rulesList.size();
		LogUltility.log(test, logger, "The rule has been deleted successfully: "+successDelete);	
		assertTrue(successDelete);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
		
		}

	@AfterMethod(alwaysRun = true)
	  public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
	    {		
			
			int testcaseID = 0;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Rules/Transcription",method.getName());
			
//				System.out.println("tryCount: " + tryCount);
//				System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
		
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
					&& Integer.parseInt(Setting.RetryFailed)!=0) {
				extent.removeTest(test);
				
		        // Close popups to get back to clean app
				 System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
			}
			else if(result.getStatus() == ITestResult.FAILURE)
			    {
				 	tryCount = 0;
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			        
			        // Close popups to get back to clean app
//				        if (Integer.parseInt(Setting.RetryFailed) != 0) {
			         System.out.println("Test Case Failed");
					 CurrentPage = GetInstance(DictionaryTab.class);
					 if(Setting.closeEveryWindow.equals("true"))
						 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//				        }
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			    	tryCount = 0;
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
			
			    extent.flush();
			    
				// Count how many a test was processed
				tryCount++;
				}

	/**
	 * Closing the application after running all the TCs
	 */
	@AfterClass(alwaysRun = true) 
	public void CloseBrowser() {
		 
		 //DriverContext._Driver.quit();
			}

}

package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CompoundsTab;
import smi.smorfet.test.pages.DictionaryTab;


/**
 * 
 * All tests for compounds search
 *
 */
public class Compounds_Search_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("compounds tab - Search");
	
		// Logger
		logger = LogManager.getLogger(CompoundsTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= compounds tab - Search");
		logger.info("compounds tab Tests - FrameworkInitilize");
	
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {if (Setting.ReportToTestlink.equals("true") && testlink == true){
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(CompoundsTab.class);
//			CurrentPage.As(CompoundsTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Compounds");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * DIC_TC--62:Verify user can add filter to the search
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_62_Verify_user_can_add_filter_to_the_search() throws InterruptedException, AWTException
	{
		//Thread.sleep(5000);
		test = extent.createTest("compounds Search Test - DIC_TC_62", "Verify user can add filter to the search");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "all records");
				
		//Type in the search field
		String searchWord = "get";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
		assertTrue(containResult);
		
		// Add the second word
		String searchWord2ND = "out";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.clear();
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord2ND);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Now change to add filter in the fourth search compobox
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "add filter");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain both searched words
		if (recordsList.isEmpty()) assertTrue(false);
		
		containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord) && s.toLowerCase().contains(searchWord2ND)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
		assertTrue(containResult);

		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--64 Verify user can change filter while searching 
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_64_Verify_user_can_change_filter_while_searching() throws InterruptedException
	{
		//Thread.sleep(5000);
		test = extent.createTest("compounds Search Test - DIC_TC_64", "Verify user can change filter while searching");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
		
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "all records");
				
		//Type in the search field
		String searchWord = "get";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word : "+searchWord +" , " + containResult);
		assertTrue(containResult);
		
		// Add the second word
		String searchWord2ND = "out";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.clear();
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord2ND);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Now change to add filter in the fourth search compobox
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "add filter");
		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain both searched words
		if (recordsList.isEmpty()) assertTrue(false);
		
		containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord) && s.toLowerCase().contains(searchWord2ND)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: "+searchWord +" , and "+searchWord2ND +"," + containResult);
		assertTrue(containResult);
		
		// Change the second word
		searchWord2ND = "in";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.clear();
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord2ND);
		LogUltility.log(test, logger, "word is typed in the search field");
		
		// Now change to add filter in the fourth search compobox
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "change filter");
		LogUltility.log(test, logger, "change filter is chosen in the all/next dropdown");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain both searched words
		if (recordsList.isEmpty()) assertTrue(false);
		
		containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(searchWord) && s.toLowerCase().contains(searchWord2ND)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: "+searchWord +" , and "+searchWord2ND +"," + containResult);
		assertTrue(containResult);

		LogUltility.log(test, logger, "Test Case PASSED");	
	}

	/**
	 * DIC_TC--59:Check volume[POS] dropdown list
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_59_Check_volume_POS_dropdown_list() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("compounds Search Test - DIC_TC_59", "Check volume[POS] dropdown list");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
		
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "all records");
				
		//Select Value from the Volume POS dropdown 
		String randomPOS = "conjunction";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, randomPOS);
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomPOS);
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen POS list value
		List<String> chosenPOS = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_POS);
		LogUltility.log(test, logger, "chosen list: " + chosenPOS);

		//verify that the POS match the searched one
		boolean samePOS = chosenPOS.contains(randomPOS);
		assertTrue(samePOS);
		LogUltility.log(test, logger, "the retrieved POS List: "+chosenPOS + " ,include the searched Spellings: " + randomPOS);
		LogUltility.log(test, logger, "Check that the retrieved POS list do include the searched POS: " + samePOS);
		assertTrue(samePOS);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

	
	
	/**
	 * DIC_TC--90: Left ARROW Check the two arrows in the footer
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_90_Left_ARROW_Check_the_two_arrows_in_the_footer() throws InterruptedException, FileNotFoundException
	{
	
	test = extent.createTest("Compounds Search Test - DIC_TC_90", "LEFT ARROW Check the two arrows in the footer");
	
	  // Focus on Compounds tab
	  CurrentPage = GetInstance(CompoundsTab.class);
	  CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	
	  // Choose a value from Volume[POS] dropdown
	  String volumePOS = "adjective";
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	  LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
	
	  // Retrieve records
	  List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	  LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	  // Choose random record from the middle of the displayed list
	  recordsList = recordsList.subList(2, 10 > recordsList.size() ? recordsList.size() : 10);
	
	  // Pick a random record
	  Random randomizer = new Random();
	  String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	  LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	  // Get index of the chosen record from the array
	  int recordIndex = recordsList.indexOf(randomrecord);
	  LogUltility.log(test, logger, "Index of the chosen record: " + recordIndex);
	
	  // Click the Left arrow
	  CurrentPage.As(CompoundsTab.class).Previous_button.click();
	
	  // Get the chosen value from the records list
	  List<String> chosenRecords = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records);
	  LogUltility.log(test, logger, "the chosen value from the records list: " + chosenRecords);
	
	  // Check that the retreived record after clicking the arrow is indeed the previous one in the list
	  String nextRecord = recordsList.get(--recordIndex);
	  LogUltility.log(test, logger, "The chosen record after press the Previous arrow: " + nextRecord);
	
	
	  boolean found = chosenRecords.get(0).equals(nextRecord);
	  LogUltility.log(test, logger, "Does the Previous arrow moved to the previous record: " + found);
	  LogUltility.log(test, logger, "the chosen record after pressing previous arrow: "+nextRecord + " ,is the same as the record index: " + chosenRecords);
	  assertTrue(found);
	    
	  LogUltility.log(test, logger, "Test Case PASSED");
	
	
	}

	/**
	  * DIC_TC--90: RIGHT ARROW Check the two arrows in the footer
	  * @throws InterruptedException 
	  * @throws FileNotFoundException 
	  */
	 @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	 public void DIC_TC_90_RIGHT_ARROW_Check_the_two_arrows_in_the_footer() throws InterruptedException, FileNotFoundException
	 {
	  test = extent.createTest("Compounds Search Test - DIC_TC_90", "RIGHT ARROW Check the two arrows in the footer");
	
	  // Focus on Compounds tab
	  CurrentPage = GetInstance(CompoundsTab.class);
	  CurrentPage.As(CompoundsTab.class).tabcompounds.click();
	    
	  // Choose a value from Volume[POS] dropdown
	  String volumePOS = "preposition";
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpVolumePOS, volumePOS);
	  LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);
	
	  // Retrieve records
	  List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
	  LogUltility.log(test, logger, "Records retreived: " + recordsList);
	
	  // Choose random record from the middle of the displayed list
	  recordsList = recordsList.subList(1, 10 > recordsList.size() ? recordsList.size() : 10);
	
	  // Pick a random record
	  Random randomizer = new Random();
	  String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
	  CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
	  LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
	
	  // Get index of the chosen record from the array
	  int recordIndex = recordsList.indexOf(randomrecord);
	  LogUltility.log(test, logger, "record index : " + recordIndex);
	
	  // Click the Right arrow
	  CurrentPage.As(CompoundsTab.class).Next_button.click();
	
	  // Get the chosen value from the records list
	  List<String> chosenRecords = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records);
	  LogUltility.log(test, logger, "Choosen value after press the next button: " + chosenRecords);
	  // Check that the retreived record after clicking the arrow is indeed the next one in the list
	  String nextRecord = recordsList.get(++recordIndex);
	  LogUltility.log(test, logger, "Choosen record after pressing the next button : " + nextRecord);
	  int recordIndex2 = recordsList.indexOf(nextRecord);
	  LogUltility.log(test, logger, "record index after press next button : " + recordIndex2);
	
	  boolean found = chosenRecords.get(0).equals(nextRecord);
	  LogUltility.log(test, logger, "Does the next arrow moved to the next record: " + found);
	  assertTrue(found);
	    
	  LogUltility.log(test, logger, "Test Case PASSED");
	 }

	/**
	 * DIC_TC--107:Check the maximum records that could be hold in the Database records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_107_Check_the_maximum_records_that_could_be_hold_in_the_Database_records() throws InterruptedException, IOException
	{
		test = extent.createTest("Compounds Tests - DIC_TC_107", "Check the maximum records that could be hold in the Database records");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
		
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "form" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		String searchWord = "a";
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(searchWord);
		LogUltility.log(test, logger, "word is typed in the search field");
	
		// Change the last filter to "all records", in case it was changed by other TC
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_all_next, "all records");
		LogUltility.log(test, logger, "all records is chosen in the dropdown");
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");

		// Check that too many retrieved records do popup is displayed
	
		boolean isDisplayed = CurrentPage.As(CompoundsTab.class).window_too_many_records.isDisplayed();
		LogUltility.log(test, logger, "Is too many retrieved records poupus is displayed: " + isDisplayed);
			
		// Click the OK button to close the popup window
		CurrentPage.As(CompoundsTab.class).btnOk.click();
		LogUltility.log(test, logger, "Click the OK button");
		
		// Check that the label do contain the max value 10000
		String recordsLablel = CurrentPage.As(CompoundsTab.class).ll_compounds_retrieved_records.getAttribute("Name");
		LogUltility.log(test, logger, "Records label info: " + recordsLablel);
	
		
	
		
		// Count the records in the records list
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		int recordsNumber = recordsList.size();
		LogUltility.log(test, logger, "Number of records in the records list: " + recordsNumber);
		assertEquals(recordsNumber, 10000);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	
	
	
	/**
	 * DIC_TC--113:Verify user can filter the search of �Semantic Groups"
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_113_Verify_user_can_filter_the_search_of_Semantic_Groups() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("compounds Search Test - DIC_TC_113", "Verify user can filter the search of �Semantic Groups�");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
				
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "Semantic Groups" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "semantic groups");
		LogUltility.log(test, logger, "semantic groups is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
		
		//Select Value from the dropdown 
		String randomSemanticGroups = "v emotion";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomSemanticGroups);
		//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
		LogUltility.log(test, logger, "chosen search value: " + randomSemanticGroups);
		
		// Click the Retrieve button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		//Get only the first visible 40 records ONLY IN ORDER TO MAKE THE TEST FINISH FAST
		recordsList = recordsList.subList(0, 39);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chosen Semantic groups list value
		List<String> chosenSemanticGroupsList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "chosen list: " + chosenSemanticGroupsList);
	//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
		//getChosenValueInDropdown
		

		//Remove the first letter for the random searched value  before compare results
		//String randomSemanticGroups1 = "desire";
		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
		String randomSemanticGroups1 = randomSemanticGroups.substring(2);
		LogUltility.log(test, logger, "Check that the retrieved Semantic groups list do include the searched Semantic groups: " + randomSemanticGroups1);
		
		//verify that the Register match the searched one
		boolean sameSemanticGroups = chosenSemanticGroupsList.contains(randomSemanticGroups1);
		assertTrue(sameSemanticGroups);
		
		LogUltility.log(test, logger, "Check that the retrieved Semantic Groups list do include the searched Semantic Groups: " + sameSemanticGroups);
		assertTrue(sameSemanticGroups);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
//	
	/**
		 * DIC_TC--114:Verify user can filter the search by �Frames�
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_114_Verify_user_can_filter_the_search_by_Frames() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_114", "Verify user can filter the search by Frames");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
			
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "form" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "frames");
			LogUltility.log(test, logger, "frames is chosen in the first search dropdown");
			
			// Choose "contains" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
			//Type in the search field
//			List<String> frameValues = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).cmpCondition_value);
			List<String> frameValues = new ArrayList<String>();
			// Work with available values for now
			frameValues.add("st ADJ");
			frameValues.add("PRP st");
			frameValues.add("intransitive");
			Random random = new Random();
			String randomFrame = frameValues.get(random.nextInt(frameValues.size()));
//			String randomFrame = "PRP st";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomFrame);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomFrame);
			LogUltility.log(test, logger, "chosen search value: " + randomFrame);
			
			// Click the Retreive button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			LogUltility.log(test, logger, "Records retreived: " + recordsList);	
		
			if(recordsList.isEmpty())
				assertTrue(true);
	
			// Choose a random record from the record list
			// Pick a random record
			String randomrecord = recordsList.get(random.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);

			// Get the frames values
			List<String> framesList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_frames);
			boolean found = framesList.contains(randomFrame);
			LogUltility.log(test, logger, "Is the searched frame found " + found);
			assertTrue(found);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	/**
	 * DIC_TC--115:Verify user can filter the search of �Origin�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_115_Verify_user_can_filter_the_search_of_Origin() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("compounds Search Test - DIC_TC_115", "Verify user can filter the search of �Origin�");

		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
				
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "origin" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "origin");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// Choose a value from the third dropdown
		String searchValue = "French";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, searchValue);
		LogUltility.log(test, logger, "Chosen search value: " + searchValue);
		
		// Click the Retreive button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
//		boolean isWindowDisplayed = CurrentPage.As(CompoundsTab.class).window_too_many_records.isDisplayed();
//		if (isWindowDisplayed) CurrentPage.As(CompoundsTab.class).window_too_many_records_ok.click();
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Only take the first 40 results in order to click on them
		recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
		
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the chose value from the origin dropdown
		List<String> originList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpOrigin);
		boolean found = searchValue.equals(originList.get(0));
		LogUltility.log(test, logger, "Is the searched word:"+searchValue + " ,found :  " + found);
		assertTrue(found);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--116 : Verify user can filter the search of �Lexical Relations"
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_116_Verify_user_can_filter_the_search_of_Lexical_Relations() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("compounds Search Test - DIC_TC_116", "Verify user can filter the search of �Lexical Relations");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
				
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "Lexical Relations" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "lexical relations");
		LogUltility.log(test, logger, "Lexical Relations is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		String randomLexicalRelation = "anti";
		//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomLexicalRelation);
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomLexicalRelation);
		LogUltility.log(test, logger, "chosen search value: " + randomLexicalRelation);
		
		// Click the Retrieve button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the Lexical relations values
		List<String> lexicalRelationsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_Lexical_relations);
		
		// Check that retrieved records do contain the searched record
		if (lexicalRelationsList.isEmpty()) assertTrue(false);
		
		// Check that retrieved records do contain the searched record
		if (lexicalRelationsList.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : lexicalRelationsList) {
		       if(s.contains(randomLexicalRelation)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
		assertTrue(containResult);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	/**
	 * DIC_TC--117:Verify user can filter the search of �Modified on�
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_117_Verify_user_can_filter_the_search_of_Modified_on() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("compounds Search Test - DIC_TC_117", "Verify user can filter the search of �Modified on�");
		
		// Focus on Compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
				
		// Focus on compounds tab
		CurrentPage = GetInstance(CompoundsTab.class);
		CurrentPage.As(CompoundsTab.class).tabcompounds.click();
		
		// Choose "Modified on" from the first search dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "modified on");
		LogUltility.log(test, logger, "Modified on is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		//Type in the search field
		String randomModifiedOn = "11/2017";
		//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomLexicalRelation);
		CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomModifiedOn);
		LogUltility.log(test, logger, "chosen search value: " + randomModifiedOn);
		
		// Click the Retrieve button
		CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		//Thread.sleep(1000);
		//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		//Thread.sleep(2000);
		// Retrieve records
		List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		// Choose a random record from the record list
		// Pick a random record
		Random randomizer = new Random();
		String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
		
		// Get the modified_on value
//		List<String> modifiedOnList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).modified_on);
		String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
		
		//verify that the the modified on match the serached one
		boolean sameModifiedOn = modifiedOnList.contains(randomModifiedOn);
		assertTrue(sameModifiedOn);
		
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched modified On:"+randomModifiedOn + " ,and it`s a : " + sameModifiedOn);
		assertTrue(sameModifiedOn);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	
	/**
		 * DIC_TC--118:Verify user can filter the search of �Semantic Fields�
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_118_Verify_user_can_filter_the_search_of_Semantic_Fields() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_118", "Verify user can filter the search of �Semantic Fields�");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
					
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "semantic fields" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "semantic fields");
			LogUltility.log(test, logger, "Lexical fields is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomLexicalField = "banking";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomLexicalField);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomLexicalField);
			LogUltility.log(test, logger, "chosen search value: " + randomLexicalField);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			//Thread.sleep(2000);
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the chosen Semantic Fields list value
			List<String> chosenlexicalFieldsList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lb_compounds_semantic_fields);
			LogUltility.log(test, logger, "chosen list: " + chosenlexicalFieldsList);
		//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
			//getChosenValueInDropdown
			
			//verify that the the Lexical field match the searched one
			boolean sameLexicalField = chosenlexicalFieldsList.contains(randomLexicalField);
			assertTrue(sameLexicalField);
			
			LogUltility.log(test, logger, "Check that the retrieved lexical fields list do include the searched Semantic Fields: " + sameLexicalField);
			assertTrue(sameLexicalField);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
		
		/**
		 * DIC_TC--119:Verify user can filter the search of �Register�
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC119_Verify_user_can_filter_the_search_of_Register() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC119", "Verify user can filter the search of �Register�");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
					
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "Register" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "Register");
			LogUltility.log(test, logger, "Register is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomRegister = "slang";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomRegister);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomRegister);
			LogUltility.log(test, logger, "chosen search value: " + randomRegister);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			//Thread.sleep(2000);
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the chosen Register list value
			List<String> chosenRegisterList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_Regester);
			LogUltility.log(test, logger, "chosen list: " + chosenRegisterList);
		//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
			//getChosenValueInDropdown
			
			//verify that the Register match the searched one
			boolean sameRegister = chosenRegisterList.contains(randomRegister);
			assertTrue(sameRegister);
			
			LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched Register: " + sameRegister);
			assertTrue(sameRegister);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
		
		/**
		 * DIC_TC-120:Verify user can filter the search of �Acronym�
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_120_Verify_user_can_filter_the_search_of_Acronym() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_120", "Verify user can filter the search of �Acronym�");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
					
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "Acronym" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "acronym");
			LogUltility.log(test, logger, "Acronym is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomAcronym = "initialism";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomAcronym);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomAcronym);
			LogUltility.log(test, logger, "chosen search value: " + randomAcronym);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			//Thread.sleep(2000);
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the chosen Acronym list value
			List<String> chosenAcronymList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_Acronym);
			LogUltility.log(test, logger, "chosen list: " + chosenAcronymList);
		//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
			//getChosenValueInDropdown
			
			//verify that the Acronym match the searched one
			boolean sameAcronym = chosenAcronymList.contains(randomAcronym);
			assertTrue(sameAcronym);
			
			LogUltility.log(test, logger, "Check that the retrieved Acronym list do include the searched Acronym: " + sameAcronym);
			assertTrue(sameAcronym);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
		
		/**
		 * DIC_TC--151:Verify user can filter records with Language filter
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_151_Verify_user_can_filter_records_with_Language_filter() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_151", "Verify user can filter records with Language filter");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
			
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "language" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "language");
			LogUltility.log(test, logger, "Language is chosen in the first search dropdown");
			
			// Choose "not equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "not equal");
			LogUltility.log(test, logger, "not equal is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomLanguage = "English";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomLanguage);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
			LogUltility.log(test, logger, "chosen search value: " + randomLanguage);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			//Thread.sleep(2000);
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			
			// If the list is empty, then test fails
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the chosen language list value
			List<String> chosenLanguage = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_Language);
			LogUltility.log(test, logger, "chosen list: " + chosenLanguage);
		//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
			//getChosenValueInDropdown
			

			//Remove the first letter for the random searched value  before compare results
			//String randomSemanticGroups1 = "desire";
			//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//			String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//			LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
			
			//verify that the Language match the searched one
			boolean sameLanguage = chosenLanguage.contains(randomLanguage);
			assertFalse(sameLanguage);
			
			LogUltility.log(test, logger, "Check that the retrieved Language list doesn`t include the searched Language, and the value should be False: " + sameLanguage);
			assertFalse(sameLanguage);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
	
		/**
		 * DIC_TC--152:Verify user can filter records with POS filter
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_152_Verify_user_can_filter_records_with_POS_filter() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_152", "Verify user can filter records with POS filter");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
					
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "POS" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "pos");
			LogUltility.log(test, logger, "POS is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomPOS = "preposition";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomPOS);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
			LogUltility.log(test, logger, "chosen search value: " + randomPOS);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			//Thread.sleep(2000);
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the chosen POS list value
			List<String> chosenPOS = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_POS);
			LogUltility.log(test, logger, "chosen list: " + chosenPOS);
		//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
			//getChosenValueInDropdown
			

			//Remove the first letter for the random searched value  before compare results
			//String randomSemanticGroups1 = "desire";
			//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//			String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//			LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
			
			//verify that the POS match the searched one
			boolean samePOS = chosenPOS.contains(randomPOS);
			assertTrue(samePOS);
			
			LogUltility.log(test, logger, "Check that the retrieved POS list do include the searched POS: " + samePOS);
			assertTrue(samePOS);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
		/**
		 * DIC_TC--153:Verify user can filter records with style filter 
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_153_Verify_user_can_filter_records_with_style_filter() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_153", "Verify user can filter records with style filter");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
					
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "style" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "style");
			LogUltility.log(test, logger, "Style is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomStyle = "idiom";
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomStyle);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomVersion);
			LogUltility.log(test, logger, "chosen search value: " + randomStyle);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the chosen Style list values
			List<String> chosenStyleList = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).Style_DD);
			LogUltility.log(test, logger, "chosen Style list: " + chosenStyleList);
		//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
		//  String versionValue = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
			
			

			//Remove the first letter for the random searched value  before compare results
			//String randomversion1 = "desire";
			//String[] randomversion1 = randomversion.split(" ");
			//String randomversion1 = randomversion.substring(2);
			//LogUltility.log(test, logger, "Check that the retrieved version list do include the searched version: " + randomversion1);
			
			//verify that the Style match the searched one
		    // boolean sameStyle = versionValue.equalsIgnoreCase(randomStyle);
		     
		     boolean sameStyle = chosenStyleList.contains(randomStyle);
				assertTrue(sameStyle);
		     
			LogUltility.log(test, logger, "the retrieved Style List: "+chosenStyleList + " ,include the searched Style: " + randomStyle);
			LogUltility.log(test, logger, "Check that the retrieved Style list do include the searched Style: " + sameStyle);
			assertTrue(sameStyle);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
	
		/**
		 * DIC_TC--154:Verify user can filter records with Frequency  filter
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_154_Verify_user_can_filter_records_with_Frequency_filter() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_154", "Verify user can filter records with Frequency filter");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
			
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "Frequency" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "frequency");
			LogUltility.log(test, logger, "Status is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomFrequency = "very rare";
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomFrequency);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticGroups);
			LogUltility.log(test, logger, "chosen search value: " + randomFrequency);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			//Thread.sleep(2000);
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			//Get only the first visible 40 records ONLY IN ORDER TO MAKE THE TEST FINISH FAST
			recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
			LogUltility.log(test, logger, "Records retreived: " + recordsList);
			
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the chosen Frequency list value
			List<String> chosenFrequency = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_frequency);
			LogUltility.log(test, logger, "chosen list: " + chosenFrequency);
		//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
			//getChosenValueInDropdown
			

			//Remove the first letter for the random searched value  before compare results
			//String randomSemanticGroups1 = "desire";
			//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//			String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//			LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
			
			//verify that the Frequency match the searched one
			boolean sameFrequency = chosenFrequency.contains(randomFrequency);
			assertTrue(sameFrequency);
			
			LogUltility.log(test, logger, "Check that the retrieved Frequency list do include the searched Frequency: " + sameFrequency);
			assertTrue(sameFrequency);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
	
		
		/**
		 * DIC_TC--155:Verify user can filter records with Definition filter
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_155_Verify_user_can_filter_records_with_Definition_filter() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_155", "Verify user can filter records with Definition filter");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
					
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "Definition" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "definition");
			LogUltility.log(test, logger, "Definition is chosen in the first search dropdown");
			
			// Choose "contains" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "equal is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomDefinition = "gained";
			//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomDefinition);
			CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomDefinition);
			LogUltility.log(test, logger, "chosen search value: " + randomDefinition);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			//Thread.sleep(2000);
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			//Get only the first visible 40 records ONLY IN ORDER TO MAKE THE TEST FINISH FAST
			recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
			LogUltility.log(test, logger, "Records retreived: " + recordsList);
			
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
//			// Get the chosen Definition list value
//			List<String> chosenDefinition = CurrentPage.As(CompoundsTab.class).getChosenValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_Definition);
//			LogUltility.log(test, logger, "chosen list: " + chosenDefinition);
			String chosenDefinition = CurrentPage.As(CompoundsTab.class).lst_Definition.getText();
			LogUltility.log(test, logger, "Text from the Definition field: " + chosenDefinition);
			//getChosenValueInDropdown
			

			//Remove the first letter for the random searched value  before compare results
			//String randomSemanticGroups1 = "desire";
			//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//			String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//			LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
			
			//verify that the Frequency match the searched one
			boolean sameDefinition = chosenDefinition.contains(randomDefinition);
			assertTrue(sameDefinition);
			LogUltility.log(test, logger, "the retrieved Definition text: "+chosenDefinition + " ,include the searched Definition: " + randomDefinition);
			LogUltility.log(test, logger, "Check that the retrieved Definitionlist do include the searched Definition: " + sameDefinition);
			assertTrue(sameDefinition);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
	
	
		/**
		 * DIC_TC--156:Verify user can filter the search of �Semantic Relation"
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_156_Verify_user_can_filter_the_search_of_Semantic_Relation() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_156", "Verify user can filter the search of �Semantic Relation�");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
					
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "Semantic Relation" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "semantic relations");
			LogUltility.log(test, logger, "semantic Relation is chosen in the first search dropdown");
			
			// Choose "contains" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomSemanticRelation = "experienced";
			//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomSemanticRelation);
			CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomSemanticRelation);
			LogUltility.log(test, logger, "chosen search value: " + randomSemanticRelation);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			//Thread.sleep(1000);
			//CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			//Thread.sleep(2000);
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose a random record from the record list
			// Pick a random record
			Random randomizer = new Random();
			String randomrecord = recordsList.get(randomizer.nextInt(recordsList.size()));
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, randomrecord);
			LogUltility.log(test, logger, "Choose from record list: " + randomrecord);
			
			// Get the chosen Semantic Relation list value
			List<String> chosenSemanticRelationList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_SemanticRelations);
			LogUltility.log(test, logger, "chosen semantic relation list: " + chosenSemanticRelationList);
		//	String modifiedOnList = CurrentPage.As(CompoundsTab.class).modified_on.getText();
			//getChosenValueInDropdown
			

			//Remove the first letter for the random searched value  before compare results
			//String randomSemanticRelation1 = "desire";
			//String[] randomSemanticRelation1 = randomSemanticRelation.split(" ");
			//String randomSemanticRelation1 = randomSemanticRelation.substring(2);
			//LogUltility.log(test, logger, "Check that the retrieved Semantic Relation list do include the searched Semantic Relation: " + randomSemanticRelation1);
			
			//verify that the Semantic Relation match the searched one
			//boolean sameSemanticRelation = chosenSemanticRelationList.contains(randomSemanticRelation);
			//assertTrue(sameSemanticRelation);
			boolean sameSemanticRelation = false;
			for (String value : chosenSemanticRelationList) {
				 sameSemanticRelation = value.contains(randomSemanticRelation);
				 if (sameSemanticRelation) break;
			}
			LogUltility.log(test, logger, "the retrieved Semantic Relation List: "+chosenSemanticRelationList + " ,include the searched Semantic Relation: " + randomSemanticRelation);
			LogUltility.log(test, logger, "Check that the retrieved Semantic Relation list do include the searched Semantic Relation: " + sameSemanticRelation);
			assertTrue(sameSemanticRelation);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
		
		
		/**
		 * DIC_TC--511:check the app behavior when clicking on empty records box 
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_511_check_the_app_behavior_when_clicking_on_empty_records_box() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_511", "check the app behavior when clicking on empty records box");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
			
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "Form" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
			LogUltility.log(test, logger, "Form is chosen in the first search dropdown");
			
			// Choose "equal" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "equal");
			LogUltility.log(test, logger, " Contains is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			//String randomForm = "coal";
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
			//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomForms);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
			String randomText = CurrentPage.As(CompoundsTab.class).RandomString(12);
			CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomText);
			LogUltility.log(test, logger, "chosen search value: " + randomText);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
			
//			boolean isWindowDisplayed = CurrentPage.As(CompoundsTab.class).window_too_many_records.isDisplayed();
//			if (isWindowDisplayed) CurrentPage.As(CompoundsTab.class).window_too_many_records_ok.click();
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
			// Check that retrieved records do contain the searched record
			if (!recordsList.isEmpty()) assertTrue(false);
			LogUltility.log(test, logger, "Result list should be empty: " + recordsList);
			
			// Click on the empty results area
			CurrentPage.As(CompoundsTab.class).lst_records.click();
			LogUltility.log(test, logger, "Click on the empty list" );
			
			// Verify that no errrors or any popup appear
			boolean badKeyWarningClosed = CurrentPage.As(CompoundsTab.class).checkNoPopupWindow();
			LogUltility.log(test, logger, "verify that No errors appear, and the value should be True : " + badKeyWarningClosed);
			assertTrue(badKeyWarningClosed);

			LogUltility.log(test, logger, "Test Case PASSED");	
		}

	
		/**
		 * DIC_TC--527:Check retrieving an empty search users get popup 
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_527_Check_retrieving_an_empty_search_users_get_popup() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_527", "Check retrieving an empty search users get popup");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
			
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Verify that the search text field is empty
			
			boolean fieldIsEmpty = CurrentPage.As(CompoundsTab.class).cmpCondition_value.getText().isEmpty();
			 if (fieldIsEmpty){
				CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
				LogUltility.log(test, logger, "Click the Retreive button since field is empty");}
			
			    else 
			        {
			    	CurrentPage.As(CompoundsTab.class).cmpCondition_value.clear();
			    	// Click the Retrieve button
					CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
					LogUltility.log(test, logger, "Click the Retreive button after clear text field");
			        }
			
			// Verify that the Empty value popup displayed
			 boolean  emptyPopup = CurrentPage.As(CompoundsTab.class).Empty_Popup.isDisplayed();
			 assertTrue(emptyPopup);	 
					 			     
			LogUltility.log(test, logger, "Empty value popu message appear: " + emptyPopup);
			assertTrue(emptyPopup);
			
			//Get message text
			String empty_text = CurrentPage.As(CompoundsTab.class).Empty_Popup_text.getAttribute("Name");
			LogUltility.log(test, logger, "Empty value popu message text: " + empty_text);
			assertEquals(empty_text, "Value is empty! Please enter value");
			
			CurrentPage.As(CompoundsTab.class).btnOk.click();
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
		
		
		
		/**
		 * DIC_TC--551:Check the user can select the first record after executing a search 
		 * @throws InterruptedException 
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void DIC_TC_551_Check_the_user_can_select_the_first_record_after_executing_a_search() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("compounds Search Test - DIC_TC_551", "Check the user can select the first record after executing a search");
			
			// Focus on Compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).CompoundsTab.click();
			
			// Focus on compounds tab
			CurrentPage = GetInstance(CompoundsTab.class);
			CurrentPage.As(CompoundsTab.class).tabcompounds.click();
			
			// Choose "Form" from the first search dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_field, "form");
			LogUltility.log(test, logger, "Form is chosen in the first search dropdown");
			
			// Choose "contains" from the condition dropdown
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_operation, "contains");
			LogUltility.log(test, logger, " Contains is chosen in the condition dropdown");
			
			//Select Value from the dropdown 
			String randomForm = "coal";
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
			//CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).cmpCondition_value, randomForms);
			//CurrentPage.As(CompoundsTab.class).cmpCondition_value.click();
			CurrentPage.As(CompoundsTab.class).cmpCondition_value.sendKeys(randomForm);
			LogUltility.log(test, logger, "chosen search value: " + randomForm);
			
			// Click the Retrieve button
			CurrentPage.As(CompoundsTab.class).btnRetrieve_same.click();
			LogUltility.log(test, logger, "Click the Retreive button");
			
//			boolean isWindowDisplayed = CurrentPage.As(CompoundsTab.class).window_too_many_records.isDisplayed();
//			if (isWindowDisplayed) CurrentPage.As(CompoundsTab.class).window_too_many_records_ok.click();
			
			// Retrieve records
			List<String> recordsList = CurrentPage.As(CompoundsTab.class).getValuesFromApp(CurrentPage.As(CompoundsTab.class).lst_records);
			recordsList = recordsList.subList(0, 39 > recordsList.size() ? recordsList.size() : 39);
			// Check that retrieved records do contain the searched record
			if (recordsList.isEmpty()) assertTrue(false);
			
			// Choose the first record from the record list
			//Random randomizer = new Random();
			//String randomrecord = recordsList.get(0);
			
			CommonFunctions.chooseValueInDropdown(CurrentPage.As(CompoundsTab.class).lst_records, recordsList.get(0));
			LogUltility.log(test, logger, "Choose from record list: " + recordsList.get(0));
			
			// Get the Key Form field value
//			List<String> chosenFormsList = CurrentPage.As(CompoundsTab.class). getValuesFromApp (CurrentPage.As(CompoundsTab.class). Forms_list);
//			LogUltility.log(test, logger, "chosen Forms list: " + chosenFormsList);
			String formValue = CurrentPage.As(CompoundsTab.class). txtKeyForm.getText();
		//  String versionValue = CurrentPage.As(CompoundsTab.class).Field_Version.getText();
			
			//verify that the Forms match the searched one
		     
		     boolean sameForms = formValue.contains(randomForm);
				assertTrue(sameForms);
		     
			LogUltility.log(test, logger, "the retrieved Forms List: "+formValue + " ,include the searched Forms: " + randomForm);
			LogUltility.log(test, logger, "verify that the first record: "+recordsList.get(0) + " , the same record as: " + formValue);
			LogUltility.log(test, logger, "Check that the retrieved Forms list do include the searched Forms: " + sameForms);
			assertTrue(sameForms);
			
			LogUltility.log(test, logger, "Test Case PASSED");	
		}
	
		@AfterMethod(alwaysRun = true)
	    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
	    {		
			
			int testcaseID = 0;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Compound/Search",method.getName());
			
//			System.out.println("tryCount: " + tryCount);
//			System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
		
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
					&& Integer.parseInt(Setting.RetryFailed)!=0) {
				extent.removeTest(test);
				
		        // Close popups to get back to clean app
				 System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
			}
			else if(result.getStatus() == ITestResult.FAILURE)
			    {
				 	tryCount = 0;
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			        
			        // Close popups to get back to clean app
//			        if (Integer.parseInt(Setting.RetryFailed) != 0) {
			         System.out.println("Test Case Failed");
					 CurrentPage = GetInstance(DictionaryTab.class);
					 if(Setting.closeEveryWindow.equals("true"))
						 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//			        }
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			    	tryCount = 0;
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
			
			    extent.flush();
			    
				// Count how many a test was processed
				tryCount++;
				}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
			}

}

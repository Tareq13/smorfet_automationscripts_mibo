package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.CorpusTab;
import smi.smorfet.test.pages.DictionaryTab;

/**
 * 
 * All tests for Dictionary search
 *
 */
public class Corpus_Search_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;

	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Corpus tab - Search");
	
		// Logger
		logger = LogManager.getLogger(CorpusTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Corpus tab - Search");
		logger.info("Corpus tab Tests - FrameworkInitilize");
	
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true){
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
			
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Smorfet app focus
		try {
			CurrentPage = GetInstance(CorpusTab.class);
//			CurrentPage.As(CorpusTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Corpus");
		} catch (Exception e) {
		}
		
	}
	
	/**
	 * DIC_TC--60:Check searching a sentence that contains a number
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_60_Check_searching_a_sentence_that_contains_a_number() throws InterruptedException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--60", "Check searching a sentence that contains a number");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus using number
	//	Random randomizer = new Random();
	//	randomizer.nextInt(10);
		String searchWord = "5";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus doesn't contain the required number
		if(sentencesList.size()==0)
		{
			LogUltility.log(test, logger, searchWord + " doesn't exists in the corpus file");		
			LogUltility.log(test, logger, "Test Case PASSED!");
			return;
		}
		
		boolean searchedWordFound = false;
		//Check if each sentence contains the searched word
		for(int i=0;i<sentencesList.size();i++)
		{
			if(sentencesList.get(i).contains(searchWord))
				searchedWordFound = true;
			
			//the sentence doesn't contain the searched word then the test fails
			if(!searchedWordFound)
			{
				LogUltility.log(test, logger,searchWord + " not found in " + sentencesList.get(i));		
				LogUltility.log(test, logger, "Test Case FAILED!");
				return;
			}		
		}
		
		assertTrue(searchedWordFound);
		LogUltility.log(test, logger, "Comparation result: " + searchedWordFound);	
		
		// Reset to default
		CorpusTab.bt_reset_search.click();
		LogUltility.log(test, logger, "Reset to default state");
					
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--303:Check the "W" button
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_303_Check_the_W_button() throws InterruptedException, AWTException, FileNotFoundException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--303", "Check the 'W' button ");

		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		//Make a search without clicking the 'w' button
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "lawyer" : "את";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// If the sentence list doesn't have the searched word "Empty result"
		if(sentencesList.size()==0)
		{
			LogUltility.log(test, logger,"The "+ searchWord + " doesn't exists in the corpus file");		
			LogUltility.log(test, logger, "Test Case PASSED!");
			return;
		}
		
		boolean searchedWordFound = false;
		//Check if each sentence contains the searched word
		for(int i=0;i<sentencesList.size();i++)
		{
			if(sentencesList.get(i).contains(searchWord))
				searchedWordFound = true;
			
			//the sentence doesn't contain the searched word then the test fails
			if(!searchedWordFound)
			{
				LogUltility.log(test, logger,searchWord + " not found in " + sentencesList.get(i));		
				LogUltility.log(test, logger, "Test Case FAILED!");
				return;
			}		
		}
		
		assertTrue(searchedWordFound);
		LogUltility.log(test, logger, "Comparation result without clicking W button: " + searchedWordFound);	
		
		//Make a search and click the 'w' button
		//Click the 'w' button
		CorpusTab.bt_condition_whole_word.click();
		LogUltility.log(test, logger, "Click the w button");
		
		// Search in corpus and that in order to have less number of sentences to work with
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// If the sentence list doesn't have the searched word
		if(sentencesList.size()==0)
		{
			LogUltility.log(test, logger,"The "+ searchWord + " doesn't exists in the corpus file");		
			LogUltility.log(test, logger, "Test Case PASSED!");
			return;
		}
		
		//Check if the retrieved sentences should include only the searched keyword as standalone word
		boolean standalone = false;
		for(int i=0; i<sentencesList.size();i++)
		{
			String splitted[] = sentencesList.get(i).split(" ");
			//Check if the sentence includes the searched word
			for(int j=0; j<splitted.length;j++)
				if(splitted[j].equals(searchWord))
					{standalone = true;}
			
			//the sentence doesn't include the searched word then the test fails
			if(!standalone)
				{
					LogUltility.log(test, logger,searchWord + " not found in " + sentencesList.get(i));		
					LogUltility.log(test, logger, "Test Case FAILED!");
					return;
				}		
		}
		
		assertTrue(standalone);
		LogUltility.log(test, logger, "Comparation result: " + standalone);	
		
		// Reset to default
		CorpusTab.bt_reset_search.click();
		LogUltility.log(test, logger, "Reset to default state");
					
		//Click the 'w' button to reset
		CorpusTab.bt_condition_whole_word.click();
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--200:Verify user can add filter to the search
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_200_Verify_user_can_add_filter_to_the_search() throws InterruptedException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--200", "Verify user can add filter to the search");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus 
		String searchWord =Setting.Language.equals("EN")? "Micro" : "שבל";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required search word "Empty result"
		if(sentencesList.size()==0)
		{
			LogUltility.log(test, logger, searchWord + " doesn't exists in the corpus file");		
			LogUltility.log(test, logger, "Test Case PASSED!");
			return;
		}
		
		boolean searchedWordFound = false;
		//Check if each sentence contains the searched word
		for(int i=0;i<sentencesList.size();i++)
		{
			if(sentencesList.get(i).contains(searchWord))
				searchedWordFound = true;
			
			//the sentence doesn't contain the searched word then the test fails
			if(!searchedWordFound)
			{
				LogUltility.log(test, logger,searchWord + " not found in " + sentencesList.get(i));		
				LogUltility.log(test, logger, "Test Case FAILED!");
				return;
			}		
		}
		
		assertTrue(searchedWordFound);
		LogUltility.log(test, logger, "Comparation result: " + searchedWordFound);	
		
		// Search in corpus by adding filter 
		String filterWord = Setting.Language.equals("EN")? "soft": "ונה";
		String newSearchWord = searchWord.concat(filterWord);
		CorpusTab.searchInCorpus(test,logger,"text","contains",filterWord,"add filter");

		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required filtered word "Empty result"
			if(sentencesList.size()==0)
			{
				LogUltility.log(test, logger, newSearchWord + " doesn't exists in the corpus file");		
				LogUltility.log(test, logger, "Test Case PASSED!");
				return;
			}
			
			boolean filterFound = false;
			//Check if each sentence contains the searched word
			for(int i=0;i<sentencesList.size();i++)
			{
				if(sentencesList.get(i).contains(newSearchWord))
					filterFound = true;
				
				//the sentence doesn't contain the searched word then the test fails
				if(!filterFound)
				{
					LogUltility.log(test, logger,newSearchWord + " not found in " + sentencesList.get(i));		
					LogUltility.log(test, logger, "Test Case FAILED!");
					return;
				}		
			}
				
			// Reset to default
			CorpusTab.bt_reset_search.click();
			LogUltility.log(test, logger, "Reset to default state");
			
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--201:Verify user can change filter while searching
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_201_Verify_user_can_change_filter_while_searching() throws InterruptedException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--201", "Verify user can change filter while searching");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus 
		String searchWord =Setting.Language.equals("EN")? "Micro" : "שבל";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required search word "Empty result"
		if(sentencesList.size()==0)
		{
			LogUltility.log(test, logger, searchWord + " doesn't exists in the corpus file");		
			LogUltility.log(test, logger, "Test Case PASSED!");
			return;
		}
		
		boolean searchedWordFound = false;
		//Check if each sentence contains the searched word
		for(int i=0;i<sentencesList.size();i++)
		{
			if(sentencesList.get(i).toLowerCase().contains(searchWord.toLowerCase()))
				searchedWordFound = true;
			
			//the sentence doesn't contain the searched word then the test fails
			if(!searchedWordFound)
			{
				LogUltility.log(test, logger,searchWord + " not found in " + sentencesList.get(i));		
				LogUltility.log(test, logger, "Test Case FAILED!");
				return;
			}		
		}
		
		assertTrue(searchedWordFound);
		LogUltility.log(test, logger, "Comparation result: " + searchedWordFound);	
		
		// Search in corpus by adding filter 
		String filterWord = Setting.Language.equals("EN")? "soft": "ונה";
		String newSearchWord = searchWord.concat(filterWord);
		CorpusTab.searchInCorpus(test,logger,"text","contains",filterWord,"add filter");

		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required filtered word "Empty result"
			if(sentencesList.size()==0)
			{
				LogUltility.log(test, logger, newSearchWord + " doesn't exists in the corpus file");		
				LogUltility.log(test, logger, "Test Case PASSED!");
				return;
			}
			
			boolean filterFound = false;
			//Check if each sentence contains the searched word
			for(int i=0;i<sentencesList.size();i++)
			{
				if(sentencesList.get(i).toLowerCase().contains(newSearchWord.toLowerCase()))
					filterFound = true;
				
				//the sentence doesn't contain the searched word then the test fails
				if(!filterFound)
				{
					LogUltility.log(test, logger,newSearchWord + " not found in " + sentencesList.get(i));		
					LogUltility.log(test, logger, "Test Case FAILED!");
					return;
				}		
			}
				
			
		assertTrue(filterFound);
		LogUltility.log(test, logger, "Comparation result: " + filterFound);	
		
		// Search in corpus by changing filter 
		filterWord = Setting.Language.equals("EN")? "waveable" : "יטא";
		newSearchWord = searchWord.concat(filterWord);
		
		CorpusTab.searchInCorpus(test,logger,"text","contains",filterWord,"change filter");

		// Retrieve sentences
		sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		
		// Corpus file doesn't contain the required filtered word "Empty result"
			if(sentencesList.size()==0)
			{
				LogUltility.log(test, logger, newSearchWord + " doesn't exists in the corpus file");		
				LogUltility.log(test, logger, "Test Case PASSED!");
				return;
			}
			
			boolean changingFilterOk = false;
			//Check if each sentence contains the searched word
			for(int i=0;i<sentencesList.size();i++)
			{
				if(sentencesList.get(i).toLowerCase().contains(newSearchWord.toLowerCase()))
					changingFilterOk = true;
				
				//the sentence doesn't contain the searched word then the test fails
				if(!changingFilterOk)
				{
					LogUltility.log(test, logger,newSearchWord + " not found in " + sentencesList.get(i));		
					LogUltility.log(test, logger, "Test Case FAILED!");
					return;
				}		
			}
				
			
		assertTrue(changingFilterOk);
		LogUltility.log(test, logger, "Comparation result: " + changingFilterOk);	
		
		// Reset to default
		CorpusTab.bt_reset_search.click();
		LogUltility.log(test, logger, "Reset to default state");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	

	/**
	 * DIC_TC--207:Verify user can filter records with POS filter
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_207_Verify_user_can_filter_records_with_POS_filter() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--207", "Verify user can filter records with POS filter");
		
		// Initialize pages variables 
		DictionaryTab DictionaryTab = GetInstance(DictionaryTab.class).As(DictionaryTab.class);
		
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		//**************
//		CorpusTab.readCorpus();
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "what" : "את";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Choose value from the first search dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,"pos");
   		LogUltility.log(test, logger, "Set the dropdown to POS");

   		// Choose value from the condition dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "equal");
   		LogUltility.log(test, logger, "Set dropdown to Equal");

   		// Get all pos available values
   		List<String> posValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		posValues.remove("unknown");
   		Random random = new Random();
   		String chosenPos = posValues.get(random.nextInt(posValues.size()));
//   		String chosenPos = "noun";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenPos);
   		LogUltility.log(test, logger, "Set dropdown to :"+chosenPos);
   		
   		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"add filter");
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: all records");
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");			
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);		
		
		// Convert the chosen POS to its code
		String posCode = DictionaryTab.POSConverter(chosenPos);
		LogUltility.log(test, logger, "The POS code is: " + posCode);
		
		// Check that the pos exists in all retrieved sentences
		boolean exists = false;
		String posFileExpression = Setting.Language.equals("EN")? "EN{"+posCode+"}" : "HE{"+posCode+"}";
		for(String sentence : sentencesList )
		{
			LogUltility.log(test, logger, "current sentence:" + sentence);
			List<String> wordsInfoInSentence = CorpusTab.getSentenceRecordsTaggingInfo(sentence);
			for(int i=0;i<wordsInfoInSentence.size();i++)
			{	
				if(wordsInfoInSentence.get(i).contains(posFileExpression))
					exists = true;
			}
			if(!exists)
			{
				// This means that it didn't find the required POS in the sentence
				LogUltility.log(test, logger, "The POS: "+chosenPos+" ,Doesn't exists in: "+sentence);
				assertTrue(exists);
			}
		}
		
		// If there is no sentences found
		if(sentencesList.size()==0)
			exists=true;
		
		LogUltility.log(test, logger, "POS was found in all sentences: "+exists);
		assertTrue(exists);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--260:Verify the search have a function to clear it
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_260_Verify_the_search_have_a_function_to_clear_it() throws InterruptedException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--260", "Verify the search have a function to clear it");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus to retrieve all the records first
		String searchWord = "";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");

		//Get the results amount
		String retRecords = CorpusTab.tb_retrieved_records.getText();
		String splitMessage [] = retRecords.split(":");
		int retRecordsNum = Integer.parseInt(splitMessage[1]);
		LogUltility.log(test, logger, "Retrieved records number: " + retRecordsNum);
		
		// Search in corpus to for specific sentence
		searchWord = Setting.Language.equals("EN")? "planet" : "ארץ";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Click the reset button
		CorpusTab.bt_reset_search.click();
		LogUltility.log(test, logger, "Click the reset button");
		
		// Retrieve sentences after ressting
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
//		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);
		int retRecordsAfterReset = sentencesList.size();
		LogUltility.log(test, logger, "Retrieved records number afte clicking reset: " + retRecordsAfterReset);
		
		// Check that the number of records after reset equals the "retRecordsNum" parameter
		boolean equals = retRecordsAfterReset == retRecordsNum;
		LogUltility.log(test, logger, "Reset works succesfully: "+equals);
		assertTrue(equals);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--378:Resetting after using the sort "text after"
	 * @throws InterruptedException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_378_Resetting_after_using_the_sort_text_after() throws InterruptedException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--378", "Resetting after using the sort \"text after\"");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus to retrieve all the records first
		String searchWord = Setting.Language.equals("EN") ?"hello" : "שלום";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Sort by "text after"
		CommonFunctions.chooseValueInDropdown(CorpusTab.dd_sort_mode, "text after");
		LogUltility.log(test, logger, "Sort by: text after");
		
		// Click reset button
		CorpusTab.bt_reset_search.click();
		LogUltility.log(test, logger, "Click reset button");
		
		// Check that the sort dropdown is set to default = number
		String sortValue  = CorpusTab.getChosenValueInDropdown(CorpusTab.dd_sort_mode).get(0);
		LogUltility.log(test, logger, "Sort dropdown value after clicking reset is: "+sortValue);
		boolean defaultValue = sortValue.equals("number");	
		LogUltility.log(test, logger, "Dropdown is set to default: "+defaultValue);
		assertTrue(defaultValue);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--695:Check �audience� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_695_Check_audience_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--695", "Check �audience� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Choose value from the first search dropdown
		String firstDD = "audience";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> audValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenAud = audValues.get(random.nextInt(audValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenAud);
 		LogUltility.log(test, logger, "Choose audience value: "+chosenAud);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "all records";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence audience matches the random audience value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String audienceFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFile(sentencesList.get(i).substring(6)).get("audience");
			if(!chosenAud.equals(audienceFromFile))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,audience value is: "+audienceFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences audience value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--696:Check �complexity� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_696_Check_complexity_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--696", "Check �complexity� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Choose value from the first search dropdown
		String firstDD = "complexity";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> complexityValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenComp = complexityValues.get(random.nextInt(complexityValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenComp);
 		LogUltility.log(test, logger, "Choose complexity value: "+chosenComp);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "all records";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   		
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence audience matches the random audience value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String audienceFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFile(sentencesList.get(i).substring(6)).get("complexity");
			if(!chosenComp.equals(audienceFromFile))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,complexity value is: "+audienceFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences complexity value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--697:Check �form type� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_697_Check_form_type_dropdown_retrieved_records() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--697", "Check �form type� dropdown retrieved records");
		
		// Initialize pages variables 
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();

		// Choose value from the first search dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,"form type");
   		LogUltility.log(test, logger, "Set the dropdown to form type");

   		// Choose value from the condition dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "equal");
   		LogUltility.log(test, logger, "Set dropdown to Equal");

   		// Get all form type available values
   		List<String> formTypeValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		formTypeValues.remove("unknown");
   		Random random = new Random();
   		String chosenFormType = formTypeValues.get(random.nextInt(formTypeValues.size()));
//   		String chosenFormType = "present s1";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenFormType);
   		LogUltility.log(test, logger, "Set dropdown to :"+chosenFormType);
   		
   		// Change the last filter value, in case it was changed by other TC
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: all records");
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");			
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);		
		
		// fix for 1.18
		if(sentencesList.size() == 1)
			if(sentencesList.get(0).equals("   "))
				sentencesList.remove(0);
		
		// Check that the form type exists in all retrieved sentences
		chosenFormType = chosenFormType.replaceAll(" ", "_");
		boolean exists = false;
		for(String sentence : sentencesList )
		{
			LogUltility.log(test, logger, "current sentence:" + sentence);
			List<String> wordsInfoInSentence = CorpusTab.getSentenceRecordsTaggingInfo(sentence);
			for(int i=0;i<wordsInfoInSentence.size();i++)
			{	
				
				if(wordsInfoInSentence.get(i).contains(chosenFormType))
					exists = true;
			}
			if(!exists)
			{
				// This means that it didn't find the required POS in the sentence
				LogUltility.log(test, logger, "The Form Type: "+chosenFormType+" ,Doesn't exists in: "+sentence);
				assertTrue(exists);
			}
		}
		
		// If there is no sentences found
		if(sentencesList.size()==0)
			exists=true;
		
		LogUltility.log(test, logger, "The Form Type was found in all sentences: "+exists);
		assertTrue(exists);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--698:Check �formality� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_698_Check_formality_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--698", "Check �formality� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "occean" : "את";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
   		
		// Choose value from the first search dropdown
		String firstDD = "formality";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> formalityValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenForm = formalityValues.get(random.nextInt(formalityValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenForm);
 		LogUltility.log(test, logger, "Choose formality value: "+chosenForm);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence formality matches the random formality value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String formalityFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFile(sentencesList.get(i).substring(6)).get("formality");
			if(!chosenForm.equals(formalityFromFile))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,formality value is: "+formalityFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences formality value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--699:Check �lingo� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_699_Check_lingo_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--699", "Check �lingo� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
//		String searchWord = "food";
//		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
   		
		// Choose value from the first search dropdown
		String firstDD = "lingo";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "contains";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> lingoValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenLingo = lingoValues.get(random.nextInt(lingoValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenLingo);
 		LogUltility.log(test, logger, "Choose lingo value: "+chosenLingo);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "all records";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence lingo matches the random lingo value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String lingoFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFile(sentencesList.get(i).substring(6)).get("lingo");
			if(!lingoFromFile.contains(chosenLingo))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,lingo value is: "+lingoFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences lingo value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--700:Check �Medium� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_700_Check_Medium_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--700", "Check �Medium� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
//		// Search in corpus and that in order to have less number of sentences to work with
//		String searchWord = "ocean";
//		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
   		
		// Choose value from the first search dropdown
		String firstDD = "medium";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> mediumValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenMed = mediumValues.get(random.nextInt(mediumValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenMed);
 		LogUltility.log(test, logger, "Choose medium value: "+chosenMed);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "all records";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence formality matches the random formality value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String medFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFile(sentencesList.get(i).substring(6)).get("medium");
			if(!chosenMed.equals(medFromFile))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,medium value is: "+medFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences medium value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--701:Check �Mood� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_701_Check_Mood_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--701", "Check �Mood� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
//		String searchWord = "food";
//		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
   		
		// Choose value from the first search dropdown
		String firstDD = "mood";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "contains";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> moodValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenMood = moodValues.get(random.nextInt(moodValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenMood);
 		LogUltility.log(test, logger, "Choose mood value: "+chosenMood);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "all records";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence lingo matches the random lingo value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String moodFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFile(sentencesList.get(i).substring(6)).get("mood");
			if(!moodFromFile.contains(chosenMood))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,mood value is: "+moodFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences mood value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--703:Check �dictionary key� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_703_Check_dictionary_key_dropdown_retrieved_records() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--703", "Check �dictionary key� dropdown retrieved records");
		
		// Initialize pages variables 
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		CurrentPage = GetInstance(DictionaryTab.class);
		DictionaryTab DictionaryTab = CurrentPage.As(DictionaryTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
//		// Search in corpus and that in order to have less number of sentences to work with
//		String searchWord = "ocean";
//		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Choose value from the first search dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,"dictionary key");
   		LogUltility.log(test, logger, "Set the dropdown to form type");

   		// Choose value from the condition dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "equal");
   		LogUltility.log(test, logger, "Set dropdown to Equal");

   		// Get dictionary keys with 'completed' status
//   	List<String> dictionaryKeys = DictionaryTab.getRecordsWithStatus("completed");
   		List<String> dictionaryKeys = CorpusTab.getTaggedDictionaryKeysInCorpus();
   		Random random = new Random();
   		String chosenDicKey = dictionaryKeys.get(random.nextInt(dictionaryKeys.size()));
   		
   		//Type in the search field
   		CorpusTab.cmpCondition_value.sendKeys(chosenDicKey);
   		LogUltility.log(test, logger, "Type in the search field:"+chosenDicKey);
   		
   		// Change the last filter value
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: all records");
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");			
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);		
		
		
		// Check that the dictionary key exists in all retrieved sentences
		boolean exists = false;
		for(String sentence : sentencesList )
		{
			LogUltility.log(test, logger, "current sentence:" + sentence);
			List<String> wordsInfoInSentence = CorpusTab.getSentenceRecordsTaggingInfo(sentence);
			for(int i=0;i<wordsInfoInSentence.size();i++)
			{	
				
				if(wordsInfoInSentence.get(i).contains(chosenDicKey))
					exists = true;
			}
			if(!exists)
			{
				// This means that it didn't find the required POS in the sentence
				LogUltility.log(test, logger, "The Dictionary Key: "+chosenDicKey+" ,Doesn't exists in: "+sentence);
				assertTrue(exists);
			}
		}
		
		// If there is no sentences found
		if(sentencesList.size()==0)
			exists=true;
		
		LogUltility.log(test, logger, "The Dictionary Key was found in all sentences: "+exists);
		assertTrue(exists);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--704:Check �grammar index� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws AWTException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_704_Check_grammar_index_dropdown_retrieved_records() throws InterruptedException, AWTException, IOException
	{
		test = extent.createTest("Corpus Tests - DIC_TC--704", "Check �grammar index� dropdown retrieved records");
		
		// Initialize pages variables 
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);

		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
//		// Search in corpus and that in order to have less number of sentences to work with
//		String searchWord = "ocean";
//		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Choose value from the first search dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,"grammar index");
   		LogUltility.log(test, logger, "Set the dropdown to form type");

   		// Choose value from the condition dropdown
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, "equal");
   		LogUltility.log(test, logger, "Set dropdown to Equal");

   		// Get dictionary keys with 'completed' status
   		List<String> grammarIndexes = CorpusTab.getGrammarIndexesFromCorpus();
   		Random random = new Random();
   		String chosenGI = grammarIndexes.get(random.nextInt(grammarIndexes.size()));
   		
   		//Type in the search field
   		CorpusTab.cmpCondition_value.sendKeys(chosenGI);
   		LogUltility.log(test, logger, "Type in the search field:"+chosenGI);
   		
   		// Change the last filter value
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,"all records");
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: all records");
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");			
		
		// Retrieve sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);
		LogUltility.log(test, logger, "sentencesList retreived: " + sentencesList);		
		
		
		// Check that the dictionary key exists in all retrieved sentences
		boolean exists = false;
		for(String sentence : sentencesList )
		{
			LogUltility.log(test, logger, "current sentence:" + sentence);
			List<String> wordsInfoInSentence = CorpusTab.getSentenceRecordsTaggingInfo(sentence);
			for(int i=0;i<wordsInfoInSentence.size();i++)
			{	
				
				if(wordsInfoInSentence.get(i).contains(chosenGI))
					exists = true;
			}
			if(!exists)
			{
				// This means that it didn't find the required POS in the sentence
				LogUltility.log(test, logger, "The Grammar Index: "+chosenGI+" ,Doesn't exists in: "+sentence);
				assertTrue(exists);
			}
		}
		
		// If there is no sentences found
		if(sentencesList.size()==0)
			exists=true;
		
		LogUltility.log(test, logger, "The Grammar index was found in all sentences: "+exists);
		assertTrue(exists);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--705:Check �sentence type� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_705_Check_sentence_type_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--705", "Check �sentence type� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
//		String searchWord = "food";
//		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
   		
		// Choose value from the first search dropdown
		String firstDD = "sentence type";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "contains";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> sentenceTypeValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenSentenceType = sentenceTypeValues.get(random.nextInt(sentenceTypeValues.size()));
//   	String chosenSentenceType = "yes no question";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenSentenceType);
 		LogUltility.log(test, logger, "Choose sentence type value: "+chosenSentenceType);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "all records";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence type matches the random type value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String sentenceTypeFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFileById(sentencesList.get(i).substring(0,6)).get("sentence type");
			if(!sentenceTypeFromFile.contains(chosenSentenceType))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,sentence type value is: "+sentenceTypeFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences sentence type value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--706:Check �social status� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_706_Check_social_status_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--706", "Check �social status� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
//		String searchWord = "food";
//		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
   		
		// Choose value from the first search dropdown
		String firstDD = "social status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "contains";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> socialStatusValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenSocialStatus = socialStatusValues.get(random.nextInt(socialStatusValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenSocialStatus);
 		LogUltility.log(test, logger, "Choose social status value: "+chosenSocialStatus);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "all records";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence social status matches the random social status value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String socialStatusFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFileById(sentencesList.get(i).substring(0,6)).get("social status");
			if(!socialStatusFromFile.contains(chosenSocialStatus))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,social status value is: "+socialStatusFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences social status value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * DIC_TC--707:Check �speech act� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_707_Check_speech_act_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--707", "Check �speech act� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "what" : "את";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
   		
		// Choose value from the first search dropdown
		String firstDD = "speech act";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "contains";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> speechActValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenSpeechAct = speechActValues.get(random.nextInt(speechActValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenSpeechAct);
 		LogUltility.log(test, logger, "Choose speech act value: "+chosenSpeechAct);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence social status matches the random social status value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String speechActFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFileById(sentencesList.get(i).substring(0,6)).get("speech act");
			if(!speechActFromFile.contains(chosenSpeechAct))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,speech act value is: "+speechActFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences speech act value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--708:Check �status� dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_708_Check_status_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--708", "Check �status� dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "what" : "את";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
		
		// Choose value from the first search dropdown
		String firstDD = "status";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "equal";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> statusValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenStatus = statusValues.get(random.nextInt(statusValues.size()));
//   		String chosenStatus = "fully tagged";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenStatus);
 		LogUltility.log(test, logger, "Choose status value: "+chosenStatus);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "all records";
 		
 		// In order to work with less 'fully tagged' sentences  		
 		if(chosenStatus.equals("fully tagged"))
 			filterDropdownValue = "add filter";
 		else
 			CorpusTab.bt_reset_search.click();
 		
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the sentence social status matches the random social status value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String statusFromFile = CorpusTab.getSenteceStatusAcoordingNumber(sentencesList.get(i).substring(0,6).trim()).replace("_", " ");
			if(!statusFromFile.equals(chosenStatus))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,status value is: "+statusFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences status value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--727:Check 'dialect' dropdown retrieved records
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_727_Check_dialect_dropdown_retrieved_records() throws InterruptedException, IOException
	{

		test = extent.createTest("Corpus Tests - DIC_TC--727", "Check 'dialect' dropdown retrieved records");
		CurrentPage = GetInstance(CorpusTab.class);
		CorpusTab CorpusTab = CurrentPage.As(CorpusTab.class);
		
		// Click on the Corpus tab
		CorpusTab.tabCorpus.click();
		
		// Search in corpus and that in order to have less number of sentences to work with
		String searchWord = Setting.Language.equals("EN")? "what" : "את";
		CorpusTab.searchInCorpus(test,logger,"text","contains",searchWord,"all records");
   		
		// Choose value from the first search dropdown
		String firstDD = "dialect";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_field,firstDD);
   		LogUltility.log(test, logger, firstDD+" is chosen from the first search dropdown");

   		// Choose value from the condition dropdown
   		String conditionDropdownValue = "contains";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_operation, conditionDropdownValue);
   		LogUltility.log(test, logger, conditionDropdownValue+" is chosen from the condition dropdown");

   		// Choose random value
   		List<String> dialectValues = CommonFunctions.getValuesFromApp(CorpusTab.cmpCondition_value);
   		Random random = new Random();
   		String chosenDialect = dialectValues.get(random.nextInt(dialectValues.size()));
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_value, chosenDialect);
 		LogUltility.log(test, logger, "Choose dialect value: "+chosenDialect);
   		
   		// Change the last filter value, in case it was changed by other TC
 		String filterDropdownValue = "add filter";
   		CommonFunctions.chooseValueInDropdown(CorpusTab.cmpCondition_all_next,filterDropdownValue);
   		LogUltility.log(test, logger, "Choose from the all/next dropdown: "+filterDropdownValue);
   		
   		// Click the Retrieve button
   		CorpusTab.btnRetrieve_same.click();
   		LogUltility.log(test, logger, "Click the Retreive button");
   			
   		// Retrieve all sentences
		List<String> sentencesList = CommonFunctions.getValuesFromApp(CorpusTab.lb_corpus_records);

		// Check that the dialect matches the random dialect value
		boolean matches = true;
		for(int i=0;i<sentencesList.size()-1;i++)
		{
			String dialectFromFile = CorpusTab.getSentenceDropdownsListsValuesFromFileById(sentencesList.get(i).substring(0,6)).get("dialect");
			if(!dialectFromFile.contains(chosenDialect))
				{
					matches = false;
					LogUltility.log(test, logger, "The sentence: "+sentencesList.get(i)+" ,dialect value is: "+dialectFromFile);
					break;
				}
		}
		
		LogUltility.log(test, logger, "All retrieved sentences dialect value matches the searched value: "+matches);
		assertTrue(matches);
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {			
			int testcaseID = 0;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Corpus/Search",method.getName());
			
//					System.out.println("tryCount: " + tryCount);
//					System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
		
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
					&& Integer.parseInt(Setting.RetryFailed)!=0) {
				extent.removeTest(test);
				
		        // Close popups to get back to clean app
				 System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
			}
			else if(result.getStatus() == ITestResult.FAILURE)
			    {
				 	tryCount = 0;
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			        
			        // Close popups to get back to clean app
//					        if (Integer.parseInt(Setting.RetryFailed) != 0) {
			         System.out.println("Test Case Failed");
					 CurrentPage = GetInstance(DictionaryTab.class);
					 if(Setting.closeEveryWindow.equals("true"))
						 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//					        }
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			    	tryCount = 0;
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
			
			    extent.flush();
			    
				// Count how many a test was processed
				tryCount++;
				}

/**
 * Closing the application after running all the TCs
 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
			}

}

package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.DictionaryTab;
import smi.smorfet.test.pages.LexiconTab;

/**
 * 
 * All tests for Lexicon search
 *
 */
public class Lexicon_Search_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Lexicon tab");
	
		// Logger
		logger = LogManager.getLogger(LexiconTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Lexicon tab");
		logger.info("Lexicon tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}
		
		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(LexiconTab.class);
//		CurrentPage.As(LexiconTab.class).Start_Window.click();
//		CurrentPage.As(LexiconTab.class).bt_abort.click();
		
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(LexiconTab.class);
//			CurrentPage.As(LexiconTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Lexicon");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * DIC_TC--10:check the Volume [by letter] dropdown
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_10_check_the_Volume_by_letter_dropdown() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Lexicon Search Test - DIC_TC_10", "check the Volume [by letter] dropdown");
		
		// Skip this test case with Hebrew language because dropdown is not yet ready
		if (Setting.Language.equals("HE"))
			return;
		
//		// Get records from the Lexicon file
//		CurrentPage = GetInstance(LexiconTab.class);
//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
//		
//		// Pick a random record
//		Random randomizer = new Random();
//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));

		//Press the Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).LexiconTab.click();
		
		// Focus on Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		
//		// Choose "form" from the first search dropdown
//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "form");
//		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
//		
//		// Choose "contains" from the condition dropdown
//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
//		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
		// get values from the volume by letter
		List<String> volumeVlues = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).cmpVolumePOS);
		LogUltility.log(test, logger, "Records retreived: " + volumeVlues);
		// Remove Number and symbols from the volume list
		volumeVlues.remove(0);
		volumeVlues.remove(1);
		
		// Select random value from the volume list list
		// Pick a random record
		Random randomizer = new Random();
		String randomVolume= volumeVlues.get(randomizer.nextInt(volumeVlues.size()));
		LogUltility.log(test, logger, "Random volume : " + randomVolume);
		
		// Change the volume by letter dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpVolumePOS, randomVolume);
		LogUltility.log(test, logger, "Volume by letter changed to : "+ randomVolume);
				
//		// Type in the search field
//		String randomrecord ="nabil";
//		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
//		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
//		
//		// Click the Retrieve button
//		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
//		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean beginWithZ = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
//			char letter = 'z';
			
		       if(s.toLowerCase().charAt(0) == randomVolume.toLowerCase().charAt(0)) continue;
		       	else {
		       		beginWithZ = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + beginWithZ);
		assertTrue(beginWithZ);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

/**
	 * DIC_TC--206:Verify user can filter records with Frequency filter 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_206_Verify_user_can_filter_records_with_Frequency_filter() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Lexicon Search Test - DIC_TC_206", "Verify user can filter records with Frequency filter");
		
//		// Get records from the Lexicon file
//		CurrentPage = GetInstance(LexiconTab.class);
//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
//		
//		// Pick a random record
//		Random randomizer = new Random();
//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));

		//Press the Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).LexiconTab.click();
		
		// Focus on Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		
		// Choose "form" from the first search dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, "frequency");
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "equal" from the condition dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "equal");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
//		// Change the last filter to "all records", in case it was changed by other TC
//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
				
		// Type in the search field
		//String randomrecord ="nabil";
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_value, "very common");
		LogUltility.log(test, logger, "word is typed in the search field: \"very common");
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList); 
		
		// Choose random record from the middle of the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick a random record
		Random randomizer = new Random();
		String randomrecord1 = recordsList.get(randomizer.nextInt(recordsList.size()));
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).lst_records, randomrecord1);
		LogUltility.log(test, logger, "Choose from record list: " + randomrecord1);
				
		// Press the goto dictionary black button 
		CurrentPage.As(LexiconTab.class).bt_goto_dictionary.click();
		LogUltility.log(test, logger, "Click the black button to navigate to dictionary , and dictionary tab displayed and the required record selected");
		
		// In the dictionary tab verify that the frequency ins the same value as the searched one 
		
		
		// Get the chosen Frequency list value
		CurrentPage = GetInstance(DictionaryTab.class);
		List<String> chosenFrequency = CurrentPage.As(DictionaryTab.class).getChosenValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_frequency);
		LogUltility.log(test, logger, "chosen list: " + chosenFrequency);
	//	String modifiedOnList = CurrentPage.As(DictionaryTab.class).modified_on.getText();
		//getChosenValueInDropdown
		

		//Remove the first letter for the random searched value  before compare results
		//String randomSemanticGroups1 = "desire";
		//String[] randomSemanticGroups1 = randomSemanticGroups.split(" ");
//				String randomSemanticGroups1 = randomSemanticGroups.substring(2);
//				LogUltility.log(test, logger, "Check that the retrieved Register list do include the searched lexical field: " + randomSemanticGroups1);
		
		//verify that the Frequency match the searched one
		boolean sameFrequency = chosenFrequency.contains("very common");
		assertTrue(sameFrequency);
		
		LogUltility.log(test, logger, "Check that the retrieved Frequency list do include the searched Frequency and value should be True: " + sameFrequency);
		assertTrue(sameFrequency);
		
		// Go back to the lexicon tab
		//Press the Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).LexiconTab.click();
		
		// Focus on Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		LogUltility.log(test, logger, "Lexicon tab displayed again");
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}

/**
	 * DIC_TC--691:Verify that user can search by form 
	 * @throws InterruptedException 
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_691_Verify_that_user_can_search_by_form() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("Lexicon Search Test - DIC_TC_691", "Verify that user can search by form");
		
//		// Get records from the Lexicon file
//		CurrentPage = GetInstance(LexiconTab.class);
//		List<String> recordsFromFile = CurrentPage.As(LexiconTab.class).getRecordsFromLexicon();
//		
//		// Pick a random record
//		Random randomizer = new Random();
//		String randomrecord = recordsFromFile.get(randomizer.nextInt(recordsFromFile.size()));

		//Press the Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).LexiconTab.click();

		// Focus on Lexicon tab
		CurrentPage = GetInstance(LexiconTab.class);
		CurrentPage.As(LexiconTab.class).tabLexicon.click();
		
		// Type in the search field
		String thirdDropdown = null;
		String randomrecord = null;
		if (Setting.Language.equals("EN")) {
			thirdDropdown = "form";
			randomrecord ="nabil";
		}
		else if (Setting.Language.equals("HE")) {
			thirdDropdown = "presentations";
			randomrecord ="אוף";
		}
		
		// Choose "form" from the first search dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_field, thirdDropdown);
		LogUltility.log(test, logger, "from is chosen in the first search dropdown");
		
		// Choose "contains" from the condition dropdown
		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_operation, "contains");
		LogUltility.log(test, logger, "contains is chosen in the condition dropdown");
		
//		// Change the last filter to "all records", in case it was changed by other TC
//		CurrentPage.As(LexiconTab.class).chooseValueInDropdown(CurrentPage.As(LexiconTab.class).cmpCondition_all_next, "all records");
//		LogUltility.log(test, logger, "add filter is chosen in the all/next dropdown");
		
		CurrentPage.As(LexiconTab.class).cmpCondition_value.sendKeys(randomrecord);
		LogUltility.log(test, logger, "word is typed in the search field: " + randomrecord);
		
		// Click the Retrieve button
		CurrentPage.As(LexiconTab.class).btnRetrieve_same.click();
		LogUltility.log(test, logger, "Click the Retreive button");
		
		// Retrieve records
		List<String> recordsList = CurrentPage.As(LexiconTab.class).getValuesFromApp(CurrentPage.As(LexiconTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Check that retrieved records do contain the searched record
		if (recordsList.isEmpty()) assertTrue(false);
		
		boolean containResult = true;
		for (String s : recordsList) {
			//System.out.println("s: " + s + " -- recordsList: " + recordsList);
		       if(s.toLowerCase().contains(randomrecord)) continue;
		       	else {
		       		containResult = false;
		           break;
		       	}
		    }
		LogUltility.log(test, logger, "Check that the retrieved records list do include the searched word: " + containResult);
		assertTrue(containResult);
		
		LogUltility.log(test, logger, "Test Case PASSED");	
	}
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Lexicon/Search",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}

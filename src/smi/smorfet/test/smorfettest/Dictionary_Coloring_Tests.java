package smi.smorfet.test.smorfettest;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.CommonFunctions;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import smi.smorfet.test.pages.DictionaryTab;

/**
 * 
 * All tests for Dictionary search
 *
 */
public class Dictionary_Coloring_Tests extends FrameworkInitialize{
	
	WiniumDriver driver = null;
	private static Logger logger = null;
//	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	
	/**
	 * Initialize - run once before all tests
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	@Parameters("testlink")
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws IOException, ClassNotFoundException {
		
		// Get rid of the report html warnings
//		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		System.setProperty(freemarker.log.Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, "none");
		
		// Get settings
		ConfigReader.GetAllConfigVariable();

		// Initialize the report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("Dictionary tab - Coloring");
	
		// Logger
		logger = LogManager.getLogger(DictionaryTab.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Dictionary tab - Coloring");
		logger.info("Dictionary tab Tests - FrameworkInitilize");
		
//		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
//		logger.info("Connect to DB " + con.toString());
		
		// Implicit wait
//		try {
//			DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		} catch (Exception e) {
//			System.out.println("e: " + e);
//		}

		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Initialize Smorfet Application
		InitializeApp(Setting.AppPath + "SMI_Back_Office.exe", Setting.WiniumServer);
		
		// Do go and close the Start window that was opened with the initialized Driver and continue
		// with an already opened Smorfet app
//		CurrentPage = GetInstance(DictionaryTab.class);
//		CurrentPage.As(DictionaryTab.class).Start_Window.click();
//		CurrentPage.As(DictionaryTab.class).bt_abort.click();
		
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		try {
			// Smorfet app focus
			CurrentPage = GetInstance(DictionaryTab.class);
//			CurrentPage.As(DictionaryTab.class).TitleBar.click();
			CommonFunctions.clickTabs("Dictionary");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * DIC_TC--374:Check the Origin drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_374_Check_the_Origin_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_374", "Check the Origin drop-down coloring");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();

		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the Origin dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpOrigin, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpOrigin);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the origin dropdown
		List<String> originList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpOrigin);
		originList.remove("unknown");
		String randomOrigin= originList.get(randomizer.nextInt(originList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpOrigin, randomOrigin);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new origin value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpOrigin);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	/**
	 * DIC_TC--373:Check the Style drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_373_Check_the_Style_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_373", "Check the Style drop-down coloring");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the style dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpStyle, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpStyle);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the style dropdown
		List<String> styleList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpStyle);
		styleList.remove("unknown");
		String randomStyle= styleList.get(randomizer.nextInt(styleList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpStyle, randomStyle);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new style value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpStyle);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--372:Check the Acronym drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_372_Check_the_Acronym_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_372", "Check the Acronym drop-down coloring");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the Acronym dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpAcronym, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpAcronym);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the acronym dropdown
		List<String> acronymList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpAcronym);
		acronymList.remove("unknown");
		String randomAcronym= acronymList.get(randomizer.nextInt(acronymList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpAcronym, randomAcronym);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new acronym value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpAcronym);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--371:Check the Register drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_371_Check_the_Register_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_371", "Check the Register drop-down coloring");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the register dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpRegister, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpRegister);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the register dropdown
		List<String> registerList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpRegister);
		registerList.remove("unknown");
		String randomRegister= registerList.get(randomizer.nextInt(registerList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpRegister, randomRegister);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new register value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpRegister);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--370:Check the Number drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_370_Check_the_Number_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_370", "Check the Number drop-down coloring");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "demonstrative";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the number dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpNumber, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpNumber);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the number dropdown
		List<String> numberList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpNumber);
		numberList.remove("unknown");
		String randomNumber= numberList.get(randomizer.nextInt(numberList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpNumber, randomNumber);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new number value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpNumber);
		LogUltility.log(test, logger, "Color of a value should be beige: " + colorValue);
		assertEquals(colorValue, "Beige");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--369:Check the Gender drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_369_Check_the_Gender_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_369", "Check the Gender drop-down coloring");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the Origin dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpGender, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpGender);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the origin dropdown
		List<String> originList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpGender);
		originList.remove("unknown");
		String randomOrigin= originList.get(randomizer.nextInt(originList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpGender, randomOrigin);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new origin value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color is Beige
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpGender);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "Beige");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--368:Check the Declinability drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_368_Check_the_Declinability_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_368", "Check the Declinability drop-down coloring");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the declinability dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpDeclinability, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpDeclinability);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the declinability dropdown
		List<String> declinabilityList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpDeclinability);
		declinabilityList.remove("unknown");
		String randomDeclinability= declinabilityList.get(randomizer.nextInt(declinabilityList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpDeclinability, randomDeclinability);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new declinability value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpDeclinability);
		LogUltility.log(test, logger, "Color of a value should be beige: " + colorValue);
		assertEquals(colorValue, "Beige");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--367:Check the Definiteness drop-down coloring
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_367_Check_the_Definiteness_drop_down_coloring() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_367", "Check the Definiteness drop-down coloring");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the definiteness dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpDefiniteness, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpDefiniteness);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the definiteness dropdown
		List<String> definitenessList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpDefiniteness);
		definitenessList.remove("unknown");
		String randomDefiniteness= definitenessList.get(randomizer.nextInt(definitenessList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpDefiniteness, randomDefiniteness);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new definiteness value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpDefiniteness);
		LogUltility.log(test, logger, "Color of a value should be beige: " + colorValue);
		assertEquals(colorValue, "Beige");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--360:Check the unknown value Frequency drop-down
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_360_Check_the_unknown_value_Frequency_drop_down() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_360", "DIC_TC--360:Check the unknown value Frequency drop-down");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the Frequency dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpFrequency, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpFrequency);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the frequency dropdown
		List<String> frequencyList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpFrequency);
		frequencyList.remove("unknown");
		String randomFrequency= frequencyList.get(randomizer.nextInt(frequencyList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpFrequency, randomFrequency);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new frequency value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpFrequency);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--358:Check the unknown value Dependency drop-down
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_358_Check_the_unknown_value_Dependency_drop_down() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_358", "Check the unknown value Dependency drop-down");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and choose unknown in the dependency dropdown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpDependency, "unknown");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpDependency);
		LogUltility.log(test, logger, "Color of unknown should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose another value than unknown from the dependency dropdown
		List<String> dependencyList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).cmpDependency);
		dependencyList.remove("unknown");
		String randomDependency= dependencyList.get(randomizer.nextInt(dependencyList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpDependency, randomDependency);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new dependency value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).cmpDependency);
		LogUltility.log(test, logger, "Color of a value should be beige: " + colorValue);
		assertEquals(colorValue, "Beige");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--343:Check the semantic groups turn to white when the user select values
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_343_Check_the_semantic_groups_turn_to_white_when_the_user_select_values() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_343", "Check the semantic groups turn to white when the user select values");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and remove any chosen value from the list
		CommonFunctions.unselectValuesFromList(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the empty list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of the empty list is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose value from the semantic groups list
		List<String> semanticGroupsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		semanticGroupsList.remove("unknown");
		String randomSemanticGroups= semanticGroupsList.get(randomizer.nextInt(semanticGroupsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups, randomSemanticGroups);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the semantic groups list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).lst_SemanticGroups);
		LogUltility.log(test, logger, "Color of list with values should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--349:Check the color of Etymology when there is no text saved
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_349_Check_the_color_of_Etymology_when_there_is_no_text_saved() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_349", "Check the color of Etymology when there is no text saved");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");
		
		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and delete the etymology from the first record
		CurrentPage.As(DictionaryTab.class).cleanDucomentControl(CurrentPage.As(DictionaryTab.class).boxEtymology);
		LogUltility.log(test, logger, "Delete everything from the Etymology textbox");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of box is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).boxEtymology);
		LogUltility.log(test, logger, "Color of the field should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Enter a text in the etymology field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(10);
		CurrentPage.As(DictionaryTab.class).boxEtymology.sendKeys(randomText);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new origin value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of the etymology field with text
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).boxEtymology);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--350:Check the color of Definition when there is no text saved
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_350_Check_the_color_of_Definition_when_there_is_no_text_saved() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_350", "Check the color of Definition when there is no text saved");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and delete the definition from the first record
		CurrentPage.As(DictionaryTab.class).cleanDucomentControl(CurrentPage.As(DictionaryTab.class).lst_Definition);
		LogUltility.log(test, logger, "Delete everything from the Definition textbox");
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the Unknown
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of box is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).lst_Definition);
		LogUltility.log(test, logger, "Color of the field should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Enter a text in the definition field
		String randomText = CurrentPage.As(DictionaryTab.class).RandomString(10);
		CurrentPage.As(DictionaryTab.class).lst_Definition.click();
		CurrentPage.As(DictionaryTab.class).lst_Definition.sendKeys(randomText);
		LogUltility.log(test, logger, "Insert text in the definiation field: " + randomText);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the new origin value
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of the definition field with text
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).lst_Definition);
		LogUltility.log(test, logger, "Color of a value should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * DIC_TC--344:Check the semantic fields turn to white when the user select values
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void DIC_TC_344_Check_the_semantic_fields_turn_to_white_when_the_user_select_values() throws InterruptedException, AWTException
	{
		test = extent.createTest("Dictionary Tests - DIC_TC_344", "Check the semantic fields turn to white when the user select values");

		// Focus on dictionary tab
		CurrentPage = GetInstance(DictionaryTab.class);
		//CurrentPage.As(DictionaryTab.class).tabDictionary.click();
		CommonFunctions.clickTabs("Dictionary");

		// Choose a value from Volume[POS] dropdown
		String volumePOS = "adadjective";
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).cmpVolumePOS, volumePOS);
		LogUltility.log(test, logger, "Choose value in voluem POS dropdown: " + volumePOS);

		// Retrieve records
		List<String> recordsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lst_records);
		LogUltility.log(test, logger, "Records retreived: " + recordsList);
		
		// Choose random record from the first 20 records in the displayed list
		recordsList = recordsList.subList(0, 20 > recordsList.size() ? recordsList.size() : 20);

		// Pick first random record
		Random randomizer = new Random();
		String randomRecordFirst = recordsList.get(randomizer.nextInt(recordsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose first record from record list: " + randomRecordFirst);

		// Pick second random record for synchronization purposes
		String randomRecordSecond = recordsList.get(randomizer.nextInt(recordsList.size()));
		
		// First go and remove any chosen value from the list
		CommonFunctions.unselectValuesFromList(CurrentPage.As(DictionaryTab.class).lb_dictionary_semantic_fields);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);
		
		// Choose back the first record and then check the color of the empty list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of the empty list is red
		String colorunknown = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).lb_dictionary_semantic_fields);
		LogUltility.log(test, logger, "Color of empty list should be red: " + colorunknown);
		assertEquals(colorunknown, "Red");
		
		// Choose value from the semantic fields list
		List<String> semanticFieldsList = CurrentPage.As(DictionaryTab.class).getValuesFromApp(CurrentPage.As(DictionaryTab.class).lb_dictionary_semantic_fields);
		semanticFieldsList.remove("unknown");
		String randomSemanticField= semanticFieldsList.get(randomizer.nextInt(semanticFieldsList.size()));
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lb_dictionary_semantic_fields, randomSemanticField);
		
		// Click to another record and back again to sync the changes
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordSecond);
		LogUltility.log(test, logger, "Choose second record from record list to sync changes: " + randomRecordSecond);

		// Choose back the first record and then check the color of the semantic groups list
		CommonFunctions.chooseValueInDropdown(CurrentPage.As(DictionaryTab.class).lst_records, randomRecordFirst);
		LogUltility.log(test, logger, "Choose the first record again: " + randomRecordFirst);
		
		// Check the color of unknown is red
		String colorValue = CommonFunctions.getColor(CurrentPage.As(DictionaryTab.class).lb_dictionary_semantic_fields);
		LogUltility.log(test, logger, "Color of list with values should be white: " + colorValue);
		assertEquals(colorValue, "White");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException, AWTException, InterruptedException
    {		
		
		int testcaseID = 0;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Dictionary/Coloring",method.getName());
		
//		System.out.println("tryCount: " + tryCount);
//		System.out.println("Integer.parseInt(Setting.RetryFailed): " + Integer.parseInt(Setting.RetryFailed));
	
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount!=Integer.parseInt(Setting.RetryFailed)
				&& Integer.parseInt(Setting.RetryFailed)!=0) {
			extent.removeTest(test);
			
	        // Close popups to get back to clean app
			 System.out.println("Test Case Failed");
			 CurrentPage = GetInstance(DictionaryTab.class);
			 if(Setting.closeEveryWindow.equals("true"))
				 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
		}
		else if(result.getStatus() == ITestResult.FAILURE)
		    {
			 	tryCount = 0;
		        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
		        test.fail(result.getThrowable());
		        String screenShotPath = Screenshot.captureScreenShot();
		        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
					} catch (Exception e) {
						LogUltility.log(test, logger, "Testlink Error: " + e);
					}
		        	}
		        
		        // Close popups to get back to clean app
//		        if (Integer.parseInt(Setting.RetryFailed) != 0) {
		         System.out.println("Test Case Failed");
				 CurrentPage = GetInstance(DictionaryTab.class);
				 if(Setting.closeEveryWindow.equals("true"))
					 CurrentPage.As(DictionaryTab.class).closeEveryWindow(this.getClass().getName());
//		        }
		    }
		    else if(result.getStatus() == ITestResult.SUCCESS)
		    {
		    	tryCount = 0;
		        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
		        // Send result to testlink
		        if (enableReportTestlink == true){
		        	try {
		        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
		            	LogUltility.log(test, logger, "Report to testtlink: " + response);
		    		} catch (Exception e) {
		    			LogUltility.log(test, logger, "Testlink Error: " + e);
		    		}
		        	
		        	}
		    }
		    else
		    {
		        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
		        test.skip(result.getThrowable());
		    }
		
		    extent.flush();
		    
			// Count how many a test was processed
			tryCount++;
			}
	
		/**
		 * Closing the application after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 //DriverContext._Driver.quit();
			}

}

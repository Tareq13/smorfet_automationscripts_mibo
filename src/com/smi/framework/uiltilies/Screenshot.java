package com.smi.framework.uiltilies;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.smi.framework.base.DriverContext;

public class Screenshot {
	
	 public static String captureScreenShot(){
		 

		try {
			  TakesScreenshot ts = (TakesScreenshot)DriverContext._Driver;
			
			  // Take screenshot and store as a file format
			  File source= ts.getScreenshotAs(OutputType.FILE);
			  
			Date date = new Date();
			// Specify the desired date format
			String DATE_FORMAT = "dd-MMM-yyyy_HH_mm_ss";
			// Create object of SimpleDateFormat and pass the desired date format.
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			String dt = sdf.format(date);
			
			String dest = System.getProperty("user.dir") +"\\test-output\\Smorfet\\ErrorScreenshots\\"+dt+".png";
//			String dest = "..\\..\\..\\..\\..\\..\\test-output\\Smorfet\\ErrorScreenshots\\"+dt+".png";
//			String dest = "ErrorScreenshots\\"+dt+".png";
//			String dest = "..\\..\\..\\..\\..\\Smorfet\\ErrorScreenshots\\"+dt+".png";
			
			  File destination = new File(dest);

		FileUtils.copyFile(source, destination);
		 System.out.println("screenshot taken");
		 return dest;
		       }

		catch (IOException e)

		{

		System.out.println(e.getMessage());
		return e.getMessage();

		    }

		}

}

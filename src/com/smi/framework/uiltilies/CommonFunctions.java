package com.smi.framework.uiltilies;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import com.gargoylesoftware.htmlunit.Page;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;


public class CommonFunctions extends BasePage{
	
	// Tabs area
	@FindBy(how=How.ID,using ="tc_TabControl")
	public static WebElement tc_TabControl;
	
   // Smorfet main window
   @FindBy(how=How.ID,using ="SMI_Root_GUI")
   public static WebElement SMI_Root_GUI;
   
   // Dictionary Manager Message: 
   @FindBy(how=How.NAME,using ="Close")
   public static WebElement XClose;
   
   // No button
   @FindBy(how=How.NAME,using ="No")
   public static WebElement No;
   
   // Attribute editor
   @FindBy(how=How.ID,using ="Attribute_Editor")
   public static WebElement Attribute_Editor;
	
   // Intro window and other app out windows
   @FindBy(how=How.NAME,using ="Attachment")
   public static WebElement WindowAttachment;
	
   // dd_condition_value
   @FindBy(how=How.ID,using ="dd_condition_value")
   public static WebElement cmpCondition_value;
	
   // Revert button
   @FindBy(how=How.ID,using ="bt_revert")
   public static WebElement bt_revert;
	
   // OK button on the Revert file popup 
   @FindBy(how=How.NAME,using ="OK")
   public static WebElement revertFileMsgOK;
   
   // Close smorfet app
   @FindBy(how=How.ID,using="Close")
   public static WebElement close_smorfet;
		
   // Smorfet maximize button
   @FindBy(how=How.ID,using ="Maximize")
   public static WebElement SMI_Maximize;
   
   // Smorfet minimize button
   @FindBy(how=How.ID,using ="Minimize")
   public static WebElement SMI_Minimize;
   
	 /**
     * Get color where the mouse is pointing at
     * @return 
     * @throws AWTException
     */
    public static String getColor(WebElement place) throws AWTException {
		
    	// Define RGB colors
    	Map<List<Integer>, String> colorsMap = new HashMap<List<Integer>, String>();
    	colorsMap.put(Arrays.asList(219 , 255, 0), "Yellow");
    	colorsMap.put(Arrays.asList(182 , 255, 0), "Yellow");
    	colorsMap.put(Arrays.asList(130 , 251, 152), "Green");
    	colorsMap.put(Arrays.asList(108 , 251, 152), "Green");
    	colorsMap.put(Arrays.asList(255 , 182, 193), "Red");
    	colorsMap.put(Arrays.asList(255 , 255, 255), "White");
    	colorsMap.put(Arrays.asList(250 , 240, 230), "Beige");
    	colorsMap.put(Arrays.asList(219 , 219, 144), "Gray");
    	colorsMap.put(Arrays.asList(0 , 120, 215), "Blue");
    	colorsMap.put(Arrays.asList(0 , 0, 0), "Black");
    	colorsMap.put(Arrays.asList(154, 205, 50), "Dark Green");
    	colorsMap.put(Arrays.asList(110, 205, 50), "Dark Green2");
    	colorsMap.put(Arrays.asList(87, 176, 50), "Dark Green3");
    	colorsMap.put(Arrays.asList	(221, 160, 221), "Pink");
    	colorsMap.put(Arrays.asList	(175, 238, 238), "Light Blue");
    	colorsMap.put(Arrays.asList(255 , 255, 0), "Yellow2");
      	colorsMap.put(Arrays.asList(144, 238, 144), "Green2");
     	colorsMap.put(Arrays.asList(39,134,205), "Blue2");
      	
    	// Point at the element in order to get its color
    	String attachPlace = place.getAttribute("ClickablePoint");
		String[] XY = attachPlace.split(",");

		Robot robot = new Robot();
	    int XPoint = 0, YPoint = 0;
		if(place.getAttribute("ControlType").equals("ControlType.List")) {
			XPoint = Integer.parseInt(XY[0])+90;
	     	YPoint = Integer.parseInt(XY[1]);}
     	else if(place.getAttribute("ControlType").equals("ControlType.Edit") )
     		{   XPoint = Integer.parseInt(XY[0]);
     			YPoint = Integer.parseInt(XY[1]);}	
     	else if( place.getAttribute("ControlType").equals("ControlType.Button"))
     	{   XPoint = Integer.parseInt(XY[0])+3;
			YPoint = Integer.parseInt(XY[1]);}	
		else { XPoint = Integer.parseInt(XY[0])+45;
	 	     YPoint = Integer.parseInt(XY[1]);}
		
        robot.mouseMove(XPoint, YPoint);
        
        java.awt.Color color = robot.getPixelColor(XPoint, YPoint);

//      Color color = robot.getPixelColor(Integer.parseInt(XY[0]), Integer.parseInt(XY[1]));
        
        // Convert the RGB to color name
        String foundColor = null;
    	for (Map.Entry<List<Integer>, String> entry : colorsMap.entrySet())
    	{
    		if (entry.getKey().equals(Arrays.asList(color.getRed() , color.getGreen(), color.getBlue())))
    			foundColor = entry.getValue();
    	}
       	
    	// If the color is blue (selected item) then move the mouse up
    	if (foundColor != null && foundColor.equals("Blue") && place.getAttribute("ControlType").equals("ControlType.List")) {
//    		XPoint = Integer.parseInt(XY[0]);
    		System.out.println("before: " + YPoint);
    		YPoint = Integer.parseInt(XY[1])-15;
    		System.out.println("before: " + YPoint);
    		robot.mouseMove(XPoint, YPoint);
           color = robot.getPixelColor(XPoint, YPoint);
           
           for (Map.Entry<List<Integer>, String> entry : colorsMap.entrySet())
	     	{
	     		if (entry.getKey().equals(Arrays.asList(color.getRed() , color.getGreen(), color.getBlue())))
	     			foundColor = entry.getValue();
	     	}
    	}
    	// If the color still also blue (selected item) then move the mouse again down and try again
    	if (foundColor != null && foundColor.equals("Blue") && place.getAttribute("ControlType").equals("ControlType.List")) {
//    		XPoint = Integer.parseInt(XY[0]);
    		YPoint = Integer.parseInt(XY[1])+30;
    		robot.mouseMove(XPoint, YPoint);
           color = robot.getPixelColor(XPoint, YPoint);
           
           for (Map.Entry<List<Integer>, String> entry : colorsMap.entrySet())
	     	{
	     		if (entry.getKey().equals(Arrays.asList(color.getRed() , color.getGreen(), color.getBlue())))
	     			foundColor = entry.getValue();
	     	}
    	}
   
    	// If the color still not recognized, it is still null, maybe because mouse on value text
    	// then move it and try again
    	if (foundColor == null && place.getAttribute("ControlType").equals("ControlType.List")) {
//    		XPoint = Integer.parseInt(XY[0]);
    		System.out.println("before: " + YPoint);
    		YPoint = Integer.parseInt(XY[1])-15;
    		System.out.println("before: " + YPoint);
    		robot.mouseMove(XPoint, YPoint);
           color = robot.getPixelColor(XPoint, YPoint);
           
           for (Map.Entry<List<Integer>, String> entry : colorsMap.entrySet())
	     	{
	     		if (entry.getKey().equals(Arrays.asList(color.getRed() , color.getGreen(), color.getBlue())))
	     			foundColor = entry.getValue();
	     	}
    	}
    	// If the color still also not reconized by mouse in text then move it again down
    	if (foundColor != null && foundColor.equals("Blue") && place.getAttribute("ControlType").equals("ControlType.List")) {
//    		XPoint = Integer.parseInt(XY[0]);
    		YPoint = Integer.parseInt(XY[1])+30;
    		robot.mouseMove(XPoint, YPoint);
           color = robot.getPixelColor(XPoint, YPoint);
           
           for (Map.Entry<List<Integer>, String> entry : colorsMap.entrySet())
	     	{
	     		if (entry.getKey().equals(Arrays.asList(color.getRed() , color.getGreen(), color.getBlue())))
	     			foundColor = entry.getValue();
	     	}
    	}
    	
    	
    	if (foundColor != null) return foundColor;
    	
    	// Print the RGB information of the pixel color
    	System.out.println("This is a new color, please add it");
        System.out.println("Red   = " + color.getRed());
        System.out.println("Green = " + color.getGreen());
        System.out.println("Blue  = " + color.getBlue());
        
    	return "";          
    }
    
	/**
	 * Click on main tabs
	 * @param tab
	 * @throws InterruptedException
	 */
   public static void clickTabs(String tab) throws InterruptedException
 	{
	   try {
			   PageFactory.initElements(DriverContext._Driver, CommonFunctions.class);
			   
			   	List<WebElement> rulesTabs = tc_TabControl.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TabItem')]"));

			   	for (WebElement tabIn : rulesTabs) {
				   	if (tabIn.getAttribute("Name").equals(tab))
				   		tabIn.click();
				}
		} catch (Exception e) {
			PageFactory.initElements(DriverContext._Driver, CommonFunctions.class);
			   
		   	List<WebElement> rulesTabs = tc_TabControl.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.TabItem')]"));

		   	for (WebElement tabIn : rulesTabs) {
			   	if (tabIn.getAttribute("Name").equals(tab))
			   		tabIn.click();
			}
	}
 	}
	   
	   /**
        * Unselect all chosen values from a list
        * @param lstBox
        * @return
        * @throws InterruptedException
        */
       public static void unselectValuesFromList(WebElement lstBox) throws InterruptedException
   	{
       	
	         // Scan first for elements in lstBOX, take buttons, lists and scrolls for later use
	       	 List<WebElement> allElelements= lstBox.findElements(By.xpath(".//*"));
	
	            List<WebElement> itemlist = new ArrayList<WebElement>();
	       	 List<WebElement> scroll  = new ArrayList<WebElement>();
	       	
	       	 for (WebElement webElement : allElelements) {
	       		 if(webElement.getAttribute("ControlType").equals("ControlType.ListItem"))
	       			itemlist.add(webElement);
	       		 else if(webElement.getAttribute("ControlType").equals("ControlType.ScrollBar"))
	       			scroll.add(webElement);
	   		}
       	
       	ArrayList<String> selectedValues= new ArrayList<String>();
       	int count = 0;
   		for(WebElement e : itemlist)
   		{
   			if (e.isSelected())
   				selectedValues.add(e.getAttribute("Name"));
   			// Count how much element is displayed
//  			System.out.println(e.getAttribute("ClickablePoint"));
   			if(e.getAttribute("ClickablePoint") != null)
   				count++;
   		}
   		
   		// Unselect value
   		for (String ItemName : selectedValues) {
       	// get list of all the available values
     		List<String> allValues = new ArrayList<String>();
     		for(WebElement e : itemlist)
     			allValues.add(e.getAttribute("Name"));
     		
     		// Check the distance of the needed value
     		int place = allValues.indexOf(ItemName);
     		if (place > count)
     			place += place / count;
     		int moves = place / count;
     		int reset = allValues.size()/count;
     		if (allValues.size() > count) reset += 1;
       	
     		if (moves == 0) reset = 0;
     		
     		if (reset > 0) {
       	// Look for scroll button
     		List<WebElement> scrollButtons= scroll.get(0).findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));

     		for(WebElement e : scrollButtons)
   		{
     			// Reset
     			if (e.getAttribute("Name").equals("Back by large amount"))
  				{
   				for (int i = 0; i < reset; i++) 
   					e.click();
  				}
     			// Scorll to place
   			if (e.getAttribute("Name").equals("Forward by large amount"))
  				{
   				for (int i = 0; i < moves; i++) 
   					e.click();
  				}
       	}
       	}
     		// Unselect the element
     		for(WebElement e : itemlist)
   		{
   			if (e.getAttribute("Name").equals(ItemName))
  				{
  				  e.click();
  				}
   		}
   		}
   	}
	   
       /**
        * Choose value from dropdown that is LISTITEM
        * @param lstBox
        * @param ItemName
        * @throws InterruptedException
        */
       public static void chooseValueInDropdown(WebElement lstBox, String ItemName) throws InterruptedException
   	{     	   	
    	// Scan for buttons first
           List<WebElement> dropdownbutton= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
           
        // Open the listbox
//     	List<WebElement> dropdownbutton1= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
     	 if(!dropdownbutton.isEmpty() && lstBox.getAttribute("ControlType").equals("ControlType.ComboBox"))
     		dropdownbutton.get(0).click();
//      	lstBox.click();
    	
     	 // Need to get the scroll after opening the dropdown
     	dropdownbutton= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
     	 
      	 // Scan after for elemnts in lstBOX, lists and scrolls
      	 List<WebElement> allElelements= lstBox.findElements(By.xpath(".//*"));

           List<WebElement> listsElements = new ArrayList<WebElement>();
      	 List<WebElement> scrollsElements  = new ArrayList<WebElement>();
      	
      	 for (WebElement webElement : allElelements) {
      		 if(webElement.getAttribute("ControlType").equals("ControlType.ListItem"))
      			 listsElements.add(webElement);
      		 else if(webElement.getAttribute("ControlType").equals("ControlType.ScrollBar"))
      			 scrollsElements.add(webElement);
  		}
  		
      	String firstShownElementInWindow = "";
  		System.out.println();
  		if (!scrollsElements.isEmpty()) {
       	
       	// get list of all the available values
     		List<String> allValues = new ArrayList<String>();
     		int count = 0;
     		for(WebElement e : listsElements) {
     			allValues.add(e.getAttribute("Name"));
     			if(e.getAttribute("ClickablePoint") != null) {
     				count++;
     				if (firstShownElementInWindow.equals(""))
     					firstShownElementInWindow = e.getAttribute("Name");
     			}
     		}

     		// Calculates moves
     		if (count != 0) {
     		
     		// Check the distance of the needed value
     		int place = allValues.indexOf(ItemName);
     		if (place >= count)
     			place += place / count;
     		int moves = place / count;
//     		int reset = allValues.size()/count;
//     		if (allValues.size() > count) reset += 1;
//     		int reset = allValues.indexOf(firstShownElementInWindow)/count;
     		
     		int resetPlace = allValues.indexOf(firstShownElementInWindow);
     		if (resetPlace >= count)
     			resetPlace += resetPlace / count;
     		float resetHelp = resetPlace / count;
     		
     		int reset = 0;
     		if (resetPlace % count != 0) reset = (int) (resetHelp + 1);
     		else reset = (int) resetHelp;
     		
     		if (reset==0 && !firstShownElementInWindow.equals(allValues.get(0)))
     			reset = 1;
 			
//     		if (moves == 0) reset -= 1;
     		
//     		if (reset > 0) {
     		// Look for scrolls
//     		List<WebElement> scroll= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ScrollBar')]"));
       	// Look for scroll button
     		for (WebElement e : scrollsElements)
     			if (e.getAttribute("ControlType").equals("ControlType.ScrollBar")) {
     			
     		List<WebElement> scrollButtons= e.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.Button')]"));
     			
     		// Reset scroll
     		for(WebElement f : scrollButtons)
   		{
     			if (reset > 0) {
	     			// Don't reset if the first value in list is already shown
	     			if (!firstShownElementInWindow.equals(allValues.get(0))) {
		     			// Reset
		     			if (f.getAttribute("Name").equals("Back by large amount"))
			  				{
			   				for (int i = 0; i < reset; i++)
			   					f.click();
			  				}
	     			}
     			}
   		}
     			
     			// To solve sum cases where need to move only one up value for the reset
     			dropdownbutton.get(0).click();
     			
     			// Moves scroll to place
     			for(WebElement f : scrollButtons)
     			{
     			// Scorll to place
   			if (f.getAttribute("Name").equals("Forward by large amount"))
  				{
   				for (int i = 0; i < moves; i++)  
   					f.click();
  				}
   		}
       	}
//     		}
     		
     		} 
     		
     		} // Scroll check
  		
     		// finally, for all
   		for(WebElement e : listsElements)
   		{
//   			System.out.println(e.getAttribute("Name"));
   			if (e.getAttribute("Name").equals(ItemName))
  				{
  				  e.click();
  				  return;
  				}
   		}
   	}
       
       public static List<String> getValuesFromApp(WebElement lstBox) throws InterruptedException
   	{
       	//lstBox.click();
//       	Thread.sleep(1000);
   		//get all children
   		//List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("\"./*[contains(@ControlType, 'ControlType.ListItem')]\""));
//       	try {
//       		List<WebElement> itemlist= cmpCondition_field.findElements(By.xpath("//*[contains(@ControlType, 'ControlType.ListItem')]"));
//   			} catch (Exception e) {
//   				System.out.println("itemlist e: " + e);
//   			}
       	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));
   		//System.out.println(itemlist.size());
   		//System.out.println(itemlist.get(0).getAttribute('WiniumDriver'));
       	ArrayList<String> itemNames= new ArrayList<String>();
   		for(WebElement e : itemlist)
   		{
   			//System.out.println(e.getAttribute("Name"));
   			itemNames.add(e.getAttribute("Name"));
   		    //itemNames.add(e.getAttribute(Name));
   		}
   		
   		// Delete unknown value, it is not usable
   		if (itemNames.contains("unknown"))
   			itemNames.remove("unknown");
   		
   		return itemNames;
   	}
       
       /**
        * Get chosen value in a dropdown
        * @param lstBox
        * @return
        * @throws InterruptedException
        */
       public static List<String> getChosenValueInDropdown(WebElement lstBox) throws InterruptedException
   	{
       	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')]"));

       	ArrayList<String> itemNames= new ArrayList<String>();
   		for(WebElement e : itemlist)
   		{
   			boolean isSelected = e.isSelected();
   			if (isSelected)
   				itemNames.add(e.getAttribute("Name"));
   		}
   		return itemNames;
   	}
       
       /**
        * Get displayed tabs names
        * @return
        * @throws InterruptedException
        */
       public static List<String> getDisplayedTabs() throws InterruptedException
   	{
          	ArrayList<String> tabNames= new ArrayList<String>();
    	   try {
			   PageFactory.initElements(DriverContext._Driver, CommonFunctions.class);
			   
			   	List<WebElement> rulesTabs = tc_TabControl.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.TabItem')]"));

		
		   		for(WebElement e : rulesTabs)
		   				tabNames.add(e.getAttribute("Name"));
		} catch (Exception ex) {
			PageFactory.initElements(DriverContext._Driver, CommonFunctions.class);
			   
		   	List<WebElement> rulesTabs = tc_TabControl.findElements(By.xpath("./*[contains(@ControlType, 'ControlType.TabItem')]"));

		   	for(WebElement e : rulesTabs)
   				tabNames.add(e.getAttribute("Name"));
			}

   		return tabNames;
   	}
       
       /**
        * Eliminates Nikud characters from a string
        * @param str
        * @return clean string without Nikud
        */
       public static String cleanStringNikud(String str)
       {
   		char[] charStr = str.toCharArray();
   		StringBuilder sb = new StringBuilder();
   		for(int i=0;i<charStr.length;i++)
   		{
   			// If the char is a Nikud charachter, then don't append it to the string
   			if(! ( (int) charStr[i] < 1477 && (int) charStr[i]>1424 ) )
   				sb.append(charStr[i]);
   		}	
   		return sb.toString();
       }
       

       /**
        * Get the last value of a list
        * @param lstBox
        * @param amount
        * @return
        * @throws InterruptedException
        */
       public static String getLastValueFromApp(WebElement lstBox) throws InterruptedException
      {
      		//get the last children
          	WebElement item= lstBox.findElement(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')][last()]"));		
      		return item.getAttribute("Name");
      }
       
       /**
        * Get the last N values of a list
        * @param lstBox
        * @param amount
        * @return
        * @throws InterruptedException
        */
       public static List<String> getLastNValuesFromApp(WebElement lstBox, int amount) throws InterruptedException
      {
    	    // adjust the amount value to work as expected: indexes in XPath are 1-based, not 0-based
   	   		amount--;
      		//get the last N children
         	List<WebElement> itemlist = lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')][position() >= last() - "+amount+"]"));
  			
        	ArrayList<String> itemNames= new ArrayList<String>();

       		for(WebElement e : itemlist)
       			itemNames.add(e.getAttribute("Name"));

       		return itemNames;
      }
       
    /**
     * Get the first N values of a list
     * @param lstBox
     * @param amount
     * @return
     * @throws InterruptedException
     */
       public static List<String> getValuesFromApp(WebElement lstBox, int amount) throws InterruptedException
   	{
   		//get all children
       	List<WebElement> itemlist= lstBox.findElements(By.xpath(".//*[contains(@ControlType, 'ControlType.ListItem')][position()<="+amount+"]"));
       	ArrayList<String> itemNames= new ArrayList<String>();
   		for(WebElement e : itemlist)
   			itemNames.add(e.getAttribute("Name"));

   		return itemNames;
   	}
       
}
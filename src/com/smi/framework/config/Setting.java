package com.smi.framework.config;

public class Setting {

	public static String UserName_DB;
	public static String Password_DB;
	
	public static String LogPath;
	public static String ExcelSheetData;
	public static String DB_Host;
	public static String DB_Name;
	
	public static String ReportToTestlink;
	public static String TestLinkProject;
	public static String TestLinkPlan;
	
	public static String AppPath;
	public static String WiniumServer;
	
	public static String RetryFailed;
	public static String closeEveryWindow;
	
	public static String Language;
}

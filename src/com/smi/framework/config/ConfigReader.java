package com.smi.framework.config;

import java.io.IOException;
import java.util.Properties;

public class ConfigReader {

	
	public static void GetAllConfigVariable() throws IOException
	{
		ConfigReader confreader= new ConfigReader();
		confreader.ReadProperties();
	}
	
	public void ReadProperties() throws IOException
	{
		Properties p= new Properties();
		p.load(getClass().getResourceAsStream("GlobalConfig.properties"));
		Setting.UserName_DB=p.getProperty("UserName_DB");
		Setting.Password_DB=p.getProperty("Password_DB");
		Setting.LogPath=p.getProperty("LogPath");
		Setting.ExcelSheetData=p.getProperty("ExcelSheetData");
		Setting.DB_Host=p.getProperty("DB_Host");
		Setting.DB_Name=p.getProperty("DB_Name");
		Setting.ReportToTestlink=p.getProperty("ReportToTestlink");
		Setting.TestLinkProject=p.getProperty("TestLinkProject");
		Setting.TestLinkPlan=p.getProperty("TestLinkPlan");
		Setting.RetryFailed=p.getProperty("RetryFailed");
		Setting.closeEveryWindow=p.getProperty("closeEveryWindow");
		
		Setting.AppPath=p.getProperty("AppPath");
		Setting.WiniumServer=p.getProperty("WiniumServer");
		
		Setting.Language=p.getProperty("Language");
	}
}
